# ChangeLog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/id-ID/1.0.0/)

## [1.5.0+1] - 2022-09-30

### Changed

- Fix pop up dialog overlfow for flip device
- Added phone number for pop up dialog qr code from profile page

## [1.5.0] - 2022-09-29

### Changed

- Remove text 'my account' on profile page
- Change device note from pop give scroll view for html content
- Resize placeholder and cursor size

## [1.4.2+2] - 2022-09-29

### Added

- Success message from API for request change device in profile page

### Changed

- Fix confirmation button for request change device

## [1.4.2+1] - 2022-09-22

### Added

- Implement change device feature

### Changed

- Fix dropdown city
- Update target iOS version

## [1.4.2] - 2022-09-06

### Changed

- Fix error hanlder when token failed
- Fix logout event when user receive error
- Fix scan qr code keep firing request and empty header data

## [1.4.1+4] - 2022-08-30

### Changed

- Fix refresh token can't catch new token
- Implement Interceptor method

## [1.4.1+3] - 2022-08-28

### Changed

- Fix height size of package item
- Fix minimal target iOS version from 8.0 to 10.0
- Rename my point detail title page

## [1.4.1+2] = 2022-08-25

### Changed

- Fix pop dialog for foldable device
- Adjust UI

## [1.4.1+1] - 2022-08-23

### Changed

- Disable resizable to false, this configuration make UI explicit to foldable device or large screen
- Update membership level bar
- Change label event
- Adjust width and height of all widgets
- Set target development for iOS with from iOS version 8.0 to 15.0

## [1.4.1] - 2022-08-19

### Changed

- Adjustment foldable devices
- Implement data membership always reload/navigate when navigate from bottom navigation bar
- Fix adjustment level bar
- Disable navigation to voucher page for associate
- Implement concurrent request
- Update network handler

## [1.4.0] - 2022-08-12

### Changed

- Foldable device UI
- Adjustment UI
- Update new parameter CircularProgressIndicator for `CustomFlatButton`
- Fix camera blank for scan qr code

## Changed

## [1.3.0+1] - 2022-07-27

### Changed

- Adjustment UI for sign in, verification otp, and register page for foldable device
- Update client id and secret for demo version

## [1.3.0] - 2022-07-18

### Changed

- Fix blank page while soft keyboard appear in deletion page
- Adjust spending power and total spending power bar
- Adjust custom dialog image ratio

## [1.2.1+7] - 2022-07-15

### Changed

- Fix keybaord appear in deletion account page
- Fix margin and box decor when data empty in voucher list

## [1.2.1+6] - 2022-07-14

### Changed

- Added safe area to home page
- Fix style logout button
- Fix API call param when outlet id is null
- Fix Scan QR freeze after scan wrong qr code image
- Modify notification item for more flexible, depends from the content
- Removed radio button from bottom sheet
- Add range masking title product from price item from 15 to 20
- Adjustment UI

## [1.2.1+5] - 2022-07-13

### Changed

- Improve custom flat button
- Improve frame title wrapper, added new param font weight
- Improve and simplify API call for each controller
- Fix and improve error handler
- Fix date picker at my point page
- Fix delete account error handler
- Fix when price 0 remove price label in detail item
- Fix overflow text in price item
- Fix inconsistent height size in price item
- Update UI foundation for personal info, due added delete account button at bottom of the screen
- Update UI outlet from dropdown to bottom sheet
- Update horizontal (Bottle and Special Offer) list, added tip for the next item
- Update font weight at title pages

## [1,2.1+4] - 2022-07-08

### Added

- New feature, now user can request for account deletion
- Added new models, repository, and controller for account deletion

### Changed

- Change flow and UI at voucher page,
- Adjustment UI
- Modify existing endpoint

## [1.2.1+3] - 2022-06-29

### Changed

- Updated border radius for each box widget
- Improve custom widget for ripple button
- Fix aspect ratio carousel in home page

## [1.2.1+2] - 2022-06-27

## Added

- Added shell script for configure project

## Changed

- Update ReadMe file

## [1.2.1+1] - 2022-06-22

## Changed

- Fix push notification on android
- Update client ID, secret, and x-client-version
- Fix precache android and ios tools development

## [1.2.0+2] - 2022-06-07

## Changed

- Removed badge user in home page

## [1.2.0+1] - 2022-06-06

## Added

- Added shimmer for personal info and profile

## Changed

- Updated UI at profil page,
- Added rank badge at home page
- Fix refresh token at splash screen
- Updated `member_home_account_info_model.dart`
- Removed timer from change device dialog
- Fix type from app name at `main.dart`

## [1.1.0+5] - 2022-06-03

## Changed

- Update UI My Account
- Update `member_profile_model.dart`
- Update shared preference and gain data when first open app at `string_helper.dart`
- Resize app bar and remove space at `term_page.dart`
- Fix dialog size and scroll view at `dialog_member_benefit.dart`
- Added button navigate from home to profile at `member_info.dart`
- Fix address on profile info `personal_info_controller.dart` `member_card_profile.dart`
- Fix shimmer when loading on personal info `personal_info_page.dart`
- Fix status verify id
- Fix area `textfield` widget
- Fix logic authorize
- Added snackbar after request verify id

## [1.1.0+4] - 2022-06-02

## Added

- Created new model `percentage_bar_model.dart` and `stay_warning_model.dart`

## Changed

- Fix push notification not show up in entire functions
- Fix error handler for token expired
- Fix font type in form sign in using email
- Fix notification icon color at `home` page
- Set function countdown on dialog remove device `profile_controller.dart` `dialog_change_device.dart`

## [1.1.0+3] - 2022-05-31

## Added

- Created new model `member_home_account_info_model.dart`, `member_profile_model.dart`, `membership_rank_model.dart`, `my_membership_model.dart`, `spending_power_model.dart`, and `total_spending_model.dart` for support assign data from New API,
- Custom thumb slider at `circle_thumb_shape.dart`
- dialog for show QR Code user at `dialog_qr_code.dart`
- Widget for wrap account info info at `my_account_detail.dart`
- Widget for wrap spending and total power at `my_membership_detail.dart`

## Changed

- Changed flow, implement new API and follow linter code correction at `authorize_controller.dart`
- Removed `_getMemberDetail()` and replace with `_getMemberAccountInfo()` verification_otp_controller.dart`
- Changed `MemberAccountInfoModel()` model to `MemberHomeAccountInfoModel()` at member_account_info_controller.dart`
- Implement methods at `personal_info_controller.dart`
- Added new methods for access `my membership` and `persona info` at `shared_pref.dart`
- Added new methods for save `my membership` data `onWriteMyMembership()` at `string_helper.dart`
- Added new properties at `member_account_info_model.dart`
- Added new function `getMembeProfile()` and rename `getMemberDetail()` to `getMemberAccountInfo()` `authorize_repository_interface.dart`
- Implement new functions from interface at `authorize_repository.dart`
- Added `personalInfoRoute` at `route_names.dart`
- Added `RouteNames.personalInfoRoute,` to list at `routes.dart`
- Removed `EndPointName.memberDetail: '/member/detail',` at `api.dart`
- Removed `memberAccountInfo` at `enums.dart`
- Added bunch of key references for `my membership` at `preferences_key.dart`
- Move UI profile to `personal_info_page.dart`
- Implement new UI at `profile_page.dart`
- Improve UI at `member_card_profile.dart`
- Fix typo name class at `profile_shimmer.dart`

## [1.1.0+2] - 2022-05-23

### Added

- Create new static class contain page names

### Changed

- Refactor and fix linter

## [1.1.0+1] - 2022-04-20

### Changed

- Changed `Food Packages` to `Food Promo` at `food_item_detail.dart`, `food_page.dart`, and `home_page.dart`
- Fix url launcher to instagram at `profile_controller.dart`

## [1.0.0+7] - 2022-04-14

### Changed

- Update human interface for permission in iOS
- Remove context pop up dialog in force update

## [1.0.0+6] - 2022-04-12

### Added

- Created `update_handler` in `middleware` folder
- Created dialog for force update for each page

### Changed

- Modify in each model data added `client_info` object
- Move `error_handler` and `network_status_service` to `middleware` folder
- Added new translation for force update dialog

## [1.0.0+5] - 2022-04-08

### Changed

- Fix center active tab in category page
- Update gradle version
- Migrate app name in Manifet file

## [1.0.0+4] - 2022-04-07

### Added

- Added `proguard-rules.pro` for minify app

### Changed

- Update `build.gradle` settings
- Update `info.plist` for permission value
- Update post mutlipart data more flexible with extension file at `api_request`
- Implement file picker at `edit_profile_controller.dart`

## [1.0.0+3] - 2022-04-06

### Added

- Added `scroll_to_index` version `2.1.1`

### Changed

- Implement scroll to index at `redeem_page.dart`
- Fix pull down refresh at `redeem_point_controller.dart`
- Fix Summary voucher not updated properly
- Create cache on gallry `gallery_detail_controller.dart` `galler_banner_detail.dart` `gallery_detail.dart` `gallery_photo_view_wrapper.data`
- Request item  per page 12 `redeem_repository.dart`

## [1.0.0+2] - 2022-04-05

### Changed

- Change package `AndroidManifest.xml` debug
- Change package `AndroidManifest.xml` profile
- Fix bug load more, update function _assignDataItemToList on redeem point controller `redeem_point_controller.dart`
- Fix bug can't scroll, update param pagedGridView on redeem Page `redeem_page.dart`
- Fix birt date, `edit_profile_page.dart` `birth_date.dart`

## [1.0.0+1] - 2022-03-16

### Changed

- Change time format to from 12 to 24 hours at voucher page
- Adjustment UI
- Take out gps validation navigate to scan QR Code
- Take out gps location permission from platform Android and iOS
- Update and release live version

## [0.30.22] - 2022-03-15

### Added

- Added icon karaoke, party and dining `dining.png` `karaoke_new.png` `party.png`
- Added icon karaoke, party and dining in `pubspec.yaml`

### Changed

- Create icon on list `assets_list.dart`
- Fix UI Profile page `member_card_profile.dart`
- Adjustment UI voucher `new_vouchers_item.dart`
- Added new query in manifest file for fix android 12 access visibily

## [0.30.21] - 2022-03-14

### Added

- Added icon karaoke and restaurant `karaoke.png` `restaurant.png`
- Added icon karaoke and restaurant in `pubspec.yaml`

### Changed

- Create icon on list `assets_list.dart`
- Create condition and UI icon karaoke and restaurant `new_vouchers_item.dart`
- Added cicular progress bar in otp page

## [0.30.18] - 2022-03-11

### Changed

- Added overlay loading in qr code UI
- Change camera UI
- Fix take picture from camera
- Fix voucher type name null for `associate` member

## [0.30.16] - 2022-03-10

### Changed

- Fix UI Profile page `member_card_profile.dart`
- Fix payload navigation from notification
- Fix on back hardware navigate from notification page

## [0.30.15] - 2022-03-09

### Changed

- Adjustment UI
- Fix snackbar show up after request device change
- Fix app bar in camera UI
- Notifications grouping
- Fix redeem detail not updated after purchased

## [0.30.10] - 2022-03-08

### Changed

- Fix redeem category id
- Fix error message helper
- Change app name from manifest and info plist file

## [0.30.00] - 2022-03-04

### Added

- Create model class for voucher category
- Create model class for country code

### Changed

- Change voucher UI
- Implement API for voucher category
- Implement geo location for open bill
- Implement change device request API
- Change file name `dialog_remove_device.dart` to `dialog_change_device.dart`
- Change file name `member_button_remove_device.dart` to `member_button_change_device.dart`
- Swap position button change my device, change word remove = change, and take out function timer `profile_page.dart`
- Take out parameter timer, fix word in text dialog `dialog_change_device.dart`
- Change word remove = change `member_button_change_device.dart`

## [0.25.54] - 2022-03-04

### Added

- Created dialog `dialog_remove_device.dart`
- Created button `member_button_remove_device.dart`
- new UI vouchers item `new_vouchers_item.dart`
- Create new helper for camera at `camera_service.dart`
- Create new page for camera UI at `id_card_picker_page.dart`

### Changed

- Create new UI vouchers item `voucher_page.dart`
- Added new montserrat text style `monsterrat_text_view.dart`
- Change `image picker` from capture id card to `camera` plugin
- Update route name
- Change `snackbar` from validatin helper using `ScaffoldMessenger`

## [0.23.54] - 2022-03-02

### Changed

- Change snackbar in each controller to scaffold messanger
- Set barrier color in showDialog `home_controller.dart`
- Set barrier color in showDialog `profile_controller.dart`
- Change word Points = points `balance_card.dart`
- Change word Points = points `my_points.dart`
- Set barrier color in showDialog `edit_profile_page.dart`
- Change word Pts = points `redeem_item_detail.dart`
- Take out widget ColorFiltered `custom_dialog.dart`
- Change word Pts = points `price_item.dart`

## [0.23.50] - 2022-02-25

### Added

- Created `member_account_info_model` for receive data from API

### Changed

- Fix date format in voucher item
- Adjustment UI for tablet
- Update `image_picker` version
- Fix isseu error handler helper
- Implement new API for update amount voucher, points and notifications
- Overwrite on back hardware button for specific case

## [0.23.45] - 2022-02-24

### Changed

- Adjustment UI
- Replace google service demo version
- Update permission file for iOS

## [0.23.42] - 2022-02-23

### Changed

- Fix white blank page cause from null variable in otp page
- Fix resend otp timer
- Fix linking to instagram
- Adjustment my point item UI

## [0.23.40] - 2022-02-22

### Changed

- Remove button close from pop up at home page
- Fix snackbar didn't appear in redeem detail item
- Added pull down to refresh in detail transactions
- Fix type title page in detail transaction and redeem page
- Fix otp resend code
- Change otp message from snackbar
- Fix verification otp `verification_otp_controller.dart`
- set dynamic defautt start `date my_points_controller.dart`
- refactor
- fix device not register for each request
- fix form keyboard dismiss issue
- Update item from my point Wigdet

## [0.23.36] - 2022-02-21

### Changed

- Remove otp dev mode
- Update model class for otp
- Update model class for pop up at home
- Added method for read id pop in shared preferences
- reconfigure app for demo mode
- Implement resend OTP with API
- Implement dialog event
- create expired on argumentData `sign_in_controller.dart`
- Create function coundown timer `verification_otp_controller.dart`
- Set up get builder and condition `verification_otp_page.dart`
- Fix url launcher for youtube
= Rename app into Black Owl Demo for beta testing

## [0.22.35] - 2022-02-18

### Added

- Added terms controller
- Created terms page for privacy policy, terms and conditions
- Created desc shimmer for description loading from explicit file
- Created default shimmer from flutter for solving snackbar showing using scaffold
- Create dialog change picture `dialog_change_picture.dart`
- Added global key references class
- Added firebase configuraration for live version

### Changed

- Fix image upload from edit profile
- Added new routes
- Fix step widget in detail transaction
- Create function to open image from gallery `edit_profile_controller.dart`
- Adjustment point to points `balance_card.dart`
- Create function show dialog `edit_profile_page.dart`
- Fix notification payload navigate to bottom navigation
- Added API handle for server maintenance

## [0.20.30] - 2022-02-17

### Added

- Added points detail model class
- Added terms model class

### Changed

- Fix point list and voucher list can't scroll down
- Fix number of item notification not update `qr_code_controller.dart`
- Adjustment color on weekend
- Adjustment UI, change +62 to 62 `edit_profile_page.dart` `register_page.dart`
- create condition if benefit list empty `member_benefit_profile.dart`
- Adjustment button edit profile `edit_profile_page` `member_button_edit_profile`
- Adjustmenr UI in `redeem_item_detail.dart`
- Fix truncated text at the bottom `city_drop_down.dart` `provice_drop_down.dart`
- Update key on points model class
- Implement point detail API
- Get list outlet, start date and end date `event_detail_controller.dart`
- Get value type of member `home_controller.dart`
- Get lis outlet, start date and end date `promo_detail_controller.dart`
- Create enum type member for balance card `enums.dart`
- Adjustment UI and create date and outlet info on bottle detail `bottle_item_detail.dart`
- Adjustment UI and create date and outlet info on bottle detail `events_detail.dart`
- Adjustment UI and create date and outlet info on bottle detail `food_item_detail.dart`
- Create parameter and condition on balance card  `balance_card.dart`
- Added parameter type of member `home_page.dart`

## [0.18.27] - 2022-02-16

### Added

- Added plugin `flutter_svg: ^0.23.0+1`, `map_launcher: ^2.2.0`, `google_maps_flutter: ^2.1.1`
- Added icon marker `marker.png`
- Added new repository for notification request
- Added new model class for notification data

### Changed

- Create API key in `AndroidManifest.xml`
- Create maps in outlet information `outlet_page.dart` `outlet_controller.dart`
- create new icon `assets_list.dart`
- Adjusment UI edit profile `edit_profile_page.dart`
- Update model class for home data
- Implement notification request from API
- Fix shimmer from promo, and redeem page

## [0.15.27] - 2022-02-15

### Added

- Added new model class and controller for redeem item detail

### Changed

- Fix data type in meta model class
- Adjustment widget for qr code button, profile card, and event carousel,
- Change bundle id to `blackowl.id` for both platform iOS and Android
- Configure notification for platform iOS
- Update point model class
- Implement request get redeem item detail and post item redeem

## [0.13.25] - 2022-02-14

### Changed

- Fix data type in meta model class
- Fix redeem page UI

## [0.13.24] - 2022-02-11

### Added

- Added `badge_shimmer` for outlet loading in voucher list
- Added `item_shimmer` for loading item in voucher page and points history page
- Added new repository for implement voucher list from API at `voucher_repository_interface.dart` and `voucher_repository.dart`
- Added new model data class `voucher_list_model.dart`, `voucher_meta_model.dart`, and `voucher_model.dart`

### Changed

- Fix image profile by change tye to `original`
- Modify custom widget `tabs` add new parameter `tab controller` for listening user behaviour on tabs
- Implement page list view and pull down to refresh at `voucher_controller`
- Modify custom widget, added new parameter for voucher item

## [0.9.24] - 2022-02-10

### Added

- Added `price_shimmer` for redeem page, and promo page
- Added `item_detail_shimmer` for item detail page

### Changed

- Fix space between tab view
- Added pull down to refresh for each page list
- Added plugin url launcher `url_launcher: ^6.0.18`
- Create function visit WA and Instagram `profile_controller.dart`
- initialization controller and create navigation  `profile_page.dart` `member_other_info_profile.dart`
- Adjustment voucher and info outlet `voucher_page.dart` `vouchers_item.dart`

## [0.9.23] - 2022-02-09

### Added

- Added dialog logout `dialog_logout.dart`
- Added `home_shimmer` for home page
- Added `profile_shimmer` for profile page
- Added `gallery_shimmer` for gallery list page
- Added custom empty state `custom_empty_state.dart`

### Changed

- Create function popup dialog logout in `profile_controller.dart` `profile_page.dart`
- Adjustment padding in Home page `home_page.dart`
- Adjustment padding and create change picture `edit_profile_page.dart`
- Adjustment UI in city drop down `city_drop_down.dart`
- Adjustment UI in email address `email_address.dart`
- Adjustemnt UI in gender `gender.dart`
- Adjustment UI in phone number `phone_number.dart`
- Adjustment UI in provice dropdown `provice_drop_down.dart`
- Create new text style `montserrat_text_view.dart`
- Implement shimmer for `profile page`
- Implement image preview for profile picture in `profile page` and `edit profile page`
- Adjustment API for update `member detail`
- Create empty state in bottle page `bottle_page.dart`
- Create empty state in food page `food_page.dart`
- Adjustment appbar `gallery_page.dart` `gallery_detail.dart`
- Create empty state on event, food and bottle `home_page.dart`
- Adjustment appbar `edit_profile_page.dart`
- Adjustment appbar `redeem_item_detail.dart` `redeem_page.dart`

## [0.9.22] - 2022-02-08

### Added

- Added new file for form login using email and phone number
- Added `dual_screen` package for detect hinge sensor from device

### Changed

- Implement update profile using API, shared preferences, and arguments

## [0.8.22] - 2022-02-07

### Changed

- Create function open banner image on `gallery_detail_controller.dart`
- Update request item `promo_repository.dart`
- Create enum Type navigation Section label `enums.dart`
- Adjustment UI in page `bottle_item_detail.dart`
- Adjustment grid item and padding on page `bottle_page.dart`
- Adjustment UI in page `events_detail.dart`
- Adjustment UI in page `events_page.dart`
- Adjustment UI in page `food_item_detail.dart`
- Adjustment grid item and padding on page `food_page.dart`
- Create page banner galery `galler_banner_detail.dart`
- Adjustment UI and navigation banner gallery `gallery_detail.dart`
- Adjustment UI in page `gallery_page.dart`
- Adjustment UI in page `home_page.dart`
- Create parameter Type navigation and condition in section label `section_label.dart`
- Adjustment UI in page `redeem_item_detail.dart`
- Adjustment grid item and padding on page `redeem_page.dart`
- Adjustment UI in page `new_event_item.dart`

## [0.8.21] - 2022-02-04

### Added

- Added validation topic for notification
- Create new model class for company info
- Create new repository for verify ID card but not yet implement to UI
- Create shimmer placeholder class
- Create new repository for open bill
- Added icon edit `edit.png`

### Changed

- Implement firebase notification with payload and topic from API
- Update model class for member detail
- Update new endpoint API
- Implement image cache network for each item
- Implement shimmer at home page including pull down refresh
- Implement request open bill in qr code view
- Change default icon on notification
- Intialization icon edit `pubspec.yaml` `assets_list.dart`
- Get data member verified, verified request, and benefit list to local variable `profile_controller.dart`
- Adjustment type data List<Sring?> `member_benefit_profile.dart`
- Create icon edit profile and coondition on status member verified `member_card_profile.dart`
- Add new key for verified and verified request status at shared preferences
- Fix cast data type error on string helper

## [0.7.21] - 2022-02-03

### Added

- Add plugin carousel_slider: ^4.0.0 `pubspec.yaml`
- Create file new item file `new_event_item.dart`

### Changed

- Create variable carousel controller and index current event `home_controller.dart`
- Adjustment padding Home `balance_card.dart` `member_info.dart` `section_label.dart` `price_item.dart`
- Create carousel on Home page `home_page.dart`
- Create parameter status voucher and condition `vouchers_item.dart`

## [0.7.20] - 2022-02-02

### Changed

- [WIP] Implement firebase notification
- Improve edit profile controller
- Fix number alias for gender in register controller
- Added recent transaction detail `route_names.dart` `routes.dart`
- Update NavigatePriceItem enum `enums.dart`
- Update condition on item recent transaction `item_recent_transactions.dart`
- Update recent transaction detail `recent_transactions_detail.dart` `detail_recent.dart`
- Update color vouchers item `vouchers_item.dart`
- Create condition disable tap on price item `price_item.dart`

## [0.6.17] - 2022-01-31

### Added

- Add new package `qr_flutter` for generate qr code as widget using custom painter
- Created new file for listing country identity

### Changed

- Implement `qr_flutter` on `profile_page.dart`
- Update edit profile page, and shared preferences
- Fix flag from sign and register
- Adjustment size dialog custom_dialog.dart
- Added outlet, notification, recent transaction `route_names.dart` `routes.dart`
- Added icon assets_list.dart `pubspec.yaml`
- Create enum type item and type status voucher `enums.dart`
- Create navigation on `member_info.dart`
- Adjustment voucher page `voucher_page.dart`
- Create ripple and navigation on item voucher `vouchers_item.dart`
- Adjustment flat button `flat_button.dart`
- Create file outlet, notification, recent transaction page outlet_page.dart notification_page.dart `recent_transactions.dart`

## [0.6.16] - 2022-01-28

### Added

- New asset for gallery icon

### Changed

- Refactor, and implement data from API into UI at detail gallery
- Change menu list at bottom navigation
- Change color to gradient in balance card
- Update model class for gallery detail
- Update gallery detail controller
- Fix firbase message configuration on iOS
- Implement data from shared preferences into profile UI
- Update custom dialog. Add parameter function, title promo event `custom_dialog.dart`
- Update translation english_us.dart
- Create type show custom flat button enums.dart
- Adjustment size flat button on sign in page `sign_in_page.dart`
- Adjustment size flat button on verification otp page `verification_otp_page.dart`
- Added icon point on list item my_points.dart
- Added icon clock pubspec.yaml vouchers_item.dart `assets_list.dart`
- Adjustment size flat button on `member_buton_edit_profile.dart`
- Adjustment and add parameter on `flat_button.dart`

## [0.5.15] - 2022-01-27

### Added

- Implement `event`, `promo`, and `gallery` controller

### Changed

- Refactor controller folders
- Update function `getEventOfDateDisplayer()` function at `date_helper.dart`
- Update `read`, `write`, and `remove` for profile data at `shared_pref.dart`
- Update `urlQueries()` and `onWriteProfile()` function at `string_helper.dart`
- Fix wrong json key for `package_model.dart`
- Fix header for refresh token now include with device id
- Removed `const` keyword from `route.dart`
- Added new color to `app_colors.dart`
- Added somes enum to `enums.dart`
- Added preferences key to `preferfences_key.dart`
- Fix and integration data to UI, custom wigdet included
- Work in progress implementation notification
- Added route gallery item detail routes.dart `route_names.dart`
- Create ripple and navigation into detail gallery page on gallery `gallery_page.dart`
- Update navigation in balance card `balance_card.dart`
- Change class voucher item `voucher_page.dart`
- Create new text style `montserrat_text_view.dart`
- Added class item gallery, list, `thumbnail gallery_item.dart`
- Added class gallery item detail `gallery_item_detail.dart`
- Added gallery photo view `gallety_photo_view_wrapper.dart`
- Added transparent route `transparent_route.dart`
- Added vouchers item `vouchers_item.dart`

## [0.4.15] - 2022-01-26

### Added

- Created model class for API
- Created string helper
- Created calling code
- Implement request API for home page
- Added `json_annotation`, `build_runner`, and `json_serializable`
- Added regex helper class

### Changed

- Improve custom widget
- resolve empty data on custom widget
- resolve data class wrong type

## [0.3.15] - 2022=01-25

### Changed

- Added color dark blue `app_colors.dart`
- Adjustment new UI in `bottle_item_detail.dart`
- Adjustment new UI in `food_item_detail.dart`
- Adjustment new UI in `redeem_item_detail.dart`
- Create list on gallery page `gallery_page.dart`
- Create navigation on Home page `home_page.dart`
- Create list item my points `my_points.dart`
- Adjustment price item `price_item.dart`
- Added style text `montserrat_text_view.dart`

## [0.3.14] - 2022=01-24

### Changed

- Create route name event detail and price item detail `route_names.dart` `routes.dart`
- Create enum type show price item, type price item, navigate price item `enums.dart`
- Added type show, type price, navigate price in `bottle_page.dart` `food_page.dart` `home_page.dart` `redeem_page.dart`
- Set minimum year date picker 0001 and maximum year 5000 `custom_date_picker.dart`
- create function navigation on event item and price item `event_item.dart` `price_item.dart`
- create style text on `montserrat_text_view.dart`
- Added file bottle item detail, event item detail, food item detail, redeem item detail `bottle_item_detail.dart` `events_item_detail.dart` `food_item_detail.dart` `redeem_item_detail.dart`

## [0.3.13] - 2022=01-22

- Fix notch bottom navigation bar

## [0.3.12] - 2022-01-21

### Added

- Created custom dialog for region phone number
- Added file event page and controller, bottel page, food page `event_controller` `event_page.dart` `bottle_page.dart` `food_page.dart`

### Changed

- Refactor code at qr code page
- fix phone number argument when navigate to register page
- Update list asset for QR Code page
- Fix icon launcher for iOS and Android
- Adjustment UI
- Implement custom dialog at sign in page
- Implement dynamic country code
- Create routename bottle and Food Promo on `route_names.dart` `routes.dart`
- Crete enum type event item and type price item `enums.dart`
- Create naviagtion and add type item on `home_page.dart`
- Fix size item on redeem page `redeem_page.dart`
- Adjustment size event item `event_item.dart`
- Adjustment size price item `price_item.dart`

## [0.2.12] - 2022-01-20

### Added

- Added new feature for scan QR code
- Added data class for province and city
- Added file page My Voucher and controller my_vouchers.dart my_vouvhers_controller.dart
- Added file page My Points my_points.dart my_points_controller.dart
- Added file general calendar and controller general_calendar.dart general_calendar_controller.dart
- Create file custom general table calendar custom_general_table_calendar.dart

### Changed

- Implement request API for province and city
- Fix phone number for sign in and register
- Added new route navigation
- Adjustment UI
- added plugin table_calendar: ^3.0.3 pubspec.yaml
- Added route name route_names.dart routes.dart
- Added color red weekend app_colors.dart
- Set null safety on title frame_action_wrapper.dart
- Added ripple button and navigation on balance_card.dart
- Added style text on montserrat_text_view.dart

## [0.1.12] - 2022-01-19

### Added

- Created custom widget for tabview at `tabs.dart`
- Created custom form for city and state at `city.dart` and `state.dart`

### Changed

- Update form register and edit profile at `register_page.dart` and `edit_profile_page.dart`
- Adjustment UI at sign in page
- Implement custom widget tab at `redeem_page.dart` and `voucher_page.dart`

## [0.1.11] - 2022-01-18

### Added

- Create custom class dialog `custom_dialog.dart`
- Added icon close rounded `assets_list.dart` `pubspec.yaml`

### Change

- Added enum promo event `enums.dart`

## [0.1.10] - 2022-01-17

### Added

- Created data class when request access token are failed at `access_token_failed.dart`

### Changed

- Disable validate session in dev mode at `authorize_controller.dart`
- Fix button when loading is `true` in the otp page at `verification_otp_page.dart`
- Create function image picker on Profile Controller `profile_controller.dart`
- Create condition after image uploaded `member_card_profile.dart`
- Create grid list on Redeem Page `redeem_page.dart`
- Change primary color each custom widget

## [0.0.10] - 2022-01-15

### Added

- Created data class when request access token are success at `access_token_success.dart`
- Created data class when request authorize are success at `authorize_success.dart`
- Created data class for request body in certain condition at `register_body.dart`
- Created data class when request otp are failed at `register_step_otp_bad_request.dart`
- Created data class when request otp are success only in development mode at `register_step_otp_success_dev.dart`
- Created data class when request register in step submit are failed at `register_step_submit_bad_request.dart`
- Created custom widget for error text at `montserrat_text_error.dart`

### Changed

- Refactor and remove unused import at `main.dart`
- Added snack bar to specific condition at `connection_status_controller.dart`
- Added function for request and validations at `register_controller.dart`, `verification_otp_controller.dart` and `sign_in_controller.dart`
- Update date signature at `date_helper.dart`
- Update functions at `validation.dart`
- Update more translation `english_us.dart`
- Added more parameter at `register_repository_interface.dart` and `register_repository.dart`
- Added network listener at `sign_in_page.dart`
- Refactor code at `verification_otp_page.dart`, `edit_profile_page.dart` and `register_page.dart`
- Improve custom widget for form at `address.dart`, `birth_date.dart`, `email_address.dart`, `first_name.dart`, `gender.dart`, `last_name.dart` and `phone_number.dart`

## [0.0.9] - 2022-01-13

### Added

- Created product items for each list type
- Created models for helping manipulate UI at `on_tap_identifier.dart` and `on_tap_tab_active.dart`
- Created models for receive raw json to data class at `pre_authorize_bad_request.dart` and `pre_authorize_not_found.dart`
- Created controller for voucher page at `voucher_controller.dart`
- Created custom widget for create space between widget at `space_sizer.dart`
- Create status membership on Profile page
- Added register page register_page.dart register_controller.dart
- Dinamic value gender and date picker
- Created custom widget for form, and custom separator
- Created controller for profile page at `profile_controller.dart`
- Create controller redeem `redeem_point_controller.dart`
- Create field OTP with plugin `verification_otp_page.dart`
- Create list on home page, adjustment UI `home_page.dart`
- Create list on voucher page, adjustment UI `voucher_page.dart`

### Changed

- Refactor home page
- Update voucher page
- Improve custom widget
- Refactor code on register page
- Implement API pre-authorize, and authorize
- Improve validation
- add more function for handling date at `date_helper.dart`
- Edit value String on english_us.dart

## [0.0.8] - 2022-01-11

### Added

- Added high resolution asset for home page logo

### Changed

- remove unused asset

## [0.0.7] - 2022-01-10

### Added

- Added `custom_gold_card_widget.dart`
- Added `member_card_profile.dart`
- Added `member_benefit_profile.dart`
- Added `member_button_edit_profile.dart`
- Added `member_other_info_profile.dart`

### Changed

- Refactor code Profile page and adjustment widget `profile_page.dart`
- Create home page UI at `home_page.dart`
- Setup authorize at repository and interface
- Improve custom widget

## [0.0.5] - 2022-01-07

### Added

- Created verification controller at `verification_otp_controller.dart`
- Created scroll configuration helper at `scroll_config.dart`
- Created validation helper at `validation.dart`
- Created class with contain route names at `route_names.dart`
- Created text button at `montserrat_button.dart`

### Changed

- Implement validation on controller at `sign_in_controller.dart`
- Changed color type at snackbar
- Added translations at `english_us.dart`
- Implement route names with separate class at `routes.dart`
- Removed unused color at `app_colors.dart`
- Fix system navigation bar to `transparent` at `frame_view.dart`
- Implement reactive state, implement email form, and prevent keyboard size at `sign_in_page.dart`
- Implement keyboard provider for prevent keyboard size at `verification_otp_page.dart`
- Removed unused code and add custon widget at `profile_page.dart`
- Removed unused paramaters at `flat_button.dart`
- Changed ripple color at `ripple_button.dart`
- Changed error form at `custom_text_field.dart`
- Adjustment size widget at `frame_scaffold_button_nav.dart`
- Removed unused import at `frame_widget_controller.dart`
- Changed text color at `montserrat_text_view.dart`
- Added connection controller `connection_status_controller.dart`
- Intial connection on Home Page `home_controller.dart`

## [0.0.4] - 2022-01-06

### Added

- Added plugin country_calling_code_picker: ^2.0.0

### Changed

- Changed roboto text into montserrat text
- Update translation for sign in, and otp screen
- Adjustment and improve style bottom navigation
- Updates app colors
- Set default country flag and calling code on login page `sign_in_page.dart`
- Set up dialog country picker on controller `sign_in_controller.dart`
- Fix flex on Frame scaffold botton nav (Andoird) `frame_scaffolde_bottom_nav.dart`
- Create class montserrat style `montserrat_text_view.dart`

## [0.0.3] - 2022-01-05

### Added

- Created OTP Screen

### Changed

- Update asset images and icons,
- Color app
- and update custom button
- Refactor and adjustment widget on verification page `verification_otp_page.dart`
- adjusment login page `sign_in_page.dart`(WIP)

## [0.0.2] - 2022-01-04

### Added

- Created authorize view for secondary splash screen
- Added custom widgets
- Created sign in view, Home, Redeems, Gallery, Events, and Profile
- Implement translations
- Implement ripple button at navigation bar

### Changed

- Changed application id for android and iOS
- Configure native splash screen on android
- Fix kotlin version is not compatible for current project
- Removed old bottom navigation bar and use responsive row instead for custom notch navigation bar
- Update routes list

## [0.0.1] - 2022-01-03

### Added

- Templating project
