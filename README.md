# BLACK OWL

## Installation

```bash
# clone branch master
git clone https://gitlab.com/work-drt/black-owl.git

# check available branches
git branch

# create your new branch 
git check out -b name_your_branch
```

## Usage

```bash
# change current branch into another
git check out branch_destination

# check commit history
git reflog
```

## Build, Run & Delivery

Before developing from the source code, or delivery APK or app bundle make sure the project structur are correct, for do that use shell/bash script to configure and help us for avoid miss commandline.

```bash
# release app for demo version
$./release.sh -d

# release app for live version
$./release.sh -r

# show help 
$./release.sh -h

```

## Project Structure

```bash
lib/
├─ controllers/
├─ helpers/
├─ lang/
├─ models/
├─ repository/
├─ routes/
├─ services/
├─ utils/
├─ views/
```

## Contributing

Before contributing to this repository, please read the code of conduct and discuss it with developers. Please follow all our instructions and interactions on this project.

Pull Request Process :

1. Ensure all dependencies development are correct.
2. Ensure files are saved before continuing to pull the request.
3. Ask permission from other developers, and discuss what changes in the file and what effect to others in the future.

Commit Process :

1. Ask other developers what files they still in progress.
2. For commit message explanation written original from Tim Pope.
3. Push branch into master, or specific branch if needed.

Merge Request Process :

1. Discuss with developers with whom you want to merge code.
2. Check files and dependencies are the same with merge destination.
3. If all is correct, please follow the instructions for the commit process.

## Code of Conduct

Before into our standards code, please follow these instructions:

1. Effective Dart by Dart Documentation
2. Code Formatting by Flutter Documentation
3. Flutter Style Guide
