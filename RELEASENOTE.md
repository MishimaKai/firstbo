# RELEASE NOTE BLACK OWL

## Google Play Store

### Version Number 1.5.0 & Version Code 10

- Bug Fixes

### Version Number 1.4.2 & Version Code 9

- Bug Fixes

### Version Number 1.4.1 & Version Code 8

- Bug Fixes

### Version Number 1.4.0 & Version Code 7

- Bug Fixes

### Version Number 1.3.0 & Version Code 6

- New Features
- Bug Fixes

### Version Number 1.2.1 & Version Code 5

- Fix bugs

### Version Number 1.2.0 & Version Code 4

- My Membership
- Fix bugs

### Version Number 1.0.0 & Version Code 3

- New features
- UI improvement
- Fix bugs

### Version Number 1.0.0 & Version Code 2

- Black Owl Membership

## Apple Store

### Version Number 1.5.0 & Version Code 44

- Bug Fixes

### Version Number 1.4.2 & Version Code 41

- Bug Fixes

### Version Number 1.4.1 & Version Code 39

- Bug Fixes

### Version Number 1.4.0 & Version Code 36

- Bug Fixes

### Version Number 1.3.0 & Version Code 28

- New Features
- Bug Fixes

### Version Number 1.2.0 & Version Code 22

- My Membership
- Fix bugs

### Version Number 1.0.0 & Build Number 20

- New features
- UI improvement
- Fix bugs

### Version Number 1.0.0 & Build Number 13

- Black Owl Membership
