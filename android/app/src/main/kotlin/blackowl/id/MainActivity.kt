package blackowl.id
import android.graphics.Color
import android.os.Build
import androidx.core.content.ContextCompat;
import android.os.Bundle
import android.view.Window;
import android.view.WindowManager;
import android.view.View
import androidx.core.view.WindowCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowInsetsControllerCompat
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        // Aligns the Flutter view vertically with the window.
        WindowCompat.setDecorFitsSystemWindows(window, false)
    
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
          // Disable the Android splash screen fade out animation to avoid
          // a flicker before the similar frame is drawn in Flutter.
          val splashScreen = installSplashScreen()
          splashScreen.setOnExitAnimationListener { splashScreenView -> splashScreenView.remove() }
        }
    
        super.onCreate(savedInstanceState)
    }
}
