import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/device_info.dart';
import '../../helpers/notification_service.dart';
import '../../helpers/shared_pref.dart';
import '../../helpers/string_helper.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/client_info/client_info_model.dart';
import '../../models/data/member_account_info/member_account_info_model.dart';
import '../../models/data/member_profile/member_profile_model.dart';
import '../../models/data/redeem/redeem_category_list_model.dart';
import '../../repositories/authorizeRepository/authorize_repository.dart';
import '../../repositories/authorizeRepository/authorize_repository_interface.dart';
import '../../repositories/redeemRepository/redeem_repository.dart';
import '../../repositories/redeemRepository/redeem_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/preferences_key.dart';
import '../../utils/status_code.dart';

class AuthorizeController extends GetxController {
  // constructor
  AuthorizeController({required this.pageContext});

  // param
  final BuildContext pageContext;

  // variables
  final IAuthorizeRepository _authorizeRepository = AuthorizeRepository();
  final IRedeemRepository _redeemRepository = RedeemRepository();
  final SharedPref _sharedPref = SharedPref();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  Rx<String?> versionApp = Rx<String>('');
  Rx<String?> appName = Rx<String>('');
  RxInt year = DateTime.now().year.obs;

  @override
  void onInit() {
    super.onInit();
    _getApplicatinInfo();
    _validateSession();
  }

  // validate user session
  dynamic _validateSession() async {
    final bool hasToken = await _sharedPref.hasData(
      PreferencesKey.keyAccessToken,
    );

    Future<void>.delayed(const Duration(milliseconds: 200), () async {
      if (hasToken) {
        final Map<String, dynamic> isValidateAccountInfo =
            await _getMemberAccountInfo();
        if (isValidateAccountInfo['code'] == StatusCode.codeOk) {
          // save data user profile
          final MemberAccountInfoModel data = MemberAccountInfoModel.fromJson(
            isValidateAccountInfo,
          );
          // validate topics
          NotificationService.validateTopic(data.memberConfig.fcMtopic);
          // write user profile
          StringHelper.onWriteProfile(
            data,
            StringHelper.hasMatchCodeToFlag(
              int.parse(data.data.countryCode),
            ),
          );
          // request member profile
          final Map<String, dynamic> isValidateProfile =
              await _getMemberProfile();
          if (isValidateProfile['code'] == StatusCode.codeOk) {
            // save data
            final MemberProfileModel data = MemberProfileModel.fromJson(
              isValidateProfile,
            );
            // validate topics
            NotificationService.validateTopic(data.memberConfig.fcMtopic);
            // write member profile
            StringHelper.onWriteMyMembership(data);
            // request redeem category
            final Map<String, dynamic> isValidateRedeemCategory =
                await _getRedeemCategory();
            if (isValidateRedeemCategory['code'] == StatusCode.codeOk) {
              final RedeemCategoryListModel dataRedeemCategory =
                  RedeemCategoryListModel.fromJson(isValidateRedeemCategory);
              // validate topics
              NotificationService.validateTopic(
                dataRedeemCategory.memberConfig.fcMtopic,
              );
              // write for initiate later
              _sharedPref.writeRedeemCategoryId(dataRedeemCategory.data![0].id);
              // check for update
              _onAttachForceUpdate(dataRedeemCategory.clientInfo);
            } else {
              Get.offAllNamed(RouteNames.signInRoute);
            }
          } else {
            Get.offAllNamed(RouteNames.signInRoute);
          }
        } else {
          Get.offAllNamed(RouteNames.signInRoute);
        }
      } else {
        Get.offAllNamed(RouteNames.signInRoute);
      }
    });
  }

  // get member account info
  Future<Map<String, dynamic>> _getMemberAccountInfo() async {
    final Map<String, dynamic> memberAccountInfo =
        await _authorizeRepository.getMemberAccountInfo();
    // code
    if (memberAccountInfo['code'] == StatusCode.codeOk) {
      return memberAccountInfo;
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        memberAccountInfo,
      );
      if (isRecall['status'] == true) {
        final Map<String, dynamic> retry =
            await _authorizeRepository.getMemberAccountInfo();
        if (retry['code'] == StatusCode.codeOk) {
          return retry;
        } else {
          return retry;
        }
      } else {
        return memberAccountInfo;
      }
    }
  }

  // get member menu profile
  Future<Map<String, dynamic>> _getMemberProfile() async {
    final Map<String, dynamic> memberProfile =
        await _authorizeRepository.getMembeProfile();
    // code 200
    if (memberProfile['code'] == StatusCode.codeOk) {
      return memberProfile;
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        memberProfile,
      );
      if (isRecall['status'] == true) {
        final Map<String, dynamic> retry =
            await _authorizeRepository.getMembeProfile();
        if (retry['code'] == StatusCode.codeOk) {
          return retry;
        } else {
          return retry;
        }
      } else {
        return memberProfile;
      }
    }
  }

  // get redeem category list
  Future<Map<String, dynamic>> _getRedeemCategory() async {
    final Map<String, dynamic> redeemCategory =
        await _redeemRepository.getRedeemCategoryList();
    // code 200
    if (redeemCategory['code'] == StatusCode.codeOk) {
      return redeemCategory;
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        redeemCategory,
      );
      if (isRecall['status'] == true) {
        final Map<String, dynamic> retry =
            await _redeemRepository.getRedeemCategoryList();
        if (retry['code'] == StatusCode.codeOk) {
          return retry;
        } else {
          return retry;
        }
      } else {
        return redeemCategory;
      }
    }
  }

  dynamic _onAttachForceUpdate(ClientInfo info) {
    if (info.forceUpdate == 1) {
      _updateHandler.handler(info, pageContext);
    } else {
      // navigate.
      Get.offNamedUntil(RouteNames.frameRoute, (Route<dynamic> route) => false);
    }
  }

  Future<void> _getApplicatinInfo() async {
    // package info
    final String? packageInfoVersion = await DeviceInfo().getVersionApp();
    final String? packageInfoName = await DeviceInfo().getAppName();
    versionApp.value = packageInfoVersion;
    appName.value = packageInfoName;
  }
}
