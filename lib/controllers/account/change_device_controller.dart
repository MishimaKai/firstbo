import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/notification_service.dart';
import '../../helpers/shared_pref.dart';
import '../../helpers/string_helper.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/client_info/client_info_model.dart';
import '../../models/data/member_account_info/member_account_info_model.dart';
import '../../models/data/member_profile/member_profile_model.dart';
import '../../models/data/reasons/reason_model.dart';
import '../../models/data/redeem/redeem_category_list_model.dart';
import '../../repositories/authorizeRepository/authorize_repository.dart';
import '../../repositories/authorizeRepository/authorize_repository_interface.dart';
import '../../repositories/homeRepository/home_repository.dart';
import '../../repositories/homeRepository/home_repository_interface.dart';
import '../../repositories/redeemRepository/redeem_repository.dart';
import '../../repositories/redeemRepository/redeem_repository_interface.dart';
import '../../repositories/verifyRepository/verify_repository.dart';
import '../../repositories/verifyRepository/verify_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/app_colors.dart';
import '../../utils/status_code.dart';
import '../../views/change_device/widgets/cancel_dialog.dart';
import '../../views/change_device/widgets/confirmation_dialog.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class ChangeDeviceController extends GetxController {
  ChangeDeviceController({required this.pageContext});

  final BuildContext pageContext;

  // helpers
  final ErrorHandler _errorHandler = ErrorHandler();
  final SharedPref _sharedPref = SharedPref();
  final UpdateHandler _updateHandler = UpdateHandler();

  // repository
  final IAuthorizeRepository _authorizeRepository = AuthorizeRepository();
  final IVerifyRepository _verifyRepository = VerifyRepository();
  final IRedeemRepository _redeemRepository = RedeemRepository();
  final IHomeRepository _homeRepository = HomeRepository();

  // variables
  RxBool isRequestChange = RxBool(false);
  RxString attention = RxString('');
  RxList<ReasonDataSelectModel> reasonList =
      RxList<ReasonDataSelectModel>(<ReasonDataSelectModel>[]);

  Rx<int?> idSelection = Rx<int?>(null);
  Rx<String?> reasonSelection = Rx<String?>(null);
  RxBool isManualInput = RxBool(false);
  TextEditingController reasonInputController = TextEditingController();
  RxBool isDualScreen = RxBool(false);
  RxBool isLoading = RxBool(false);
  RxBool isChangeDeviceProceed = RxBool(false);
  RxBool isLoadingForCancel = RxBool(false);

  ScrollController scrollController = ScrollController();

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic initiateArgs(BuildContext context) {
    final Map<String, dynamic> args = Get.arguments as Map<String, dynamic>;
    final Map<String, dynamic> messageAndErrorCode =
        _errorHandler.messageErrorCodeHandler(args);

    final ReasonModel model =
        ReasonModel.fromJson(args['data'] as Map<String, dynamic>);

    if (messageAndErrorCode['errorMessage'] != null) {
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(montserratSnackbar(
        title: 'Info',
        message: messageAndErrorCode['errorMessage'].toString(),
        status: true,
        customColor: AppColors.darkBlue,
      ));
    }

    isRequestChange.value = model.isRequestChangeDevice == 1; // true
    attention.value = model.attention;
    if (model.reasonDataSelect != null) {
      reasonList.value = model.reasonDataSelect!;
    } else {
      reasonList.clear();
    }

    if (isRequestChange.isTrue) {
      isChangeDeviceProceed.value = true;
    } else {
      isChangeDeviceProceed.value = false;
    }
  }

  dynamic changeReason(int? id, BuildContext context) {
    idSelection.value = id;
    for (final ReasonDataSelectModel element in reasonList) {
      if (idSelection.value == element.id) {
        reasonSelection.value = element.title;
        isManualInput.value = element.allowInputText == 1;
      }
    }
    Navigator.of(context).pop();
  }

  dynamic showDialogConfirm(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: Colors.transparent.withOpacity(0.1),
      builder: (BuildContext dialogContext) {
        return Obx(
          () => ConfirmationDialog(
            content: attention.value,
            isDualScreen: isDualScreen.value,
            onPress: () {
              _postChangeDevice(dialogContext);
            },
            isLoading: isLoading.value,
          ),
        );
      },
    );
  }

  dynamic showDialogCancel(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.transparent.withOpacity(0.1),
      builder: (BuildContext dialogContext) {
        return Obx(
          () => CancelDialog(
            isDualScreen: isDualScreen.value,
            isLoading: isLoadingForCancel.value,
            onPress: () async {
              await _postCancelChangeDevice(dialogContext);
            },
          ),
        );
      },
    );
  }

  dynamic _postChangeDevice(BuildContext context) async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _verifyRepository.postResetDeviceLogin(
      idSelection.value!,
      reasonInputController.text,
    );

    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      isChangeDeviceProceed.value = true;
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    } else {
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
      isLoading.value = false;
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _postCancelChangeDevice(BuildContext context) async {
    isLoadingForCancel.value = true;
    final Map<String, dynamic> result =
        await _verifyRepository.postCancelResetDeviceLogin();

    if (result['code'] == StatusCode.codeOk) {
      isLoadingForCancel.value = false;
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();

      final Map<String, dynamic> resultHome = await _homeRepository.getHome();

      if (resultHome['code'] == StatusCode.codeOk) {
        isLoadingForCancel.value = false;
        Get.offAllNamed(RouteNames.frameRoute);
      } else {
        isLoadingForCancel.value = false;
        // ignore: use_build_context_synchronously
        Navigator.of(context).pop();
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(resultHome);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    } else {
      isLoadingForCancel.value = false;
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic onRefresh() async {
    final Map<String, dynamic> memberAccountInfo =
        await _getMemberAccountInfo();

    if (memberAccountInfo['code'] == StatusCode.codeOk) {
      // member account info
      final MemberAccountInfoModel dataAccountInfo =
          MemberAccountInfoModel.fromJson(memberAccountInfo);
      // validate topics
      NotificationService.validateTopic(dataAccountInfo.memberConfig.fcMtopic);
      // write user profile
      StringHelper.onWriteProfile(
        dataAccountInfo,
        StringHelper.hasMatchCodeToFlag(
          int.parse(dataAccountInfo.data.countryCode),
        ),
      );
      final List<Map<String, dynamic>> result = await Future.wait(
        <Future<Map<String, dynamic>>>[
          _getMemberProfile(),
          _getRedeemCategory(),
        ],
      );

      final Map<String, dynamic> memberProfile = result[0];
      final Map<String, dynamic> redeemCategory = result[1];

      if (memberProfile['code'] == StatusCode.codeOk &&
          redeemCategory['code'] == StatusCode.codeOk) {
        // member priofile
        final MemberProfileModel dataProfile =
            MemberProfileModel.fromJson(memberProfile);
        // validate topics
        NotificationService.validateTopic(dataProfile.memberConfig.fcMtopic);
        // write member profile
        StringHelper.onWriteMyMembership(dataProfile);

        // redeem category
        final RedeemCategoryListModel dataRedeemCategory =
            RedeemCategoryListModel.fromJson(redeemCategory);
        // validate topics
        NotificationService.validateTopic(
          dataRedeemCategory.memberConfig.fcMtopic,
        );
        // write for initiate later
        _sharedPref.writeRedeemCategoryId(dataRedeemCategory.data![0].id);

        // navigate to frame
        _onAttachForceUpdate(dataRedeemCategory.clientInfo);
      } else {
        final Map<String, dynamic> handlerMemberProfile =
            _errorHandler.messageErrorCodeHandler(memberProfile);
        final Map<String, dynamic> handlerRedeemCategory =
            _errorHandler.messageErrorCodeHandler(redeemCategory);

        if (handlerMemberProfile['errorCode'] ==
            handlerRedeemCategory['errorCode']) {
          _messageBar(handlerMemberProfile['errorMessage'].toString());
        } else {
          Future<void>.microtask(() {
            if (handlerMemberProfile['errorMessage'] != null) {
              _messageBar(handlerMemberProfile['errorMessage'].toString());
            }
          }).whenComplete(() {
            if (handlerRedeemCategory['errorMessage'] != null) {
              _messageBar(redeemCategory['errorMessage'].toString());
            }
          });
        }
      }
    } else {
      final Map<String, dynamic> handlerMemberAccountInfo =
          _errorHandler.messageErrorCodeHandler(memberAccountInfo);

      _messageBar(handlerMemberAccountInfo['errorMessage'].toString());
    }
  }

  Future<Map<String, dynamic>> _getMemberAccountInfo() async {
    final Map<String, dynamic> memberAccountInfo =
        await _authorizeRepository.getMemberAccountInfo();

    return memberAccountInfo;
  }

  Future<Map<String, dynamic>> _getMemberProfile() async {
    final Map<String, dynamic> memberProfile =
        await _authorizeRepository.getMembeProfile();

    return memberProfile;
  }

  Future<Map<String, dynamic>> _getRedeemCategory() async {
    final Map<String, dynamic> redeemCategory =
        await _redeemRepository.getRedeemCategoryList();

    return redeemCategory;
  }

  dynamic _messageBar(String param, [String? title, bool? status]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: status ?? false,
    ));
  }

  dynamic _onAttachForceUpdate(ClientInfo info) {
    if (info.forceUpdate == 1) {
      _updateHandler.handler(info, pageContext);
    } else {
      // navigate.
      Get.offNamedUntil(RouteNames.frameRoute, (Route<dynamic> route) => false);
    }
  }
}
