import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/date_helper.dart';
import '../../helpers/validation.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/client_info/client_info_model.dart';
import '../../models/data/province_city/province_city_model.dart';
import '../../models/data/register/failed/register_step_otp_bad_request_model.dart';
import '../../models/data/register/register_body_model.dart';
import '../../models/data/register/success/register_step_otp_success_model.dart';
import '../../repositories/provinceCityRepository/province_city_repository.dart';
import '../../repositories/provinceCityRepository/province_city_repository_interface.dart';
import '../../repositories/registerRepository/register_repository.dart';
import '../../repositories/registerRepository/register_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class RegisterController extends GetxController {
  // constructor
  RegisterController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // helper instance
  final IRegisterRepository _registerRepository = RegisterRepository();
  final IProvinceCityRepository _provinceCityRepository =
      ProvinceCityRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();
  // scroll controller
  ScrollController scrollController = ScrollController();
  // controler text field
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController emailAddress = TextEditingController();
  TextEditingController userAddress = TextEditingController();


  // receive argument from register page
  final dynamic _receiveArg = Get.arguments;

  Rx<String> phoneNumber = RxString('');
  // default country
  String flagName = 'flags/idn.png';
  //default callin code
  String defaultCountryCallingCode = '62';
  // selected gender
  RxString genderSelection = 'Male'.obs;
  // selected date
  Rx<DateTime> selectedDate = DateTime.now().obs;

  RxBool isLoading = RxBool(false);
  RxString firstNameErrorMessage = RxString('');
  RxString lastNameErrorMessage = RxString('');
  RxString phoneNumberErrorMessage = RxString('');
  RxString emailAddressErrorMessage = RxString('');
  RxString addressErrorMessage = RxString('');
  RxString cityErrorMessage = RxString('');

  RxBool isPhoneNumberReadOnly = RxBool(false);
  RxBool isEmailAddressReadOnly = RxBool(false);

  RxList<Datum> provinceList = RxList<Datum>();
  RxList<Datum> cityList = RxList<Datum>();

  Rx<int?> provinceId = Rx<int?>(null);
  Rx<int?> cityId = Rx<int?>(null);

  Map<String, dynamic> _argumentData = <String, dynamic>{};

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _initMemberAccess();
    _getProvince();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // get value from sign in page
  dynamic _initMemberAccess() {
    flagName = _receiveArg?['flag'] as String;
    defaultCountryCallingCode = '${_receiveArg?['country_code']}';
    // type 1 phone number, 2 email address
    if (_receiveArg?['type'] == 1) {
      phoneNumber.value = '${_receiveArg?['member_access']}';
      isPhoneNumberReadOnly.value = true;
      isEmailAddressReadOnly.value = false;
    } else {
      emailAddress.text = '${_receiveArg?['member_access']}';
      isPhoneNumberReadOnly.value = false;
      isEmailAddressReadOnly.value = true;
    }
  }

  // update value gender
  dynamic onChangeGender(dynamic gender, BuildContext context) {
    genderSelection.value = gender.toString();
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  // update value date
  dynamic onChangeDatePicker(dynamic value) {
    selectedDate.value = value as DateTime;
  }

  // gain first name error message from local validation
  bool _firstNameError() {
    String? message;
    final dynamic temp = Validation.validateFirstName(
      pageContext: pageContext,
      value: firstName.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    firstNameErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // gain last name error message from local validation
  bool _lastNameError() {
    String? message;
    final dynamic temp = Validation.validateLastName(
      pageContext: pageContext,
      value: lastName.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    lastNameErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // gain address error message from local validation
  bool _addressError() {
    String? message;
    final dynamic temp = Validation.validateAddress(
      pageContext: pageContext,
      value: userAddress.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    addressErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // convert date
  String _convertDateOfBirth() {
    return DateHelper.getBirthOfDateSender(selectedDate.toString());
  }

  // onChange province
  dynamic onChangeProvince(int? newValue) {
    provinceId.value = newValue;
    if (provinceId.value != null) {
      _getCity(provinceId.value!);
    }
  }

  // onChange city
  dynamic onChangeCity(int? newValue) {
    cityId.value = newValue;
  }

  dynamic _messageBar(String param) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic _forceUpdate(ClientInfo info) {
    _updateHandler.handler(info, pageContext, isDualScreen.value);
  }

  // request GET province
  dynamic _getProvince() async {
    final Map<String, dynamic> result =
        await _provinceCityRepository.getProvince();
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final ProvinceCityModel dataProvince = ProvinceCityModel.fromJson(result);
      provinceList.value = dataProvince.data;
      // validate version
      _forceUpdate(dataProvince.clientInfo);
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  // request GET
  dynamic _getCity(int param) async {
    final Map<String, dynamic> result =
        await _provinceCityRepository.getCity(param);

    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final ProvinceCityModel dataCity = ProvinceCityModel.fromJson(result);
      cityList.value = dataCity.data;
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  // request POST method register for gain OTP
  dynamic _postRegisterStepOTP() async {
    final Map<String, dynamic> result =
        await _registerRepository.postRegisterOtp(
            int.parse(defaultCountryCallingCode),
            phoneNumber.value,
            emailAddress.text,
            firstName.text,
            lastName.text,
            _convertDateOfBirth(),
            genderSelection.value == 'Male' ? 1 : 2,
            cityId.value ?? 0, // city id
            userAddress.text);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      _whenReceiveRegisterData(result);
    } else {
      isLoading.value = false;
      _handlerRegisterStepOTP(result);
    }
  }

  dynamic _whenReceiveRegisterData(Map<String, dynamic> param) {
    final RegisterBodyModel dataBody = RegisterBodyModel.fromJson(
      <String, dynamic>{
        'country_code': defaultCountryCallingCode,
        'phone': phoneNumber.value,
        'email': emailAddress.text,
        'first_name': firstName.text,
        'last_name': lastName.text,
        'dob': _convertDateOfBirth(),
        'gender': genderSelection.value == 'Male' ? 1.toString() : 2.toString(),
        'city_id': cityId.toString(),
        'address': userAddress.text,
      },
    );
    final RegisterStepOtpSuccessModel dataSuccess =
        RegisterStepOtpSuccessModel.fromJson(param);
    // navigate with brings data class to OTP page
    _onNavigateToOTPRoute(dataBody, dataSuccess);
  }

  dynamic _onNavigateToOTPRoute(
    RegisterBodyModel databody,
    RegisterStepOtpSuccessModel dataOtp,
  ) {
    Future<void>.microtask(() {
      GetInstance().resetInstance();
      _argumentData = <String, dynamic>{
        'data_body': databody.toJson(),
        'data_otp': dataOtp.data.toJson(),
        'member_registered': _receiveArg?['member_registered'],
        'flag': flagName,
      };
      isLoading.value = false;
    }).whenComplete(() {
      Get.offAllNamed(RouteNames.verificationOtpRoute,
          arguments: _argumentData);
    });
  }

  dynamic _whenReceiveRegisterValidate(Map<String, dynamic> param) {
    final RegisterStepOtpBadRequestModel dataBadRequest =
        RegisterStepOtpBadRequestModel.fromJson(param);
    // error message for first name
    firstNameErrorMessage.value =
        dataBadRequest.error?.message?.firstName?[0] ?? '';
    // error message for first name
    lastNameErrorMessage.value =
        dataBadRequest.error?.message?.lastName?[0] ?? '';
    // error message for phone number
    phoneNumberErrorMessage.value =
        dataBadRequest.error?.message?.phone?[0] ?? '';
    // error message for email adddress
    emailAddressErrorMessage.value =
        dataBadRequest.error?.message?.email?[0] ?? '';
    // error message for address
    addressErrorMessage.value =
        dataBadRequest.error?.message?.address?[0] ?? '';
    // error message for city
    cityErrorMessage.value = dataBadRequest.error?.message?.cityId?[0] ?? '';
  }

  dynamic _handlerRegisterStepOTP(Map<String, dynamic> result) {
    // if code 400
    if (result['code'] == StatusCode.codeBadRequest) {
      // if code 1003
      if (result['error']['code'] == StatusCode.appCodeValidateData) {
        _whenReceiveRegisterValidate(result);
      } else {
        // set defaul value
        _resetValue();
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    } else {
      // set defaul value
      _resetValue();
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _resetValue() {
    firstNameErrorMessage.value = '';
    lastNameErrorMessage.value = '';
    phoneNumberErrorMessage.value = '';
    emailAddressErrorMessage.value = '';
    addressErrorMessage.value = '';
  }

  dynamic onPressRegister() {
    // set loading to true
    isLoading.value = true;
    // validate
    final bool isValidateFirstName = _firstNameError();
    final bool isValidateLastName = _lastNameError();
    final bool isValidateAddress = _addressError();

    if (isValidateFirstName && isValidateLastName && isValidateAddress) {
      _postRegisterStepOTP();
    } else {
      isLoading.value = false;
    }
  }
}
