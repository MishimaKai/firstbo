import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/device_info.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/client_info/client_info_model.dart';
import '../../models/data/country_code/country_code_model.dart';
import '../../models/data/pre_authorize/pre_authorize_bad_request_model.dart';
import '../../models/data/pre_authorize/pre_authorize_not_found_model.dart';
import '../../models/data/register/success/register_step_otp_success_model.dart';
import '../../repositories/signInRepository/sign_in_repository.dart';
import '../../repositories/signInRepository/sign_in_repository_interface.dart';
import '../../repositories/tokenRepository/token_repository.dart';
import '../../repositories/tokenRepository/token_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class SignInController extends GetxController {
  // constructor
  SignInController({required this.pageContext});
  // param
  final BuildContext pageContext;
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();
  // helper instance
  final ITokenRepository _tokenRepository = TokenRepository();
  final ISignInRepository _signInRepository = SignInRepository();
  // widget controller
  ScrollController scrollController = ScrollController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  // focus node
  FocusNode phoneNumberFocusNode = FocusNode();
  // default flag
  Rx<String> flagName = Rx<String>('flags/idn.png');
  //default callin code
  Rx<String> defaultCountryCallingCode = RxString('62');

  RxBool isUsePhoneNumber = RxBool(true);
  RxBool isLoading = RxBool(false);
  RxList<CountryIdentity> countryCodeList = RxList<CountryIdentity>();

  Map<String, dynamic> _argumentData = <String, dynamic>{};

  Rx<String?> versionApp = Rx<String>('');

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    _getApplicatinInfo();
    super.onInit();
    _getCountryCode();
    flagName.value = 'flags/idn.png';
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // argument for next page
  Map<String, dynamic> _argsRoutePage(String flagName, bool isMemberVerified) {
    return <String, dynamic>{
      'flag': flagName,
      'country_code': defaultCountryCallingCode.value,
      'member_access': isUsePhoneNumber.isTrue
          ? isMemberVerified
              ? '${defaultCountryCallingCode.value}${phoneNumberController.text}'
              : phoneNumberController.text
          : emailAddressController.text,
      'type': isUsePhoneNumber.isTrue ? 1 : 2,
      'member_registered': isMemberVerified,
    };
  }

  // change form type
  dynamic changeFormType() {
    if (isUsePhoneNumber.isTrue) {
      isUsePhoneNumber.value = false;
    } else {
      isUsePhoneNumber.value = true;
    }
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic _forceUpdate(ClientInfo info) {
    _updateHandler.handler(info, pageContext, isDualScreen.value);
  }

  // validate
  dynamic onValidateLogin(BuildContext context) {
    isLoading.value = true;
    _postPreAuthorize();
  }

  dynamic _getCountryCode() async {
    final Map<String, dynamic> result =
        await _signInRepository.getCountryCode();
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final CountryCodeModel dataCountryCode =
          CountryCodeModel.fromJson(result);
      countryCodeList.value = dataCountryCode.data;
      // validate version
      _forceUpdate(dataCountryCode.clientInfo);
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _postPreAuthorize() async {
    final Map<String, dynamic> result = await _tokenRepository.postPreAuthorize(
      isUsePhoneNumber.isTrue
          ? '${defaultCountryCallingCode.value}${phoneNumberController.text}'
          : emailAddressController.text,
      isUsePhoneNumber.isTrue ? 1 : 2,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      final RegisterStepOtpSuccessModel dataSuccess =
          RegisterStepOtpSuccessModel.fromJson(result);
      // navigate to other screen
      _onNavigateToOTPRoute(dataSuccess);
    } else {
      isLoading.value = false;
      // handle error code
      _handlerPreAuthotize(result);
    }
  }

  dynamic _onNavigateToOTPRoute(RegisterStepOtpSuccessModel dataOtp) {
    Future<void>.microtask(() {
      GetInstance().resetInstance();
      _argumentData = <String, dynamic>{
        'flag': flagName.value,
        'member_access': isUsePhoneNumber.isTrue
            ? '${defaultCountryCallingCode.value}${phoneNumberController.text}'
            : emailAddressController.text,
        'type': isUsePhoneNumber.isTrue ? 1 : 2,
        'member_registered': true,
        'data_otp': dataOtp.data.toJson(),
      };
    }).whenComplete(() {
      Get.offAllNamed(RouteNames.verificationOtpRoute,
          arguments: _argumentData);
    });
  }

  dynamic _whenReceivePreAuthValidate(Map<String, dynamic> param) {
    final PreAuthorizeNotFoundModel data =
        PreAuthorizeNotFoundModel.fromJson(param);

    if (data.error.code == StatusCode.appCodeRequiredField) {
      if (isUsePhoneNumber.isTrue) {
        Get.offAllNamed(
          RouteNames.registerRoute,
          arguments: _argsRoutePage(flagName.value, false),
        );
        flagName.value = 'flags/idn.png';
      } else {
        emailAddressController.text = '';
        isUsePhoneNumber.value = true;
        // show snackbar
        _messageBar(
          'Please use phone number for create your account',
          'Sign In Failed',
        );
      }
    } else {
      // show snackbar
      _messageBar(data.error.message, 'Sign In Failed');
    }
  }

  dynamic _whenReceivePreAuthNotFound(Map<String, dynamic> param) {
    final Map<String, dynamic> messageAndErrorCode =
        _errorHandler.messageErrorCodeHandler(param);

    if (messageAndErrorCode['errorCode'] == StatusCode.codeNotFound) {
      // show snackbar
      _messageBar(
        messageAndErrorCode['errorMessage'].toString(),
        'Sign In Failed',
      );
    } else {
      // if code 1006
      if (param['error']['code'] == StatusCode.appCodeRequiredField) {
        _whenReceivePreAuthValidate(param);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(param);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _whenReceivePreAuthBadRequest(Map<String, dynamic> param) {
    // if code 1003
    if (param['error']['code'] == StatusCode.appCodeValidateData) {
      final PreAuthorizeBadRequestModel dataBadRequest =
          PreAuthorizeBadRequestModel.fromJson(param);
      final List<String> errorList = dataBadRequest.error.message.memberAccess;
      String containError = '';
      for (final String element in errorList) {
        containError += '$element\n';
      }
      // show snackbar
      _messageBar(containError);
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(param);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _handlerPreAuthotize(Map<String, dynamic> result) {
    // if code 404
    if (result['code'] == StatusCode.codeNotFound) {
      _whenReceivePreAuthNotFound(result);
      // if code 400
    } else if (result['code'] == StatusCode.codeBadRequest) {
      _whenReceivePreAuthBadRequest(result);
      // if others error
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic updatePhoneIdentify(
    String flagNameParam,
    String countryName,
    int countryCode,
  ) {
    //default callin code
    defaultCountryCallingCode.value = countryCode.toString();
    // flag name
    flagName.value = flagNameParam;
  }

  Future<void> _getApplicatinInfo() async {
    // package info
    final String? packageInfoVersion = await DeviceInfo().getVersionApp();
    versionApp.value = packageInfoVersion;
  }
}
