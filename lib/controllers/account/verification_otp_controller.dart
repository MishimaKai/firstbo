import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/date_helper.dart';
import '../../helpers/notification_service.dart';
import '../../helpers/shared_pref.dart';
import '../../helpers/string_helper.dart';
import '../../middleware/error_handler.dart';
import '../../models/data/access_token/failed/access_token_failed_model.dart';
import '../../models/data/access_token/success/access_token_success_model.dart';
import '../../models/data/authorize/authorize_model.dart';
import '../../models/data/member_account_info/member_account_info_model.dart';
import '../../models/data/member_profile/member_profile_model.dart';
import '../../models/data/redeem/redeem_category_list_model.dart';
import '../../models/data/register/failed/register_step_submit_bad_request_model.dart';
import '../../repositories/authorizeRepository/authorize_repository.dart';
import '../../repositories/authorizeRepository/authorize_repository_interface.dart';
import '../../repositories/redeemRepository/redeem_repository.dart';
import '../../repositories/redeemRepository/redeem_repository_interface.dart';
import '../../repositories/registerRepository/register_repository.dart';
import '../../repositories/registerRepository/register_repository_interface.dart';
import '../../repositories/tokenRepository/token_repository.dart';
import '../../repositories/tokenRepository/token_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class VerificationOTPController extends GetxController {
  // constructor
  VerificationOTPController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // helper instance
  final IRegisterRepository _registerRepository = RegisterRepository();
  final ITokenRepository _tokenRepository = TokenRepository();
  final IAuthorizeRepository _authorizeRepository = AuthorizeRepository();
  final IRedeemRepository _redeemRepository = RedeemRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final SharedPref _sharedPref = SharedPref();
  // widget controller
  ScrollController scrollController = ScrollController();
  TextEditingController otpNumber = TextEditingController();
  // receive argument from register page
  final dynamic _receiveArg = Get.arguments;
  // rx variables
  RxString memberAccess = RxString('');
  RxString otpInstruction = RxString('');
  RxBool isLoading = RxBool(false);
  RxBool isLoadingButton = RxBool(false);
  RxBool isLoadingResend = RxBool(false);
  // set variable timer
  Timer? _timer;
  // set condition
  RxBool isResendCode = RxBool(false);
  // set variable timer String
  RxString timerText = RxString('');
  // set varible expired otp
  DateTime? expiredOTP;

  @override
  void onInit() {
    super.onInit();
    _onCheckMemberAccess();
  }

  @override
  void dispose() {
    super.dispose();
    // dispose timer
    _timer?.cancel();
  }

  // validate argument member access
  dynamic _onCheckMemberAccess() {
    if (_receiveArg['member_registered'] == false) {
      // type 1 phone number, 2 email address
      if (_receiveArg['data_otp']['type'] == 1) {
        // phone number
        memberAccess.value = '${_receiveArg['data_otp']['member_access']}';
        otpInstruction.value = 'otp_instruction_phone'.tr;

        // get expired otp
        expiredOTP = DateTime.parse(_receiveArg['data_otp']['exp'].toString());
        // start timer
        _timer?.cancel();
        _startCountdown(expiredOTP!);
      } else {
        // email address
        memberAccess.value =
            _receiveArg['data_otp']['member_access'].toString();
        otpInstruction.value = 'otp_instruction_email'.tr;

        // get expired otp
        expiredOTP = DateTime.parse(_receiveArg['data_otp']['exp'].toString());
        _timer?.cancel();
        _startCountdown(expiredOTP!);
      }
    } else {
      // type 1 phone number, 2 email address
      if (_receiveArg['type'] == 1) {
        // phone number
        memberAccess.value = '${_receiveArg['member_access']}';
        otpInstruction.value = 'otp_instruction_phone'.tr;

        // get expired otp
        expiredOTP = DateTime.parse(_receiveArg['data_otp']['exp'].toString());
        _timer?.cancel();
        _startCountdown(expiredOTP!);
        // log('message exp otps: $expiredOTP');
      } else {
        // email address
        memberAccess.value = _receiveArg['member_access'].toString();
        otpInstruction.value = 'otp_instruction_email'.tr;

        // get expired otp
        expiredOTP = DateTime.parse(_receiveArg['data_otp']['exp'].toString());
        _timer?.cancel();
        _startCountdown(expiredOTP!);
      }
    }
  }

  /// counting datetime
  int daysBetween(DateTime from, DateTime to) {
    return to.difference(from).inSeconds;
  }

  /// function countdown
  void _startCountdown(DateTime expire) {
    /// count date
    int difference = daysBetween(DateTime.now(), expire);
    // log('message jumlah: $difference');
    const Duration sec = Duration(seconds: 1);
    // set timer
    _timer = Timer.periodic(sec, (Timer timer) {
      final Duration clockTimer = Duration(seconds: difference);
      if (clockTimer.inSeconds <= 1) {
        _timer?.cancel();
        isResendCode.value = true;
      } else {
        // decrement timer
        difference--;
        // insert date to text
        timerText.value =
            '${clockTimer.inMinutes.remainder(60).toString()}:${(clockTimer.inSeconds.remainder(60) % 60).toString().padLeft(2, '0')}';
      }
    });
  }

  dynamic _messageBar(String param, [String? title, bool? status]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: status ?? false,
    ));
  }

  // request POST register submit
  dynamic _postRegisterStepSubmit() async {
    final Map<String, dynamic> result =
        await _registerRepository.postRegisterSubmit(
      int.parse(_receiveArg['data_body']['country_code'].toString()),
      _receiveArg['data_body']['phone'].toString(),
      _receiveArg['data_body']['email'].toString(),
      _receiveArg['data_body']['first_name'].toString(),
      _receiveArg['data_body']['last_name'].toString(),
      DateHelper.getBirthOfDateSender(
        _receiveArg['data_body']['dob'].toString(),
      ),
      int.parse(_receiveArg['data_body']['gender'].toString()),
      int.parse(_receiveArg['data_body']['city_id'].toString()),
      _receiveArg['data_body']['address'].toString(),
      otpNumber.text,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // set loading to false
      isLoading.value = false;
      // store data auhtorize into data class
      final AuthorizeModel dataSuccess = AuthorizeModel.fromJson(result);
      // then request access token
      _proceedVerifySubmit(dataSuccess);
    } else {
      // set loading to false
      isLoading.value = false;
      isLoadingButton.value = false;
      // handle error
      _handlerRegisterStepSubmit(result);
    }
  }

  dynamic _whenReceiveOTPValidate(Map<String, dynamic> param) {
    final RegisterStepSubmitBadRequestModel dataBadRequest =
        RegisterStepSubmitBadRequestModel.fromJson(param);
    // type 1 phone number, 2 email address
    if (_receiveArg['data_otp']['type'] == 1) {
      final List<String>? errorListPhone = dataBadRequest.error?.message?.phone;
      String containErrorPhone = '';
      for (final String element in errorListPhone!) {
        containErrorPhone += '$element\n';
      }
      // show snackbar
      _messageBar(containErrorPhone, 'sign_in_label'.tr);
    } else {
      final List<String>? errorListEmail = dataBadRequest.error?.message?.email;
      String containErrorEmail = '';
      for (final String element in errorListEmail!) {
        containErrorEmail += '$element\n';
      }
      // show snackbar
      _messageBar(containErrorEmail, 'sign_in_label'.tr);
    }
  }

  dynamic _handlerRegisterStepSubmit(Map<String, dynamic> result) {
    // if code 400
    if (result['code'] == StatusCode.codeBadRequest) {
      // if code 1003
      if (result['error']['code'] == StatusCode.appCodeValidateData) {
        _whenReceiveOTPValidate(result);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _proceedVerifySubmit(AuthorizeModel param) async {
    // concurent request
    final Future<List<Map<String, dynamic>>> concurrentAccessToken =
        Future.wait(<Future<Map<String, dynamic>>>[_postAccessToken(param)]);

    final Future<Map<String, dynamic>> concurrentWrapper =
        concurrentAccessToken.then(
      (List<Map<String, dynamic>> value) {
        final Map<String, dynamic> result = value[0];

        if (result['code'] == StatusCode.codeOk) {
          _whenReceiveAccessTokenData(result);
        } else if (result['code'] == StatusCode.codeUnauthorized) {
          _whenReceiveAccessTokenUnAuth(result);
        } else {
          final Map<String, dynamic> messageAndErrorCode =
              _errorHandler.messageErrorCodeHandler(result);
          // navigate to sign in
          navigateToSignInPage(messageAndErrorCode['errorMessage'].toString());
        }

        return result;
      },
    );

    concurrentWrapper.then((Map<String, dynamic> value) async {
      if (value['code'] == StatusCode.codeOk) {
        final Map<String, dynamic> resultMemberInfo =
            await _getMemberAccountInfo();

        if (resultMemberInfo['code'] == StatusCode.codeOk) {
          _whenReceiveMemberAccountInfoData(resultMemberInfo);

          final List<Future<Map<String, dynamic>>> requestList =
              <Future<Map<String, dynamic>>>[
            _getMemberProfile(),
            _getRedeemCategory(),
          ];

          Future.wait(requestList).then((List<Map<String, dynamic>> value) {
            final Map<String, dynamic> resultMemberProfile = value[0];
            final Map<String, dynamic> resultRedeemCategory = value[1];

            if (resultMemberProfile['code'] == StatusCode.codeOk) {
              _whenReceiveMemberProfileData(resultMemberProfile);
            }

            if (resultRedeemCategory['code'] == StatusCode.codeOk) {
              _whenReceiveRedeemCategory(resultRedeemCategory);
            }

            if (resultMemberProfile['code'] != StatusCode.codeOk ||
                resultRedeemCategory['code'] != StatusCode.codeOk) {
              // set loading to false
              isLoading.value = false;
              isLoadingButton.value = false;

              final Map<String, dynamic> messageAndErrorCode =
                  _errorHandler.messageErrorCodeHandler(
                resultMemberProfile['code'] != StatusCode.codeOk
                    ? resultMemberProfile
                    : resultRedeemCategory,
              );
              // show snackbar
              _messageBar(messageAndErrorCode['errorMessage'].toString());
            }
          });
        } else {
          // set loading to false
          isLoading.value = false;
          isLoadingButton.value = false;

          final Map<String, dynamic> messageAndErrorCode =
              _errorHandler.messageErrorCodeHandler(resultMemberInfo);
          // show snackbar
          _messageBar(messageAndErrorCode['errorMessage'].toString());
        }
      } else {
        // set loading to false
        isLoading.value = false;
        isLoadingButton.value = false;
      }
    });
  }

  dynamic _proceedVerifyMemberSubmit() {
    // concurent request
    // ignore: always_specify_types
    Future.wait([_postAuthorize()]).then((List<Map<String, dynamic>> value) {
      if (value[0]['code'] == StatusCode.codeOk) {
        _whenReceiveAuthData(value[0]);
      } else {
        // set loading to false
        isLoading.value = false;
        isLoadingButton.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(value[0]);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    });
  }

  dynamic _whenReceiveAccessTokenData(Map<String, dynamic> param) async {
    // save access token and refresh token
    final AccessTokenSuccessModel dataSuccess =
        AccessTokenSuccessModel.fromJson(param);

    // write access token and refresh token into shared preferences
    await _sharedPref.writeAccessToken(dataSuccess.data.token.accessToken);
    await _sharedPref.writeRefreshToken(dataSuccess.data.token.refreshToken);
  }

  dynamic _whenReceiveAccessTokenUnAuth(Map<String, dynamic> param) {
    // if code 1004
    if (param['error']['code'] == StatusCode.appCodeFailedSendingEmail) {
      final AccessTokenFailedModel dataFailed =
          AccessTokenFailedModel.fromJson(param);
      // navigate to sign in
      navigateToSignInPage(dataFailed.error.message);
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(param);
      // navigate to sign in
      navigateToSignInPage(messageAndErrorCode['errorMessage'].toString());
    }
  }

  dynamic _whenReceiveMemberAccountInfoData(Map<String, dynamic> param) {
    // save data
    final MemberAccountInfoModel data = MemberAccountInfoModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // write user profile
    StringHelper.onWriteProfile(
      data,
      StringHelper.hasMatchCodeToFlag(int.parse(data.data.countryCode)),
    );
  }

  dynamic _whenReceiveMemberProfileData(Map<String, dynamic> param) {
    // save data
    final MemberProfileModel data = MemberProfileModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // write member profile
    StringHelper.onWriteMyMembership(data);
  }

  dynamic _whenReceiveRedeemCategory(Map<String, dynamic> param) {
    final RedeemCategoryListModel dataRedeemCategory =
        RedeemCategoryListModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(dataRedeemCategory.memberConfig.fcMtopic);
    // write for initiate later
    _sharedPref.writeRedeemCategoryId(dataRedeemCategory.data![0].id);
    // navigate to frame route
    navigateToFramePage();
  }

  dynamic _whenReceiveAuthData(Map<String, dynamic> param) {
    // set loading to false
    isLoading.value = false;
    // store data auhtorize into data class
    final AuthorizeModel dataSuccess = AuthorizeModel.fromJson(param);
    // then concuring request
    _proceedVerifySubmit(dataSuccess);
  }

  // request POST access token
  Future<Map<String, dynamic>> _postAccessToken(AuthorizeModel param) async {
    final Map<String, dynamic> result = await _tokenRepository.postAccessToken(
      param.data.authorization.code,
    );
    return result;
  }

  // request GET member account info
  Future<Map<String, dynamic>> _getMemberAccountInfo() async {
    final Map<String, dynamic> result =
        await _authorizeRepository.getMemberAccountInfo();
    return result;
  }

  // request GET member profile
  Future<Map<String, dynamic>> _getMemberProfile() async {
    final Map<String, dynamic> result =
        await _authorizeRepository.getMembeProfile();
    return result;
  }

  // request GET redeem category
  Future<Map<String, dynamic>> _getRedeemCategory() async {
    final Map<String, dynamic> result =
        await _redeemRepository.getRedeemCategoryList();
    return result;
  }

  // request POST authorize
  Future<Map<String, dynamic>> _postAuthorize() async {
    final Map<String, dynamic> result = await _tokenRepository.postAuthorize(
        memberAccess.value, _receiveArg['type'] as int, otpNumber.text);
    return result;
  }

  // navigate to sign in
  dynamic navigateToSignInPage(String param) {
    Future<void>.microtask(() {
      isLoading.value = false;
      isLoadingButton.value = false;
      GetInstance().resetInstance();
      // show snackbar
      _messageBar(param);
      GetInstance().resetInstance();
    }).whenComplete(() {
      // navigate to sign in
      Get.offAllNamed(RouteNames.signInRoute);
    });
  }

  // navigate to frame route
  dynamic navigateToFramePage() {
    Future<void>.microtask(() {
      isLoading.value = false;
      isLoadingButton.value = false;
      FocusManager.instance.primaryFocus?.unfocus();
      GetInstance().resetInstance();
    }).whenComplete(() {
      // navigate to frame UI
      Get.offAllNamed(RouteNames.frameRoute);
    });
  }

  // on press sign in
  dynamic onPressSignIn() {
    isLoading.value = true;
    isLoadingButton.value = true;
    if (_receiveArg['member_registered'] == false) {
      // request POST register Submit
      _postRegisterStepSubmit();
    } else {
      // request POST authorize for sign in
      _proceedVerifyMemberSubmit();
    }
  }

  // resend OTP
  dynamic resendOTPCode() async {
    timerText.value = '';
    _timer?.cancel();
    isLoadingResend.value = true;
    final Map<String, dynamic> result =
        await _authorizeRepository.postResendOTP(
      _receiveArg['member_registered'] == false ? 'registed' : 'login',
      memberAccess.value,
      _receiveArg['member_registered'] == false
          ? _receiveArg['data_otp']['type'] as int
          : _receiveArg['type'] as int,
    );
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      expiredOTP = DateTime.parse(result['data']['exp'].toString());
      _startCountdown(expiredOTP!);
      isResendCode.value = false;
      isLoadingResend.value = false;
      // type 1 phone number, 2 email address
      if (_receiveArg['member_registered'] == false) {
        if (_receiveArg['data_otp']['type'] == 1) {
          _messageBar(
            'OTP has been sent to your mobile number',
            'Verification code',
            true,
          );
        } else {
          _messageBar(
            'OTP has been sent to your email address',
            'Verification code',
            true,
          );
        }
      } else {
        // type 1 phone number, 2 email address
        if (_receiveArg['type'] == 1) {
          _messageBar(
            'OTP has been sent to your mobile number',
            'Verification code',
            true,
          );
        } else {
          _messageBar(
            'OTP has been sent to your email address',
            'Verification code',
            true,
          );
        }
      }
    } else {
      isLoadingResend.value = false;
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }
}
