import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../helpers/date_helper.dart';
import '../../../helpers/notification_service.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/points/points_list_model.dart';
import '../../../models/data/points/points_model.dart';
import '../../../repositories/pointRepository/point_repository.dart';
import '../../../repositories/pointRepository/point_repository_interface.dart';
import '../../../routes/route_names.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/frame/frame_widget_controller.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class MyPointsController extends GetxController {
  // constructor
  MyPointsController({required this.pageContext});
  // paramm
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();

  // initate helper
  final IPointRepository _pointRepository = PointRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // set dynamic default start date
  Rx<DateTime> rangeStartGeneralCalendar =
      DateTime.utc(DateTime.now().year).obs;
  // set default end date
  Rx<DateTime> rangeEndGeneralCalendar = DateTime.now().obs;

  // Title Page
  String titlePage = 'My Points';

  List<PointsModel> pointsHistoryList = <PointsModel>[];

  // page controller
  PagingController<int, PointsModel> pointsHistoryController =
      PagingController<int, PointsModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    pointsHistoryController.addPageRequestListener((int pageKey) {
      getPointHistory(
        DateHelper.getBirthOfDateSender(
          rangeStartGeneralCalendar.value.toString(),
        ),
        DateHelper.getBirthOfDateSender(
          rangeEndGeneralCalendar.value.toString(),
        ),
        pageKey,
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    pointsHistoryController.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic onRefreshPage() {
    pointsHistoryList.clear();
    pointsHistoryController.refresh();
  }

  // open general calendar
  dynamic onOpenCalendar() async {
    final dynamic result = await Get.toNamed(RouteNames.generalCalendar,
        arguments: <String, dynamic>{
          'start_date': rangeStartGeneralCalendar.value,
          'end_date': rangeEndGeneralCalendar.value,
          'title_page': titlePage,
        });

    if (result != null) {
      Future<void>.microtask(() {
        rangeStartGeneralCalendar.value = result[0] as DateTime;
        rangeEndGeneralCalendar.value = result[1] as DateTime;
        titlePage = result[2] as String;
      }).whenComplete(() {
        // then refresh controller and variable list
        onRefreshPage();
      });
    }
  }

  dynamic getPointHistory(String startDate, String endDate, int pageKey) async {
    // then request
    final Map<String, dynamic> result =
        await _pointRepository.getPointHistory(startDate, endDate, pageKey);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      _assignDataToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getPointHistory(startDate, endDate, pageKey);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataToList(Map<String, dynamic> result, int pageKey) {
    final PointListModel data = PointListModel.fromJson(result);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
    // assing data to list
    pointsHistoryList = data.data ?? <PointsModel>[];
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      pointsHistoryController.appendLastPage(pointsHistoryList);
    } else {
      final int nextPage = pageKey + 1;
      pointsHistoryController.appendPage(pointsHistoryList, nextPage);
    }
  }
}
