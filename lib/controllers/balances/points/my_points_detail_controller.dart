import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/date_helper.dart';
import '../../../helpers/notification_service.dart';
import '../../../middleware/error_handler.dart';
import '../../../models/data/points/points_detail_model.dart';
import '../../../repositories/pointRepository/point_repository.dart';
import '../../../repositories/pointRepository/point_repository_interface.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/frame/frame_widget_controller.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class MyPointsDetailController extends GetxController {
  // constructor
  MyPointsDetailController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // set connetion controller
  final FrameController _frameController = Get.find();
  // initate helper
  final IPointRepository _pointRepository = PointRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  // variables
  dynamic args = Get.arguments;
  PointsDetailModel? dataDetail;
  RxBool isLoading = RxBool(false);

  @override
  void onInit() {
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    getPointDetail();
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic getPointDetail() async {
    isLoading.value = true;
    // then request
    final Map<String, dynamic> result =
        await _pointRepository.getPointDetail(args as int);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoading.value = false;
      _assignData(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getPointDetail();
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignData(Map<String, dynamic> param) {
    final PointsDetailModel data = PointsDetailModel.fromJson(param);
    dataDetail = data;
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
  }

  int getDeductedPoints() {
    if (dataDetail != null) {
      return dataDetail?.data.deductedPoint ?? 0;
    } else {
      return 0;
    }
  }

  String getStatus() {
    if (dataDetail != null) {
      return dataDetail?.data.statusName ?? '';
    } else {
      return '';
    }
  }

  String getItemName() {
    if (dataDetail != null) {
      return dataDetail?.data.item.name ?? '';
    } else {
      return '';
    }
  }

  int getItemAmount() {
    if (dataDetail != null) {
      return dataDetail?.data.item.amount ?? 0;
    } else {
      return 0;
    }
  }

  String getItemImage() {
    if (dataDetail != null) {
      return dataDetail?.data.item.image.original ?? '';
    } else {
      return '';
    }
  }

  String getNote() {
    if (dataDetail != null) {
      return dataDetail?.data.note ?? '-';
    } else {
      return '-';
    }
  }

  List<StatusProcess?> getStatusList() {
    if (dataDetail != null) {
      return dataDetail?.data.statusProcess ?? <StatusProcess>[];
    } else {
      return <StatusProcess>[];
    }
  }

  String getStatusNameFromList(int param) {
    final List<StatusProcess?> containList = getStatusList();
    return containList[param]?.name ?? '';
  }

  bool getStatusFromList(int param) {
    final List<StatusProcess?> containList = getStatusList();
    if (containList[param]?.status == 1) {
      return true;
    } else {
      return false;
    }
  }

  String getDatePoints() {
    if (dataDetail != null) {
      final String day =
          DateHelper.getDayDisplayer(dataDetail?.data.pointCreatedAt ?? '');
      final String date =
          DateHelper.getDateDisplayer(dataDetail?.data.pointCreatedAt ?? '');
      final String month = DateHelper.getMonthDisplayer(
          dataDetail?.data.pointCreatedAt ?? '', false);
      final String year =
          DateHelper.getYearDisplayer(dataDetail?.data.pointCreatedAt ?? '');

      return '$day, $date $month $year';
    } else {
      return '';
    }
  }
}
