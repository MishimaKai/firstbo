import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/notification_service.dart';
import '../../../helpers/string_helper.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/redeem/redeem_item_detail_model.dart';
import '../../../repositories/redeemRepository/redeem_repository.dart';
import '../../../repositories/redeemRepository/redeem_repository_interface.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/frame/frame_widget_controller.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class RedeemItemDetailController extends GetxController {
  // constructor
  RedeemItemDetailController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // set connetion controller
  final FrameController _frameController = Get.find();
  // initiate helper
  final IRedeemRepository _redeemRepository = RedeemRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  // final SharedPref _sharedPref = SharedPref();
  final UpdateHandler _updateHandler = UpdateHandler();
  // variables
  RedeemItemDetailModel? dataRedeem;
  final dynamic _receiveArg = Get.arguments;
  RxBool isLoading = RxBool(false);
  RxBool isLoadingRedeem = RxBool(false);
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    _getRedeemItemDetail();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  // get request redeen iten detail
  dynamic _getRedeemItemDetail() async {
    isLoading.value = true;
    // then request
    final Map<String, dynamic> result =
        await _redeemRepository.getRedeemItemDetail(_receiveArg as int);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoading.value = false;
      _assignDataItem(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getRedeemItemDetail();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  // post redeem
  Future<bool> postRedeem() async {
    isLoadingRedeem.value = true;
    final Map<String, dynamic> result = await _redeemRepository.postRedeem(
      _receiveArg as int,
    );
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoadingRedeem.value = false;
      return true;
      // if code 400
    } else {
      isLoadingRedeem.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        final bool recallResult = await postRedeem();
        return recallResult;
      } else {
        isLoadingRedeem.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
        return false;
      }
    }
  }

  dynamic _assignDataItem(Map<String, dynamic> param) {
    final RedeemItemDetailModel data = RedeemItemDetailModel.fromJson(param);
    dataRedeem = data;
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
  }

  String getRedeemImage() {
    if (dataRedeem != null) {
      return dataRedeem?.data.image.original ?? '';
    } else {
      return '';
    }
  }

  String getRedeemName() {
    if (dataRedeem != null) {
      return dataRedeem?.data.name ?? '';
    } else {
      return '';
    }
  }

  String getRedeemPoints() {
    if (dataRedeem != null) {
      return StringHelper.convertToRupiah(dataRedeem?.data.amount ?? 0);
    } else {
      return '';
    }
  }

  String getRedeemDetail() {
    if (dataRedeem != null) {
      return dataRedeem?.data.fullDesc ?? '';
    } else {
      return '';
    }
  }
}
