import 'dart:developer';

import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../helpers/notification_service.dart';
import '../../../helpers/shared_pref.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/UI/tabs/on_tap_tab_active.dart';
import '../../../models/data/redeem/redeem_category_list_model.dart';
import '../../../models/data/redeem/redeem_category_model.dart';
import '../../../models/data/redeem/redeem_item_list_model.dart';
import '../../../models/data/redeem/redeem_item_model.dart';
import '../../../repositories/redeemRepository/redeem_repository.dart';
import '../../../repositories/redeemRepository/redeem_repository_interface.dart';
import '../../../utils/global_value.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class RedeemPointController extends GetxController {
  // contructor
  RedeemPointController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // initiate helper
  final IRedeemRepository _redeemRepository = RedeemRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final SharedPref _sharedPref = SharedPref();
  final UpdateHandler _updateHandler = UpdateHandler();
  // page controller
  PagingController<int, RedeemItemModel> redeemItemListController =
      PagingController<int, RedeemItemModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );

  RxBool isLoading = RxBool(false);
  List<RedeemCategoryModel> categoryList = <RedeemCategoryModel>[];
  List<OnTapTabActive> onTapActiveList = <OnTapTabActive>[];

  List<RedeemItemModel> itemList = <RedeemItemModel>[];
  RxInt categoryWillRefreshId = RxInt(GlobalValue.redeemCategoryId);
  int _lastCategoryRequest = 1;

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    getCategory();
    redeemItemListController.addPageRequestListener((int pageKey) {
      getItemList(categoryWillRefreshId.value, pageKey);
    });
  }

  @override
  void dispose() {
    super.dispose();
    redeemItemListController.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic onRefreshPage() async {
    final int? id = await _sharedPref.readRedeemCategoryId();
    if (id != null) {
      categoryWillRefreshId.value = id;
    }
    itemList.clear();
    // ignore: unnecessary_statements
    categoryList.clear;
    onTapActiveList.clear();
    getCategory();
    log('category id = ${categoryWillRefreshId.value}');
    redeemItemListController.refresh();
  }

  // get request category list for redeem
  dynamic getCategory() async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _redeemRepository.getRedeemCategoryList();
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoading.value = false;
      _assignDataCategoryToList(result);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getCategory();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataCategoryToList(Map<String, dynamic> param) {
    final RedeemCategoryListModel data =
        RedeemCategoryListModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
    categoryWillRefreshId.value = data.data![0].id;
    // write in shared preferences
    _sharedPref.writeRedeemCategoryId(categoryWillRefreshId.value);
    categoryList = data.data ?? <RedeemCategoryModel>[];
    for (final RedeemCategoryModel element in categoryList) {
      onTapActiveList.add(
        OnTapTabActive(
          name: element.name,
          index: element.id,
          // ignore: avoid_bool_literals_in_conditional_expressions
          isActive: element.id == 1 ? true : false,
        ),
      );
    }
  }

  dynamic getItemList(int categoryId, int pageKey) async {
    final Map<String, dynamic> result =
        await _redeemRepository.getRedeemItemList(
      categoryId,
      pageKey,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      _lastCategoryRequest = categoryId;
      _assignDataItemToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getItemList(categoryId, pageKey);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataItemToList(Map<String, dynamic> param, int pageKey) {
    final RedeemItemListModel data = RedeemItemListModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // this checker for make sure list is empty first after page change
    itemList = data.data ?? <RedeemItemModel>[];
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      redeemItemListController.appendLastPage(itemList);
    } else {
      final int nextPage = pageKey + 1;
      redeemItemListController.appendPage(itemList, nextPage);
    }
  }

  dynamic onTapChangeTab(String paramName, int paramIndex) async {
    for (int index = 0; index < onTapActiveList.length; index++) {
      if (paramName == onTapActiveList[index].name) {
        onTapActiveList[paramIndex].isActive = true;
        categoryWillRefreshId.value = onTapActiveList[index].index;
        categoryWillRefreshId.refresh();
        if (categoryWillRefreshId.value != _lastCategoryRequest) {
          itemList.clear();
          redeemItemListController.refresh();
        }
      } else {
        onTapActiveList[index].isActive = false;
      }
    }
  }
}
