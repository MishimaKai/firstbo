import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/notification_service.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/client_info/client_info_model.dart';
import '../../../models/data/voucher/category/category_item.dart';
import '../../../models/data/voucher/category/category_list.dart';
import '../../../repositories/voucherRepository/voucher_repository.dart';
import '../../../repositories/voucherRepository/voucher_repository_interface.dart';
import '../../../routes/route_names.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/frame/frame_widget_controller.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class VoucherCategoryController extends GetxController {
  // constructor
  VoucherCategoryController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // helper
  final IVoucherRepository _voucherRepository = VoucherRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // widget controller
  final FrameController _frameController = Get.find();

  // variable
  RxBool isLoading = RxBool(false);
  RxList<CategoryItemModel> categoryList = RxList<CategoryItemModel>();
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    _frameController.isOutsideFrame.value = true;
    super.onInit();
    _getVoucherCategory();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // update handler
  dynamic _listenUpdate(ClientInfo info) {
    _updateHandler.handler(info, pageContext, isDualScreen.value);
  }

  // listen error message and show into snackbar
  dynamic _messageBar(String message) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: 'label_failed'.tr,
      message: message,
      status: false,
    ));
  }

  // pull down refresh
  dynamic refreshByPulldown() {
    categoryList.clear();
    _getVoucherCategory();
  }

  // request voucher category
  dynamic _getVoucherCategory() async {
    // enable loading page
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _voucherRepository.getVoucherCategory();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // disable loading
      isLoading.value = false;
      // check data category is empty or not
      if (result['data'] != null) {
        if (result['data'] != <dynamic>[]) {
          final CategoryListModel data = CategoryListModel.fromJson(result);
          categoryList.value = data.data;
          // validate topics
          NotificationService.validateTopic(data.memberConfig.fcMtopic);
          // list update
          _listenUpdate(data.clientInfo);
        } else {
          categoryList.clear();
        }
      } else {
        categoryList.clear();
      }
    } else {
      // disable loading
      isLoading.value = true;
      // error handler
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getVoucherCategory();
      } else {
        // disbale loading
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // call snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic onNavigateToList(int index) {
    Map<String, dynamic> args = <String, dynamic>{};
    Future<void>.microtask(() {
      final int type = categoryList[index].type;
      int? outletId;
      final String roomName = categoryList[index].group;
      final String hourName = categoryList[index].name;
      final String formattedAmount = categoryList[index].formattedAmount;
      if (categoryList[index].outlet != null) {
        outletId = categoryList[index].outlet![0].id;
        args = <String, dynamic>{
          'type': type,
          'outletId': outletId,
          'group': roomName,
          'name': hourName,
          'amount': formattedAmount,
        };
      } else {
        args = <String, dynamic>{
          'type': type,
          'outletId': outletId,
          'group': roomName,
          'name': hourName,
          'amount': formattedAmount,
        };
      }
    }).whenComplete(() {
      Get.toNamed(RouteNames.voucherListRoute, arguments: args);
    });
  }
}
