import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../helpers/notification_service.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/client_info/client_info_model.dart';
import '../../../models/data/voucher/list/voucher_item.dart';
import '../../../models/data/voucher/list/voucher_list.dart';
import '../../../models/data/voucher/outlet_item.dart';
import '../../../repositories/voucherRepository/voucher_repository.dart';
import '../../../repositories/voucherRepository/voucher_repository_interface.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class VoucherListController extends GetxController {
  // constructor
  VoucherListController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // helper
  final IVoucherRepository _voucherRepository = VoucherRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();
  // widget controller
  PagingController<int, VoucherItemModel> voucherListController =
      PagingController<int, VoucherItemModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );
  // variable
  dynamic arguments = Get.arguments;
  RxBool isLoading = RxBool(true);
  RxList<VoucherItemModel> voucherList = RxList<VoucherItemModel>(
    <VoucherItemModel>[],
  );
  RxList<OutletItemModel> outletList =
      RxList<OutletItemModel>(<OutletItemModel>[]);
  RxInt selectedOutletId = RxInt(0);
  RxString selectedOutlet = RxString('');
  RxString selectedFormattedAmount = RxString('');
  RxString roomName = RxString('');
  RxString hourName = RxString('');
  RxString formattedAmount = RxString('');
  RxBool isDualScreen = RxBool(false);

  bool isFirstRequest = true;
  int type = 0;
  int? outletId;

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    // get agrs
    type = arguments['type'] as int;
    outletId = arguments['outletId'] as int?;
    roomName.value = arguments['group'] as String;
    hourName.value = arguments['name'] as String;
    formattedAmount.value = arguments['amount'] as String;
    // paged list controller
    voucherListController.addPageRequestListener((int pageKey) {
      _voucherList(type, outletId, pageKey);
    });
  }

  @override
  void dispose() {
    super.dispose();
    voucherListController.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // update handler
  dynamic _listenUpdate(ClientInfo info) {
    _updateHandler.handler(info, pageContext, isDualScreen.value);
  }

  // listen error message and show into snackbar
  dynamic _messageBar(String message) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: 'label_failed'.tr,
      message: message,
      status: false,
    ));
  }

  // pull down refresh
  dynamic refreshByPulldown() {
    // enable loading true
    isLoading.value = true;
    voucherListController.refresh();
  }

  dynamic onTapOutletItem(int? param, BuildContext context) {
    int? containId;
    String? containOutlateName;
    String? containFormattedAmount;
    for (int index = 0; index < outletList.length; index++) {
      if (param == outletList[index].id) {
        containId = outletList[index].id;
        containOutlateName = outletList[index].name;
        containFormattedAmount = outletList[index].formattedAmount;
      }
    }

    selectedOutletId.value = containId ?? selectedOutletId.value;
    selectedOutlet.value = containOutlateName ?? selectedOutlet.value;
    selectedFormattedAmount.value =
        containFormattedAmount ?? selectedFormattedAmount.value;

    if (outletId != selectedOutletId.value) {
      outletId = selectedOutletId.value;
      voucherListController.refresh();
    }

    Navigator.of(context).pop();
  }

  bool onWhenItemNeededEmpty() {
    if (outletList.isEmpty && voucherList.isEmpty) {
      return true;
    } else if (outletList.isEmpty && voucherList.isNotEmpty) {
      return true;
    } else if (outletList.isNotEmpty && voucherList.isEmpty) {
      return false;
    } else {
      return false;
    }
  }

  // request voucher listBlankOn
  dynamic _voucherList(int type, int? outletId, int pageKey) async {
    final Map<String, dynamic> result =
        await _voucherRepository.getVoucherList(type, outletId, pageKey);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // assign when success
      _assignToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _voucherList(type, outletId, pageKey);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  // assign data from API
  dynamic _assignToList(Map<String, dynamic> param, int pageKey) {
    isLoading.value = false;
    final VoucherListModel data = VoucherListModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // listener
    _listenUpdate(data.clientInfo);
    // assign to rx
    formattedAmount.value = data.voucherInfo.formattedAmount;
    voucherList.value = data.data ?? <VoucherItemModel>[];
    // assign only once
    if (pageKey == 1) {
      outletList.value = data.voucherInfo.outlet ?? <OutletItemModel>[];
      if (outletList.isNotEmpty && isFirstRequest) {
        selectedOutletId.value = outletList[0].id;
        selectedOutlet.value = outletList[0].name;
        selectedFormattedAmount.value = outletList[0].formattedAmount;
        isFirstRequest = false;
      }
    }
    // membershipType = data.memberConfig.disableQRAccess;
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      voucherListController.appendLastPage(voucherList);
    } else {
      final int nextPage = pageKey + 1;
      voucherListController.appendPage(voucherList, nextPage);
    }
  }
}
