import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class GeneralCalendarController extends GetxController {
  // SET TITLE PAGE
  String? titlePage;
  // SET RANGE DATE
  DateTime? rangeStartGeneralCalendar;
  DateTime? rangeEndGeneralCalendar;

  // FOCUS CALENDAR
  DateTime focusedDayGeneralCalendar = DateTime.now();

  // default first day calendar
  DateTime firstDayCalendar = DateTime.utc(DateTime.january);
  // default last day calendar
  DateTime lastDayCalendar = DateTime.utc(DateTime.december);
  // default format calendar
  CalendarFormat calendarFormat = CalendarFormat.month;
  DateTime? selectedDayGeneralCalendar;

  // custom weekend
  bool isWeekend(
    DateTime day, {
    List<int> weekendDays = const <int>[DateTime.sunday],
  }) {
    return weekendDays.contains(day.weekday);
  }

  // Get value from previous page
  void assingValue() {
    final dynamic data = Get.arguments;
    if (data['start_date'] != null &&
        data['end_date'] != null &&
        data['title_page'] != null) {
      rangeStartGeneralCalendar = data['start_date'] as DateTime;
      rangeEndGeneralCalendar = data['end_date'] as DateTime;
      titlePage = data['title_page'] as String;
    }
  }

  @override
  void onInit() {
    assingValue();
    setFirstLastDayCalendar();
    super.onInit();
  }

  dynamic setFirstLastDayCalendar() async {
    // 3 years ago
    firstDayCalendar = DateTime.now().subtract(const Duration(days: 1095));
    // 3 years later
    lastDayCalendar = DateTime.now().add(const Duration(days: 1095));
    // log('message: $firstDayCalendar');
    // log('message: $lastDayCalendar');
    // ignore: always_specify_types
    update(['generalCalendar']);
  }

  // selected day
  void daySelectedGeneralCalendar(DateTime selectedDay, DateTime focusedDay) {
    rangeEndGeneralCalendar = selectedDay;
    // ignore: always_specify_types
    update(['generalCalendar']);

    if (selectedDay.isBefore(rangeStartGeneralCalendar!)) {
      rangeStartGeneralCalendar = selectedDay;
      rangeEndGeneralCalendar = null;
      // ignore: always_specify_types
      update(['generalCalendar']);
    } else {
      if (rangeStartGeneralCalendar == rangeEndGeneralCalendar) {
        // ignore: always_specify_types
        Future.delayed(const Duration(milliseconds: 100), () {
          Get.back(
            // ignore: always_specify_types
            result: [
              rangeStartGeneralCalendar,
              rangeEndGeneralCalendar,
              titlePage,
            ],
          );
        });
      } else {
        // ignore: always_specify_types
        Future.delayed(const Duration(milliseconds: 100), () {
          Get.back(
            // ignore: always_specify_types
            result: [
              rangeStartGeneralCalendar,
              rangeEndGeneralCalendar,
              titlePage,
            ],
          );
        });
      }
    }
  }

  // format Change
  void formatChangeGeneralCalendar(CalendarFormat format) {
    if (calendarFormat != format) {
      calendarFormat = format;
      // ignore: always_specify_types
      update(['generalCalendar']);
    }
    // calendarFormat = format;
    // update(['generalCalendar']);
  }

  // page change
  void pageChangeGeneralCalendar(DateTime focusedDay) {
    focusedDayGeneralCalendar = focusedDay;
    // update widget
    // ignore: always_specify_types
    update(['generalCalendar']);

    // getCalendarData();
  }

  // range change
  void onRangeSelectedGeneralCalendar(
    DateTime? start,
    DateTime? end,
    DateTime focusedDay,
  ) {
    selectedDayGeneralCalendar = null;
    focusedDayGeneralCalendar = focusedDay;
    rangeStartGeneralCalendar = start;
    rangeEndGeneralCalendar = end;
    // ignore: always_specify_types
    update(['generalCalendar']);
  }

  // selection mode
  RangeSelectionMode onSetSelectionMode() {
    if (rangeEndGeneralCalendar == null) {
      return RangeSelectionMode.disabled;
    } else {
      return RangeSelectionMode.enforced;
    }
  }
}
