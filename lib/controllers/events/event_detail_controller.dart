import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/date_helper.dart';
import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/event/event_detail_model.dart';
import '../../models/data/outlet_info/outlet_info_model.dart';
import '../../repositories/eventRepository/event_repository.dart';
import '../../repositories/eventRepository/event_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class EventDetailController extends GetxController {
  // constructor
  EventDetailController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();
  // set connetion controller

  // initiate helper
  final IEventRepository _eventRepository = EventRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  EventDetailModel? dataEvent;

  final dynamic _receiveArgs = Get.arguments;
  RxBool isLoading = RxBool(false);
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    _getEventDetail(_receiveArgs as int);
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic _getEventDetail(int id) async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _eventRepository.getEventDetail(id);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      _assignDataToDetail(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getEventDetail(id);
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataToDetail(Map<String, dynamic> result) {
    // when success
    final EventDetailModel data = EventDetailModel.fromJson(result);
    dataEvent = data;
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
  }

  String getEventTitle() {
    if (dataEvent != null) {
      return dataEvent?.data.title ?? '';
    } else {
      return '';
    }
  }

  String getImageEvent() {
    if (dataEvent != null) {
      return dataEvent?.data.image?.original ?? '';
    } else {
      return '';
    }
  }

  String getDescEvent() {
    if (dataEvent != null) {
      return dataEvent?.data.fullDesc ?? '';
    } else {
      return '';
    }
  }

  String getOutletNames() {
    if (dataEvent != null) {
      // ignore: use_if_null_to_convert_nulls_to_bools
      if (dataEvent?.data.outletInfo?.isNotEmpty == true) {
        final List<OutletInfoModel> outletInfo =
            dataEvent?.data.outletInfo ?? <OutletInfoModel>[];
        String result = '';
        for (final OutletInfoModel element in outletInfo) {
          result += '${element.name} ';
        }

        return result;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  /// get list outlet
  List<OutletInfoModel> getListOutlet() {
    if (dataEvent != null) {
      // ignore: use_if_null_to_convert_nulls_to_bools
      if (dataEvent?.data.outletInfo?.isNotEmpty == true) {
        final List<OutletInfoModel> outletInfo =
            dataEvent?.data.outletInfo ?? <OutletInfoModel>[];
        return outletInfo;
      } else {
        return <OutletInfoModel>[];
      }
    } else {
      return <OutletInfoModel>[];
    }
  }

  // get start date
  String getStartDateDetail() {
    if (dataEvent != null) {
      return dataEvent?.data.startDate ?? '';
    } else {
      return '';
    }
  }

  // get date
  String getEndDateDetail() {
    if (dataEvent != null) {
      return dataEvent?.data.endDate ?? '';
    } else {
      return '';
    }
  }

  String getWrappedDate() {
    String result = '';
    // start
    final String startDateDisplay = DateHelper.getDateDisplayer(
      getStartDateDetail(),
    );
    final String startMonthDisplay = DateHelper.getMonthDisplayer(
      getStartDateDetail(),
      false,
    );
    final String startYearDisplay = DateHelper.getYearDisplayer(
      getStartDateDetail(),
    );

    // end
    final String endDateDisplay = DateHelper.getDateDisplayer(
      getEndDateDetail(),
    );
    final String endMonthDisplay = DateHelper.getMonthDisplayer(
      getEndDateDetail(),
      false,
    );
    final String endYearDisplay = DateHelper.getYearDisplayer(
      getEndDateDetail(),
    );

    result =
        '$startDateDisplay $startMonthDisplay $startYearDisplay - $endDateDisplay $endMonthDisplay $endYearDisplay';

    return result;
  }
}
