import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/gallery/gallery_list_model.dart';
import '../../models/data/gallery/gallery_model.dart';
import '../../repositories/galleryRepository/gallery_repository.dart';
import '../../repositories/galleryRepository/gallery_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class GalleryController extends GetxController {
  // constructor
  GalleryController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // initiate helper
  final IGalleryRepository _galleryRepository = GalleryRepository();
  // final SharedPref _sharedPref = SharedPref();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // page controller
  PagingController<int, GalleryModel> galleryListController =
      PagingController<int, GalleryModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );

  // variable
  List<GalleryModel> galleryList = <GalleryModel>[];

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    galleryListController.addPageRequestListener((int pageKey) {
      getGalleryList(pageKey);
    });
  }

  @override
  void dispose() {
    super.dispose();
    galleryListController.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic onRefreshPage() {
    galleryList.clear();
    galleryListController.refresh();
  }

  dynamic getGalleryList(int pageKey) async {
    final Map<String, dynamic> result =
        await _galleryRepository.getGalleryList(pageKey);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      _assignDataToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getGalleryList(pageKey);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataToList(Map<String, dynamic> result, int pageKey) {
    final GalleryListModel data = GalleryListModel.fromJson(result);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
    // assign data to list
    galleryList = data.data ?? <GalleryModel>[];
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      galleryListController.appendLastPage(galleryList);
    } else {
      final int nextPage = pageKey + 1;
      galleryListController.appendPage(galleryList, nextPage);
    }
  }
}
