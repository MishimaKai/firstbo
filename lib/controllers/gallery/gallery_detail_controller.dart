import 'package:cached_network_image/cached_network_image.dart';
import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/gallery/gallery_detail_model.dart';
import '../../repositories/galleryRepository/gallery_repository.dart';
import '../../repositories/galleryRepository/gallery_repository_interface.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';
import '../../utils/status_code.dart';
import '../../views/gallery/widgets/gallery_photo_view_wrapper.dart';
import '../../views/gallery/widgets/transparent_route.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class GalleryDetailController extends GetxController {
  // constructor
  GalleryDetailController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // set connetion controller
  final FrameController _frameController = Get.find();

  // initiate helper
  final IGalleryRepository _galleryRepository = GalleryRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  GalleryDetailModel? dataGallery;

  final dynamic _receiveArgs = Get.arguments;
  RxBool isLoading = RxBool(false);
  RxBool isDualScreen = RxBool(false);
  int currentIndex = 0;
  List<MutipleImageModel> imageDetailList = <MutipleImageModel>[];
  String imageDetailBanner = '';
  String tagImageBanner = '';

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    getGalleryDetail(_receiveArgs as int);
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic getGalleryDetail(int id) async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _galleryRepository.getGalleryDetail(id);

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      _assignDataToDetail(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getGalleryDetail(id);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        ));
      }
    }
  }

  dynamic _assignDataToDetail(Map<String, dynamic> result) {
    // when success
    final GalleryDetailModel data = GalleryDetailModel.fromJson(result);
    dataGallery = data;
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(
      data.clientInfo,
      pageContext,
      isDualScreen.value,
    );
    // assign data
    imageDetailList = dataGallery?.data.multipleImage ?? <MutipleImageModel>[];
    imageDetailBanner = dataGallery?.data.image?.original ?? '';
    tagImageBanner = dataGallery?.data.id.toString() ?? '';
  }

  // open image from gallery detail [list image]
  void openImages(BuildContext context, final int index) {
    Navigator.of(context).push(
      TransparentRoute(
        builder: (BuildContext context) => GalleryPhotoViewWrapper(
          galleryItems: imageDetailList,
          backgroundDecoration: BoxDecoration(
            color: AppColors.darkBackground.withOpacity(0.5),
          ),
          initialIndex: index,
        ),
      ),
    );
  }

  // open image from gallery detail [banner image]
  void openImagesBanner(BuildContext context) {
    Navigator.of(context).push(
      TransparentRoute(builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: PhotoView(
            // imageProvider: NetworkImage(getImageDetail()),
            loadingBuilder: (BuildContext context, ImageChunkEvent? event) {
              return Center(
                child: SizedBox(
                  width: SizeConfig.horizontal(10),
                  height: SizeConfig.horizontal(10),
                  child: CircularProgressIndicator(
                    color: AppColors.darkFlatGold,
                  ),
                ),
              );
            },
            imageProvider: CachedNetworkImageProvider(
              imageDetailBanner,
            ),
            backgroundDecoration: BoxDecoration(
              color: AppColors.darkBackground.withOpacity(0.5),
            ),
            heroAttributes: PhotoViewHeroAttributes(tag: tagImageBanner),
          ),
        );
      }),
    );
  }

  // get image detail
  String getImageDetail() {
    if (dataGallery != null) {
      return dataGallery?.data.image?.original ?? '';
    } else {
      return '';
    }
  }

  // get title
  String getTitleDetail() {
    if (dataGallery != null) {
      return dataGallery?.data.title ?? '';
    } else {
      return '';
    }
  }

  // get date
  String getDateDetail() {
    if (dataGallery != null) {
      return dataGallery?.data.galleryDate ?? '';
    } else {
      return '';
    }
  }

  // get desc
  String getDescDetail() {
    if (dataGallery != null) {
      return dataGallery?.data.fullDesc ?? '';
    } else {
      return '';
    }
  }
}
