import 'package:carousel_slider/carousel_controller.dart';
import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/notification_service.dart';
import '../../helpers/shared_pref.dart';
import '../../middleware/error_handler.dart';
import '../../models/data/event/event_model.dart';
import '../../models/data/gallery/gallery_model.dart';
import '../../models/data/home/home_model.dart';
import '../../models/data/package/package_model.dart';
import '../../repositories/homeRepository/home_repository.dart';
import '../../repositories/homeRepository/home_repository_interface.dart';
import '../../utils/preferences_key.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class HomeController extends GetxController {
  // constructor
  HomeController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // initiate helper
  final IHomeRepository _homeRepository = HomeRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final SharedPref _sharedPref = SharedPref();

  final CarouselController carouselController = CarouselController();

  RxBool isLoading = RxBool(false);
  RxBool isShowDialog = RxBool(false);
  HomeModel? dataHome;
  final RxInt currentEvent = 0.obs;

  // type pop up 1 = image 2 = video
  int type = 1;
  String? image;
  String? source;
  String thumbnail = '';

  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    getRequestHome();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  // request GET home
  dynamic getRequestHome() async {
    isLoading.value = true;
    final Map<String, dynamic> result = await _homeRepository.getHome();

    if (result['code'] == StatusCode.codeOk) {
      // data success
      isLoading.value = false;
      _assignData(result);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getRequestHome();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignData(Map<String, dynamic> result) async {
    final HomeModel dataSuccess = HomeModel.fromJson(result);
    dataHome = dataSuccess;
    // validate topics
    NotificationService.validateTopic(dataSuccess.memberConfig.fcMtopic);
    // check if new version is avaiable
    if (dataSuccess.clientInfo.forceUpdate == 0) {
      // validate pop up
      final bool isValidatePopUp = await _validatePopUp();
      if (isValidatePopUp) {
        isShowDialog.value = true;
      } else {
        isShowDialog.value = false;
      }
    }
  }

  Future<bool> _validatePopUp() async {
    if (dataHome != null) {
      if (dataHome?.data.activePopup is Map<String, dynamic>) {
        final int id = dataHome?.data.activePopup['id'] as int;
        // asssign to variable controller
        type =
            dataHome?.data.activePopup['type'] as int; //type 1= image, 2= video
        image = dataHome?.data.activePopup['image'] as String?;
        source = dataHome?.data.activePopup['source'] as String?;
        // gain thumbnail from video id
        if (source != null) {
          final String videoId =
              Uri.parse(source!).queryParameters['v'].toString();
          thumbnail = 'https://img.youtube.com/vi/$videoId/0.jpg';
        }

        // read from shared preference
        final bool isIdExist =
            await _sharedPref.hasData(PreferencesKey.keyPopUpId);
        // log('is id exist = $_isIdExist');
        // if data id is exist
        if (isIdExist) {
          final int? getId = await _sharedPref.readIdPopUp();
          // if from memory is different from API, then
          if (getId != id) {
            // remove old id from memory
            _sharedPref.removeIdPopUp();
            // write the new one
            _sharedPref.writeIdPopUp(id);

            return true;
          } else {
            return false;
          }
        } else {
          // if no exist
          _sharedPref.writeIdPopUp(id);

          return true;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // gain list event
  List<EventModel> getEventList() {
    if (dataHome != null) {
      return dataHome?.data.upComingEvent ?? <EventModel>[];
    } else {
      return <EventModel>[];
    }
  }

  // gain list bottle packages
  List<PackageModel> getBottlePackages() {
    if (dataHome != null) {
      return dataHome?.data.bottlePackages.data ?? <PackageModel>[];
    } else {
      return <PackageModel>[];
    }
  }

  // gain list Special Offers
  List<PackageModel> getFoodPackages() {
    if (dataHome != null) {
      return dataHome?.data.foodPackages.data ?? <PackageModel>[];
    } else {
      return <PackageModel>[];
    }
  }

  // gain list gallery
  List<GalleryModel> getGalleryList() {
    if (dataHome != null) {
      return dataHome?.data.galleries ?? <GalleryModel>[];
    } else {
      return <GalleryModel>[];
    }
  }
}
