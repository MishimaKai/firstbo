import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/member_home_account_info/member_home_account_info_model.dart';
import '../../repositories/homeRepository/home_repository.dart';
import '../../repositories/homeRepository/home_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/enums.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class MemberAccountInfoController extends GetxController {
  // constructor
  MemberAccountInfoController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // initiate helper
  final IHomeRepository _homeRepository = HomeRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  RxBool isLoadingAmount = RxBool(false);
  RxBool isDualScreen = RxBool(false);
  MemberHomeAccountInfoModel? dataAccountInfo;
  int membershipType = 0;

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    getRequestMemberAccountInfo();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // request GET member account info
  dynamic getRequestMemberAccountInfo() async {
    isLoadingAmount.value = true;
    final Map<String, dynamic> result =
        await _homeRepository.getMemberAccountInfo();
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoadingAmount.value = false;
      final MemberHomeAccountInfoModel data =
          MemberHomeAccountInfoModel.fromJson(result);
      // assign data
      dataAccountInfo = data;
      membershipType = data.memberConfig.disableQRAccess;
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // validate version
      // ignore: use_build_context_synchronously
      _updateHandler.handler(
        data.clientInfo,
        pageContext,
        isDualScreen.value,
      );
    } else {
      // check if token expired
      isLoadingAmount.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getRequestMemberAccountInfo();
      } else {
        isLoadingAmount.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        ));
      }
    }
  }

  // gain image profile
  String getImageProfile() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.image.original ?? '';
    } else {
      return '';
    }
  }

  // gain first name from data class
  String getFirstName() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.firstName ?? '';
    } else {
      return '';
    }
  }

  // gain last name from data class
  String getLastName() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.lastName ?? '';
    } else {
      return '';
    }
  }

  // gain membership badge
  String getMembershipBadge() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.membershipImage ?? '';
    } else {
      return '';
    }
  }

  // gain membership name
  String getMemberhipName() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.membershipName ?? '';
    } else {
      return '';
    }
  }

  // gain total notification
  String? getTotalNotification() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.totalNotification;
    } else {
      return null;
    }
  }

  // gain balance from voucher
  int getBalanceVoucher() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.totalVoucher ?? 0;
    } else {
      return 0;
    }
  }

  // gain balance from point
  int getBalancePoint() {
    if (dataAccountInfo != null) {
      return dataAccountInfo?.data.totalPoint ?? 0;
    } else {
      return 0;
    }
  }

  /// get type of member
  TypeMemberForBalanceCard getTypeMember() {
    if (dataAccountInfo?.memberConfig.disableQRAccess == 1) {
      return TypeMemberForBalanceCard.associate;
    } else {
      return TypeMemberForBalanceCard.other;
    }
  }

  // navigate to voucher page
  dynamic navigateToVoucher() async {
    final dynamic result = await Get.toNamed(RouteNames.voucherCategoryRoute);
    if (result != null && result['update_balance'] == true) {
      getRequestMemberAccountInfo();
    }
  }

  // navigate to points page
  dynamic navigateToPoints() async {
    final dynamic result = await Get.toNamed(RouteNames.myPointsPage);
    if (result != null && result['update_balance'] == true) {
      getRequestMemberAccountInfo();
    }
  }

  // navigate to notifications page
  dynamic navigateToNotifcation() async {
    final dynamic result = await Get.toNamed(RouteNames.notification);
    if (result != null && result['update_balance'] == true) {
      getRequestMemberAccountInfo();
    }
  }
}
