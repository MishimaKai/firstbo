import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/notifications/notification_list_model.dart';
import '../../models/data/notifications/notification_model.dart';
import '../../repositories/notifications/notification_repository.dart';
import '../../repositories/notifications/notification_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class NotificationController extends GetxController {
  // constructor
  NotificationController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();
  // initiate helper
  final INotificationRepository _notificationRepository =
      NotificationRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // paging controller
  PagingController<int, NotificationModel> notificationListController =
      PagingController<int, NotificationModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );

  // variable
  List<NotificationModel> notificationList = <NotificationModel>[];
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    notificationListController.addPageRequestListener((int pageKey) {
      getNotificationList(pageKey);
    });
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  @override
  void dispose() {
    super.dispose();
    notificationListController.dispose();
    // BackButtonInterceptor.remove(interceptOnBackNavigate);
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic onRefreshPage() {
    notificationList.clear();
    notificationListController.refresh();
  }

  dynamic getNotificationList(int pageKey) async {
    final Map<String, dynamic> result =
        await _notificationRepository.getNotificationList(pageKey);

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      _assignDataToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getNotificationList(pageKey);
      } else {
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignDataToList(Map<String, dynamic> param, int pageKey) {
    final NotificationListModel data = NotificationListModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(
      data.clientInfo,
      pageContext,
      isDualScreen.value,
    );
    // assign data to list
    notificationList = data.data ?? <NotificationModel>[];
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      notificationListController.appendLastPage(notificationList);
    } else {
      final int nextpage = pageKey + 1;
      notificationListController.appendPage(notificationList, nextpage);
    }
  }
}
