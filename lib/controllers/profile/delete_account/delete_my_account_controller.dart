import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/country_code/country_code_model.dart';
import '../../../models/data/delete_account/delete_account_model.dart';
import '../../../repositories/deleteAccountRepository/delete_account_interface.dart';
import '../../../repositories/deleteAccountRepository/delete_account_repository.dart';
import '../../../repositories/signInRepository/sign_in_repository.dart';
import '../../../repositories/signInRepository/sign_in_repository_interface.dart';
import '../../../routes/route_names.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class DeleteMyAccountController extends GetxController {
  // constructor
  DeleteMyAccountController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // helper
  final UpdateHandler _updateHandler = UpdateHandler();
  final ErrorHandler _errorHandler = ErrorHandler();
  // respository
  ISignInRepository signInRepository = SignInRepository();
  IDeleteAccount deleteAccountRepository = DeleteAccountRepository();
  // widget controller
  ScrollController scrollController = ScrollController();
  TextEditingController phoneNumberController = TextEditingController();

  // variable
  // default flag
  Rx<String> flagName = Rx<String>('flags/idn.png');
  //default callin code
  Rx<String> defaultCountryCallingCode = RxString('62');
  RxBool isLoading = RxBool(false);
  RxList<CountryIdentity> countryCodeList = RxList<CountryIdentity>();
  Rx<String?> textValidate = Rx<String?>(null);
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _getCountryCode();
    flagName.value = 'flags/idn.png';
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  // scroll controller
  dynamic resetScrollView() {
    scrollController.animateTo(
      0.0,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  dynamic _showSnackBar({
    required String title,
    required String message,
    required bool status,
  }) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title,
      message: message,
      status: status,
    ));
  }

  dynamic _getCountryCode() async {
    final Map<String, dynamic> result = await signInRepository.getCountryCode();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final CountryCodeModel dataCountryCode =
          CountryCodeModel.fromJson(result);
      countryCodeList.value = dataCountryCode.data;
      // validate version
      // ignore: use_build_context_synchronously
      _updateHandler.handler(
        dataCountryCode.clientInfo,
        pageContext,
        isDualScreen.value,
      );
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      _showSnackBar(
        title: 'label_failed'.tr,
        message: messageAndErrorCode['errorMessage'].toString(),
        status: false,
      );
    }
  }

  dynamic updatePhoneIdentify(
    String flagNameParam,
    String countryName,
    int countryCode,
  ) {
    //default callin code
    defaultCountryCallingCode.value = countryCode.toString();
    // flag name
    flagName.value = flagNameParam;
  }

  dynamic navigateToVerify() {
    isLoading.value = true;
    _postProceedOTP();
  }

  dynamic _postProceedOTP() async {
    final Map<String, dynamic> result =
        await deleteAccountRepository.postDeleteAccountOTP(
      defaultCountryCallingCode.value,
      phoneNumberController.text,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      final DeleteAccountModel data = DeleteAccountModel.fromJson(result);
      Get.offNamed(
        RouteNames.deleteVerifyMyAccountPage,
        arguments: <String, dynamic>{
          'country_code': defaultCountryCallingCode.value,
          'phone': phoneNumberController.text,
          'exp': data.data.expired,
        },
      );
      _showSnackBar(
        title: 'Verification code',
        message: 'OTP has been sent to your mobile number',
        status: true,
      );
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _postProceedOTP();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);

        textValidate.value = messageAndErrorCode['errorMessage'].toString();
      }
    }
  }
}
