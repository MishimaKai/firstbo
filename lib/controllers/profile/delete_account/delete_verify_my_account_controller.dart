import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/notification_service.dart';
import '../../../helpers/shared_pref.dart';
import '../../../helpers/string_helper.dart';
import '../../../middleware/error_handler.dart';
import '../../../models/data/delete_account/delete_account_model.dart';
import '../../../repositories/deleteAccountRepository/delete_account_interface.dart';
import '../../../repositories/deleteAccountRepository/delete_account_repository.dart';
import '../../../routes/route_names.dart';
import '../../../utils/status_code.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class DeleteVerifyMyAccountController extends GetxController {
// constructor
  DeleteVerifyMyAccountController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // helper
  final ErrorHandler _errorHandler = ErrorHandler();
  final SharedPref _sharedPref = SharedPref();

  // repository
  final IDeleteAccount deleteAccountRepository = DeleteAccountRepository();
  // widget controller
  TextEditingController otpNumber = TextEditingController();
  ScrollController scrollController = ScrollController();

  // variable
  dynamic args = Get.arguments;
  RxString phoneNumber = RxString('');

  // set variable timer
  Timer? _timer;
  // set condition
  RxBool isResendCode = RxBool(false);
  // set variable timer String
  RxString timerText = RxString('');

  /// set varible expired otp
  DateTime? expiredOTP;

  RxBool isLoadingResendOTP = RxBool(false);
  RxBool isLoadingSubmit = RxBool(false);

  @override
  void onInit() {
    super.onInit();
    // get expired otp
    expiredOTP = DateTime.parse(args['exp'].toString());
    // start timer
    _timer?.cancel();
    _startCountdown(expiredOTP!);
    phoneNumber.value = '${args['country_code']}${args['phone']}';
  }

  @override
  void dispose() {
    super.dispose();
    // dispose timer
    _timer?.cancel();
  }

  // scroll controller
  dynamic resetScrollView() {
    scrollController.animateTo(
      0.0,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }

  dynamic _showSnackBar({
    required String title,
    required String message,
    required bool status,
  }) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title,
      message: message,
      status: status,
    ));
  }

  /// counting datetime
  int daysBetween(DateTime from, DateTime to) {
    return to.difference(from).inSeconds;
  }

  /// function countdown
  void _startCountdown(DateTime expire) {
    /// count date
    int difference = daysBetween(DateTime.now(), expire);
    // log('message jumlah: $difference');
    const Duration sec = Duration(seconds: 1);
    // set timer
    _timer = Timer.periodic(sec, (Timer timer) {
      final Duration clockTimer = Duration(seconds: difference);
      if (clockTimer.inSeconds <= 1) {
        _timer?.cancel();
        isResendCode.value = true;
      } else {
        // decrement timer
        difference--;
        // insert date to text
        timerText.value =
            '${clockTimer.inMinutes.remainder(60).toString()}:${(clockTimer.inSeconds.remainder(60) % 60).toString().padLeft(2, '0')}';
      }
    });
  }

  dynamic onSubmit() {
    _deleteAccountSubmit();
  }

  dynamic resendOTP() async {
    timerText.value = '';
    _timer?.cancel();
    isLoadingResendOTP.value = true;
    final Map<String, dynamic> result =
        await deleteAccountRepository.postDeleteAccountOTP(
      args['country_code'] as String,
      args['phone'] as String,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      final DeleteAccountModel data = DeleteAccountModel.fromJson(result);
      expiredOTP = DateTime.parse(data.data.expired);
      _startCountdown(expiredOTP!);
      isLoadingResendOTP.value = false;
      isResendCode.value = false;
      _showSnackBar(
        title: 'Verification code',
        message: 'OTP has been sent to your mobile number',
        status: true,
      );
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await resendOTP();
      } else {
        isLoadingResendOTP.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        _showSnackBar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        );
      }
    }
  }

  dynamic _deleteAccountSubmit() async {
    isLoadingSubmit.value = true;
    final Map<String, dynamic> result =
        await deleteAccountRepository.postDeleteAccountSubmit(
      args['country_code'] as String,
      args['phone'] as String,
      otpNumber.text,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoadingSubmit.value = false;

      NotificationService.removeAndUnSubTopics();
      _sharedPref.removeAccessToken();
      _sharedPref.removeRefreshsToken();
      _sharedPref.removeIsChangeUserProfile();
      _sharedPref.removeIdPopUp();
      _sharedPref.removeRedeemCategoryId();
      StringHelper.onRemoveProfile();
      GetInstance().resetInstance();
      Get.offAllNamed(RouteNames.authortizeRoute);

      _showSnackBar(
        title: 'Success',
        message: 'Your account has been deleted',
        status: true,
      );
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _deleteAccountSubmit();
      } else {
        isLoadingSubmit.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        _showSnackBar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        );
      }
    }
  }
}
