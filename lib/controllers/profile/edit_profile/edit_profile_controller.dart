import 'dart:io';
import 'dart:typed_data';

import 'package:country_calling_code_picker/country_code_picker.dart';
import 'package:dual_screen/dual_screen.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';

import '../../../helpers/date_helper.dart';
import '../../../helpers/notification_service.dart';
import '../../../helpers/string_helper.dart';
import '../../../helpers/validation.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/member_detail/failed/member_update_bad_request_model.dart';
import '../../../models/data/province_city/province_city_model.dart';
import '../../../models/data/verify/verify_model.dart';
import '../../../repositories/provinceCityRepository/province_city_repository.dart';
import '../../../repositories/provinceCityRepository/province_city_repository_interface.dart';
import '../../../repositories/verifyRepository/verify_repository.dart';
import '../../../repositories/verifyRepository/verify_repository_interface.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/status_code.dart';
import '../../../views/gallery/widgets/transparent_route.dart';
import '../../../views/widgets/frame/frame_widget_controller.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class EditProfileController extends GetxController {
  // constructor
  EditProfileController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();
  final IProvinceCityRepository _provinceCityRepository =
      ProvinceCityRepository();
  final IVerifyRepository _verifyRepository = VerifyRepository();

  // error handler and shared preferences
  // final SharedPref _sharedPref = SharedPref();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // set image picker
  final ImagePicker imagePicker = ImagePicker();

  ScrollController scrollController = ScrollController();
  // controler text field
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  // TextEditingController phoneNumber = TextEditingController();
  Rx<String> phoneNumber = RxString('');
  TextEditingController emailAddress = TextEditingController();
  TextEditingController userAddress = TextEditingController();

  String phoneNumberOriginal = '';
  Rx<String> profilePic = Rx<String>('');

  RxList<Datum> provinceList = RxList<Datum>();
  RxList<Datum> cityList = RxList<Datum>();

  Rx<int?> provinceId = Rx<int?>(null);
  Rx<int?> cityId = Rx<int?>(null);

  //default callin code
  String defaultCountryCallingCode = '62';
  // selected gender
  RxString genderSelection = 'Male'.obs;
  // selected date
  Rx<DateTime> selectedDate = DateTime.now().obs;

  File? cameraImageProfile;
  RxBool isImageFromFile = RxBool(false);

  RxBool isLoading = RxBool(false);
  RxString firstNameErrorMessage = RxString('');
  RxString lastNameErrorMessage = RxString('');
  RxString phoneNumberErrorMessage = RxString('');
  RxString emailAddressErrorMessage = RxString('');
  RxString addressErrorMessage = RxString('');
  RxString cityErrorMessage = RxString('');

  RxBool isPhoneNumberReadOnly = RxBool(false);
  RxBool isEmailAddressReadOnly = RxBool(false);

  RxBool isDualScreen = RxBool(false);

  // default country
  Image defaultCountry = Image.asset(
    'flags/idn.png',
    package: countryCodePackageName,
  );

  final dynamic _receiveArg = Get.arguments;

  FocusNode? editProfileNode;

  @override
  // ignore: avoid_void_async
  void onInit() async {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    await _getProvince();
    _initValueArgs();
    await _getCity(provinceId.value!, false);
    editProfileNode = FocusNode();
  }

  @override
  void dispose() {
    editProfileNode?.dispose();
    super.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: false,
    ));
  }

  dynamic _initValueArgs() {
    firstName.text = '${_receiveArg?['first_name']}';
    lastName.text = '${_receiveArg?['last_name']}';
    // phone number
    final String temp = '${_receiveArg?['phone']}';
    phoneNumber.value = temp.substring(
      2,
      '${_receiveArg?['phone']}'.length,
    ); // assign to edit profile form
    phoneNumberOriginal = '${_receiveArg?['phone']}';
    emailAddress.text = '${_receiveArg?['email']}';
    genderSelection.value = '${_receiveArg?['gender']}';
    selectedDate.value = DateTime(
      int.parse(
        DateHelper.getYearDisplayerSimplified('${_receiveArg?['dob']}'),
      ),
      int.parse(
        DateHelper.getMonthDisplayerSimplified('${_receiveArg?['dob']}'),
      ),
      int.parse(DateHelper.getDateDisplayer('${_receiveArg?['dob']}')),
    );
    cityId.value = _receiveArg?['city_id'] as int?;
    provinceId.value = _receiveArg?['province_id'] as int?;
    userAddress.text = '${_receiveArg?['address']}';
    isImageFromFile.value = _receiveArg?['is_image_from_file'] as bool;
    if (isImageFromFile.isTrue) {
      cameraImageProfile = _receiveArg?['profile_pic'] as File?;
    } else {
      profilePic.value = '${_receiveArg?['profile_pic']}';
    }
  }

  // open image from gallery detail
  void openImageProfile(BuildContext context) {
    Navigator.of(context).push(
      TransparentRoute(builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Obx(() {
            if (isImageFromFile.isTrue) {
              return PhotoView(
                imageProvider: FileImage(cameraImageProfile!),
                backgroundDecoration: BoxDecoration(
                  color: AppColors.darkBackground.withOpacity(0.5),
                ),
                heroAttributes: const PhotoViewHeroAttributes(tag: 0),
              );
            } else {
              return PhotoView(
                imageProvider: NetworkImage(profilePic.value),
                backgroundDecoration: BoxDecoration(
                  color: AppColors.darkBackground.withOpacity(0.5),
                ),
                heroAttributes: const PhotoViewHeroAttributes(tag: 0),
              );
            }
          }),
        );
      }),
    );
  }

  // update value gender
  dynamic onChangeGender(dynamic gender, BuildContext context) {
    genderSelection.value = gender.toString();
    final FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  // update value date
  dynamic onChangeDatePicker(dynamic value) {
    selectedDate.value = value as DateTime;
  }

  // gain first name error message from local validation
  bool _firstNameError() {
    String? message;
    final dynamic temp = Validation.validateFirstName(
      pageContext: pageContext,
      value: firstName.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    firstNameErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // gain last name error message from local validation
  bool _lastNameError() {
    String? message;
    final dynamic temp = Validation.validateLastName(
      pageContext: pageContext,
      value: lastName.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    lastNameErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // gain address error message from local validation
  bool _addressError() {
    String? message;
    final dynamic temp = Validation.validateAddress(
      pageContext: pageContext,
      value: userAddress.text,
      useSnackbar: false,
    );
    message = temp is String ? temp : null;
    addressErrorMessage.value = message ?? '';
    // return true if value are valid
    if (message != null) {
      return false;
    } else {
      return true;
    }
  }

  // onChange province
  dynamic onChangeProvince(int? newValue) {
    provinceId.value = newValue;
    // log('province id = ${provinceId.value}');
    if (provinceId.value != null) {
      _getCity(provinceId.value!, true);
    }
  }

  // onChange city
  dynamic onChangeCity(int? newValue) {
    cityId.value = newValue;
  }

  // request GET province
  dynamic _getProvince() async {
    final Map<String, dynamic> result =
        await _provinceCityRepository.getProvince();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final ProvinceCityModel dataProvince = ProvinceCityModel.fromJson(result);
      provinceList.value = dataProvince.data;
      // validate version
      // ignore: use_build_context_synchronously
      _updateHandler.handler(
        dataProvince.clientInfo,
        pageContext,
        isDualScreen.value,
      );
    } else {
      // show snackbar
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  // request GET
  dynamic _getCity(int param, bool isForpick) async {
    if (isForpick) {
      cityId.value = null;
    }
    final Map<String, dynamic> result =
        await _provinceCityRepository.getCity(param);

    if (result['code'] == StatusCode.codeOk) {
      // save into data class
      final ProvinceCityModel dataCity = ProvinceCityModel.fromJson(result);
      cityList.value = dataCity.data;
    } else {
      // show snackbar
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  // update data profile
  dynamic _postUpdateProfile() async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _verifyRepository.postUpdateProfile(
      firstName.text,
      lastName.text,
      _convertDateOfBirth(),
      emailAddress.text,
      genderSelection.value == 'Male' ? 1 : 2,
      cityId.value ?? 0,
      userAddress.text,
      cameraImageProfile,
    );

    if (result['code'] == StatusCode.codeOk) {
      _assignValue(result);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _postUpdateProfile();
      } else {
        isLoading.value = false;
        _onReceiverErrorBadRequest(result);
      }
    }
  }

  dynamic _assignValue(Map<String, dynamic> param) {
    final VerifyModel data = VerifyModel.fromJson(param);
    Future<void>.microtask(() {
      // write on memory
      StringHelper.onWriteEditProfile(
        firstName.text,
        lastName.text,
        emailAddress.text,
        genderSelection.value,
        selectedDate.value.toString(),
        cityId.value!,
        provinceId.value!,
        userAddress.text,
      );
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // set loading to false
      isLoading.value = false;
      // isImageFromFile.value = false;
    }).whenComplete(() {
      // then navigate
      Get.back(result: <String, dynamic>{
        'first_name': firstName.text,
        'last_name': lastName.text,
        'phone': phoneNumberOriginal,
        'email': emailAddress.text,
        'gender': genderSelection.value,
        'dob': selectedDate.value.toString(),
        'city_id': cityId.value,
        'province_id': provinceId.value,
        'address': userAddress.text,
        'profile_pic':
            isImageFromFile.isTrue ? cameraImageProfile : profilePic.value,
        'is_image_from_file': isImageFromFile.value,
      });
    });
  }

  dynamic _resetValue() {
    firstNameErrorMessage.value = '';
    lastNameErrorMessage.value = '';
    phoneNumberErrorMessage.value = '';
    emailAddressErrorMessage.value = '';
    addressErrorMessage.value = '';
  }

  dynamic _onReceiverErrorBadRequest(Map<String, dynamic> param) {
    if (param['error']['code'] == StatusCode.appCodeValidateData) {
      final MemberUpdateBadRequestModel dataBadRequest =
          MemberUpdateBadRequestModel.fromJson(param);

      // error message for first name
      firstNameErrorMessage.value =
          dataBadRequest.error?.message?.firstName?[0] ?? '';
      // error message for first name
      lastNameErrorMessage.value =
          dataBadRequest.error?.message?.lastName?[0] ?? '';
      // error message for email adddress
      emailAddressErrorMessage.value =
          dataBadRequest.error?.message?.email?[0] ?? '';
      // error message for address
      addressErrorMessage.value =
          dataBadRequest.error?.message?.address?[0] ?? '';
      // error message for city
      cityErrorMessage.value = dataBadRequest.error?.message?.cityId?[0] ?? '';
    } else {
      // set defaul value
      _resetValue();
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(param);
      // show snackbar
      _messageBar(messageAndErrorCode['errorMessage'].toString());
    }
  }

  // convert date
  String _convertDateOfBirth() {
    return DateHelper.getBirthOfDateSender(selectedDate.toString());
  }

  // get image from camera
  dynamic imgFromCameraProfile(BuildContext context) async {
    Navigator.pop(context);
    final XFile? image = await imagePicker.pickImage(
      source: ImageSource.camera,
      preferredCameraDevice: CameraDevice.front,
      maxHeight: 512,
      maxWidth: 512,
      imageQuality: 100,
    );
    // update(['profileInfo']);
    if (image != null) {
      cameraImageProfile = null;
      cameraImageProfile = File(image.path);
      isImageFromFile.value = true;
    }
    update();
  }

  // get image from gallery
  dynamic imgFromCameraGallery(BuildContext context) async {
    Navigator.pop(context);
    if (Platform.isIOS) {
      final FilePickerResult? image = await FilePicker.platform.pickFiles(
        type: FileType.image,
      );
      if (image != null) {
        // log('image upload = ${_image.files.single.path}');
        // convert to jpg format
        final String? jpegPath =
            await HeicToJpg.convert(image.files.single.path!);
        if (jpegPath != null) {
          // log('image upload = $_jpegPath');
          // compress file and update variable
          cameraImageProfile = await _compressBeforeUpload(jpegPath);
          // log('image upload = ${cameraImageProfile!.path}');
        }
        // set trigger image from file to true
        isImageFromFile.value = true;
        update();
      }
    } else {
      final FilePickerResult? image = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: <String>['jpg', 'jpeg', 'png'],
      );

      if (image != null) {
        // compress file and update variable
        cameraImageProfile =
            await _compressBeforeUpload(image.files.single.path!);
        // set trigger image from file to true
        isImageFromFile.value = true;
        update();
      }
    }
  }

  Future<File> _compressBeforeUpload(String pathFile) async {
    final Uint8List? convertImage = await FlutterImageCompress.compressWithFile(
      pathFile,
      minHeight: 512,
      minWidth: 512,
      quality: 80,
    );
    // call temporary directory
    final Directory tempDir = await getTemporaryDirectory();
    // get file name
    final String fileName = File(pathFile).uri.pathSegments.last;
    // set path
    final File willSaveFile = await File('${tempDir.path}/$fileName').create();
    // convert data type Uint8List to List<Int>
    final List<int> convertedUint8 = List<int>.from(convertImage!);
    // write on directory
    willSaveFile.writeAsBytesSync(convertedUint8);

    return willSaveFile;
  }

  dynamic onPressVerify() async {
    // set loading to true
    isLoading.value = true;

    // validate
    final bool isValidateFirstName = _firstNameError();
    final bool isValidateLastName = _lastNameError();
    //final bool isValidatePhoneNumber = _phoneNumberError();
    // final bool isValidateEmail = _emailAdressError();
    final bool isValidateAddress = _addressError();

    if (isValidateFirstName &&
        isValidateLastName &&
        // isValidatePhoneNumber &&
        // isValidateEmail &&
        isValidateAddress) {
      await _postUpdateProfile();
    } else {
      isLoading.value = false;
    }
  }
}
