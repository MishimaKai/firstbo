import 'dart:io';

import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

import '../../../helpers/notification_service.dart';
import '../../../middleware/error_handler.dart';
import '../../../middleware/update_handler.dart';
import '../../../models/data/member_account_info/member_account_info_model.dart';
import '../../../models/data/verify/verify_model.dart';
import '../../../repositories/authorizeRepository/authorize_repository.dart';
import '../../../repositories/authorizeRepository/authorize_repository_interface.dart';
import '../../../repositories/verifyRepository/verify_repository.dart';
import '../../../repositories/verifyRepository/verify_repository_interface.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/status_code.dart';
import '../../../views/gallery/widgets/transparent_route.dart';
import '../../../views/widgets/layouts/montserrat_snackbar.dart';

class PersonalInfoController extends GetxController {
  // constructor
  PersonalInfoController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // authorize controller
  final IAuthorizeRepository _authorizeRepository =
      AuthorizeRepository(); // only for request member detail
  // verify repository
  final IVerifyRepository _verifyRepository = VerifyRepository();

  // helpers
  final ErrorHandler _errorHandler = ErrorHandler();
  // final SharedPref _sharedPref = SharedPref();
  final UpdateHandler _updateHandler = UpdateHandler();

  // image file
  File? cameraImageProfile;

  // personal info
  Rx<String> memberPhoneNumber = Rx<String>('');
  Rx<String> firstName = Rx<String>('');
  Rx<String> lastName = Rx<String>('');
  Rx<String> memberGender = Rx<String>('');
  Rx<String> memberEmailAddress = Rx<String>('');
  Rx<String> memberDob = Rx<String>('');
  Rx<String> memberAddress = Rx<String>('');
  Rx<String> profilePicture = Rx<String>('');
  RxInt memberVerified = RxInt(0);
  RxInt memberVerifiedRequest = RxInt(0);
  Rx<int?> provinceId = Rx<int?>(null);
  Rx<int?> cityId = Rx<int?>(null);
  Rx<String> provinceName = Rx<String>('');
  Rx<String> cityName = Rx<String>('');

  // loading request
  RxBool isLoading = RxBool(false);
  RxBool isLoadingForVerify = RxBool(false);
  // identify image from file or network
  RxBool isImageFromFile = RxBool(false);
  RxBool isDualScreen = RxBool(false);
  RxDouble foldHinge = RxDouble(0);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    getMemberAccountInfo();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });

    DualScreenInfo.hingeAngleEvents.listen((double hingeAngle) {
      foldHinge.value = hingeAngle;
    });
  }

  dynamic _messageBar(String param, [String? title, bool? status]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: status ?? false,
    ));
  }

  dynamic _assignValue(MemberAccountInfoModel param) {
    // validate version
    _updateHandler.handler(param.clientInfo, pageContext, isDualScreen.value);

    // profile picture
    profilePicture.value = param.data.image.original ?? '';
    isImageFromFile.value = false;

    // assign profile datas
    memberPhoneNumber.value = param.data.phone;

    // first name and last name
    firstName.value = param.data.firstName;
    lastName.value = param.data.lastName;

    // gender
    memberGender.value = param.data.genderName;

    // email address
    memberEmailAddress.value = param.data.email;

    // birth of date
    memberDob.value = param.data.dob;

    // address
    provinceId.value = param.data.province;
    cityId.value = param.data.city;
    memberAddress.value = param.data.address;
    provinceName.value = param.data.provinceName;
    cityName.value = param.data.cityName;

    // verified
    memberVerified.value = param.data.verified;

    // verified request
    memberVerifiedRequest.value = param.data.verifiedRequest;
  }

  // get personal info data from API
  dynamic getMemberAccountInfo() async {
    // set local rx variable loading to true
    isLoading.value = true;
    // then request
    final Map<String, dynamic> memberAccountInfo =
        await _authorizeRepository.getMemberAccountInfo();
    // if code 200
    if (memberAccountInfo['code'] == StatusCode.codeOk) {
      // set loading to false
      isLoading.value = false;
      // save data personal info
      final MemberAccountInfoModel data = MemberAccountInfoModel.fromJson(
        memberAccountInfo,
      );
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // assign values
      _assignValue(data);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        memberAccountInfo,
      );
      if (isRecall['status'] == true) {
        await getMemberAccountInfo();
      } else {
        // set loading to false
        isLoading.value = false;
        // error handler
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(
          memberAccountInfo,
        );
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _postVerifyUser(File param) async {
    final Map<String, dynamic> result =
        await _verifyRepository.postVerifyRequest(param);
    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      final VerifyModel data = VerifyModel.fromJson(result);

      isLoadingForVerify.value = false;
      // update personal info
      await getMemberAccountInfo();
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // show snackbar
      _messageBar('Verification is on progress', 'Verify Updated', true);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _postVerifyUser(param);
      } else {
        isLoadingForVerify.value = false;
        // error handler
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic imgFromCameraIdCard() async {
    final dynamic result = await Get.toNamed(RouteNames.idCardRoute);

    if (result != null) {
      isLoading.value = true;
      isLoadingForVerify.value = true;
      await _postVerifyUser(result['image'] as File);
    }
  }

  // open image from gallery detail
  void openImageProfile(BuildContext context) {
    Navigator.of(context).push(
      TransparentRoute(builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Obx(() {
            if (isImageFromFile.isTrue) {
              return PhotoView(
                imageProvider: FileImage(cameraImageProfile!),
                backgroundDecoration: BoxDecoration(
                  color: AppColors.darkBackground.withOpacity(0.5),
                ),
                heroAttributes: const PhotoViewHeroAttributes(tag: 0),
              );
            } else {
              return PhotoView(
                imageProvider: NetworkImage(profilePicture.value),
                backgroundDecoration: BoxDecoration(
                  color: AppColors.darkBackground.withOpacity(0.5),
                ),
                heroAttributes: const PhotoViewHeroAttributes(tag: 0),
              );
            }
          }),
        );
      }),
    );
  }

  dynamic onPressNavigateToEditProfile(BuildContext context) async {
    final dynamic result = await Get.toNamed(RouteNames.editProfileRoute,
        arguments: <String, dynamic>{
          'first_name': firstName.value,
          'last_name': lastName.value,
          'phone': memberPhoneNumber.value,
          'email': memberEmailAddress.value,
          'gender': memberGender.value,
          'dob': memberDob.value,
          'city_id': cityId.value,
          'province_id': provinceId.value,
          'address': memberAddress.value,
          'profile_pic': isImageFromFile.isTrue
              ? cameraImageProfile
              : profilePicture.value,
          'is_image_from_file': isImageFromFile.value,
        });

    if (result != null) {
      firstName.value = '${result['first_name']}';
      lastName.value = '${result['last_name']}';
      memberPhoneNumber.value = '${result['phone']}';
      memberEmailAddress.value = '${result['email']}';
      memberGender.value = '${result['gender']}';
      memberDob.value = '${result['dob']}';
      cityId.value = result['city_id'] as int?;
      provinceId.value = result['province_id'] as int?;
      memberAddress.value = '${result['address']}';
      isImageFromFile.value = result['is_image_from_file'] as bool;
      if (isImageFromFile.isTrue) {
        cameraImageProfile = result['profile_pic'] as File?;
      } else {
        profilePicture.value = '${result['profile_pic']}';
      }

      await getMemberAccountInfo();

      // show snackbar
      _messageBar('Profile info successfully updated', 'Profile Updated', true);
    }
  }

  dynamic onPressNavigateDeleteMyAccount() {
    Get.toNamed(RouteNames.deleteMyAccountPage);
  }
}
