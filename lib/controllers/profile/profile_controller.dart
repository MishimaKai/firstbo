import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../helpers/date_helper.dart';
import '../../helpers/device_info.dart';
import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/member_account_info/member_account_info_model.dart';
import '../../models/data/member_profile/member_profile_model.dart';
import '../../models/data/membership_rank/membership_rank_model.dart';
import '../../models/data/reasons/reason_model.dart';
import '../../repositories/authorizeRepository/authorize_repository.dart';
import '../../repositories/authorizeRepository/authorize_repository_interface.dart';
import '../../repositories/verifyRepository/verify_repository.dart';
import '../../repositories/verifyRepository/verify_repository_interface.dart';
import '../../routes/route_names.dart';
import '../../utils/status_code.dart';
import '../../views/profile/widgets/dialog_change_device.dart';
import '../../views/profile/widgets/dialog_logout.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class ProfileController extends GetxController {
  // contructor
  ProfileController({required this.pageContext});
  // param
  final BuildContext pageContext;

  // authorize controller
  final IAuthorizeRepository _authorizeRepository =
      AuthorizeRepository(); // only for request member detail
  // verify repository
  final IVerifyRepository _verifyRepository = VerifyRepository();

  // helpers
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  Rx<String?> versionApp = Rx<String>('');

  // my membership profile
  Rx<String> currentMembership = Rx<String>('');
  Rx<String?> nextMembership = Rx<String?>(null);
  RxInt isReachedTransaction = RxInt(0);
  RxInt currentTransaction = RxInt(0);
  RxInt minTransaction = RxInt(0);
  // my membership -- spending power
  RxInt currentSpendingPower = RxInt(0);
  RxInt minExistingMembershipSpendingPower = RxInt(0);
  Rx<int?> minNextMembershipSpendingPower = Rx<int?>(null);
  Rx<int?> reqNextMembershipSpendingPower = Rx<int?>(null);
  // spending power -- percentage
  RxDouble spendingPowerPercentageMin = RxDouble(0);
  RxDouble spendingPowerPercentageCurrent = RxDouble(0);
  RxDouble spendingPowerPercentageMax = RxDouble(0);
  // spending power -- stay warning box
  RxInt spendingPowerStayWarningShow = RxInt(0);
  Rx<int?> spendingPowerStayWarningSpendSp = Rx<int?>(null);
  // my membership total spending
  RxInt currentTotalSpending = RxInt(0);
  RxInt minExistingMembershipTotalSpending = RxInt(0);
  Rx<int?> minNextLevelMembershipTotalSpending = Rx<int?>(null);
  Rx<int?> reqNextLevelMembershipTotalSpending = Rx<int?>(null);
  Rx<String> displayCurrentTotalSpending = Rx<String>('');
  Rx<String?> displayReqNextLevelMembershipTotalSpending = Rx<String?>(null);
  // total spending -- percentage
  RxDouble totalspendingPercentageMin = RxDouble(0);
  RxDouble totalspendingPercentageCurrent = RxDouble(0);
  RxDouble totalspendingPercentageMax = RxDouble(0);

  // membership reset
  Rx<String> membershipReset = Rx<String>('');

  // membership rank
  RxList<MembershipRankItemModel?> membershipRank =
      RxList<MembershipRankItemModel?>(<MembershipRankItemModel?>[]);

  // membership name and benefit
  Rx<String> membershipName = Rx<String>('');
  Rx<String> membershipBenefit = Rx<String>('');

  // status
  RxBool isMembershipEmpty = RxBool(false);

  // personal info
  Rx<String> memberPhoneNumber = Rx<String>('');
  Rx<String> firstName = Rx<String>('');
  Rx<String> lastName = Rx<String>('');
  RxInt memberVerified = RxInt(0);
  RxInt memberVerifiedRequest = RxInt(0);

  // company info
  String instagramAccount = '';
  String whatsappAccount = '';

  // reset device request
  RxInt resetDeviceRequest = RxInt(0);

  // loading request
  RxBool isLoading = RxBool(false);
  RxBool isLoadingProfile = RxBool(false);
  RxBool isLoadingPersonalInfo = RxBool(false);
  RxBool isLoadingRemoveDevice = RxBool(false);

  RxBool isPullDownRequest = RxBool(false);

  RxBool isDualScreen = RxBool(false);

  // change devices
  RxString attention = RxString('');
  RxList<ReasonDataSelectModel> reasonList =
      RxList<ReasonDataSelectModel>(<ReasonDataSelectModel>[]);

  Rx<int?> idSelection = Rx<int?>(null);
  Rx<String?> reasonSelection = Rx<String?>(null);
  RxBool isManualInput = RxBool(false);
  TextEditingController reasonInputController = TextEditingController();

  @override
  Future<void> onInit() async {
    _identifyDevice();
    super.onInit();
    _getApplicatinInfo();
    await _initRequest();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title, bool? status]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: status ?? false,
    ));
  }

  // get personal info data from API
  dynamic getMemberAccountInfo() async {
    // set local rx variable loading to true
    isLoadingPersonalInfo.value = true;
    // then request
    final Map<String, dynamic> memberAccountInfo =
        await _authorizeRepository.getMemberAccountInfo();
    // if code 200
    if (memberAccountInfo['code'] == StatusCode.codeOk) {
      // set loading to false
      isLoadingPersonalInfo.value = false;
      // save data personal info
      final MemberAccountInfoModel data = MemberAccountInfoModel.fromJson(
        memberAccountInfo,
      );
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // assign data to local rx variables
      _assignFromPersonalInfoData(data);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        memberAccountInfo,
      );
      if (isRecall['status'] == true) {
        await getMemberAccountInfo();
      } else {
        // set loading to false
        isLoadingPersonalInfo.value = false;
        // error handler
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(
          memberAccountInfo,
        );
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignFromPersonalInfoData(MemberAccountInfoModel param) {
    // validate version
    _updateHandler.handler(param.clientInfo, pageContext, isDualScreen.value);

    // assign profile datas
    memberPhoneNumber.value = param.data.phone;

    // first name and last name
    firstName.value = param.data.firstName;
    lastName.value = param.data.lastName;

    // verified
    memberVerified.value = param.data.verified;

    // verified request
    memberVerifiedRequest.value = param.data.verifiedRequest;
  }

  // get my membership profile data from API
  dynamic _getMemberProfile() async {
    // set local rx variable loading to true
    isLoadingProfile.value = true;
    // then request
    final Map<String, dynamic> memberProfile =
        await _authorizeRepository.getMembeProfile();
    // if code 200
    if (memberProfile['code'] == StatusCode.codeOk) {
      // set loading to false
      isLoadingProfile.value = false;
      // assign data to local rx variables
      _assignFromProfileData(memberProfile);
    } else {
      // check if token expired
      final Map<String, dynamic> isRecall = await _errorHandler.handler(
        memberProfile,
      );
      if (isRecall['status'] == true) {
        await _getMemberProfile();
      } else {
        // set loading to false
        isLoadingProfile.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(
          memberProfile,
        );
        // show snackbar
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignFromProfileData(Map<String, dynamic> param) {
    // save data profile
    final MemberProfileModel data = MemberProfileModel.fromJson(param);
    attention.value = data.data.resetDevice.attention;
    if (data.data.resetDevice.reasonDataSelect != null) {
      reasonList.value = data.data.resetDevice.reasonDataSelect!;
    } else {
      reasonList.clear();
    }

    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // other infos
    // membership rank
    membershipRank.value = data.data.membershipRank;
    // membership name
    membershipName.value = data.data.membershipName;
    // membership benefit
    for (final MembershipRankItemModel? element in membershipRank) {
      if (element?.isActive == 1) {
        membershipBenefit.value = element?.benefit ?? '';
      }
    }
    // instagram account
    instagramAccount = data.data.companyInfo.instagram;
    // whatsapp account
    whatsappAccount = data.data.companyInfo.phone;
    // reset device request
    resetDeviceRequest.value = data.data.resetDeviceRequest;

    if (data.data.myMembership == null) {
      isMembershipEmpty.value = true;
    } else {
      isMembershipEmpty.value = false;

      currentMembership.value = data.data.myMembership!.currentMembership;
      nextMembership.value = data.data.myMembership!.nextMembership;
      isReachedTransaction.value = data.data.myMembership!.isReachedTransaction;
      currentTransaction.value = data.data.myMembership!.currentTransaction;
      minTransaction.value = data.data.myMembership!.minTransaction;
      // my membership -- spending power
      currentSpendingPower.value =
          data.data.myMembership!.spendingPowser.current;
      minExistingMembershipSpendingPower.value =
          data.data.myMembership!.spendingPowser.minExistingMembership;

      if (data.data.myMembership!.spendingPowser.minNextMembership != null) {
        minNextMembershipSpendingPower.value =
            data.data.myMembership!.spendingPowser.minNextMembership;
      } else {
        minNextMembershipSpendingPower.value = null;
      }

      if (data.data.myMembership!.spendingPowser.reqNextLevelMembership !=
          null) {
        reqNextMembershipSpendingPower.value =
            data.data.myMembership!.spendingPowser.reqNextLevelMembership;
      } else {
        reqNextMembershipSpendingPower.value = null;
      }

      // spending power -- percentage
      spendingPowerPercentageMin.value =
          data.data.myMembership!.spendingPowser.percentageBar.min;
      spendingPowerPercentageCurrent.value =
          data.data.myMembership!.spendingPowser.percentageBar.current;
      spendingPowerPercentageMax.value =
          data.data.myMembership!.spendingPowser.percentageBar.max;

      // spending power -- stay warning box
      spendingPowerStayWarningShow.value =
          data.data.myMembership!.spendingPowser.stayWarningBox.show;
      spendingPowerStayWarningSpendSp.value =
          data.data.myMembership!.spendingPowser.stayWarningBox.spendSp;

      // my membership total spending
      currentTotalSpending.value =
          data.data.myMembership!.totalSpending.current;
      minExistingMembershipTotalSpending.value =
          data.data.myMembership!.totalSpending.minExistingMembership;

      if (data.data.myMembership!.totalSpending.minNextMembership != null) {
        minNextLevelMembershipTotalSpending.value =
            data.data.myMembership!.totalSpending.minNextMembership;
      } else {
        minNextLevelMembershipTotalSpending.value = null;
      }

      reqNextLevelMembershipTotalSpending.value =
          data.data.myMembership!.totalSpending.reqNextLevelMembership;
      displayReqNextLevelMembershipTotalSpending.value =
          data.data.myMembership!.totalSpending.displayReqNextLevelMembership;
      displayCurrentTotalSpending.value =
          data.data.myMembership!.totalSpending.displayCurrent;

      // total spending -- percentage
      totalspendingPercentageMin.value =
          data.data.myMembership!.totalSpending.percentageBar.min;
      totalspendingPercentageCurrent.value =
          data.data.myMembership!.totalSpending.percentageBar.current;
      totalspendingPercentageMax.value =
          data.data.myMembership!.totalSpending.percentageBar.max;

      // membership reset
      membershipReset.value = DateHelper.getEventOfDateDisplayer(
        data.data.myMembership!.spendingPowser.stayWarningBox.resetDate,
      );
    }
  }

  dynamic _initRequest() async {
    await getMemberAccountInfo();
    await _getMemberProfile();
  }

  // function for pull down refresh
  dynamic onRefreshData() async {
    if (isPullDownRequest.isTrue) {
      // on set loading to false;
      isLoading.value = true;
      // then request API
      await getMemberAccountInfo();
      await _getMemberProfile();

      if (isLoadingProfile.isFalse && isLoadingPersonalInfo.isFalse) {
        isLoading.value = false;
      }

      isPullDownRequest.value = false;
    } else {
      await _getMemberProfile();
    }
  }

  Future<void> _getApplicatinInfo() async {
    // package info
    final String? packageInfoVersion = await DeviceInfo().getVersionApp();
    versionApp.value = packageInfoVersion;
  }

  // submit remove device
  dynamic submitRemoveDevice(BuildContext dialogContext) async {
    isLoadingRemoveDevice.value = true;
    final Map<String, dynamic> result = await _verifyRepository.postResetDevice(
        idSelection.value!, reasonInputController.text);
    if (result['code'] == StatusCode.codeOk) {
      await onRefreshData();
      // ignore: use_build_context_synchronously
      Navigator.of(dialogContext).pop();
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(dialogContext).showSnackBar(
        montserratSnackbar(
          status: true,
          title: 'Success',
          message: result['message'].toString(),
        ),
      );

      // reset status
      isLoadingRemoveDevice.value = false;
      reasonSelection.value = null;
      idSelection.value = null;
      isManualInput.value = false;
      reasonInputController.clear();
    } else {
      // reset status
      isLoadingRemoveDevice.value = false;
      reasonSelection.value = null;
      idSelection.value = null;
      isManualInput.value = false;
      reasonInputController.clear();
      // ignore: use_build_context_synchronously
      Navigator.of(dialogContext).pop();
      final Map<String, dynamic> messageAndErrorCode =
          _errorHandler.messageErrorCodeHandler(result);
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(dialogContext).showSnackBar(
        montserratSnackbar(
          status: false,
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
        ),
      );
    }
  }

  // show popup remove device
  dynamic popUpRemoveDevice(BuildContext context) {
    reasonSelection.value = null;
    idSelection.value = null;
    isManualInput.value = false;
    reasonInputController.clear();

    return showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: Colors.transparent.withOpacity(0.1),
      builder: (BuildContext dialogContext) {
        return Obx(
          () => DialogChangeDevice(
            reasonSelection: reasonSelection.value,
            reasonInputController: reasonInputController,
            isManualInput: isManualInput.value,
            loading: isLoadingRemoveDevice.value,
            isDualScreen: isDualScreen.value,
            onPressedYesDialog: () async {
              submitRemoveDevice(dialogContext);
            },
            onPressedNoDialog: () {
              Navigator.of(context).pop();
            },
            onChanged: (int? value) {
              changeReason(value);
            },
            reasonList: reasonList,
          ),
        );
      },
    );
  }

  // show popup logout
  dynamic popUpLogout(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.transparent.withOpacity(0.1),
      builder: (BuildContext context) {
        return DialogLogout(
          onPressedInDialog: () => _errorHandler.userLogout(
            isDirectToLogin: false,
            isUsePostLogout: true,
          ),
          isDualScreen: isDualScreen.value,
        );
      },
    );
  }

  bool getStatusMyDevice() {
    if (resetDeviceRequest.value == 0) {
      return false;
    } else {
      return true;
    }
  }

  // navigate to personal info
  dynamic navigateToPersonalInfo() {
    Get.toNamed(RouteNames.personalInfoRoute);
  }

  // naviagate to terms (0) or privacy Policy (1)
  dynamic navigateToTerms(int param) {
    Get.toNamed(RouteNames.termsRoute, arguments: param);
  }

  // function for navigate to instagram apps
  dynamic visitInstagram() async {
    final String insta = 'https://www.instagram.com/$instagramAccount';
    // convert url string to Uri
    final Uri uriSouce = Uri.parse(insta);
    // then launch url
    final bool nativeAppLaunch = await launchUrl(
      uriSouce,
      mode: LaunchMode.externalNonBrowserApplication,
    );

    if (!nativeAppLaunch) {
      await launchUrl(uriSouce);
    }
  }

  // function for navigate to whatsapp apps
  dynamic visitWhastapp(BuildContext context) async {
    final String source =
        // 'https://api.whatsapp.com/send/?phone=$whatsappAccount&text&app_absent=0';
        'https://wa.me/$whatsappAccount';

    // convert url string to Uri
    final Uri uriSouce = Uri.parse(source);

    final bool nativeAppLaunch = await launchUrl(
      uriSouce,
      mode: LaunchMode.externalNonBrowserApplication,
    );

    if (!nativeAppLaunch) {
      // then launch url
      // await launchUrl(uriSouce);
      // show snackbar
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(montserratSnackbar(
        title: 'label_failed'.tr,
        message: 'WhatsApp is not installed',
        status: false,
      ));
    }
  }

  dynamic changeReason(int? id) {
    idSelection.value = id;
    for (final ReasonDataSelectModel element in reasonList) {
      if (idSelection.value == element.id) {
        reasonSelection.value = element.title;
        isManualInput.value = element.allowInputText == 1;
      }
    }
  }
}
