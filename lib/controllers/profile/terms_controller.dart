import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/terms/terms_model.dart';
import '../../repositories/termsRepository/terms_repository.dart';
import '../../repositories/termsRepository/terms_respository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class TermsController extends GetxController {
  // constructor
  TermsController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();
  // initiate helper
  final ITermsRepository _termsRepository = TermsRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // variable
  dynamic args = Get.arguments;
  RxBool isLoading = RxBool(false);
  RxBool isDualScreen = RxBool(false);

  String termsData = '';

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    if (args == 0) {
      _getTnC();
    } else {
      _getPrivacyPolicy();
    }
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _messageBar(String param, [String? title, bool? status]) {
    ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
      title: title ?? 'label_failed'.tr,
      message: param,
      status: status ?? false,
    ));
  }

  String getTitle() {
    if (args == 0) {
      return 'Terms & Condition';
    } else {
      return 'Privacy & Policy';
    }
  }

  dynamic _getTnC() async {
    isLoading.value = true;
    final Map<String, dynamic> result = await _termsRepository.getTnC();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoading.value = false;
      _assignTotermsData(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getTnC();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _getPrivacyPolicy() async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _termsRepository.getPrivacyPolicy();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // when success
      isLoading.value = false;
      _assignTotermsData(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getPrivacyPolicy();
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        _messageBar(messageAndErrorCode['errorMessage'].toString());
      }
    }
  }

  dynamic _assignTotermsData(Map<String, dynamic> param) {
    final TermsModel data = TermsModel.fromJson(param);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
    // check data type
    if (data.data is Map<String, dynamic>) {
      termsData = '${data.data['full_desc']}';
    } else {
      termsData = '';
    }
  }
}
