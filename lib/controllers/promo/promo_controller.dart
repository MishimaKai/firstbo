import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/package/package_list_model.dart';
import '../../models/data/package/package_model.dart';
import '../../repositories/promoRepository/promo_repository.dart';
import '../../repositories/promoRepository/promo_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class PromoController extends GetxController {
  // constructor
  PromoController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();

  // initiate helper
  final IPromoRepository _promoRepository = PromoRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  // page controller
  PagingController<int, PackageModel> promoListController =
      PagingController<int, PackageModel>(
    firstPageKey: 1,
    invisibleItemsThreshold: 1,
  );

  // variable
  RxBool isLoading = RxBool(false);
  RxBool isDualScreen = RxBool(false);
  List<PackageModel> promoList = <PackageModel>[];
  final dynamic _args = Get.arguments;

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    promoListController.addPageRequestListener((int pageKey) {
      getPromoList(_args as int, pageKey);
    });
  }

  @override
  void dispose() {
    super.dispose();
    promoListController.dispose();
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic onRefreshPage() {
    promoList.clear();
    promoListController.refresh();
  }

  dynamic getPromoList(int type, int pageKey) async {
    // isLoading.value = true;
    final Map<String, dynamic> result = await _promoRepository.getPromoList(
      type,
      pageKey,
    );

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // isLoading.value = false;
      _assignDataToList(result, pageKey);
    } else {
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await getPromoList(type, pageKey);
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        ));
      }
    }
  }

  dynamic _assignDataToList(Map<String, dynamic> result, int pageKey) {
    // when succrss
    final PackageListModel data = PackageListModel.fromJson(result);
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
    // assign data to list
    promoList = data.data ?? <PackageModel>[];
    final bool isLastPage = data.meta.links?.next == null;
    if (isLastPage) {
      promoListController.appendLastPage(promoList);
    } else {
      final int nextPage = pageKey + 1;
      promoListController.appendPage(promoList, nextPage);
    }
  }
}
