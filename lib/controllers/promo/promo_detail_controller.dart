import 'package:dual_screen/dual_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../helpers/date_helper.dart';
import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../middleware/update_handler.dart';
import '../../models/data/outlet_info/outlet_info_model.dart';
import '../../models/data/package/package_detail_model.dart';
import '../../repositories/promoRepository/promo_repository.dart';
import '../../repositories/promoRepository/promo_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../../views/widgets/layouts/montserrat_snackbar.dart';

class PromoDetailController extends GetxController {
  // constructor
  PromoDetailController({required this.pageContext});
  // param
  final BuildContext pageContext;

  final FrameController _frameController = Get.find();

  // intiate helper
  final IPromoRepository _promoRepository = PromoRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  final UpdateHandler _updateHandler = UpdateHandler();

  PackageDetailModel? dataPromo;

  final dynamic _receiveArgs = Get.arguments;
  RxBool isLoading = RxBool(false);
  RxBool isDualScreen = RxBool(false);

  @override
  void onInit() {
    _identifyDevice();
    super.onInit();
    _frameController.isOutsideFrame.value = true;
    _getPromoDetail(_receiveArgs as int);
  }

  dynamic _identifyDevice() {
    DualScreenInfo.hasHingeAngleSensor.then((bool hasHingeSensor) {
      isDualScreen.value = hasHingeSensor;
    });
  }

  dynamic _getPromoDetail(int id) async {
    isLoading.value = true;
    final Map<String, dynamic> result =
        await _promoRepository.getPromoDetail(id);

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      isLoading.value = false;
      _assignDataToDetail(result);
    } else {
      isLoading.value = true;
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        await _getPromoDetail(id);
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        // ignore: use_build_context_synchronously
        ScaffoldMessenger.of(pageContext).showSnackBar(montserratSnackbar(
          title: 'label_failed'.tr,
          message: messageAndErrorCode['errorMessage'].toString(),
          status: false,
        ));
      }
    }
  }

  dynamic _assignDataToDetail(Map<String, dynamic> result) {
    // when success
    final PackageDetailModel data = PackageDetailModel.fromJson(result);
    dataPromo = data;
    // validate topics
    NotificationService.validateTopic(data.memberConfig.fcMtopic);
    // validate version
    _updateHandler.handler(data.clientInfo, pageContext, isDualScreen.value);
  }

  String getPromoImage() {
    if (dataPromo != null) {
      return dataPromo?.data.image?.original ?? '';
    } else {
      return '';
    }
  }

  String getPromoName() {
    if (dataPromo != null) {
      return dataPromo?.data.title ?? '';
    } else {
      return '';
    }
  }

  int getPromoPrice() {
    if (dataPromo != null) {
      // return StringHelper.convertToRupiah(dataPromo?.data.price ?? 0);
      return dataPromo?.data.price ?? 0;
    } else {
      return 0;
    }
  }

  String getPromoDetail() {
    // log('desc for bottle ${dataPromo?.data.fullDesc}');
    if (dataPromo != null) {
      return dataPromo?.data.fullDesc ?? '';
    } else {
      return '';
    }
  }

  List<OutletInfoModel> getListOutlet() {
    if (dataPromo != null) {
      // ignore: use_if_null_to_convert_nulls_to_bools
      if (dataPromo?.data.outletInfo?.isNotEmpty == true) {
        final List<OutletInfoModel> outletInfo =
            dataPromo?.data.outletInfo ?? <OutletInfoModel>[];
        return outletInfo;
      } else {
        return <OutletInfoModel>[];
      }
    } else {
      return <OutletInfoModel>[];
    }
  }

  // get start date
  String getStartDateDetail() {
    if (dataPromo != null) {
      return dataPromo?.data.startDate ?? '';
    } else {
      return '';
    }
  }

  // get date
  String getEndDateDetail() {
    if (dataPromo != null) {
      return dataPromo?.data.endDate ?? '';
    } else {
      return '';
    }
  }

  String getWrappedDate() {
    String result = '';
    // start
    final String startDateDisplay = DateHelper.getDateDisplayer(
      getStartDateDetail(),
    );
    final String startMonthDisplay = DateHelper.getMonthDisplayer(
      getStartDateDetail(),
      false,
    );
    final String startYearDisplay = DateHelper.getYearDisplayer(
      getStartDateDetail(),
    );

    // end
    final String endDateDisplay = DateHelper.getDateDisplayer(
      getEndDateDetail(),
    );
    final String endMonthDisplay = DateHelper.getMonthDisplayer(
      getEndDateDetail(),
      false,
    );
    final String endYearDisplay = DateHelper.getYearDisplayer(
      getEndDateDetail(),
    );

    result =
        '$startDateDisplay $startMonthDisplay $startYearDisplay - $endDateDisplay $endMonthDisplay $endYearDisplay';

    return result;
  }
}
