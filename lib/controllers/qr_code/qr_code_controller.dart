import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../helpers/notification_service.dart';
import '../../middleware/error_handler.dart';
import '../../models/data/verify/verify_model.dart';
import '../../repositories/paymentRepository/payment_repository.dart';
import '../../repositories/paymentRepository/payment_repository_interface.dart';
import '../../utils/status_code.dart';
import '../../views/widgets/frame/frame_widget_controller.dart';
import '../home/member_account_info_controller.dart';

class QRCodeController extends GetxController {
  // constructor
  QRCodeController({required this.pageContext});
  // param
  final BuildContext pageContext;
  // instance helper
  final IPaymentRepository _paymentRepository = PaymentRepository();
  final ErrorHandler _errorHandler = ErrorHandler();
  // instance controller
  final FrameController _frameController = Get.find();
  final MemberAccountInfoController _infoController = Get.find();

  Barcode? result;
  QRViewController? qrViewController;
  RxBool isLoading = RxBool(false);

  @override
  void onInit() {
    super.onInit();
    _frameController.isOutsideFrame.value = true;
  }

  @override
  void dispose() {
    qrViewController?.dispose();
    super.dispose();
  }

  void onQRViewCreated(QRViewController controller, BuildContext context) {
    qrViewController = controller;
    doResumeCamera();
    controller.scannedDataStream.listen((Barcode scanData) async {
      // log('result = ${scanData.code}');
      result = scanData;
      if (scanData.code != null) {
        doStopCamera();
        isLoading.value = true;
        await _requestOpenBill(scanData.code!);
      }
    });
  }

  // flash
  dynamic doToggleFlash() async {
    await qrViewController?.toggleFlash();
  }

  // stop camera
  dynamic doStopCamera() async {
    await qrViewController?.stopCamera();
  }

  // resume camera
  dynamic doResumeCamera() async {
    await qrViewController?.resumeCamera();
  }

  // pause camera
  dynamic doPauseCamera() async {
    await qrViewController?.pauseCamera();
  }

  // request open bill
  dynamic _requestOpenBill(String qrCode) async {
    final Map<String, dynamic> result =
        await _paymentRepository.openBill(qrCode);

    // code 200
    if (result['code'] == StatusCode.codeOk) {
      final VerifyModel data = VerifyModel.fromJson(result);
      // validate topics
      NotificationService.validateTopic(data.memberConfig.fcMtopic);
      // then back to frame widget
      isLoading.value = false;
      _infoController.getRequestMemberAccountInfo();
      Get.back(result: <String, dynamic>{
        'title': 'Scan Success',
        'success': true,
        'message': result['message'],
      });
    } else {
      // log('$_result');
      final Map<String, dynamic> isRecall = await _errorHandler.handler(result);
      if (isRecall['status'] == true) {
        isLoading.value = true;
        await _requestOpenBill(qrCode);
      } else {
        isLoading.value = false;
        final Map<String, dynamic> messageAndErrorCode =
            _errorHandler.messageErrorCodeHandler(result);
        Get.back(result: <String, dynamic>{
          'title': 'label_failed'.tr,
          'success': false,
          'message': messageAndErrorCode['errorMessage'],
        });
      }
    }
  }
}
