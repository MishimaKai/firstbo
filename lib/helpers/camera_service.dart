import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart';

class CameraService {
  CameraService._();
  // camera controller
  static CameraController? cameraController;
  // list avaiable camera
  static List<CameraDescription> cameras = <CameraDescription>[];
  // set boolean flash
  static bool isFlashOn = false;
  // capture id card
  static File? captureId;

  // check avaiable camera
  static dynamic onCheckCamera() async {
    try {
      cameras = await availableCameras();
    } catch (e) {
      log('$e');
    }
  }

  // initiate
  static dynamic onInitCamera() {
    if (cameras.isNotEmpty) {
      cameraController = CameraController(cameras[0], ResolutionPreset.max);
    }
  }

  // dispose
  static dynamic onDisposeCamera() {
    isFlashOn = false;
    captureId = null;
    cameraController?.dispose();
  }

  // life cycle handler
  static dynamic lifeCycleHandler(AppLifecycleState state) {
    final CameraController? controller = cameraController;

    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      controller.dispose();
    } else if (state == AppLifecycleState.resumed) {
      // onNewCameraSelected(_controller.description);
    }
  }

  // launch camera
  static Widget onLaunchCamera() {
    if (cameraController?.value.isInitialized == false) {
      return Container();
    } else {
      return CameraPreview(cameraController!);
    }
  }

  // pause camera
  static dynamic onHoldPreview(bool isPictureTaken) {
    if (isPictureTaken) {
      cameraController?.pausePreview();
    } else {
      cameraController?.resumePreview();
    }
  }

  // toggle flash
  static dynamic toggleFlashCamera() {
    if (isFlashOn) {
      isFlashOn = false;
      cameraController?.setFlashMode(FlashMode.off);
    } else {
      isFlashOn = true;
      cameraController?.setFlashMode(FlashMode.torch);
    }
  }

  // take picture
  static dynamic takePictureFromCamera() async {
    // on check flash camera
    if (!isFlashOn) {
      cameraController?.setFlashMode(FlashMode.off);
    }
    // then take a picture
    final XFile? takeId = await cameraController?.takePicture();
    if (takeId != null) {
      // compres image quality and size
      final Uint8List? convertImage =
          await FlutterImageCompress.compressWithFile(
        takeId.path,
        minHeight: 512,
        minWidth: 512,
      );
      // call temporary directory
      final Directory tempDir = await getTemporaryDirectory();
      // get file name
      final String fileName = File(takeId.path).uri.pathSegments.last;
      // set path
      final File willSave = await File('${tempDir.path}/$fileName').create();
      if (convertImage != null) {
        // convert data type Uint8List to List<Int>
        final List<int> convertedUint8 = List<int>.from(convertImage);
        // write on directory
        willSave.writeAsBytesSync(convertedUint8);
        // assign variable
        captureId = willSave;
      }
    }
  }
}
