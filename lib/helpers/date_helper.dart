import 'package:intl/intl.dart';

class DateHelper {
  DateHelper._();
  static String getDayDisplayer(String date) {
    if (date.isNotEmpty) {
      final String dayCreated = DateFormat('EEEE').format(DateTime.parse(date));
      // return _dayCreated.substring(0, 3);
      return dayCreated;
    } else {
      return '';
    }
  }

  static String getDateDisplayer(String date) {
    if (date.isNotEmpty) {
      final String dateCreated = DateFormat('dd').format(DateTime.parse(date));
      return dateCreated;
    } else {
      return '';
    }
  }

  static String getMonthDisplayer(String date, [bool isSubString = true]) {
    if (date.isNotEmpty) {
      final String monthCreated =
          DateFormat('MMMM').format(DateTime.parse(date));
      if (isSubString) {
        return monthCreated.substring(0, 3);
      } else {
        return monthCreated;
      }
    } else {
      return '';
    }
  }

  static String getMonthDisplayerSimplified(String date) {
    if (date.isNotEmpty) {
      final String monthCreated = DateFormat('MM').format(DateTime.parse(date));
      return monthCreated;
    } else {
      return '';
    }
  }

  static String getYearDisplayer(String date) {
    if (date.isNotEmpty) {
      final String yearCreated = DateFormat('y').format(DateTime.parse(date));
      return yearCreated;
    } else {
      return '';
    }
  }

  static String getYearDisplayerSimplified(String date) {
    if (date.isNotEmpty) {
      final String yearCreated =
          DateFormat('yyyy').format(DateTime.parse(date));
      return yearCreated;
    } else {
      return '';
    }
  }

  static String getBirthOfDateDisplayer(String date) {
    if (date.isNotEmpty) {
      final String birthOfDate =
          DateFormat('dd MMM y').format(DateTime.parse(date));
      return birthOfDate;
    } else {
      return '';
    }
  }

  static String getEventOfDateDisplayer(String date) {
    if (date.isNotEmpty) {
      final String birthOfDate =
          DateFormat('dd MMMM y').format(DateTime.parse(date));
      return birthOfDate;
    } else {
      return '';
    }
  }

  static String getBirthOfDateSender(String date) {
    if (date.isNotEmpty) {
      final String birthOfDate =
          DateFormat('yyyy-MM-dd').format(DateTime.parse(date));
      return birthOfDate;
    } else {
      return '';
    }
  }

  // 12 hours format AM and PM
  static String getTimeDisplayer(String date) {
    if (date.isNotEmpty) {
      final String time = DateFormat('HH:mm a').format(DateTime.parse(date));
      return time;
    } else {
      return '';
    }
  }

  // 24 hours format
  static String getTime24HoursDisplayer(String date) {
    if (date.isNotEmpty) {
      final String time = DateFormat('HH:mm').format(DateTime.parse(date));
      return time;
    } else {
      return '';
    }
  }
}
