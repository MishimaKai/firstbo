import 'package:device_info_plus/device_info_plus.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:platform_device_id/platform_device_id.dart';

class DeviceInfo {
  // private method
  Future<AndroidDeviceInfo> _getDeviceInfoAndroid() async {
    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

    return androidInfo;
  }

  Future<IosDeviceInfo> _getDeviceInfoIos() async {
    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;

    return iosInfo;
  }

  // get id device info
  Future<String?> getDeviceIdAndroid() async {
//    final AndroidDeviceInfo androidInfo = await _getDeviceInfoAndroid();
//    return androidInfo.androidId;
    final String? deviceId = await PlatformDeviceId.getDeviceId;
    return deviceId;
  }

  Future<String?> getDeviceIdIos() async {
    final IosDeviceInfo iosInfo = await _getDeviceInfoIos();
    return iosInfo.identifierForVendor;
  }

  // get sdk version
  Future<int?> getDeviceSdkAndroid() async {
    final AndroidDeviceInfo androidInfo = await _getDeviceInfoAndroid();
    return androidInfo.version.sdkInt;
  }

  Future<String?> getVersionApp() async {
    final PackageInfo appInfo = await PackageInfo.fromPlatform();
    return appInfo.version;
  }

  Future<String?> getAppName() async {
    final PackageInfo appInfo = await PackageInfo.fromPlatform();
    return appInfo.appName;
  }

  // get OS Version
  Future<String?> getDeviceOSIos() async {
    final IosDeviceInfo iosInfo = await _getDeviceInfoIos();
    return iosInfo.systemName;
  }

  Future<String?> getDeviceOSAndroid() async {
    final AndroidDeviceInfo androidInfo = await _getDeviceInfoAndroid();
    return androidInfo.version.release;
  }

  // get package name
  Future<String?> getPackageName() async {
    final PackageInfo appInfo = await PackageInfo.fromPlatform();
    return appInfo.packageName;
  }
}
