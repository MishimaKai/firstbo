import 'dart:developer';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../routes/route_names.dart';
import '../views/widgets/frame/frame_widget_controller.dart';
import 'notification_service.dart';

class FirebaseConfig {
  FirebaseConfig._();
  // find frame controller
  static final FrameController controller = Get.find();
  // platform option
  static FirebaseOptions get _platformOptions {
    if (Platform.isIOS) {
      // demo version
      return const FirebaseOptions(
        apiKey: 'AIzaSyANAFOKX1DFVtvWP6EQjMYJ3g0LoFcaWrk',
        appId: '1:857024676001:ios:41d2ef88550d36803eb0e0',
        messagingSenderId: '857024676001',
        projectId: 'black-owl-demo', // demo version
        iosBundleId: 'blackowl.id',
      );

      // live version
      // return const FirebaseOptions(
      //   apiKey: 'AIzaSyAcBjgJKN0zkJZtNh6HqZmksQ-R6mb_iJk',
      //   appId: '1:764506798998:ios:89cb3a59cb1f02a2e1bbb7',
      //   messagingSenderId: '764506798998',
      //   projectId: 'blackowl-membership',
      //   iosBundleId: 'blackowl.id',
      // );
    } else {
      // demo version
      return const FirebaseOptions(
        apiKey: 'AIzaSyCVEVdMLy5rn4Z2UJhFfIBJQvVymym1cQU',
        appId: '1:857024676001:android:e40fe05bc1c40a9f3eb0e0',
        messagingSenderId: '857024676001',
        projectId: 'black-owl-demo',
      );

      // live version
      // return const FirebaseOptions(
      //   apiKey: 'AIzaSyDZqmWGxYSTPkAfuqyODp-g7XJi9E2d5wA', // live version
      //   appId: '1:764506798998:android:d3dedf0850ba1092e1bbb7', // live version
      //   messagingSenderId: '764506798998', // live version
      //   projectId: 'blackowl-membership', // live version
      // );
    }
  }

  static Future<void> initFireBase() async {
    try {
      await Firebase.initializeApp(options: FirebaseConfig._platformOptions);
    } on FirebaseException catch (e) {
      if (e.code == 'duplicate-app') {
        // log('error firebase = $e');
        Firebase.initializeApp();
      } else {
        // log('error firebase = $e');
      }
    } catch (e) {
      rethrow;
    }
    // firebase message
    FirebaseMessaging? messaging;
    messaging = FirebaseMessaging.instance;
    messaging
        .getToken()
        .then((String? value) => log('TOKEN FCM from init = $value'));
    // listen when received
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      // log('message recieved');
      // log(event.notification!.body!);
      // basic notification
      NotificationService.showNotification(
        event.hashCode,
        event.notification!.title!,
        event.notification!.body!,
        event.data.isNotEmpty ? '${event.data['screen']}' : '',
      );
      _configureSelectNotificationSubject();
    });
    // listen when open app
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('Message clicked! from init ${message.data}');
      _navigateFromPayload(message.data['screen'].toString());
    });
  }

  static Future<void> firebaseMessagingBackgroundHandler(
      RemoteMessage message) async {
    await Firebase.initializeApp(options: FirebaseConfig._platformOptions);
    // log('Handling a background message = ${message.messageId}');
    // firebase message
    FirebaseMessaging? messaging;
    // messaging?.setAutoInitEnabled(false);
    messaging = FirebaseMessaging.instance;
    messaging
        .getToken()
        .then((String? value) => log('TOKEN FCM from background = $value'));
    // listen when received
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      // log('message recieved');
      // log(event.notification!.body!);
      NotificationService.showNotification(
        event.hashCode,
        event.notification!.title!,
        event.notification!.body!,
        event.data.isNotEmpty ? event.data['screen'].toString() : '',
      );
      _configureSelectNotificationSubject();
    });
    // listen when open app
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('Message clicked! from background ${message.data}');
      _navigateFromPayload(message.data['screen'].toString());
    });
  }

  static dynamic _navigateFromPayload(String param) {
    if (param == '/home') {
      // give logic if user outside frame route
      if (controller.isOutsideFrame.isTrue) {
        Get.offAllNamed(RouteNames.frameRoute);
        controller.isOutsideFrame.value = false;
      }
      // than execute on tap navigation from bottom nav bar widget
      return controller.onTapNavigation(0);
    } else if (param == '/gallery') {
      // give logic if user outside frame route
      if (controller.isOutsideFrame.isTrue) {
        Get.offAllNamed(RouteNames.frameRoute);
        controller.isOutsideFrame.value = false;
      }
      // than execute on tap navigation from bottom nav bar widget
      return controller.onTapNavigation(1);
    } else if (param == '/redeem') {
      // give logic if user outside frame route
      if (controller.isOutsideFrame.isTrue) {
        Get.offAllNamed(RouteNames.frameRoute);
        controller.isOutsideFrame.value = false;
      }
      // than execute on tap navigation from bottom nav bar widget
      return controller.onTapNavigation(3);
    } else if (param == '/profile') {
      // give logic if user outside frame route
      if (controller.isOutsideFrame.isTrue) {
        Get.offAllNamed(RouteNames.frameRoute);
        controller.isOutsideFrame.value = false;
        // than execute on tap navigation from bottom nav bar widget
        return controller.onTapNavigation(4);
      } else {
        if (controller.defaultIndex.value != 4) {
          return controller.onTapNavigation(4);
        }
      }
    } else {
      controller.isOutsideFrame.value = true;
      Get.toNamed(param);
    }
  }

  static dynamic _configureSelectNotificationSubject() {
    NotificationService.selectNotificationSubject.stream
        .listen((String? event) {
      log('TEST PAYLOAD = $event');
      log('TEST PAYLOAD GENERIC = ${NotificationService.selectedNotificationPayload}');
      if (event != null) {
        if (event.isEmpty) {
          controller.isOutsideFrame.value = false;
          Get.offAllNamed(RouteNames.frameRoute);
        } else {
          _navigateFromPayload(event);
        }
      } else {
        controller.isOutsideFrame.value = false;
        Get.offAllNamed(RouteNames.frameRoute);
      }
    });
  }
}
