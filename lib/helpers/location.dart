import 'package:geolocator/geolocator.dart';

import 'permission_handler.dart';

class Location {
  Future<List<double>> getCurrentLocation() async {
    final bool isGranted = await PermissionHandler.locationPermission();

    // log("CHECK IS GRANTED GPS = $isGranted");

    if (isGranted) {
      Position? position = await Geolocator.getLastKnownPosition();
      position = await Geolocator.getCurrentPosition();

      return <double>[position.longitude, position.latitude];
    } else {
      return <double>[0.0, 0.0];
    }
  }
}
