import 'dart:developer';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:get/get.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

import '../models/UI/notifications/received_notification_model.dart';
import 'shared_pref.dart';

class NotificationService {
  NotificationService._();
  // init helper
  static final SharedPref _sharedPref = SharedPref();
  static final FirebaseMessaging _instance = FirebaseMessaging.instance;
  // init plugin
  static final FlutterLocalNotificationsPlugin
      _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  // android settings
  static const AndroidInitializationSettings _initializationSettingsAndroid =
      AndroidInitializationSettings('blackowl_transparent'); // transparent
  // AndroidInitializationSettings('@mipmap/ic_launcher'); // app icon

  // rx dart stream
  static final Rx<ReceivedNotification> didReceiveLocalNotificationSubject =
      Rx<ReceivedNotification>(ReceivedNotification(id: 0));

  static final Rx<String?> selectNotificationSubject = Rx<String?>('');

  static String? selectedNotificationPayload;
  static NotificationAppLaunchDetails? _notificationAppLaunchDetails;

  static const String _channelId = 'Black Owl Notification';
  static const String _channelGroupId = 'blackowl.id';
  static const String _channelName = 'Black Owl';
  static const String _channelDecription = "Black Owl's Notification";

  // android channel
  static const AndroidNotificationChannelGroup _channelGroup =
      AndroidNotificationChannelGroup(
    _channelGroupId, // id
    _channelName, // name
    description: _channelDecription,
  );

  static const AndroidNotificationChannel _channel = AndroidNotificationChannel(
    _channelId, // id
    _channelName, // name
    groupId: _channelGroupId,
    description: _channelDecription,
    importance: Importance.high,
  );

  // ios settings
  // this settings for iOS 10 or older
  static final IOSInitializationSettings _iosInitializationSettings =
      IOSInitializationSettings(
    requestAlertPermission: false,
    requestBadgePermission: false,
    requestSoundPermission: false,
    onDidReceiveLocalNotification: (
      int id,
      String? title,
      String? body,
      String? payload,
    ) async {
      didReceiveLocalNotificationSubject.value = ReceivedNotification(
        id: id,
        title: title,
        body: body,
        payload: payload,
      );
    },
  );

  // configurat inf for version 10 older iOS
  static void configureDidReceiveLocalNotification({
    required BuildContext context,
    required Function(ReceivedNotification event) onPressed,
  }) {
    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification event) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: event.title != null ? Text(event.title!) : null,
          content: event.body != null ? Text(event.body!) : null,
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                onPressed(event);
              },
              child: const Text('OK'),
            ),
          ],
        ),
      );
    });
  }

  // android settings
  static final AndroidNotificationDetails _androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
    _channel.id,
    _channel.name,
    channelDescription: _channel.description,
    importance: _channel.importance,
    priority: Priority.high,
    ticker: 'ticker',
  );

  static const IOSNotificationDetails _iOSPlatformChannelSpecifics =
      IOSNotificationDetails(threadIdentifier: _channelGroupId);

  // init settings
  static final InitializationSettings _initializationSettings =
      InitializationSettings(
    android: _initializationSettingsAndroid,
    iOS: _iosInitializationSettings,
  );

  // init notifications detail
  static dynamic initDetails() async {
    _notificationAppLaunchDetails = await _flutterLocalNotificationsPlugin
        .getNotificationAppLaunchDetails();

    // log('TEST LAUCH DETAIL = ${_notificationAppLaunchDetails?.didNotificationLaunchApp}');
    if (_notificationAppLaunchDetails?.didNotificationLaunchApp ?? false) {
      selectedNotificationPayload = _notificationAppLaunchDetails!.payload;
    }
  }

  // init payload
  static dynamic initPayload() async {
    await _flutterLocalNotificationsPlugin.initialize(
      _initializationSettings,
      onSelectNotification: (String? payload) async {
        log('notification payload : $payload');
        if (payload != null) {
          selectedNotificationPayload = payload;
          selectNotificationSubject.value = payload;
          selectNotificationSubject.refresh();
        }
      },
    );
  }

  // init configure time zone
  static Future<void> configureTimedZone() async {
    tz.initializeTimeZones();
    final String timeZoneName = await FlutterNativeTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZoneName));
  }

  // request permission
  static Future<void> requestPermission() async {
    if (Platform.isIOS) {
      await _flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              IOSFlutterLocalNotificationsPlugin>()
          ?.requestPermissions(alert: true, badge: true, sound: true);
    } else {
      // group channel
      await _flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannelGroup(_channelGroup);

      // single notif channel
      await _flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(_channel);
    }
  }

  // basic notification
  static Future<void> showNotification(
    int hasCode,
    String title,
    String body,
    String payload,
  ) async {
    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: _androidPlatformChannelSpecifics,
      iOS: _iOSPlatformChannelSpecifics,
    );

    await _flutterLocalNotificationsPlugin.show(
      hasCode,
      title,
      body,
      platformChannelSpecifics,
      payload: payload,
    );
  }

  // basic notification for qr code
  static Future<void> showNotificationQrCode(String title, String body) async {
    final NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: _androidPlatformChannelSpecifics,
      iOS: _iOSPlatformChannelSpecifics,
    );

    await _flutterLocalNotificationsPlugin.show(
      0,
      title,
      body,
      platformChannelSpecifics,
      // payload: RouteNames.frameRoute,
    );
  }

  // validate subscribe to topic
  static dynamic validateTopic(List<String> paramList) async {
    // read topic list from shared preferences
    final List<String>? currentTopics = await _sharedPref.readFCMTopicList();
    // check if data is exist
    if (currentTopics != null) {
      currentTopics.sort(); // sort topic list from shared preferences
      paramList.sort(); // sort topic list from parameter
      // if current topic same length as from parameter
      if (currentTopics.length == paramList.length) {
        // set on temp variable
        final List<String> tempCurrentTopics = currentTopics;
        final List<String> tempParamList = paramList;

        // check for each item
        for (int index = 0; index < tempParamList.length; index++) {
          // if item doesn't match each others
          if (tempCurrentTopics.contains(tempParamList[index]) == false) {
            // then unsubscribe from current topic
            _instance.unsubscribeFromTopic(tempCurrentTopics[index]);
            // subscribe with the new one from parameter
            _instance.subscribeToTopic(tempParamList[index]);
            // remove old unused topic item
            tempCurrentTopics.removeWhere((String element) {
              return element == tempCurrentTopics[index];
            });
            // then add with the new one
            tempCurrentTopics.add(tempParamList[index]);
          }
        }
        // after manipulate temporary list do remove and write shared preferenfes
        // remove old topics
        await _sharedPref.removeFCMTopicList();
        // rewrite new topic list
        await _sharedPref.writeFCMTopicList(tempCurrentTopics);

        // if current topics length less than parameter and parameter is not empty
      } else if (currentTopics.length < paramList.length &&
          paramList.isNotEmpty) {
        // set on temp variable
        final List<String> tempCurrentTopics = currentTopics;
        final List<String> tempParamList = paramList;

        // set length how far different between topic list from data and parameter
        final int emptyLength = tempParamList.length - tempCurrentTopics.length;

        // set temp variable for will subscribe and unsubcribe
        final List<String> willUnsub = <String>[];
        final List<String> willSub = <String>[];

        // then add empty string for balance data list same as param list length
        for (int index = 0; index < emptyLength; index++) {
          tempCurrentTopics.add(' ');
        }

        // check for each item
        for (int index = 0; index < tempParamList.length; index++) {
          // if item doesn't match each others
          if (tempCurrentTopics.contains(tempParamList[index]) == false) {
            // add to will unsubscribe list except empty string
            if (tempCurrentTopics[index] != ' ') {
              willUnsub.add(tempCurrentTopics[index]);
            }
            // add will subscribe to list if item match before
            willSub.add(tempParamList[index]);
          }
        }

        // read will unsubscribe list
        for (final String element in willUnsub) {
          // unsubscribe for each item list
          _instance.unsubscribeFromTopic(element);
          // remove old unused topic item
          tempCurrentTopics.removeWhere((String item) => item == ' ');
          tempCurrentTopics.removeWhere((String item) => item == element);
        }

        for (final String element in willSub) {
          // subscribe to each item list
          _instance.subscribeToTopic(element);
          // then add with the new one
          tempCurrentTopics.add(element);
        }

        // after manipulate temporary list do remove and write shared preferenfes
        // remove old topics
        await _sharedPref.removeFCMTopicList();
        // rewrite new topic list
        await _sharedPref.writeFCMTopicList(tempCurrentTopics);

        // if current topics length high than parameter and parameter is not empty
      } else if (currentTopics.length > paramList.length &&
          paramList.isNotEmpty) {
        // set on temp variable
        final List<String> tempCurrentTopics = currentTopics;
        final List<String> tempParamList = paramList;

        // set length how far different between topic list from data and parameter
        final int emptyLength = tempCurrentTopics.length - tempParamList.length;

        // set temp variable for will subscribe and unsubcribe
        final List<String> willUnsub = <String>[];
        final List<String> willSub = <String>[];

        // then add empty string for balance param list same as data list length
        for (int index = 0; index < emptyLength; index++) {
          tempParamList.add(' ');
        }

        // check for each item
        for (int index = 0; index < tempCurrentTopics.length; index++) {
          // if item doesn't match each others
          if (tempParamList.contains(tempCurrentTopics[index]) == false) {
            // add to will unsubscribe list
            willUnsub.add(tempCurrentTopics[index]);
            // add will subscribe to list if item match before except empty string
            if (tempParamList[index] != ' ') {
              willSub.add(tempParamList[index]);
            }
          }
        }

        for (final String element in willUnsub) {
          // unsubscribe for each item list
          _instance.unsubscribeFromTopic(element);
          // remove old unused topic item
          tempCurrentTopics.removeWhere((String item) => item == element);
        }

        // ignore: prefer_foreach
        for (final String element in willSub) {
          // subscribe to each item list
          _instance.subscribeToTopic(element);
        }

        // after manipulate temporary list do remove and write shared preferenfes
        // remove old topics
        await _sharedPref.removeFCMTopicList();
        // rewrite new topic list
        await _sharedPref.writeFCMTopicList(tempCurrentTopics);

        // if parameter list is empty
      } else {
        // then unsubscribe all topics
        // ignore: prefer_foreach
        for (final String element in currentTopics) {
          _instance.unsubscribeFromTopic(element);
        }
        // remove old topics
        await _sharedPref.removeFCMTopicList();
      }

      // check if data not is exist
    } else {
      // do write into shared preferences
      await _sharedPref.writeFCMTopicList(paramList);
      // then subscribe to all topics
      // ignore: prefer_foreach
      for (final String element in paramList) {
        _instance.subscribeToTopic(element);
      }
    }
  }

  static dynamic removeAndUnSubTopics() async {
    // read topic list from shared preferences
    final List<String>? currentTopics = await _sharedPref.readFCMTopicList();
    // check if data is exist
    if (currentTopics != null) {
      // ignore: prefer_foreach
      for (final String element in currentTopics) {
        _instance.unsubscribeFromTopic(element);
      }
      await _sharedPref.removeFCMTopicList();
    }
  }
}
