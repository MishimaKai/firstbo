import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart' as ph;

import '../utils/enums.dart';

class PermissionHandler {
  PermissionHandler._();
  static Future<bool> locationPermission() async {
    if (await ph.Permission.location.request().isGranted) {
      return true;
    } else {
      return false;
    }
  }

  static Future<LocationPermissionType> locationPermissionService() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return LocationPermissionType.servicesDisable;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return LocationPermissionType.locationDenied;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return LocationPermissionType.servicesDisable;
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return LocationPermissionType.locationEnable;
  }

  static void openSetting() {
    ph.openAppSettings();
  }
}
