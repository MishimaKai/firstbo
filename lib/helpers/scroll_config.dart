import 'package:flutter/material.dart';

class ScrollConfig {
  ScrollConfig._();

  /// detect keyboard height and status
  /// [status] are false then disable scroll physics
  static ScrollPhysics? isUseScroll(bool status) {
    // log("is keyboard status = $status");
    if (status == false) {
      return const NeverScrollableScrollPhysics();
    } else {
      return null;
    }
  }

  // scroll controller
  static dynamic resetScrollView(ScrollController scrollController) {
    scrollController.animateTo(
      0.0,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeOut,
    );
  }
}
