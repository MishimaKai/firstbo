import 'package:shared_preferences/shared_preferences.dart';

import '../utils/country_code.dart';
import '../utils/language_code.dart';
import '../utils/preferences_key.dart';

class SharedPref {
  // instances
  final Future<SharedPreferences> prefsInstance =
      SharedPreferences.getInstance();

  //? Access and Refresh Token
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  //? access token
  Future<void> writeAccessToken(String accesstoken) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyAccessToken, accesstoken);
  }

  //? refresh token
  Future<void> writeRefreshToken(String refreshToken) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyRefreshToken, refreshToken);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  //? access token
  Future<String?> readAccessToken() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? accessToken = prefs.getString(PreferencesKey.keyAccessToken);
    return accessToken;
  }

  //? refresh token
  Future<String?> readRefreshToken() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? refreshToken =
        prefs.getString(PreferencesKey.keyRefreshToken);
    return refreshToken;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  //? remove access token
  Future<void> removeAccessToken() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyAccessToken);
  }

  //? remove refresh token
  Future<void> removeRefreshsToken() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyRefreshToken);
  }

  // ###################################################################

  //? Translations
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  //? language code
  Future<void> writeLanguage([
    String languageCode = LanguageCode.englishUS,
  ]) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyLanguage, languageCode);
  }

  //? country code
  Future<void> writeCountry([
    String countryCode = CountryCode.unitedState,
  ]) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyCountry, countryCode);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  //? read language for translation, default : English
  Future<String?> readLanguage() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? lang = prefs.getString(PreferencesKey.keyLanguage);
    return lang;
  }

  //? read country for translation, default : United State
  Future<String?> readCountry() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? country = prefs.getString(PreferencesKey.keyCountry);
    return country;
  }
  // ###################################################################

  //? redeem category
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  Future<void> writeRedeemCategoryId(int param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyRedeemCategoryId, param);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  Future<int?> readRedeemCategoryId() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? id = prefs.getInt(PreferencesKey.keyRedeemCategoryId);
    return id;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  Future<void> removeRedeemCategoryId() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyRedeemCategoryId);
  }
  // ###################################################################

  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  Future<void> writeIdPopUp(int id) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyPopUpId, id);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  Future<int?> readIdPopUp() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? id = prefs.getInt(PreferencesKey.keyPopUpId);
    return id;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  Future<void> removeIdPopUp() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyPopUpId);
  }
  // ###################################################################

  //? User Profile
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  //? first name
  Future<void> writeFirstName(String firstNameParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileFirstName, firstNameParam);
  }

  //? last name
  Future<void> writeLastName(String lastNameParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileLastName, lastNameParam);
  }

  //? phone number
  Future<void> writePhoneNumber(String phoneParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfilePhoneNumber, phoneParam);
  }

  //? email address
  Future<void> writeEmailAddress(String emailParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileEmailAddress, emailParam);
  }

  //? gender
  Future<void> writeGender(String genderParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileGender, genderParam);
  }

  //? birth day
  Future<void> writeDob(String dobParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileDob, dobParam);
  }

  //? city id
  Future<void> writeCityNumber(int numberParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileCityNumber, numberParam);
  }

  //? province id
  Future<void> writeStateNumber(int numberParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileStateNumber, numberParam);
  }

  //? address
  Future<void> writeAddress(String addressParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileAddress, addressParam);
  }

  //? country number
  Future<void> writeCountryNumber(int numberParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileCountryNumber, numberParam);
  }

  //? flag name
  Future<void> writeCountryFlag(String flagParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileCountryFlag, flagParam);
  }

  //? membership name
  Future<void> writeMembershipName(String membershipParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserProfileMembership, membershipParam);
  }

  //? membership benefit
  Future<void> writeMembershipBenefit(String membershipBenefit) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(
        PreferencesKey.keyUserProfileMembershipBenefit, membershipBenefit);
  }

  //? member verified
  Future<void> writeMemberVerified(int param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileVerified, param);
  }

  //? member verified request
  Future<void> writeMemberVerifiedRequest(int param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileVerifiedRequest, param);
  }

  //? member reset device request
  Future<void> writeMemberDeviceRequest(int param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyUserProfileResetDeviceRequest, param);
  }

  //? profile user picture
  Future<void> writeUserPicture(String pictureParam) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyUserPicture, pictureParam);
  }

  //? instargam account
  Future<void> writeInstagramAccount(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyCompanyProfileInstagram, param);
  }

  //? whatsapp account
  Future<void> writeWhatsAppAccount(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyCompanyProfileWhatsapp, param);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  //? first name
  Future<String?> readUserProfileFirstName() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileFirstName = prefs.getString(
      PreferencesKey.keyUserProfileFirstName,
    );
    return profileFirstName;
  }

  //? last name
  Future<String?> readUserProfileLastName() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileLastName = prefs.getString(
      PreferencesKey.keyUserProfileLastName,
    );
    return profileLastName;
  }

  //? phone number
  Future<String?> readUserProfilePhoneNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profilePhoneNumber = prefs.getString(
      PreferencesKey.keyUserProfilePhoneNumber,
    );
    return profilePhoneNumber;
  }

  //? email address
  Future<String?> readUserProfileEmailAddress() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileEmailAddress = prefs.getString(
      PreferencesKey.keyUserProfileEmailAddress,
    );
    return profileEmailAddress;
  }

  //? gender
  Future<String?> readUserProfileGender() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileGender = prefs.getString(
      PreferencesKey.keyUserProfileGender,
    );
    return profileGender;
  }

  //? birth day
  Future<String?> readUserProfileDob() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileDob = prefs.getString(
      PreferencesKey.keyUserProfileDob,
    );
    return profileDob;
  }

  //? city id
  Future<int?> readUserProfileCityNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? profileCityNumber = prefs.getInt(
      PreferencesKey.keyUserProfileCityNumber,
    );
    return profileCityNumber;
  }

  //? state/province id
  Future<int?> readUserProfileStateNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? profileStateNumber = prefs.getInt(
      PreferencesKey.keyUserProfileStateNumber,
    );
    return profileStateNumber;
  }

  //? address
  Future<String?> readUserProfileAddress() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileAddress = prefs.getString(
      PreferencesKey.keyUserProfileAddress,
    );
    return profileAddress;
  }

  //? country number
  Future<int?> readUserProfileCountryNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? profileCountryNumber = prefs.getInt(
      PreferencesKey.keyUserProfileCountryNumber,
    );
    return profileCountryNumber;
  }

  //? flag name
  Future<String?> readUserProfileCountryFlag() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileFlagNumber = prefs.getString(
      PreferencesKey.keyUserProfileCountryFlag,
    );
    return profileFlagNumber;
  }

  //? membership
  Future<String?> readUserProfileMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileAddress = prefs.getString(
      PreferencesKey.keyUserProfileMembership,
    );
    return profileAddress;
  }

  //? membership list
  Future<String?> readProfileMembershipBenefit() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? benefits = prefs.getString(
      PreferencesKey.keyUserProfileMembershipBenefit,
    );

    return benefits;
  }

  //? verified
  Future<int?> readProfileMembershipVerified() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? verified = prefs.getInt(PreferencesKey.keyUserProfileVerified);

    return verified;
  }

  //? verified request
  Future<int?> readProfileMembershipVerifiedRequest() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? verifiedRequest = prefs.getInt(
      PreferencesKey.keyUserProfileVerifiedRequest,
    );

    return verifiedRequest;
  }

  //? reset device
  Future<int?> readProfileMembershipResetDeviceRequest() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? resetDeviceRequest = prefs.getInt(
      PreferencesKey.keyUserProfileResetDeviceRequest,
    );

    return resetDeviceRequest;
  }

  //? profile user picture
  Future<String?> readUserProfilPicture() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? profileAddress = prefs.getString(
      PreferencesKey.keyUserPicture,
    );
    return profileAddress;
  }

  //? instargam account
  Future<String?> readInstagramAccount() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? instagram = prefs.getString(
      PreferencesKey.keyCompanyProfileInstagram,
    );

    return instagram;
  }

  //? whatsapp account
  Future<String?> readWhatsAppAccount() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? whatsapp = prefs.getString(
      PreferencesKey.keyCompanyProfileWhatsapp,
    );

    return whatsapp;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  // remove user profile
  Future<void> removeUserProfileFirstName() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileFirstName);
  }

  Future<void> removeUserProfileLastName() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileLastName);
  }

  Future<void> removeUserProfilePhoneNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfilePhoneNumber);
  }

  Future<void> removeUserProfileEmailAddress() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileEmailAddress);
  }

  Future<void> removeUserProfileGender() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileGender);
  }

  Future<void> removeUserProfileDob() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileDob);
  }

  Future<void> removeUserProfileCityNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileCityNumber);
  }

  Future<void> removeUserProfileStateNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileStateNumber);
  }

  Future<void> removeUserProfileAddress() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileAddress);
  }

  Future<void> removeUserProfileCountryNumber() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileCountryNumber);
  }

  Future<void> removeUserProfileCountryFlag() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileCountryFlag);
  }

  Future<void> removeUserProfileMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileMembership);
  }

  Future<void> removeUserProfileMembershipBeneft() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileMembershipBenefit);
  }

  Future<void> removeUserProfileVerified() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileVerified);
  }

  Future<void> removeUserProfileVerifiedRequest() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileVerifiedRequest);
  }

  Future<void> removeUserProfileResetDeviceRequest() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileResetDeviceRequest);
  }

  Future<void> removeUserProfilePicture() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserPicture);
  }

  //? instargam account
  Future<void> removeInstagramAccount() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyCompanyProfileInstagram);
  }

  //? whatsapp account
  Future<void> removeWhatsAppAccount() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyCompanyProfileWhatsapp);
  }

  // ###################################################################

  //? My Membership
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  //? current membership
  Future<void> writeCurrentMembership(String status) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyCurrentMembership, status);
  }

  //? next membership
  Future<void> writeNextMembership(String status) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyNextMembership, status);
  }

  //? is reached transaction
  Future<void> writeIsReachedTransaction(int value) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyIsReachedTransaction, value);
  }

  //? current transaction
  Future<void> writeCurrentTransaction(int value) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyCurrentTransaction, value);
  }

  //? min transaction
  Future<void> writeMinTransaction(int value) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyMinTransaction, value);
  }

  //? current spending power
  Future<void> writeSpendingPowerCurrent(int current) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keySpendingPowerCurrent, current);
  }

  //? min existing membership spending power
  Future<void> writeSpendingPowerMinExistingMembership(int minExisting) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(
      PreferencesKey.keySpendingPowerMinExistingMembership,
      minExisting,
    );
  }

  //? min next membership spanding power
  Future<void> writeSpendingPowerMinNextMembership(int minNext) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keySpendingPowerMinNextMembership, minNext);
  }

  //? req next membership spending power
  Future<void> writeSpendingPowerReqNextMembership(int reqNextLevel) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(
      PreferencesKey.keySpendingPowerReqNextLevelMembership,
      reqNextLevel,
    );
  }

  //? current total spending
  Future<void> writeTotalSpendingCurrent(int current) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyTotalSpendingCurrent, current);
  }

  //? current display total spending
  Future<void> writeTotalSpendingDisplayCurrent(String display) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyTotalSpendingDisplayCurrent, display);
  }

  //? min existing membership total spending
  Future<void> writeTotalSpendingMinExistingMembership(int minExisting) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(
      PreferencesKey.keyTotalSpendingMinExistingMembership,
      minExisting,
    );
  }

  //? min next membership total spanding
  Future<void> writeTotalSpendingMinNextLevelMembership(int minNext) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(PreferencesKey.keyTotalSpendingMinNextMembership, minNext);
  }

  //? req next membership total spending
  Future<void> writeTotalSpendingReqNextMembership(int reqNextLevel) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setInt(
      PreferencesKey.keyTotalSpendingReqNextLevelMembership,
      reqNextLevel,
    );
  }

  //? display req next level membership total spending
  Future<void> writeTotalSpendingDisplayReqNextLevelMembership(
    String display,
  ) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(
      PreferencesKey.keyTotalSpendingDisplayReqNextLevelMembership,
      display,
    );
  }

  //? spending power percentage
  Future<void> writeSpendingPowerPercentage(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keySpendingPowerPercentage, param);
  }

  //? spending power percentage
  Future<void> writeSpendingPowerStayWarningBox(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keySpendingPowerStayWarningBox, param);
  }

  //? spending power percentage
  Future<void> writeTotalSpendingPercentage(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyTotalSpendingPercentage, param);
  }

  //? reset membership
  Future<void> writeMembershipReset(String date) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyMembershipReset, date);
  }

  //? membership rank
  Future<void> writeMembershipRank(String param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setString(PreferencesKey.keyMembershipRank, param);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  //? current membership
  Future<String?> readCurrentMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(PreferencesKey.keyCurrentMembership);
    return value;
  }

  //? next membership
  Future<String?> readNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(PreferencesKey.keyNextMembership);
    return value;
  }

  //? is reached transaction
  Future<int?> readIsReachedTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(PreferencesKey.keyIsReachedTransaction);
    return value;
  }

  //? current transaction
  Future<int?> readCurrentTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(PreferencesKey.keyCurrentTransaction);
    return value;
  }

  //? min transaction
  Future<int?> readMinTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(PreferencesKey.keyMinTransaction);
    return value;
  }

  //? current spending power
  Future<int?> readSpendingPowerCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(PreferencesKey.keySpendingPowerCurrent);
    return value;
  }

  //? min existing membership spending power
  Future<int?> readSpendingPowerMinExistingMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keySpendingPowerMinExistingMembership,
    );
    return value;
  }

  //? min next membership spanding power
  Future<int?> readSpendingPowerMinNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keySpendingPowerMinNextMembership,
    );
    return value;
  }

  //? req next membership spending power
  Future<int?> readSpendingPowerReqNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keySpendingPowerReqNextLevelMembership,
    );
    return value;
  }

  //? current total spending
  Future<int?> readTotalSpendingCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(PreferencesKey.keyTotalSpendingCurrent);
    return value;
  }

  //? current display total spending
  Future<String?> readTotalSpendingDisplayCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(
      PreferencesKey.keyTotalSpendingDisplayCurrent,
    );
    return value;
  }

  //? min existing membership total spending
  Future<int?> readTotalSpendingMinExistingMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keyTotalSpendingMinExistingMembership,
    );
    return value;
  }

  //? min next level membership total spending
  Future<int?> readTotalSpendingMinNextLevelMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keyTotalSpendingMinNextMembership,
    );
    return value;
  }

  //? req next membership total spending
  Future<int?> readTotalSpendingReqNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final int? value = prefs.getInt(
      PreferencesKey.keyTotalSpendingReqNextLevelMembership,
    );
    return value;
  }

  //? display req next level membership total spending
  Future<String?> readTotalSpendingDisplayReqNextLevelMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(
      PreferencesKey.keyTotalSpendingDisplayReqNextLevelMembership,
    );
    return value;
  }

  //? spending power percentage
  Future<String?> readSpendingPowerPercentage() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(
      PreferencesKey.keySpendingPowerPercentage,
    );
    return value;
  }

  //? spending power percentage
  Future<String?> readSpendingPowerStayWarningBox() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(
      PreferencesKey.keySpendingPowerStayWarningBox,
    );
    return value;
  }

  //? spending power percentage
  Future<String?> readTotalSpendingPercentage() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(
      PreferencesKey.keyTotalSpendingPercentage,
    );
    return value;
  }

  //? membership reset
  Future<String?> readMembershipReset() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(PreferencesKey.keyMembershipReset);
    return value;
  }

  //? membership rank
  Future<String?> readMembershipRank() async {
    final SharedPreferences prefs = await prefsInstance;
    final String? value = prefs.getString(PreferencesKey.keyMembershipRank);
    return value;
  }

  // ###################################################################

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  /// My Membership
  //? current membership
  Future<void> removeCurrentMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyCurrentMembership);
  }

  //? next membership
  Future<void> removeNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyNextMembership);
  }

  //? is reached transaction
  Future<void> removeIsReachedTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyIsReachedTransaction);
  }

  //? current transaction
  Future<void> removeCurrentTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyCurrentTransaction);
  }

  //? min transaction
  Future<void> removeMinTransaction() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyMinTransaction);
  }

  //? current spending power
  Future<void> removeSpendingPowerCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerCurrent);
  }

  //? min existing membership spending power
  Future<void> removeSpendingPowerMinExistingMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerMinExistingMembership);
  }

  //? min next membership spanding power
  Future<void> removeSpendingPowerMinNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerMinNextMembership);
  }

  //? req next membership spending power
  Future<void> removeSpendingPowerReqNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerReqNextLevelMembership);
  }

  //? current total spending
  Future<void> removeTotalSpendingCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingCurrent);
  }

  //? current display total spending
  Future<void> removeTotalSpendingDisplayCurrent() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingDisplayCurrent);
  }

  //? min existing membership total spending
  Future<void> removeTotalSpendingMinExistingMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingMinExistingMembership);
  }

  //? min next membership total spanding
  Future<void> removeTotalSpendingMinNextLevelMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingMinNextMembership);
  }

  //? req next membership total spending
  Future<void> removeTotalSpendingReqNextMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingReqNextLevelMembership);
  }

  //? display req next level membership total spending
  Future<void> removeTotalSpendingDisplayReqNextLevelMembership() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingDisplayReqNextLevelMembership);
  }

  //? spending power percentage
  Future<void> removeSpendingPowerPercentage() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerPercentage);
  }

  //? spending power percentage
  Future<void> removeSpendingPowerStayWarningBox() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerStayWarningBox);
  }

  //? spending power percentage
  Future<void> removeTotalSpendingPercentage() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyTotalSpendingPercentage);
  }

  //? membership reset
  Future<void> removeMembershipReset() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyMembershipReset);
  }

  //? membership rank
  Future<void> removeMembershipRank() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keySpendingPowerMinExistingMembership);
  }

  // ###################################################################
  //? is user profile changed
  ///[SAVE_FROM_SHARED_PREFERENCE]
  Future<void> writeIsChangeUserProfile(bool param) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setBool(PreferencesKey.keyUserProfileIsUserChanged, param);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  Future<bool?> readIsChangeUserProfile() async {
    final SharedPreferences prefs = await prefsInstance;
    final bool? isChanged = prefs.getBool(
      PreferencesKey.keyUserProfileIsUserChanged,
    );
    return isChanged;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  Future<void> removeIsChangeUserProfile() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyUserProfileIsUserChanged);
  }
  // ###################################################################

  // ###################################################################
  //? firebase message
  ///[SAVE_FROM_SHARED_PREFERENCE]
  Future<void> writeFCMTopicList(List<String> paramList) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setStringList(PreferencesKey.keyFCMTopicList, paramList);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  Future<List<String>?> readFCMTopicList() async {
    final SharedPreferences prefs = await prefsInstance;
    final List<String>? fcmTopicList =
        prefs.getStringList(PreferencesKey.keyFCMTopicList);
    return fcmTopicList;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  Future<void> removeFCMTopicList() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyFCMTopicList);
  }

  //? Save device type
  // ###################################################################
  ///[SAVE_FROM_SHARED_PREFERENCE]
  //? is fold device
  Future<void> writeIsFoldDevice(bool isFoldDevice) async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.setBool(PreferencesKey.keyFoldDevice, isFoldDevice);
  }

  ///[READ_FROM_SHARED_PREFERENCE]
  //? is fold device
  Future<bool?> readIsFoldDevice() async {
    final SharedPreferences prefs = await prefsInstance;
    final bool? isFoldDevice = prefs.getBool(PreferencesKey.keyFoldDevice);
    return isFoldDevice;
  }

  ///[REMOVE_FROM_SHARED_PREFERENCE]
  Future<void> removeIsFoldDevice() async {
    final SharedPreferences prefs = await prefsInstance;
    prefs.remove(PreferencesKey.keyFoldDevice);
  }
  // ###################################################################

  // ###################################################################

  Future<bool> hasData(String key) async {
    final SharedPreferences prefs = await prefsInstance;
    return prefs.containsKey(key);
  }
}
