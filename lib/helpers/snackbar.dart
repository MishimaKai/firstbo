import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../utils/app_colors.dart';
import '../utils/enums.dart';
import '../utils/size_config.dart';

class Snackbar {
  Snackbar._();

  static dynamic show(
    SnackbarType snackbarType,
    String title,
    String message, {
    final Color? backgroundColor,
    final Color? colorText,
    final Duration? duration,
    final SnackPosition? position,
  }) {
    if (Get.isSnackbarOpen) {
      return Get.back();
    } else {
      final Color? background;
      if (snackbarType == SnackbarType.error) {
        background = AppColors.redAlert;
      } else {
        if (snackbarType == SnackbarType.success) {
          background = AppColors.greenInfo;
        } else {
          if (snackbarType == SnackbarType.info) {
            background = AppColors.yellowEvent;
          } else {
            background = backgroundColor;
          }
        }
      }
      return Get.snackbar(
        title,
        message,
        snackPosition: position ?? SnackPosition.BOTTOM,
        colorText: colorText ?? Colors.white,
        backgroundColor: background,
        snackStyle: SnackStyle.FLOATING,
        duration: duration ?? const Duration(milliseconds: 2000),
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(1),
          horizontal: SizeConfig.horizontal(4),
        ),
        margin: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(1),
          horizontal: SizeConfig.horizontal(2),
        ),
      );
    }
  }
}
