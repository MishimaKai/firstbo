import 'dart:convert';

import 'package:intl/intl.dart';

import '../models/data/member_account_info/member_account_info_model.dart';
import '../models/data/member_profile/member_profile_model.dart';
import '../models/data/membership_rank/membership_rank_model.dart';
import '../utils/country_identifier.dart';
import 'date_helper.dart';
import 'shared_pref.dart';

class StringHelper {
  StringHelper._();
  // private variable
  static final SharedPref _sharedPref = SharedPref();
  static final NumberFormat _formatCurrency = NumberFormat.currency(
    locale: 'in_ID',
    symbol: '',
    decimalDigits: 0,
  );
  static String convertToRupiah(int rawPrice) {
    return _formatCurrency.format(rawPrice);
  }

  /// this method receive Map data and will return to string url-encoded
  static String urlQueries(Map<String, dynamic> data) {
    final List<String> queries = <String>[];
    data.forEach((String key, dynamic value) {
      final String name = '$key=$value';
      queries.add(name);
    });
    final String stringQueries = queries.join('&');
    return stringQueries;
  }

  static dynamic onWriteMyMembership(MemberProfileModel param) {
    if (param.data.myMembership != null) {
      // current membership
      _sharedPref
          .writeCurrentMembership(param.data.myMembership!.currentMembership);
      // next membership
      if (param.data.myMembership!.nextMembership != null) {
        _sharedPref
            .writeNextMembership(param.data.myMembership!.nextMembership!);
      } else {
        _sharedPref.removeNextMembership();
      }

      // is reached transaction
      _sharedPref.writeIsReachedTransaction(
          param.data.myMembership!.isReachedTransaction);
      // current transaction
      _sharedPref
          .writeCurrentTransaction(param.data.myMembership!.currentTransaction);
      // min transaction
      _sharedPref.writeMinTransaction(param.data.myMembership!.minTransaction);

      // spending power
      _sharedPref.writeSpendingPowerCurrent(
        param.data.myMembership!.spendingPowser.current,
      );
      _sharedPref.writeSpendingPowerMinExistingMembership(
        param.data.myMembership!.spendingPowser.minExistingMembership,
      );
      if (param.data.myMembership!.spendingPowser.minNextMembership != null) {
        _sharedPref.writeSpendingPowerMinNextMembership(
          param.data.myMembership!.spendingPowser.minNextMembership!,
        );
      } else {
        _sharedPref.removeSpendingPowerMinNextMembership();
      }

      if (param.data.myMembership!.spendingPowser.reqNextLevelMembership !=
          null) {
        _sharedPref.writeSpendingPowerReqNextMembership(
          param.data.myMembership!.spendingPowser.reqNextLevelMembership!,
        );
      } else {
        _sharedPref.removeSpendingPowerReqNextMembership();
      }

      // spending power -- percentage bar
      final String tempSpendingPowerPercentage = json.encode(
        param.data.myMembership!.spendingPowser.percentageBar,
      );
      _sharedPref.writeSpendingPowerPercentage(tempSpendingPowerPercentage);

      // spending power -- stay warning box
      final String temSpendingPowerStayWarningBox = json.encode(
        param.data.myMembership!.spendingPowser.stayWarningBox,
      );
      _sharedPref.writeSpendingPowerStayWarningBox(
        temSpendingPowerStayWarningBox,
      );

      // total spending
      _sharedPref.writeTotalSpendingCurrent(
        param.data.myMembership!.totalSpending.current,
      );
      _sharedPref.writeTotalSpendingDisplayCurrent(
        param.data.myMembership!.totalSpending.displayCurrent,
      );
      _sharedPref.writeTotalSpendingMinExistingMembership(
        param.data.myMembership!.totalSpending.minExistingMembership,
      );
      if (param.data.myMembership!.totalSpending.minNextMembership != null) {
        _sharedPref.writeTotalSpendingMinNextLevelMembership(
          param.data.myMembership!.totalSpending.minNextMembership!,
        );
      } else {
        _sharedPref.removeTotalSpendingMinNextLevelMembership();
      }

      if (param.data.myMembership!.totalSpending.reqNextLevelMembership !=
          null) {
        _sharedPref.writeTotalSpendingReqNextMembership(
          param.data.myMembership!.totalSpending.reqNextLevelMembership!,
        );
      } else {
        _sharedPref.removeTotalSpendingReqNextMembership();
      }

      if (param
              .data.myMembership!.totalSpending.displayReqNextLevelMembership !=
          null) {
        _sharedPref.writeTotalSpendingDisplayReqNextLevelMembership(
          param.data.myMembership!.totalSpending.displayReqNextLevelMembership!,
        );
      } else {
        _sharedPref.removeTotalSpendingDisplayReqNextLevelMembership();
      }

      // total spending - percentage bar
      final String tempTotalSpendingPercentage = json.encode(
        param.data.myMembership!.totalSpending.percentageBar,
      );
      _sharedPref.writeTotalSpendingPercentage(tempTotalSpendingPercentage);

      // membership reset
      final String dateTemp = DateHelper.getEventOfDateDisplayer(
        param.data.myMembership!.spendingPowser.stayWarningBox.resetDate,
      );
      _sharedPref.writeMembershipReset(dateTemp);
    }

    // membership rank
    final String temp = json.encode(param.data.membershipRank);
    _sharedPref.writeMembershipRank(temp);

    // reset device request status
    _sharedPref.writeMemberDeviceRequest(param.data.resetDeviceRequest);

    // company profile
    _sharedPref.writeInstagramAccount(param.data.companyInfo.instagram);
    _sharedPref.writeWhatsAppAccount(param.data.companyInfo.phone);

    // membership name
    _sharedPref.writeMembershipName(param.data.membershipName);

    // member benefit
    String tempBenefit = '';
    if (param.data.membershipRank.isNotEmpty) {
      for (final MembershipRankItemModel? element
          in param.data.membershipRank) {
        if (element?.isActive == 1) {
          tempBenefit = element?.benefit ?? '';
        }
      }
    }
    _sharedPref.writeMembershipBenefit(tempBenefit);
  }

  static dynamic onWriteProfile(MemberAccountInfoModel param, String flag) {
    // write user profile
    _sharedPref.writeUserPicture(param.data.image.original ?? '');
    _sharedPref.writeFirstName(param.data.firstName);
    _sharedPref.writeLastName(param.data.lastName);
    _sharedPref.writePhoneNumber(param.data.phone);
    _sharedPref.writeEmailAddress(param.data.email);
    _sharedPref.writeGender(param.data.genderName);
    _sharedPref.writeDob(param.data.dob);
    _sharedPref.writeCityNumber(param.data.city);
    _sharedPref.writeStateNumber(param.data.province);
    _sharedPref.writeAddress(param.data.address);
    _sharedPref.writeCountryNumber(int.parse(param.data.countryCode));

    // verified status
    _sharedPref.writeMemberVerified(param.data.verified);
    _sharedPref.writeMemberVerifiedRequest(param.data.verifiedRequest);

    // flag
    _sharedPref.writeCountryFlag(flag);
  }

  static dynamic onWriteEditProfile(
    String firstName,
    String lastName,
    String email,
    String gender,
    String dob,
    int city,
    int province,
    String address,
  ) {
    _sharedPref.writeFirstName(firstName);
    _sharedPref.writeLastName(lastName);
    _sharedPref.writeEmailAddress(email);
    _sharedPref.writeGender(gender);
    _sharedPref.writeDob(dob);
    _sharedPref.writeCityNumber(city);
    _sharedPref.writeStateNumber(province);
    _sharedPref.writeAddress(address);
  }

  static dynamic onRemoveProfile() {
    // remove user profile
    _sharedPref.removeUserProfilePicture();
    _sharedPref.removeUserProfileFirstName();
    _sharedPref.removeUserProfileLastName();
    _sharedPref.removeUserProfilePhoneNumber();
    _sharedPref.removeUserProfileEmailAddress();
    _sharedPref.removeUserProfileGender();
    _sharedPref.removeUserProfileDob();
    _sharedPref.removeUserProfileCityNumber();
    _sharedPref.removeUserProfileStateNumber();
    _sharedPref.removeUserProfileAddress();
    _sharedPref.removeUserProfileCountryNumber();
    _sharedPref.removeUserProfileCountryFlag();

    _sharedPref.removeUserProfileVerified();
    _sharedPref.removeUserProfileVerifiedRequest();
  }

  static dynamic onRemoveMyMembership() {
    // remove my membership data
    // current membership
    _sharedPref.removeCurrentMembership();
    // next membership
    _sharedPref.removeNextMembership();
    // is reached transaction
    _sharedPref.removeIsReachedTransaction();
    // current transaction
    _sharedPref.removeCurrentTransaction();
    // min transaction
    _sharedPref.removeMinTransaction();

    // spending power
    _sharedPref.removeSpendingPowerCurrent();
    _sharedPref.removeSpendingPowerMinExistingMembership();
    _sharedPref.removeSpendingPowerMinNextMembership();
    _sharedPref.removeSpendingPowerReqNextMembership();

    // total spending
    _sharedPref.removeTotalSpendingCurrent();
    _sharedPref.removeTotalSpendingDisplayCurrent();
    _sharedPref.removeTotalSpendingMinExistingMembership();
    _sharedPref.removeTotalSpendingMinNextLevelMembership();
    _sharedPref.removeTotalSpendingReqNextMembership();
    _sharedPref.removeTotalSpendingDisplayReqNextLevelMembership();

    // member benefit
    _sharedPref.removeUserProfileMembership();
    _sharedPref.removeUserProfileMembershipBeneft();

    // membership reset
    _sharedPref.removeMembershipReset();

    // membership rank
    _sharedPref.removeMembershipRank();
    _sharedPref.removeSpendingPowerPercentage();
    _sharedPref.removeSpendingPowerStayWarningBox();
    _sharedPref.removeTotalSpendingPercentage();

    // device status
    _sharedPref.removeUserProfileResetDeviceRequest();

    // social
    _sharedPref.removeInstagramAccount();
    _sharedPref.removeWhatsAppAccount();
  }

  static String hasMatchCodeToFlag(int countryCode) {
    String result = '';
    for (final Map<String, dynamic> element in CountryIdentifier.countryList) {
      if (countryCode == element['code']) {
        result = element['flag'].toString();
      }
    }

    return result;
  }
}
