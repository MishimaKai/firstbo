import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../lang/english_us.dart';
import '../utils/country_code.dart';
import '../utils/language_code.dart';
import '../utils/preferences_key.dart';
import 'shared_pref.dart';

class BlackOwlTranslations extends Translations {
  // default language 'en', 'US'
  static const Locale locale =
      Locale(LanguageCode.englishUS, CountryCode.unitedState);
  static const Locale fallBackLocale = Locale(
    LanguageCode.englishUS,
    CountryCode.unitedState,
  );

  dynamic initLocale() async {
    final SharedPref sharedPref = SharedPref();
    // check if language has been write before
    final bool isPreferredLang = await sharedPref.hasData(
      PreferencesKey.keyLanguage,
    );

    if (!isPreferredLang) {
      await sharedPref.writeLanguage();
      await sharedPref.writeCountry();
    } else {
      final String? getLanguage = await sharedPref.readLanguage();
      final String? getCountry = await sharedPref.readCountry();
      // language 'id', 'ID'
      Get.updateLocale(Locale(getLanguage!, getCountry));
    }
  }

  @override
  Map<String, Map<String, String>> get keys => <String, Map<String, String>>{
        'en_US': englishUS,
      };
}
