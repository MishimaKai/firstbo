const Map<String, String> englishUS = <String, String>{
  // general
  'label_yes': 'Yes',
  'label_no': 'No',
  'label_info': 'Info',
  'label_error': 'Error',
  'label_success': 'Success',
  'label_failed': 'Failed',
  'label_error_something': 'An error occurred please try again later',
  // network status
  'network_status_label_error': 'Network Error',
  'network_status_label_success': 'Network Avaiable',
  'network_status_disconnect': 'No network avaiable please try again later',
  // sign in in page
  'sign_in_motto_label': 'Sign in to stay connected',
  'input_phone_placeholder': 'Please input your phone number',
  'next_label': 'Next',
  'sign_in_with_email': 'Sign in with email',
  'sign_in_with_phone_number': 'Sign in with phone number',
  'sign_in_hint_phone_number': '812-3456-789',
  'sign_in_hint_email_address': 'enter your email address',
  // register page
  'register_first_name_form_label': 'First Name',
  'register_last_name_form_label': 'Last Name',
  'register_phone_number_form_label': 'Phone Number',
  'register_email_address_form_label': 'Email Address',
  'register_gender_form_label': 'Gender',
  'register_birth_date_form_label': 'Birth Date',
  'register_address_form_label': 'Your Address',
  'register_button_label': 'Register',
  // edit profile page
  'edit_profile_button_label': 'Apply',
  // otp page
  'otp_instruction_phone': 'The OTP has been sent to your mobile number',
  'otp_instruction_email': 'The OTP has been sent to your email address',
  'sign_in_label': 'Sign in',
  'resend_otp_label': 'Resend OTP code',
  'back_to_sign_in_label': 'Back to sign in',
  // validation for sign in
  'sign_in_failed_label': 'Sign In Failed',
  // validation for delete account
  'delete_account_failed_label': 'Delete Account Failed',
  // phone number error messages
  'phone_number_empty': 'Phone number is empty',
  'phone_number_invalid': 'Invalid phone number characters',
  'phone_number_range_number_brunei': 'Phone number at least 10 characters',
  'phone_number_range_number_cambodia':
      'Phone number at least 11 or 12 characters',
  'phone_number_range_number_indonesia':
      'Phone number at least 10 or 13 characters',
  'phone_number_range_number_laos': 'Phone number at least 11 characters',
  'phone_number_range_number_malaysia':
      'Phone number at least 10 or 12 characters',
  'phone_number_range_number_myanmar':
      'Phone number at least 10 or 13 characters',
  'phone_number_range_number_philipines': 'Phone number at least 12 characters',
  'phone_number_range_number_singapore': 'Phone number at least 10 characters',
  'phone_number_range_number_thailand':
      'Phone number at least 10 or 11 characters',
  'phone_number_range_number_timor': 'Phone number at least 11 characters',
  'phone_number_range_number_vietnam':
      'Phone number at least 9 or 10 characters',
  // email address error messages
  'email_address_empty': 'Email address is empty',
  'email_address_invalid': 'Format invalid email address',
  // validation for register
  'register_failed_label': 'Register Failed',
  // name
  'first_name_empty': 'First name is empty',
  'last_name_empty': 'Last name is empty',
  // date
  'dob_empty': 'Date is empty',
  'dob_invalid': 'Invalid date format',
  // address
  'address_empty': 'Address is empty',
  // OTP error messages
  'otp_failed_label': 'Verification Failed',
  'otp_empty': 'Verification code is empty',
  'otp_invalid': 'Invalid verification code characters',
  'otp_range': 'Verification code at least 6 characters',
  // Notification dialog
  'title_id': 'Success',
  'description_id': 'Your ID card already verified',
  'title_redeem': 'Redeem Points',
  'description_redeem_verification':
      'Do you want to redeem Airpods Pro for 1.275 points?',
  'description_redeem_success':
      'You successfully redeemed 1.275 points for Airpods Pro',
  'button_redeem_verification': 'Yes, Redeem My Points',
  'button_redeem_success': 'Back to Redeem Points',
  // profile page
  'account_unverified_label': 'Verify your account now',
  'account_verifying_label': 'Verification is in progress',
  'account_verified_label': 'Account Verified',
  // location permission
  'location_turn_on_warning': 'Please turn on location service',
  'location_enable_permission_warning':
      'Please allow location from app setting',
  // dialog
  'force_update_dialog_label': 'New Version Available',
  'force_update_dialog_message':
      'Please update to new version for continue using the application',
  // events
  'events_title': "What's New",
  'events_empty_label': 'There’s no events yet. Please stay tune!',
  'bottle_title': 'Bottle Packages',
  'bottle_empty_label': 'No item in this category',
  'food_title': 'Special Offers',
  'food_empty_label': 'No item in this category',
  // change device
  'change_device_instruction':
      "You're trying to login on device that Black Owl doesn't recognize. Please request for 'Change Device'.",
};
