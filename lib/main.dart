import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'helpers/camera_service.dart';
import 'helpers/firebase_config.dart';
import 'helpers/notification_service.dart';
import 'helpers/translations.dart';
import 'routes/routes.dart';
import 'views/authorize/authorize_page.dart';

void main() {
  // binding
  WidgetsFlutterBinding.ensureInitialized();
  // initiate camera
  CameraService.onCheckCamera();
  // initiate firebase
  FirebaseConfig.initFireBase();
  // set background messaging handler early on
  FirebaseMessaging.onBackgroundMessage(
    FirebaseConfig.firebaseMessagingBackgroundHandler,
  );
  runApp(const BlackOwl());
}

class BlackOwl extends StatefulWidget {
  // constructor
  const BlackOwl({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BlackOwlState();
}

class BlackOwlState extends State<BlackOwl> {
  @override
  void initState() {
    super.initState();
    // init helper
    NotificationService.configureTimedZone(); // set time zone
    NotificationService.initDetails(); // initiate app launch details
    NotificationService.initPayload(); // initiate payload
    NotificationService.requestPermission(); // request permission
  }

  @override
  void dispose() {
    // dispose notification
    NotificationService.didReceiveLocalNotificationSubject.close();
    NotificationService.selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      // ignore: always_specify_types
      builder: (BuildContext context, widget) => ResponsiveWrapper.builder(
        ClampingScrollWrapper.builder(context, widget!),
        minWidth: 375,
        maxWidth: 812,
        breakpoints: const <ResponsiveBreakpoint>[
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
      title: 'Black Owl Membership',
      debugShowCheckedModeBanner: false,
      translations: BlackOwlTranslations(),
      locale: BlackOwlTranslations.locale,
      fallbackLocale: BlackOwlTranslations.fallBackLocale,
      getPages: AppRoutes.routes,
      defaultTransition: Transition.noTransition,
      home: const AuthorizeView(),
    );
  }
}
