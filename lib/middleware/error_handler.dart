import 'package:dio/dio.dart';
import 'package:get/get.dart';

import '../helpers/notification_service.dart';
import '../helpers/shared_pref.dart';
import '../helpers/string_helper.dart';
import '../models/data/access_token/success/access_token_success_model.dart';
import '../repositories/tokenRepository/token_repository.dart';
import '../repositories/tokenRepository/token_repository_interface.dart';
import '../routes/route_names.dart';
import '../utils/app_colors.dart';
import '../utils/status_code.dart';

class ErrorHandler {
  static final SharedPref _sharedPref = SharedPref();
  final ITokenRepository _tokenRepository = TokenRepository();

  Future<bool> refreshToken() async {
    final Map<String, dynamic> result =
        await _tokenRepository.postRefreshToken();

    // if code 200
    if (result['code'] == StatusCode.codeOk) {
      // save access token and refresh token
      final AccessTokenSuccessModel dataSuccess =
          AccessTokenSuccessModel.fromJson(result);
      await _sharedPref.writeAccessToken(dataSuccess.data.token.accessToken);
      await _sharedPref.writeRefreshToken(dataSuccess.data.token.refreshToken);

      return true;
    } else {
      final Map<String, dynamic> messageAndErrorCode =
          messageErrorCodeHandler(result);

      if (messageAndErrorCode['errorCode'] != StatusCode.appCodeTokenInvalid) {
        Get.snackbar(
          'label_failed'.tr,
          messageAndErrorCode['errorMessage'].toString(),
          backgroundColor: AppColors.redAlert,
          colorText: AppColors.whiteBackground,
          snackPosition: SnackPosition.BOTTOM,
        );
      }
      CancelToken().cancel();
      userLogout(isDirectToLogin: true, isUsePostLogout: false);

      return false;
    }
  }

  dynamic userLogout({
    required bool isDirectToLogin,
    required bool isUsePostLogout,
  }) async {
    if (isUsePostLogout) {
      await _tokenRepository.postLogout();
    }
    NotificationService.removeAndUnSubTopics();
    _sharedPref.removeAccessToken();
    _sharedPref.removeRefreshsToken();
    _sharedPref.removeIsChangeUserProfile();
    _sharedPref.removeIdPopUp();
    _sharedPref.removeRedeemCategoryId();
    _sharedPref.removeIsFoldDevice();
    StringHelper.onRemoveProfile();
    StringHelper.onRemoveMyMembership();
    GetInstance().resetInstance();

    if (isDirectToLogin) {
      Get.offAllNamed(RouteNames.signInRoute);
    } else {
      Get.offAllNamed(RouteNames.authortizeRoute);
    }
  }

  // other handling errors
  Future<Map<String, dynamic>> handler(Map<String, dynamic> result) async {
    // call error handler method
    final Map<String, dynamic> messageAndErrorCode = messageErrorCodeHandler(
      result,
    );
    // return action by error code checker method
    int? errorCode;
    String? errorMessage;
    // client info
    String? existingVersion;
    String? newestVersion;
    int? forceUpdate;

    // contain Map
    Map<String, dynamic> containMap = <String, dynamic>{};

    // log('_errorCode = $_errorCode');
    // log('_errorMessage = $_errorMessage');
    if (messageAndErrorCode.containsKey('existingVersion') &&
        messageAndErrorCode.containsKey('newestVersion') &&
        messageAndErrorCode.containsKey('forceUpdate')) {
      // assign variable
      errorCode = messageAndErrorCode['errorCode'] as int;
      errorMessage = messageAndErrorCode['errorMessage'] as String;
      // client info
      existingVersion = messageAndErrorCode['existingVersion'] as String;
      newestVersion = messageAndErrorCode['newestVersion'] as String;
      forceUpdate = messageAndErrorCode['forceUpdate'] as int;
      // assign contain map
      containMap = <String, dynamic>{
        'code': errorCode,
        'message': errorMessage,
        'existing': existingVersion,
        'newest': newestVersion,
        'update': forceUpdate,
      };
    } else {
      // assign variable
      errorCode = messageAndErrorCode['errorCode'] as int;
      errorMessage = messageAndErrorCode['errorMessage'] as String;
      // assign contain map
      containMap = <String, dynamic>{
        'code': errorCode,
        'message': errorMessage,
      };
    }
    final bool resultStatus = await _codeChecker(errorCode, errorMessage);
    // then add status for snackbar and refresh token status
    containMap.addAll(<String, dynamic>{'status': resultStatus});
    // then return the value
    return containMap;
  }

  Map<String, dynamic> messageErrorCodeHandler(Map<String, dynamic> result) {
    final dynamic code = result['code'];
    final bool isContainErrorKey = result.containsKey('error');
    final bool isContainClientInfo = result.containsKey('client_info');
    // error code and message
    int? errorCode;
    String? errorMessage;
    // client info
    String? existingVersion;
    String? newestVersion;
    int? forceUpdate;

    // contain Map
    Map<String, dynamic> containMap = <String, dynamic>{};

    if (isContainErrorKey) {
      final Map<String, dynamic> init = result['error'] as Map<String, dynamic>;
      final bool isContainMessageKey = init.containsKey('message');
      final bool isContainCodeKey = init.containsKey('code');

      if (isContainCodeKey) {
        errorCode = result['error']['code'] as int?;
      }

      if (isContainMessageKey) {
        if (result['error']['message'] is String) {
          errorMessage = result['error']['message'] as String?;
        } else {
          final String meessageErrorKey = isContainErrorKey
              ? result['error']['message'].keys.elementAt(0).toString()
              : '';
          errorMessage =
              result['error']['message'][meessageErrorKey][0].toString();
        }
      } else {
        final String messageKey = result['error'].keys.elementAt(0).toString();
        errorMessage = result['error'][messageKey][0].toString();
      }
    } else {
      errorCode = code as int?;
      errorMessage = result['message'] as String?;
    }

    if (isContainClientInfo) {
      existingVersion = result['client_info']['existing_version'] as String?;
      newestVersion = result['client_info']['newest_version'] as String?;
      forceUpdate = result['client_info']['force_update'] as int?;

      // assign to contain variable
      containMap = <String, dynamic>{
        'errorCode': errorCode,
        'errorMessage': errorMessage,
        'existingVersion': existingVersion,
        'newestVersion': newestVersion,
        'forceUpdate': forceUpdate,
      };
    } else {
      // assign to contain variable
      containMap = <String, dynamic>{
        'errorCode': errorCode,
        'errorMessage': errorMessage,
      };
    }

    return containMap;
  }

  Future<bool> _codeChecker(int code, String errorMessage) async {
    if (code == StatusCode.appCodeTokenInvalid) {
      return true;
    } else if (code == StatusCode.codeUnauthorized) {
      // userLogout();
      return false;
    } else if (code == StatusCode.codeBadRequest) {
      return false;
    } else if (code == StatusCode.appCodeFailedSendingEmail) {
      return false;
    } else if (code == StatusCode.appCodeUserInvalid) {
      return false;
    } else if (code == StatusCode.appCodeEmptyBody) {
      return false;
    } else {
      return false;
    }
  }
}
