import 'package:flutter/material.dart';
import 'package:open_store/open_store.dart';

import '../helpers/device_info.dart';
import '../models/data/client_info/client_info_model.dart';
import '../views/widgets/dialogs/force_update_dialog.dart';

class UpdateHandler {
  // handling client info
  dynamic handler(
    ClientInfo param,
    BuildContext paramContext, [
    bool? isDualScreen,
  ]) {
    // check force update status
    if (param.forceUpdate == 1) {
      return showDialog(
        context: paramContext,
        barrierDismissible: false,
        builder: (BuildContext context) {
          FocusManager.instance.primaryFocus?.unfocus();
          return ForceUpdateDialog(
            isDualScreen: isDualScreen ?? false,
            onTap: () async {
              // Navigator.of(context).pop();
              OpenStore.instance.open(
                appStoreId: '1609916429',
                androidAppBundleId: await DeviceInfo().getPackageName(),
              );
            },
          );
        },
      );
    }
  }
}
