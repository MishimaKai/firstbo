class OnTapTabActive {
  OnTapTabActive({
    required this.name,
    required this.index,
    required this.isActive,
  });

  late String name;
  late int index;
  late bool isActive;
}
