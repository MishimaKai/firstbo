import 'package:json_annotation/json_annotation.dart';

/// This allows the `AccessTokenFailedModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'access_token_failed_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class AccessTokenFailedModel {
  // constructor
  AccessTokenFailedModel({required this.message, required this.error});

  // variable paramateers
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'error')
  final Error error;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$AccessTokenFailedModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory AccessTokenFailedModel.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenFailedModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$AccessTokenFailedModelToJson`.
  Map<String, dynamic> toJson() => _$AccessTokenFailedModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Error {
  // constructor
  Error({required this.code, required this.message});

  // variable paramateers
  @JsonKey(name: 'code')
  final int code;
  @JsonKey(name: 'message')
  final String message;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ErrorFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ErrorToJson`.
  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}
