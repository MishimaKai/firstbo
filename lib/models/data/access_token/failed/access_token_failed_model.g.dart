// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'access_token_failed_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccessTokenFailedModel _$AccessTokenFailedModelFromJson(
        Map<String, dynamic> json) =>
    AccessTokenFailedModel(
      message: json['message'] as String,
      error: Error.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AccessTokenFailedModelToJson(
        AccessTokenFailedModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
    };

Error _$ErrorFromJson(Map<String, dynamic> json) => Error(
      code: json['code'] as int,
      message: json['message'] as String,
    );

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };
