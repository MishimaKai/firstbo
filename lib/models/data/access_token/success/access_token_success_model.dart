import 'package:json_annotation/json_annotation.dart';

import '../../client_info/client_info_model.dart';

/// This allows the `AccessTokenSuccessModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'access_token_success_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class AccessTokenSuccessModel {
  // constructor
  AccessTokenSuccessModel({required this.data, required this.clientInfo});

  // variable parameters
  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$AccessTokenSuccessModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory AccessTokenSuccessModel.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenSuccessModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$AccessTokenSuccessModelToJson`.
  Map<String, dynamic> toJson() => _$AccessTokenSuccessModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({required this.token});

  // variable parameters
  @JsonKey(name: 'token')
  final Token token;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Token {
  // constructor
  Token({
    required this.accessToken,
    required this.accessTokenExpire,
    required this.refreshToken,
    required this.refreshTokenExpire,
  });

  // variable parameters
  @JsonKey(name: 'access_token')
  final String accessToken;
  @JsonKey(name: 'access_token_expire')
  final int accessTokenExpire;
  @JsonKey(name: 'refresh_token')
  final String refreshToken;
  @JsonKey(name: 'refresh_token_expire')
  final int refreshTokenExpire;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$TokenFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$TokenToJson`.
  Map<String, dynamic> toJson() => _$TokenToJson(this);
}
