// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'access_token_success_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccessTokenSuccessModel _$AccessTokenSuccessModelFromJson(
        Map<String, dynamic> json) =>
    AccessTokenSuccessModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AccessTokenSuccessModelToJson(
        AccessTokenSuccessModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      token: Token.fromJson(json['token'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'token': instance.token,
    };

Token _$TokenFromJson(Map<String, dynamic> json) => Token(
      accessToken: json['access_token'] as String,
      accessTokenExpire: json['access_token_expire'] as int,
      refreshToken: json['refresh_token'] as String,
      refreshTokenExpire: json['refresh_token_expire'] as int,
    );

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      'access_token': instance.accessToken,
      'access_token_expire': instance.accessTokenExpire,
      'refresh_token': instance.refreshToken,
      'refresh_token_expire': instance.refreshTokenExpire,
    };
