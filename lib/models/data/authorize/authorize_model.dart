import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';

/// This allows the `AuthorizeModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'authorize_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class AuthorizeModel {
  // constructor
  AuthorizeModel({
    required this.message,
    required this.data,
    required this.clientInfo,
  });

  // variable paramateers
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$AuthorizeModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory AuthorizeModel.fromJson(Map<String, dynamic> json) =>
      _$AuthorizeModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$AuthorizeModelToJson`.
  Map<String, dynamic> toJson() => _$AuthorizeModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({required this.authorization});

  // variable paramateers
  @JsonKey(name: 'authorization')
  final Authorization authorization;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Authorization {
  // constructor
  Authorization({required this.code, required this.expire});

  // variable paramateers
  @JsonKey(name: 'code')
  final String code;
  @JsonKey(name: 'expire')
  final int expire;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$AuthorizationFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Authorization.fromJson(Map<String, dynamic> json) =>
      _$AuthorizationFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$AuthorizationToJson`.
  Map<String, dynamic> toJson() => _$AuthorizationToJson(this);
}
