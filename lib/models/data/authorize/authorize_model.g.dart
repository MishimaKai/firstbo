// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authorize_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthorizeModel _$AuthorizeModelFromJson(Map<String, dynamic> json) =>
    AuthorizeModel(
      message: json['message'] as String,
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AuthorizeModelToJson(AuthorizeModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'data': instance.data,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      authorization:
          Authorization.fromJson(json['authorization'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'authorization': instance.authorization,
    };

Authorization _$AuthorizationFromJson(Map<String, dynamic> json) =>
    Authorization(
      code: json['code'] as String,
      expire: json['expire'] as int,
    );

Map<String, dynamic> _$AuthorizationToJson(Authorization instance) =>
    <String, dynamic>{
      'code': instance.code,
      'expire': instance.expire,
    };
