import 'package:json_annotation/json_annotation.dart';

/// This allows the `ClientInfo` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'client_info_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class ClientInfo {
  // constructor
  ClientInfo({
    required this.existingVersion,
    required this.newestVersion,
    required this.forceUpdate,
  });

  // variable parameters
  @JsonKey(name: 'existing_version')
  final String existingVersion;
  @JsonKey(name: 'newest_version')
  final String newestVersion;
  @JsonKey(name: 'force_update')
  final int forceUpdate;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ClientInfoFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory ClientInfo.fromJson(Map<String, dynamic> json) =>
      _$ClientInfoFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ClientInfoToJson`.
  Map<String, dynamic> toJson() => _$ClientInfoToJson(this);
}
