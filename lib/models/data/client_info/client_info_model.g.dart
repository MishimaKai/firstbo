// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClientInfo _$ClientInfoFromJson(Map<String, dynamic> json) => ClientInfo(
      existingVersion: json['existing_version'] as String,
      newestVersion: json['newest_version'] as String,
      forceUpdate: json['force_update'] as int,
    );

Map<String, dynamic> _$ClientInfoToJson(ClientInfo instance) =>
    <String, dynamic>{
      'existing_version': instance.existingVersion,
      'newest_version': instance.newestVersion,
      'force_update': instance.forceUpdate,
    };
