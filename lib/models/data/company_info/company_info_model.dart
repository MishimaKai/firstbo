import 'package:json_annotation/json_annotation.dart';

/// This allows the `CompanyInfoModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'company_info_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CompanyInfoModel {
  // constructor
  CompanyInfoModel({
    required this.tnc,
    required this.policy,
    required this.phone,
    required this.instagram,
  });

  @JsonKey(name: 'tnc')
  final dynamic tnc;
  @JsonKey(name: 'policy')
  final dynamic policy;
  @JsonKey(name: 'phone')
  final String phone;
  @JsonKey(name: 'instagram')
  final String instagram;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$CompanyInfoModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory CompanyInfoModel.fromJson(Map<String, dynamic> json) =>
      _$CompanyInfoModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$CompanyInfoModelToJson`.
  Map<String, dynamic> toJson() => _$CompanyInfoModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Tnc {
  // constructor
  Tnc({
    required this.no,
    required this.effectiveDate,
    required this.fullDesc,
  });

  @JsonKey(name: 'no')
  final String no;
  @JsonKey(name: 'effective_date')
  final String effectiveDate;
  @JsonKey(name: 'full_desc')
  final String fullDesc;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$TncFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Tnc.fromJson(Map<String, dynamic> json) => _$TncFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$TncToJson`.
  Map<String, dynamic> toJson() => _$TncToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Policy {
  // constructor
  Policy({
    required this.no,
    required this.effectiveDate,
    required this.fullDesc,
  });

  @JsonKey(name: 'no')
  final String no;
  @JsonKey(name: 'effective_date')
  final String effectiveDate;
  @JsonKey(name: 'full_desc')
  final String fullDesc;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PolicyFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Policy.fromJson(Map<String, dynamic> json) => _$PolicyFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PolicyToJson`.
  Map<String, dynamic> toJson() => _$PolicyToJson(this);
}
