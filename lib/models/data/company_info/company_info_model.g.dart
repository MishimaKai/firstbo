// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompanyInfoModel _$CompanyInfoModelFromJson(Map<String, dynamic> json) =>
    CompanyInfoModel(
      tnc: json['tnc'],
      policy: json['policy'],
      phone: json['phone'] as String,
      instagram: json['instagram'] as String,
    );

Map<String, dynamic> _$CompanyInfoModelToJson(CompanyInfoModel instance) =>
    <String, dynamic>{
      'tnc': instance.tnc,
      'policy': instance.policy,
      'phone': instance.phone,
      'instagram': instance.instagram,
    };

Tnc _$TncFromJson(Map<String, dynamic> json) => Tnc(
      no: json['no'] as String,
      effectiveDate: json['effective_date'] as String,
      fullDesc: json['full_desc'] as String,
    );

Map<String, dynamic> _$TncToJson(Tnc instance) => <String, dynamic>{
      'no': instance.no,
      'effective_date': instance.effectiveDate,
      'full_desc': instance.fullDesc,
    };

Policy _$PolicyFromJson(Map<String, dynamic> json) => Policy(
      no: json['no'] as String,
      effectiveDate: json['effective_date'] as String,
      fullDesc: json['full_desc'] as String,
    );

Map<String, dynamic> _$PolicyToJson(Policy instance) => <String, dynamic>{
      'no': instance.no,
      'effective_date': instance.effectiveDate,
      'full_desc': instance.fullDesc,
    };
