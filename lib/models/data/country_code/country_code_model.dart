import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';

/// This allows the `CountryCodeModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'country_code_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CountryCodeModel {
  // constructor
  CountryCodeModel({required this.data, required this.clientInfo});

  @JsonKey(name: 'data')
  final List<CountryIdentity> data;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$CountryCodeModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory CountryCodeModel.fromJson(Map<String, dynamic> json) =>
      _$CountryCodeModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$CountryCodeModelToJson`.
  Map<String, dynamic> toJson() => _$CountryCodeModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CountryIdentity {
  // constructor
  CountryIdentity({
    required this.name,
    required this.flag,
    required this.code,
  });

  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'flag')
  final String flag;
  @JsonKey(name: 'code')
  final String code;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$CountryIdentityFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory CountryIdentity.fromJson(Map<String, dynamic> json) =>
      _$CountryIdentityFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$CountryIdentityToJson`.
  Map<String, dynamic> toJson() => _$CountryIdentityToJson(this);
}
