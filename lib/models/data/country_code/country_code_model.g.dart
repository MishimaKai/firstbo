// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country_code_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryCodeModel _$CountryCodeModelFromJson(Map<String, dynamic> json) =>
    CountryCodeModel(
      data: (json['data'] as List<dynamic>)
          .map((e) => CountryIdentity.fromJson(e as Map<String, dynamic>))
          .toList(),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CountryCodeModelToJson(CountryCodeModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'client_info': instance.clientInfo,
    };

CountryIdentity _$CountryIdentityFromJson(Map<String, dynamic> json) =>
    CountryIdentity(
      name: json['name'] as String,
      flag: json['flag'] as String,
      code: json['code'] as String,
    );

Map<String, dynamic> _$CountryIdentityToJson(CountryIdentity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'flag': instance.flag,
      'code': instance.code,
    };
