import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `DeleteAccountModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'delete_account_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class DeleteAccountModel {
  // constructor
  DeleteAccountModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final DeleteAccountDataModel data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DeleteAccountModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory DeleteAccountModel.fromJson(Map<String, dynamic> json) =>
      _$DeleteAccountModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DeleteAccountModelToJson`.
  Map<String, dynamic> toJson() => _$DeleteAccountModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class DeleteAccountDataModel {
  // constructor
  DeleteAccountDataModel({
    required this.memberAccess,
    required this.type,
    required this.expired,
  });

  @JsonKey(name: 'member_access')
  final String memberAccess;
  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'exp')
  final String expired;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DeleteAccountDataModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory DeleteAccountDataModel.fromJson(Map<String, dynamic> json) =>
      _$DeleteAccountDataModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DeleteAccountDataModelToJson`.
  Map<String, dynamic> toJson() => _$DeleteAccountDataModelToJson(this);
}
