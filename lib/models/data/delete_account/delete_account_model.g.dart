// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_account_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeleteAccountModel _$DeleteAccountModelFromJson(Map<String, dynamic> json) =>
    DeleteAccountModel(
      data:
          DeleteAccountDataModel.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DeleteAccountModelToJson(DeleteAccountModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

DeleteAccountDataModel _$DeleteAccountDataModelFromJson(
        Map<String, dynamic> json) =>
    DeleteAccountDataModel(
      memberAccess: json['member_access'] as String,
      type: json['type'] as int,
      expired: json['exp'] as String,
    );

Map<String, dynamic> _$DeleteAccountDataModelToJson(
        DeleteAccountDataModel instance) =>
    <String, dynamic>{
      'member_access': instance.memberAccess,
      'type': instance.type,
      'exp': instance.expired,
    };
