import 'package:json_annotation/json_annotation.dart';

/// This allows the `Device` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'device_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Device {
  // constructor
  Device({
    required this.deviceId,
    required this.deviceOS,
    required this.deviceAppVersion,
    required this.lastLoginAt,
    required this.lastLoginIp,
  });

  // variable parameters
  @JsonKey(name: 'device_id')
  final String deviceId;
  @JsonKey(name: 'device_os')
  final String deviceOS;
  @JsonKey(name: 'device_app_version')
  final String deviceAppVersion;
  @JsonKey(name: 'last_login_at')
  final String lastLoginAt;
  @JsonKey(name: 'last_login_ip')
  final String lastLoginIp;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DeviceFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Device.fromJson(Map<String, dynamic> json) => _$DeviceFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DeviceToJson`.
  Map<String, dynamic> toJson() => _$DeviceToJson(this);
}
