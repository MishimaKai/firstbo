// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Device _$DeviceFromJson(Map<String, dynamic> json) => Device(
      deviceId: json['device_id'] as String,
      deviceOS: json['device_os'] as String,
      deviceAppVersion: json['device_app_version'] as String,
      lastLoginAt: json['last_login_at'] as String,
      lastLoginIp: json['last_login_ip'] as String,
    );

Map<String, dynamic> _$DeviceToJson(Device instance) => <String, dynamic>{
      'device_id': instance.deviceId,
      'device_os': instance.deviceOS,
      'device_app_version': instance.deviceAppVersion,
      'last_login_at': instance.lastLoginAt,
      'last_login_ip': instance.lastLoginIp,
    };
