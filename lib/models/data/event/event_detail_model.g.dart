// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventDetailModel _$EventDetailModelFromJson(Map<String, dynamic> json) =>
    EventDetailModel(
      data: EventModel.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$EventDetailModelToJson(EventDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
