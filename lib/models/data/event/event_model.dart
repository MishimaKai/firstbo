import 'package:json_annotation/json_annotation.dart';

import '../image/image_model.dart';
import '../outlet_info/outlet_info_model.dart';

/// This allows the `EventModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'event_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class EventModel {
  // constructor
  EventModel({
    this.id,
    this.title,
    this.fullDesc,
    this.image,
    this.startDate,
    this.endDate,
    this.outletInfo,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'title')
  final String? title;
  @JsonKey(name: 'full_desc')
  final String? fullDesc;
  @JsonKey(name: 'image')
  final Image? image;
  @JsonKey(name: 'start_date')
  final String? startDate;
  @JsonKey(name: 'end_date')
  final String? endDate;
  @JsonKey(name: 'outlet_info')
  final List<OutletInfoModel>? outletInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$EventFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory EventModel.fromJson(Map<String, dynamic> json) =>
      _$EventModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$EventToJson`.
  Map<String, dynamic> toJson() => _$EventModelToJson(this);
}
