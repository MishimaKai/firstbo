// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventModel _$EventModelFromJson(Map<String, dynamic> json) => EventModel(
      id: json['id'] as int?,
      title: json['title'] as String?,
      fullDesc: json['full_desc'] as String?,
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
      startDate: json['start_date'] as String?,
      endDate: json['end_date'] as String?,
      outletInfo: (json['outlet_info'] as List<dynamic>?)
          ?.map((e) => OutletInfoModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EventModelToJson(EventModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'full_desc': instance.fullDesc,
      'image': instance.image,
      'start_date': instance.startDate,
      'end_date': instance.endDate,
      'outlet_info': instance.outletInfo,
    };
