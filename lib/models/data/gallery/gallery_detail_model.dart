import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `GalleryDetailModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'gallery_detail_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class GalleryDetailModel {
  // constructor
  GalleryDetailModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$GalleryModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory GalleryDetailModel.fromJson(Map<String, dynamic> json) =>
      _$GalleryDetailModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$GalleryModelToJson`.
  Map<String, dynamic> toJson() => _$GalleryDetailModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.id,
    required this.galleryDate,
    required this.image,
    required this.title,
    required this.fullDesc,
    required this.multipleImage,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'gallery_date')
  final String? galleryDate;
  @JsonKey(name: 'image')
  final Image? image;
  @JsonKey(name: 'title')
  final String? title;
  @JsonKey(name: 'full_desc')
  final String? fullDesc;
  @JsonKey(name: 'multiple_img')
  final List<MutipleImageModel> multipleImage;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MutipleImageModel {
  // constructor
  MutipleImageModel({required this.image, required this.displayOrder});

  @JsonKey(name: 'image')
  final Image? image;
  @JsonKey(name: 'display_order')
  final int? displayOrder;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MutipleImageModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory MutipleImageModel.fromJson(Map<String, dynamic> json) =>
      _$MutipleImageModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MutipleImageModelToJson`.
  Map<String, dynamic> toJson() => _$MutipleImageModelToJson(this);
}
