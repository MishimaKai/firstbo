// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gallery_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GalleryDetailModel _$GalleryDetailModelFromJson(Map<String, dynamic> json) =>
    GalleryDetailModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GalleryDetailModelToJson(GalleryDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int?,
      galleryDate: json['gallery_date'] as String?,
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
      title: json['title'] as String?,
      fullDesc: json['full_desc'] as String?,
      multipleImage: (json['multiple_img'] as List<dynamic>)
          .map((e) => MutipleImageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'gallery_date': instance.galleryDate,
      'image': instance.image,
      'title': instance.title,
      'full_desc': instance.fullDesc,
      'multiple_img': instance.multipleImage,
    };

MutipleImageModel _$MutipleImageModelFromJson(Map<String, dynamic> json) =>
    MutipleImageModel(
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
      displayOrder: json['display_order'] as int?,
    );

Map<String, dynamic> _$MutipleImageModelToJson(MutipleImageModel instance) =>
    <String, dynamic>{
      'image': instance.image,
      'display_order': instance.displayOrder,
    };
