import 'package:json_annotation/json_annotation.dart';

import '../image/image_model.dart';

/// This allows the `GalleryModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'gallery_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class GalleryModel {
  // constructor
  GalleryModel({
    this.id,
    this.galleryDate,
    this.image,
    this.title,
    this.fullDesc,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'gallery_date')
  final String? galleryDate;
  @JsonKey(name: 'image')
  final Image? image;
  @JsonKey(name: 'title')
  final String? title;
  @JsonKey(name: 'full_desc')
  final String? fullDesc;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$GalleryModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory GalleryModel.fromJson(Map<String, dynamic> json) =>
      _$GalleryModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$GalleryModelToJson`.
  Map<String, dynamic> toJson() => _$GalleryModelToJson(this);
}
