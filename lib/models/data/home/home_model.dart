// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../event/event_model.dart';
import '../gallery/gallery_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';
import '../package/package_model.dart';

/// This allows the `HomeModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'home_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class HomeModel {
  // constructor
  HomeModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  // variable parameters
  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$HomeModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory HomeModel.fromJson(Map<String, dynamic> json) =>
      _$HomeModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$HomeModelToJson`.
  Map<String, dynamic> toJson() => _$HomeModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.memberInfo,
    required this.totalNotifications,
    required this.upComingEvent,
    required this.bottlePackages,
    required this.foodPackages,
    required this.galleries,
    required this.activePopup,
  });

  // variable parameters
  @JsonKey(name: 'member_info')
  final MemberInfo memberInfo;
  @JsonKey(name: 'total_notification')
  final String? totalNotifications;
  @JsonKey(name: 'upcoming_events')
  final List<EventModel>? upComingEvent;
  @JsonKey(name: 'bottle_packages')
  final BottlePackages bottlePackages;
  @JsonKey(name: 'food_packages')
  final FoodPackages foodPackages;
  @JsonKey(name: 'galleries')
  final List<GalleryModel>? galleries;
  @JsonKey(name: 'active_popup')
  final dynamic activePopup;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$HomeModelToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberInfo {
  // constructor
  MemberInfo({
    required this.id,
    required this.selector,
    required this.image,
    required this.firstName,
    required this.lastName,
    required this.membershipName,
    required this.totalVoucher,
    required this.totalPoint,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'selector')
  final String selector;
  @JsonKey(name: 'image')
  final Image image;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'membership_name')
  final String membershipName;
  @JsonKey(name: 'total_voucher')
  final int totalVoucher;
  @JsonKey(name: 'total_point')
  final int totalPoint;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberInfoFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory MemberInfo.fromJson(Map<String, dynamic> json) =>
      _$MemberInfoFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberInfoToJson`.
  Map<String, dynamic> toJson() => _$MemberInfoToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class BottlePackages {
  // constructor
  BottlePackages({required this.type, required this.data});

  // variable parameters
  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'data')
  final List<PackageModel>? data;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$BottlePackagesFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory BottlePackages.fromJson(Map<String, dynamic> json) =>
      _$BottlePackagesFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$BottlePackagesToJson`.
  Map<String, dynamic> toJson() => _$BottlePackagesToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class FoodPackages {
  // constructor
  FoodPackages({required this.type, required this.data});

  // variable parameters
  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'data')
  final List<PackageModel>? data;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$FoodPackagesFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory FoodPackages.fromJson(Map<String, dynamic> json) =>
      _$FoodPackagesFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$FoodPackagesToJson`.
  Map<String, dynamic> toJson() => _$FoodPackagesToJson(this);
}
