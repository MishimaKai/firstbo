// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeModel _$HomeModelFromJson(Map<String, dynamic> json) => HomeModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$HomeModelToJson(HomeModel instance) => <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      memberInfo:
          MemberInfo.fromJson(json['member_info'] as Map<String, dynamic>),
      totalNotifications: json['total_notification'] as String?,
      upComingEvent: (json['upcoming_events'] as List<dynamic>?)
          ?.map((e) => EventModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      bottlePackages: BottlePackages.fromJson(
          json['bottle_packages'] as Map<String, dynamic>),
      foodPackages:
          FoodPackages.fromJson(json['food_packages'] as Map<String, dynamic>),
      galleries: (json['galleries'] as List<dynamic>?)
          ?.map((e) => GalleryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      activePopup: json['active_popup'],
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'member_info': instance.memberInfo,
      'total_notification': instance.totalNotifications,
      'upcoming_events': instance.upComingEvent,
      'bottle_packages': instance.bottlePackages,
      'food_packages': instance.foodPackages,
      'galleries': instance.galleries,
      'active_popup': instance.activePopup,
    };

MemberInfo _$MemberInfoFromJson(Map<String, dynamic> json) => MemberInfo(
      id: json['id'] as int,
      selector: json['selector'] as String,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      membershipName: json['membership_name'] as String,
      totalVoucher: json['total_voucher'] as int,
      totalPoint: json['total_point'] as int,
    );

Map<String, dynamic> _$MemberInfoToJson(MemberInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'selector': instance.selector,
      'image': instance.image,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'membership_name': instance.membershipName,
      'total_voucher': instance.totalVoucher,
      'total_point': instance.totalPoint,
    };

BottlePackages _$BottlePackagesFromJson(Map<String, dynamic> json) =>
    BottlePackages(
      type: json['type'] as int,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => PackageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BottlePackagesToJson(BottlePackages instance) =>
    <String, dynamic>{
      'type': instance.type,
      'data': instance.data,
    };

FoodPackages _$FoodPackagesFromJson(Map<String, dynamic> json) => FoodPackages(
      type: json['type'] as int,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => PackageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FoodPackagesToJson(FoodPackages instance) =>
    <String, dynamic>{
      'type': instance.type,
      'data': instance.data,
    };
