import 'package:json_annotation/json_annotation.dart';

/// This allows the `Image` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'image_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Image {
  // constructor
  Image({
    required this.original,
    required this.thumb150x150,
    required this.thumb300x300,
  });

  // variable parameters
  @JsonKey(name: 'original')
  final String? original;
  @JsonKey(name: 'thumb-150x150')
  final String? thumb150x150;
  @JsonKey(name: 'thumb-300x300')
  final String? thumb300x300;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ImageFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory Image.fromJson(Map<String, dynamic> json) => _$ImageFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ImageToJson`.
  Map<String, dynamic> toJson() => _$ImageToJson(this);
}
