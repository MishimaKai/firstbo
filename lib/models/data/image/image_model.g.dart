// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Image _$ImageFromJson(Map<String, dynamic> json) => Image(
      original: json['original'] as String?,
      thumb150x150: json['thumb-150x150'] as String?,
      thumb300x300: json['thumb-300x300'] as String?,
    );

Map<String, dynamic> _$ImageToJson(Image instance) => <String, dynamic>{
      'original': instance.original,
      'thumb-150x150': instance.thumb150x150,
      'thumb-300x300': instance.thumb300x300,
    };
