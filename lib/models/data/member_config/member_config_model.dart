import 'package:json_annotation/json_annotation.dart';

/// This allows the `MemberConfig` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'member_config_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberConfigModel {
  // constructor
  MemberConfigModel({required this.fcMtopic, required this.disableQRAccess});

  // variable parameters
  @JsonKey(name: 'fcm_topic')
  final List<String> fcMtopic;
  @JsonKey(name: 'disabled_qr_access')
  final int disableQRAccess;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberConfigModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory MemberConfigModel.fromJson(Map<String, dynamic> json) =>
      _$MemberConfigModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberConfigModelToJson`.
  Map<String, dynamic> toJson() => _$MemberConfigModelToJson(this);
}
