// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_config_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberConfigModel _$MemberConfigModelFromJson(Map<String, dynamic> json) =>
    MemberConfigModel(
      fcMtopic:
          (json['fcm_topic'] as List<dynamic>).map((e) => e as String).toList(),
      disableQRAccess: json['disabled_qr_access'] as int,
    );

Map<String, dynamic> _$MemberConfigModelToJson(MemberConfigModel instance) =>
    <String, dynamic>{
      'fcm_topic': instance.fcMtopic,
      'disabled_qr_access': instance.disableQRAccess,
    };
