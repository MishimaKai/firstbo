// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../company_info/company_info_model.dart';
import '../device/device_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `MemberDetailModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'member_detail_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberDetailModel {
  // constructor
  MemberDetailModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberDetailModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory MemberDetailModel.fromJson(Map<String, dynamic> json) =>
      _$MemberDetailModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberDetailModelToJson`.
  Map<String, dynamic> toJson() => _$MemberDetailModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.id,
    required this.selector,
    required this.image,
    required this.countryCode,
    required this.phone,
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.dob,
    required this.gender,
    required this.genderName,
    required this.city,
    required this.cityName,
    required this.province,
    required this.provinceName,
    required this.address,
    required this.verified,
    required this.verifiedReqest,
    required this.resetDeviceRequest,
    required this.membership,
    required this.membershipName,
    required this.memberBenefit,
    required this.companyInfo,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'selector')
  final String selector;
  @JsonKey(name: 'image')
  final Image image;
  @JsonKey(name: 'country_code')
  final String countryCode;
  @JsonKey(name: 'phone')
  final String phone;
  @JsonKey(name: 'email')
  final String email;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'dob')
  final String dob;
  @JsonKey(name: 'gender')
  final int gender;
  @JsonKey(name: 'gender_name')
  final String genderName;
  @JsonKey(name: 'city')
  final int city;
  @JsonKey(name: 'city_name')
  final String cityName;
  @JsonKey(name: 'province')
  final int province;
  @JsonKey(name: 'province_name')
  final String provinceName;
  @JsonKey(name: 'address')
  final String address;
  @JsonKey(name: 'verified')
  final int verified;
  @JsonKey(name: 'verified_request')
  final int verifiedReqest;
  @JsonKey(name: 'reset_device_request')
  final int resetDeviceRequest;
  @JsonKey(name: 'membership')
  final int membership;
  @JsonKey(name: 'membership_name')
  final String membershipName;
  @JsonKey(name: 'membership_benefit')
  final List<String?> memberBenefit;
  @JsonKey(name: 'company_info')
  final CompanyInfoModel companyInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}
