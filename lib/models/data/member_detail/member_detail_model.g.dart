// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberDetailModel _$MemberDetailModelFromJson(Map<String, dynamic> json) =>
    MemberDetailModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberDetailModelToJson(MemberDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int,
      selector: json['selector'] as String,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
      countryCode: json['country_code'] as String,
      phone: json['phone'] as String,
      email: json['email'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      dob: json['dob'] as String,
      gender: json['gender'] as int,
      genderName: json['gender_name'] as String,
      city: json['city'] as int,
      cityName: json['city_name'] as String,
      province: json['province'] as int,
      provinceName: json['province_name'] as String,
      address: json['address'] as String,
      verified: json['verified'] as int,
      verifiedReqest: json['verified_request'] as int,
      resetDeviceRequest: json['reset_device_request'] as int,
      membership: json['membership'] as int,
      membershipName: json['membership_name'] as String,
      memberBenefit: (json['membership_benefit'] as List<dynamic>)
          .map((e) => e as String?)
          .toList(),
      companyInfo: CompanyInfoModel.fromJson(
          json['company_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'selector': instance.selector,
      'image': instance.image,
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'dob': instance.dob,
      'gender': instance.gender,
      'gender_name': instance.genderName,
      'city': instance.city,
      'city_name': instance.cityName,
      'province': instance.province,
      'province_name': instance.provinceName,
      'address': instance.address,
      'verified': instance.verified,
      'verified_request': instance.verifiedReqest,
      'reset_device_request': instance.resetDeviceRequest,
      'membership': instance.membership,
      'membership_name': instance.membershipName,
      'membership_benefit': instance.memberBenefit,
      'company_info': instance.companyInfo,
    };
