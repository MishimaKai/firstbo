// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `MemberHomeAccountInfoModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'member_home_account_info_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberHomeAccountInfoModel {
  // constructor
  MemberHomeAccountInfoModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  // variable parameters
  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberHomeAccountInfoModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory MemberHomeAccountInfoModel.fromJson(Map<String, dynamic> json) =>
      _$MemberHomeAccountInfoModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberHomeAccountInfoModelToJson`.
  Map<String, dynamic> toJson() => _$MemberHomeAccountInfoModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.id,
    required this.selector,
    required this.image,
    required this.firstName,
    required this.lastName,
    required this.membershipImage,
    required this.membershipGroupName,
    required this.membershipName,
    required this.totalVoucher,
    required this.totalPoint,
    required this.totalNotification,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'selector')
  final String selector;
  @JsonKey(name: 'image')
  final Image image;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'membership_image')
  final String membershipImage;
  @JsonKey(name: 'membership_group_name')
  final String membershipGroupName;
  @JsonKey(name: 'membership_name')
  final String membershipName;
  @JsonKey(name: 'total_voucher')
  final int totalVoucher;
  @JsonKey(name: 'total_point')
  final int totalPoint;
  @JsonKey(name: 'total_notification')
  final String? totalNotification;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataModelToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}
