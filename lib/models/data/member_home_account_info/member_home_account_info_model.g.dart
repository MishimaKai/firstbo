// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_home_account_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberHomeAccountInfoModel _$MemberHomeAccountInfoModelFromJson(
        Map<String, dynamic> json) =>
    MemberHomeAccountInfoModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberHomeAccountInfoModelToJson(
        MemberHomeAccountInfoModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int,
      selector: json['selector'] as String,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      membershipImage: json['membership_image'] as String,
      membershipGroupName: json['membership_group_name'] as String,
      membershipName: json['membership_name'] as String,
      totalVoucher: json['total_voucher'] as int,
      totalPoint: json['total_point'] as int,
      totalNotification: json['total_notification'] as String?,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'selector': instance.selector,
      'image': instance.image,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'membership_image': instance.membershipImage,
      'membership_group_name': instance.membershipGroupName,
      'membership_name': instance.membershipName,
      'total_voucher': instance.totalVoucher,
      'total_point': instance.totalPoint,
      'total_notification': instance.totalNotification,
    };
