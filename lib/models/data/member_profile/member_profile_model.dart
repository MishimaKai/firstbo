// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../company_info/company_info_model.dart';
import '../device/device_model.dart';
import '../member_config/member_config_model.dart';
import '../membership_rank/membership_rank_model.dart';
import '../my_membership/my_membership_model.dart';
import '../reasons/reason_model.dart';

part 'member_profile_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberProfileModel {
  // constructor
  MemberProfileModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  // variable parameters
  @JsonKey(name: 'data')
  final MemberProfileDataModel data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberProfileModelFromJson()` constructor.

  factory MemberProfileModel.fromJson(Map<String, dynamic> json) =>
      _$MemberProfileModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberProfileModelToJson`.

  Map<String, dynamic> toJson() => _$MemberProfileModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MemberProfileDataModel {
  // constructor
  MemberProfileDataModel({
    required this.id,
    required this.selector,
    required this.countryCode,
    required this.phone,
    required this.firstName,
    required this.lastName,
    required this.displayName,
    required this.resetDeviceRequest,
    required this.membershipId,
    required this.membershipName,
    required this.myMembership,
    required this.membershipRank,
    required this.companyInfo,
    required this.resetDevice,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'selector')
  final String selector;
  @JsonKey(name: 'country_code')
  final String countryCode;
  @JsonKey(name: 'phone')
  final String phone;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'display_name')
  final String displayName;
  @JsonKey(name: 'reset_device_request')
  final int resetDeviceRequest;
  @JsonKey(name: 'membership_id')
  final int membershipId;
  @JsonKey(name: 'membership_name')
  final String membershipName;
  @JsonKey(name: 'my_membership')
  final MyMembershipModel? myMembership;
  @JsonKey(name: 'membership_rank')
  final List<MembershipRankItemModel?> membershipRank;
  @JsonKey(name: 'company_info')
  final CompanyInfoModel companyInfo;
  @JsonKey(name: 'reset_device')
  final ResetDeviceProfileModel resetDevice;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberProfileDataModelFromJson()` constructor.

  factory MemberProfileDataModel.fromJson(Map<String, dynamic> json) =>
      _$MemberProfileDataModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberProfileDataModelToJson`.

  Map<String, dynamic> toJson() => _$MemberProfileDataModelToJson(this);
}

@JsonSerializable()
class ResetDeviceProfileModel {
  ResetDeviceProfileModel({this.reasonDataSelect, required this.attention});

  factory ResetDeviceProfileModel.fromJson(Map<String, dynamic> json) =>
      _$ResetDeviceProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResetDeviceProfileModelToJson(this);

  @JsonKey(name: 'reason_data_select')
  final List<ReasonDataSelectModel>? reasonDataSelect;
  @JsonKey(name: 'attention_text')
  final String attention;
}
