// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'member_profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MemberProfileModel _$MemberProfileModelFromJson(Map<String, dynamic> json) =>
    MemberProfileModel(
      data:
          MemberProfileDataModel.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberProfileModelToJson(MemberProfileModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

MemberProfileDataModel _$MemberProfileDataModelFromJson(
        Map<String, dynamic> json) =>
    MemberProfileDataModel(
      id: json['id'] as int,
      selector: json['selector'] as String,
      countryCode: json['country_code'] as String,
      phone: json['phone'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      displayName: json['display_name'] as String,
      resetDeviceRequest: json['reset_device_request'] as int,
      membershipId: json['membership_id'] as int,
      membershipName: json['membership_name'] as String,
      myMembership: json['my_membership'] == null
          ? null
          : MyMembershipModel.fromJson(
              json['my_membership'] as Map<String, dynamic>),
      membershipRank: (json['membership_rank'] as List<dynamic>)
          .map((e) => e == null
              ? null
              : MembershipRankItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      companyInfo: CompanyInfoModel.fromJson(
          json['company_info'] as Map<String, dynamic>),
      resetDevice: ResetDeviceProfileModel.fromJson(
          json['reset_device'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MemberProfileDataModelToJson(
        MemberProfileDataModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'selector': instance.selector,
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'display_name': instance.displayName,
      'reset_device_request': instance.resetDeviceRequest,
      'membership_id': instance.membershipId,
      'membership_name': instance.membershipName,
      'my_membership': instance.myMembership,
      'membership_rank': instance.membershipRank,
      'company_info': instance.companyInfo,
      'reset_device': instance.resetDevice,
    };

ResetDeviceProfileModel _$ResetDeviceProfileModelFromJson(
        Map<String, dynamic> json) =>
    ResetDeviceProfileModel(
      reasonDataSelect: (json['reason_data_select'] as List<dynamic>?)
          ?.map(
              (e) => ReasonDataSelectModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      attention: json['attention_text'] as String,
    );

Map<String, dynamic> _$ResetDeviceProfileModelToJson(
        ResetDeviceProfileModel instance) =>
    <String, dynamic>{
      'reason_data_select': instance.reasonDataSelect,
      'attention_text': instance.attention,
    };
