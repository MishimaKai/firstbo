// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

part 'membership_rank_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MembershipRankItemModel {
  // constructor
  MembershipRankItemModel({
    required this.image,
    required this.name,
    required this.benefit,
    required this.tnc,
    required this.rank,
    required this.isActive,
  });

  @JsonKey(name: 'image')
  final String image;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'benefit')
  final String benefit;
  @JsonKey(name: 'tnc')
  final String tnc;
  @JsonKey(name: 'rank')
  final int rank;
  @JsonKey(name: 'is_active')
  final int isActive;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MembershipRankItemModelFromJson()` constructor.

  factory MembershipRankItemModel.fromJson(Map<String, dynamic> json) =>
      _$MembershipRankItemModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MembershipRankItemModelToJson`.

  Map<String, dynamic> toJson() => _$MembershipRankItemModelToJson(this);
}
