// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'membership_rank_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MembershipRankItemModel _$MembershipRankItemModelFromJson(
        Map<String, dynamic> json) =>
    MembershipRankItemModel(
      image: json['image'] as String,
      name: json['name'] as String,
      benefit: json['benefit'] as String,
      tnc: json['tnc'] as String,
      rank: json['rank'] as int,
      isActive: json['is_active'] as int,
    );

Map<String, dynamic> _$MembershipRankItemModelToJson(
        MembershipRankItemModel instance) =>
    <String, dynamic>{
      'image': instance.image,
      'name': instance.name,
      'benefit': instance.benefit,
      'tnc': instance.tnc,
      'rank': instance.rank,
      'is_active': instance.isActive,
    };
