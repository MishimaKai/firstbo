// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meta_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MetaModel _$MetaModelFromJson(Map<String, dynamic> json) => MetaModel(
      page: json['page'] as int,
      perPage: json['per_page'] as int,
      count: json['count'] as int,
      totalPage: json['total_page'] as int,
      totalCount: json['total_count'] as int,
      links: json['links'] == null
          ? null
          : Links.fromJson(json['links'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MetaModelToJson(MetaModel instance) => <String, dynamic>{
      'page': instance.page,
      'per_page': instance.perPage,
      'count': instance.count,
      'total_page': instance.totalPage,
      'total_count': instance.totalCount,
      'links': instance.links,
    };

Links _$LinksFromJson(Map<String, dynamic> json) => Links(
      first: json['first'] as String?,
      prev: json['prev'] as String?,
      self: json['self'] as String?,
      next: json['next'] as String?,
      last: json['last'] as String?,
    );

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'first': instance.first,
      'prev': instance.prev,
      'self': instance.self,
      'next': instance.next,
      'last': instance.last,
    };
