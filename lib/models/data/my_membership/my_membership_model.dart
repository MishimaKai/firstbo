// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../spending_power/spending_power_model.dart';
import '../total_spending/total_spending_model.dart';

part 'my_membership_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class MyMembershipModel {
  // constructor
  MyMembershipModel({
    required this.currentMembership,
    required this.nextMembership,
    required this.isReachedTransaction,
    required this.currentTransaction,
    required this.minTransaction,
    required this.spendingPowser,
    required this.totalSpending,
  });

  // variable parameters
  @JsonKey(name: 'current_membership')
  final String currentMembership;
  @JsonKey(name: 'next_membership')
  final String? nextMembership;
  @JsonKey(name: 'is_reached_trx')
  final int isReachedTransaction;
  @JsonKey(name: 'current_trx')
  final int currentTransaction;
  @JsonKey(name: 'min_trx')
  final int minTransaction;
  @JsonKey(name: 'spending_power')
  final SpendingPowerModel spendingPowser;
  @JsonKey(name: 'total_spending')
  final TotalSpendingModel totalSpending;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MemberProfileModelFromJson()` constructor.

  factory MyMembershipModel.fromJson(Map<String, dynamic> json) =>
      _$MyMembershipModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MemberProfileModelToJson`.

  Map<String, dynamic> toJson() => _$MyMembershipModelToJson(this);
}
