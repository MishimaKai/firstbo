// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_membership_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyMembershipModel _$MyMembershipModelFromJson(Map<String, dynamic> json) =>
    MyMembershipModel(
      currentMembership: json['current_membership'] as String,
      nextMembership: json['next_membership'] as String?,
      isReachedTransaction: json['is_reached_trx'] as int,
      currentTransaction: json['current_trx'] as int,
      minTransaction: json['min_trx'] as int,
      spendingPowser: SpendingPowerModel.fromJson(
          json['spending_power'] as Map<String, dynamic>),
      totalSpending: TotalSpendingModel.fromJson(
          json['total_spending'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MyMembershipModelToJson(MyMembershipModel instance) =>
    <String, dynamic>{
      'current_membership': instance.currentMembership,
      'next_membership': instance.nextMembership,
      'is_reached_trx': instance.isReachedTransaction,
      'current_trx': instance.currentTransaction,
      'min_trx': instance.minTransaction,
      'spending_power': instance.spendingPowser,
      'total_spending': instance.totalSpending,
    };
