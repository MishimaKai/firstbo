import 'package:json_annotation/json_annotation.dart';

/// This allows the `NotificationModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'notification_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class NotificationModel {
  // constructor
  NotificationModel({
    required this.id,
    required this.topic,
    required this.title,
    required this.subtitle,
    required this.refTable,
    required this.refTableId,
    required this.seenAt,
    required this.createdAt,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'topic')
  final String topic;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'subtitle')
  final String subtitle;
  @JsonKey(name: 'ref_table')
  final String refTable;
  @JsonKey(name: 'ref_table_id')
  final int refTableId;
  @JsonKey(name: 'seen_ta')
  final String? seenAt;
  @JsonKey(name: 'created_at')
  final String createdAt;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$NotificationModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      _$NotificationModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$NotificationModelToJson`.
  Map<String, dynamic> toJson() => _$NotificationModelToJson(this);
}
