// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationModel _$NotificationModelFromJson(Map<String, dynamic> json) =>
    NotificationModel(
      id: json['id'] as int,
      topic: json['topic'] as String,
      title: json['title'] as String,
      subtitle: json['subtitle'] as String,
      refTable: json['ref_table'] as String,
      refTableId: json['ref_table_id'] as int,
      seenAt: json['seen_ta'] as String?,
      createdAt: json['created_at'] as String,
    );

Map<String, dynamic> _$NotificationModelToJson(NotificationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'topic': instance.topic,
      'title': instance.title,
      'subtitle': instance.subtitle,
      'ref_table': instance.refTable,
      'ref_table_id': instance.refTableId,
      'seen_ta': instance.seenAt,
      'created_at': instance.createdAt,
    };
