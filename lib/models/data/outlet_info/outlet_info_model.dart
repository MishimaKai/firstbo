import 'package:json_annotation/json_annotation.dart';

/// This allows the `OutletInfo` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'outlet_info_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class OutletInfoModel {
  // constructor
  OutletInfoModel({
    required this.id,
    required this.selector,
    required this.name,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'selector')
  final String selector;
  @JsonKey(name: 'name')
  final String name;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$OutletInfoFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory OutletInfoModel.fromJson(Map<String, dynamic> json) =>
      _$OutletInfoModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$OutletInfoToJson`.
  Map<String, dynamic> toJson() => _$OutletInfoModelToJson(this);
}
