// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutletInfoModel _$OutletInfoModelFromJson(Map<String, dynamic> json) =>
    OutletInfoModel(
      id: json['id'] as int,
      selector: json['selector'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$OutletInfoModelToJson(OutletInfoModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'selector': instance.selector,
      'name': instance.name,
    };
