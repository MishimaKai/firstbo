// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PackageDetailModel _$PackageDetailModelFromJson(Map<String, dynamic> json) =>
    PackageDetailModel(
      data: PackageModel.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PackageDetailModelToJson(PackageDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
