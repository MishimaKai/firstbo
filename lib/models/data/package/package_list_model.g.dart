// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PackageListModel _$PackageListModelFromJson(Map<String, dynamic> json) =>
    PackageListModel(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => PackageModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      meta: MetaModel.fromJson(json['meta'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PackageListModelToJson(PackageListModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'meta': instance.meta,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
