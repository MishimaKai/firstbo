import 'package:json_annotation/json_annotation.dart';

import '../image/image_model.dart';
import '../outlet_info/outlet_info_model.dart';

/// This allows the `PackageModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'package_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PackageModel {
  // constructor
  PackageModel({
    this.id,
    this.type,
    this.title,
    this.fullDesc,
    this.price,
    this.image,
    this.startDate,
    this.endDate,
    this.outletInfo,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'type')
  final int? type;
  @JsonKey(name: 'title')
  final String? title;
  @JsonKey(name: 'full_desc')
  final String? fullDesc;
  @JsonKey(name: 'price')
  final int? price;
  @JsonKey(name: 'image')
  final Image? image;
  @JsonKey(name: 'start_date')
  final String? startDate;
  @JsonKey(name: 'end_date')
  final String? endDate;
  @JsonKey(name: 'outlet_info')
  final List<OutletInfoModel>? outletInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PackageModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory PackageModel.fromJson(Map<String, dynamic> json) =>
      _$PackageModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PackageModelToJson`.
  Map<String, dynamic> toJson() => _$PackageModelToJson(this);
}
