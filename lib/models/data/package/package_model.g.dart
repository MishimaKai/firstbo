// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PackageModel _$PackageModelFromJson(Map<String, dynamic> json) => PackageModel(
      id: json['id'] as int?,
      type: json['type'] as int?,
      title: json['title'] as String?,
      fullDesc: json['full_desc'] as String?,
      price: json['price'] as int?,
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
      startDate: json['start_date'] as String?,
      endDate: json['end_date'] as String?,
      outletInfo: (json['outlet_info'] as List<dynamic>?)
          ?.map((e) => OutletInfoModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PackageModelToJson(PackageModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'title': instance.title,
      'full_desc': instance.fullDesc,
      'price': instance.price,
      'image': instance.image,
      'start_date': instance.startDate,
      'end_date': instance.endDate,
      'outlet_info': instance.outletInfo,
    };
