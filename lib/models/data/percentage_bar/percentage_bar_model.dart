import 'package:json_annotation/json_annotation.dart';

part 'percentage_bar_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PercentageBarModel {
  // constructor
  PercentageBarModel({
    required this.min,
    required this.current,
    required this.max,
  });

  @JsonKey(name: 'min')
  final double min;
  @JsonKey(name: 'curr')
  final double current;
  @JsonKey(name: 'max')
  final double max;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PercentageBarModelFromJson()` constructor.

  // ignore: sort_constructors_first
  factory PercentageBarModel.fromJson(Map<String, dynamic> json) =>
      _$PercentageBarModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PercentageBarModelToJson`.

  Map<String, dynamic> toJson() => _$PercentageBarModelToJson(this);
}
