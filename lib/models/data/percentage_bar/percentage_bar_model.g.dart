// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'percentage_bar_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PercentageBarModel _$PercentageBarModelFromJson(Map<String, dynamic> json) =>
    PercentageBarModel(
      min: (json['min'] as num).toDouble(),
      current: (json['curr'] as num).toDouble(),
      max: (json['max'] as num).toDouble(),
    );

Map<String, dynamic> _$PercentageBarModelToJson(PercentageBarModel instance) =>
    <String, dynamic>{
      'min': instance.min,
      'curr': instance.current,
      'max': instance.max,
    };
