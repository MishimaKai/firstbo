// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `PointsDetailModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'points_detail_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PointsDetailModel {
  // constructor
  PointsDetailModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PointsDetailModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PointsDetailModel.fromJson(Map<String, dynamic> json) =>
      _$PointsDetailModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PointsDetailModelToJson`.
  Map<String, dynamic> toJson() => _$PointsDetailModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.id,
    required this.deductedPoint,
    required this.status,
    required this.statusName,
    required this.statusProcess,
    required this.note,
    required this.noteAt,
    required this.item,
    required this.pointCreatedAt,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'deducted_point')
  final int deductedPoint;
  @JsonKey(name: 'status')
  final int status;
  @JsonKey(name: 'status_name')
  final String statusName;
  @JsonKey(name: 'status_process')
  final List<StatusProcess>? statusProcess;
  @JsonKey(name: 'note')
  final String? note;
  @JsonKey(name: 'note_at')
  final String? noteAt;
  @JsonKey(name: 'item')
  final Item item;
  @JsonKey(name: 'point_created_at')
  final String pointCreatedAt;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class StatusProcess {
  // constructor
  StatusProcess({
    required this.name,
    required this.status,
  });

  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'status')
  final int status;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$StatusProcessFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory StatusProcess.fromJson(Map<String, dynamic> json) =>
      _$StatusProcessFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$StatusProcessToJson`.
  Map<String, dynamic> toJson() => _$StatusProcessToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Item {
  // constructor
  Item({
    required this.id,
    required this.name,
    required this.fullDesc,
    required this.amount,
    required this.image,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'full_desc')
  final String fullDesc;
  @JsonKey(name: 'amount')
  final int amount;
  @JsonKey(name: 'image')
  final Image image;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ItemFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ItemToJson`.
  Map<String, dynamic> toJson() => _$ItemToJson(this);
}
