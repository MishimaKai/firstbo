// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'points_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointsDetailModel _$PointsDetailModelFromJson(Map<String, dynamic> json) =>
    PointsDetailModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PointsDetailModelToJson(PointsDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int,
      deductedPoint: json['deducted_point'] as int,
      status: json['status'] as int,
      statusName: json['status_name'] as String,
      statusProcess: (json['status_process'] as List<dynamic>?)
          ?.map((e) => StatusProcess.fromJson(e as Map<String, dynamic>))
          .toList(),
      note: json['note'] as String?,
      noteAt: json['note_at'] as String?,
      item: Item.fromJson(json['item'] as Map<String, dynamic>),
      pointCreatedAt: json['point_created_at'] as String,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'deducted_point': instance.deductedPoint,
      'status': instance.status,
      'status_name': instance.statusName,
      'status_process': instance.statusProcess,
      'note': instance.note,
      'note_at': instance.noteAt,
      'item': instance.item,
      'point_created_at': instance.pointCreatedAt,
    };

StatusProcess _$StatusProcessFromJson(Map<String, dynamic> json) =>
    StatusProcess(
      name: json['name'] as String,
      status: json['status'] as int,
    );

Map<String, dynamic> _$StatusProcessToJson(StatusProcess instance) =>
    <String, dynamic>{
      'name': instance.name,
      'status': instance.status,
    };

Item _$ItemFromJson(Map<String, dynamic> json) => Item(
      id: json['id'] as int,
      name: json['name'] as String,
      fullDesc: json['full_desc'] as String,
      amount: json['amount'] as int,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ItemToJson(Item instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'full_desc': instance.fullDesc,
      'amount': instance.amount,
      'image': instance.image,
    };
