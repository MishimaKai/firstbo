import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../member_config/member_config_model.dart';
import 'points_meta_model.dart';
import 'points_model.dart';

/// This allows the `PointListModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'points_list_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PointListModel {
  // constructor
  PointListModel({
    required this.data,
    required this.meta,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final List<PointsModel>? data;
  @JsonKey(name: 'meta')
  final PointMetaModel meta;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PointListModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory PointListModel.fromJson(Map<String, dynamic> json) =>
      _$PointListModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PointListModelToJson`.
  Map<String, dynamic> toJson() => _$PointListModelToJson(this);
}
