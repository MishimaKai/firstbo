// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'points_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointListModel _$PointListModelFromJson(Map<String, dynamic> json) =>
    PointListModel(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => PointsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      meta: PointMetaModel.fromJson(json['meta'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PointListModelToJson(PointListModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'meta': instance.meta,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
