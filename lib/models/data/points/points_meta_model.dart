// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

/// This allows the `PointMetaModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'points_meta_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PointMetaModel {
  // constructor
  PointMetaModel({
    required this.search,
    required this.page,
    required this.perPage,
    required this.count,
    required this.totalPage,
    required this.totalCount,
    required this.links,
  });

  @JsonKey(name: 'search')
  final SearchModel search;
  @JsonKey(name: 'page')
  final int page;
  @JsonKey(name: 'per_page')
  final int perPage;
  @JsonKey(name: 'count')
  final int count;
  @JsonKey(name: 'total_page')
  final int totalPage;
  @JsonKey(name: 'total_count')
  final int totalCount;
  @JsonKey(name: 'links')
  final Links? links;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PointMetaModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PointMetaModel.fromJson(Map<String, dynamic> json) =>
      _$PointMetaModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PointMetaModelToJson`.
  Map<String, dynamic> toJson() => _$PointMetaModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class SearchModel {
  // constructor
  SearchModel({
    required this.fromDate,
    required this.toDate,
  });

  @JsonKey(name: 'from_date')
  final String fromDate;
  @JsonKey(name: 'to_date')
  final String toDate;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$SearchModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory SearchModel.fromJson(Map<String, dynamic> json) =>
      _$SearchModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$SearchModelToJson`.
  Map<String, dynamic> toJson() => _$SearchModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Links {
  // constructor
  Links({
    required this.first,
    required this.prev,
    required this.self,
    required this.next,
    required this.last,
  });

  @JsonKey(name: 'first')
  final String? first;
  @JsonKey(name: 'prev')
  final String? prev;
  @JsonKey(name: 'self')
  final String? self;
  @JsonKey(name: 'next')
  final String? next;
  @JsonKey(name: 'last')
  final String? last;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$LinksFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$LinksToJson`.
  Map<String, dynamic> toJson() => _$LinksToJson(this);
}
