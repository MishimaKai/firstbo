// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'points_meta_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointMetaModel _$PointMetaModelFromJson(Map<String, dynamic> json) =>
    PointMetaModel(
      search: SearchModel.fromJson(json['search'] as Map<String, dynamic>),
      page: json['page'] as int,
      perPage: json['per_page'] as int,
      count: json['count'] as int,
      totalPage: json['total_page'] as int,
      totalCount: json['total_count'] as int,
      links: json['links'] == null
          ? null
          : Links.fromJson(json['links'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PointMetaModelToJson(PointMetaModel instance) =>
    <String, dynamic>{
      'search': instance.search,
      'page': instance.page,
      'per_page': instance.perPage,
      'count': instance.count,
      'total_page': instance.totalPage,
      'total_count': instance.totalCount,
      'links': instance.links,
    };

SearchModel _$SearchModelFromJson(Map<String, dynamic> json) => SearchModel(
      fromDate: json['from_date'] as String,
      toDate: json['to_date'] as String,
    );

Map<String, dynamic> _$SearchModelToJson(SearchModel instance) =>
    <String, dynamic>{
      'from_date': instance.fromDate,
      'to_date': instance.toDate,
    };

Links _$LinksFromJson(Map<String, dynamic> json) => Links(
      first: json['first'] as String?,
      prev: json['prev'] as String?,
      self: json['self'] as String?,
      next: json['next'] as String?,
      last: json['last'] as String?,
    );

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'first': instance.first,
      'prev': instance.prev,
      'self': instance.self,
      'next': instance.next,
      'last': instance.last,
    };
