import 'package:json_annotation/json_annotation.dart';

/// This allows the `PointsModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'points_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PointsModel {
  // constructor
  PointsModel({
    required this.id,
    required this.refTable,
    required this.refTableId,
    required this.amount,
    required this.title,
    required this.description,
    required this.expiredAt,
    required this.createdAt,
    required this.viewDetail,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'ref_table')
  final String? refTable;
  @JsonKey(name: 'ref_table_id')
  final int? refTableId;
  @JsonKey(name: 'amount')
  final int? amount;
  @JsonKey(name: 'title')
  final String? title;
  @JsonKey(name: 'description')
  final String? description;
  @JsonKey(name: 'expired_at')
  final String? expiredAt;
  @JsonKey(name: 'created_at')
  final String? createdAt;
  @JsonKey(name: 'view_detail')
  final int? viewDetail;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PointsModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory PointsModel.fromJson(Map<String, dynamic> json) =>
      _$PointsModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PointsModelToJson`.
  Map<String, dynamic> toJson() => _$PointsModelToJson(this);
}
