// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'points_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointsModel _$PointsModelFromJson(Map<String, dynamic> json) => PointsModel(
      id: json['id'] as int,
      refTable: json['ref_table'] as String?,
      refTableId: json['ref_table_id'] as int?,
      amount: json['amount'] as int?,
      title: json['title'] as String?,
      description: json['description'] as String?,
      expiredAt: json['expired_at'] as String?,
      createdAt: json['created_at'] as String?,
      viewDetail: json['view_detail'] as int?,
    );

Map<String, dynamic> _$PointsModelToJson(PointsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'ref_table': instance.refTable,
      'ref_table_id': instance.refTableId,
      'amount': instance.amount,
      'title': instance.title,
      'description': instance.description,
      'expired_at': instance.expiredAt,
      'created_at': instance.createdAt,
      'view_detail': instance.viewDetail,
    };
