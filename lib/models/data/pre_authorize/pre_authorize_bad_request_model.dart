// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

/// This allows the `PreAuthorizeBadRequestModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'pre_authorize_bad_request_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PreAuthorizeBadRequestModel {
  // constructor
  PreAuthorizeBadRequestModel({required this.message, required this.error});

  // variable parameters
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'error')
  final Error error;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PreAuthorizeBadRequestModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PreAuthorizeBadRequestModel.fromJson(Map<String, dynamic> json) =>
      _$PreAuthorizeBadRequestModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PreAuthorizeBadRequestModelToJson`.
  Map<String, dynamic> toJson() => _$PreAuthorizeBadRequestModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Error {
  // constructor
  Error({required this.code, required this.message});

  // variable parameters
  @JsonKey(name: 'code')
  final int code;
  @JsonKey(name: 'message')
  final Message message;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ErrorFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ErrorToJson`.
  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Message {
  // constructor
  Message({required this.memberAccess});

  @JsonKey(name: 'member_access')
  final List<String> memberAccess;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MessageFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MessageToJson`.
  Map<String, dynamic> toJson() => _$MessageToJson(this);
}
