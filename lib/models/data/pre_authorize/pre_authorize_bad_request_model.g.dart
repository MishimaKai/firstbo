// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pre_authorize_bad_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PreAuthorizeBadRequestModel _$PreAuthorizeBadRequestModelFromJson(
        Map<String, dynamic> json) =>
    PreAuthorizeBadRequestModel(
      message: json['message'] as String,
      error: Error.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PreAuthorizeBadRequestModelToJson(
        PreAuthorizeBadRequestModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
    };

Error _$ErrorFromJson(Map<String, dynamic> json) => Error(
      code: json['code'] as int,
      message: Message.fromJson(json['message'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      memberAccess: (json['member_access'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'member_access': instance.memberAccess,
    };
