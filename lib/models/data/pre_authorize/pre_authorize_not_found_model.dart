// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

/// This allows the `PreAuthorizeBadRequestModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'pre_authorize_not_found_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class PreAuthorizeNotFoundModel {
  // constructor
  PreAuthorizeNotFoundModel({required this.message, required this.error});

  // variable parameters
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'error')
  final Error error;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PreAuthorizeBadRequestModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory PreAuthorizeNotFoundModel.fromJson(Map<String, dynamic> json) =>
      _$PreAuthorizeNotFoundModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PreAuthorizeNotFoundModelToJson`.
  Map<String, dynamic> toJson() => _$PreAuthorizeNotFoundModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Error {
  // constructor
  Error({required this.code, required this.message});

// variable parameters
  @JsonKey(name: 'code')
  final int code;
  @JsonKey(name: 'message')
  final String message;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$PreAuthorizeBadRequestModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$PreAuthorizeNotFoundModelToJson`.
  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}
