// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pre_authorize_not_found_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PreAuthorizeNotFoundModel _$PreAuthorizeNotFoundModelFromJson(
        Map<String, dynamic> json) =>
    PreAuthorizeNotFoundModel(
      message: json['message'] as String,
      error: Error.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PreAuthorizeNotFoundModelToJson(
        PreAuthorizeNotFoundModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
    };

Error _$ErrorFromJson(Map<String, dynamic> json) => Error(
      code: json['code'] as int,
      message: json['message'] as String,
    );

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };
