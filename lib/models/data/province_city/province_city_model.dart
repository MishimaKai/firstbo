// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';

/// This allows the `ProvinceCityModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'province_city_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class ProvinceCityModel {
  // constructor
  ProvinceCityModel({
    required this.message,
    required this.data,
    required this.clientInfo,
  });

  // variable parameters
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'data')
  final List<Datum> data;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ProvinceCityModellFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory ProvinceCityModel.fromJson(Map<String, dynamic> json) =>
      _$ProvinceCityModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ProvinceCityModelToJson`.
  Map<String, dynamic> toJson() => _$ProvinceCityModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Datum {
  // constructor
  Datum({required this.id, required this.name});

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'name')
  final String name;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DatumFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Datum.fromJson(Map<String, dynamic> json) => _$DatumFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DatumFromJson`.
  Map<String, dynamic> toJson() => _$DatumToJson(this);
}
