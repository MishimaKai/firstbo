// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'province_city_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProvinceCityModel _$ProvinceCityModelFromJson(Map<String, dynamic> json) =>
    ProvinceCityModel(
      message: json['message'] as String,
      data: (json['data'] as List<dynamic>)
          .map((e) => Datum.fromJson(e as Map<String, dynamic>))
          .toList(),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProvinceCityModelToJson(ProvinceCityModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'data': instance.data,
      'client_info': instance.clientInfo,
    };

Datum _$DatumFromJson(Map<String, dynamic> json) => Datum(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$DatumToJson(Datum instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
