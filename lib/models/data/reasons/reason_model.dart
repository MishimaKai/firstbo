import 'package:json_annotation/json_annotation.dart';

part 'reason_model.g.dart';

@JsonSerializable()
class ReasonModel {
  ReasonModel({
    required this.isRequestChangeDevice,
    this.reasonDataSelect,
    required this.attention,
  });

  factory ReasonModel.fromJson(Map<String, dynamic> json) =>
      _$ReasonModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReasonModelToJson(this);

  @JsonKey(name: 'is_request_change_device')
  final int isRequestChangeDevice;
  @JsonKey(name: 'reason_data_select')
  final List<ReasonDataSelectModel>? reasonDataSelect;
  @JsonKey(name: 'attention_text')
  final String attention;
}

@JsonSerializable()
class ReasonDataSelectModel {
  ReasonDataSelectModel({
    required this.id,
    required this.title,
    required this.allowInputText,
  });

  factory ReasonDataSelectModel.fromJson(Map<String, dynamic> json) =>
      _$ReasonDataSelectModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReasonDataSelectModelToJson(this);

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'allow_input_text')
  final int allowInputText;
}
