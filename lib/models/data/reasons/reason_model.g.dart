// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reason_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReasonModel _$ReasonModelFromJson(Map<String, dynamic> json) => ReasonModel(
      isRequestChangeDevice: json['is_request_change_device'] as int,
      reasonDataSelect: (json['reason_data_select'] as List<dynamic>?)
          ?.map(
              (e) => ReasonDataSelectModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      attention: json['attention_text'] as String,
    );

Map<String, dynamic> _$ReasonModelToJson(ReasonModel instance) =>
    <String, dynamic>{
      'is_request_change_device': instance.isRequestChangeDevice,
      'reason_data_select': instance.reasonDataSelect,
      'attention_text': instance.attention,
    };

ReasonDataSelectModel _$ReasonDataSelectModelFromJson(
        Map<String, dynamic> json) =>
    ReasonDataSelectModel(
      id: json['id'] as int,
      title: json['title'] as String,
      allowInputText: json['allow_input_text'] as int,
    );

Map<String, dynamic> _$ReasonDataSelectModelToJson(
        ReasonDataSelectModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'allow_input_text': instance.allowInputText,
    };
