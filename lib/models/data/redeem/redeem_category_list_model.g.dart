// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'redeem_category_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedeemCategoryListModel _$RedeemCategoryListModelFromJson(
        Map<String, dynamic> json) =>
    RedeemCategoryListModel(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => RedeemCategoryModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RedeemCategoryListModelToJson(
        RedeemCategoryListModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
