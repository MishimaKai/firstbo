// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'redeem_category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedeemCategoryModel _$RedeemCategoryModelFromJson(Map<String, dynamic> json) =>
    RedeemCategoryModel(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$RedeemCategoryModelToJson(
        RedeemCategoryModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
