// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../client_info/client_info_model.dart';
import '../device/device_model.dart';
import '../image/image_model.dart';
import '../member_config/member_config_model.dart';

/// This allows the `RedeemItemDetailModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'redeem_item_detail_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RedeemItemDetailModel {
  // constructor
  RedeemItemDetailModel({
    required this.data,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  // A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RedeemItemDetailModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory RedeemItemDetailModel.fromJson(Map<String, dynamic> json) =>
      _$RedeemItemDetailModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RedeemItemDetailModelToJson`.
  Map<String, dynamic> toJson() => _$RedeemItemDetailModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.id,
    required this.categoryId,
    required this.categoryName,
    required this.image,
    required this.name,
    required this.fullDesc,
    required this.status,
    required this.amount,
    required this.createdAt,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'category_id')
  final int categoryId;
  @JsonKey(name: 'category_name')
  final String categoryName;
  @JsonKey(name: 'image')
  final Image image;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'full_desc')
  final String fullDesc;
  @JsonKey(name: 'amount')
  final int amount;
  @JsonKey(name: 'status')
  final int status;
  @JsonKey(name: 'created_at')
  final String createdAt;

  // A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}
