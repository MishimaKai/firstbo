// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'redeem_item_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedeemItemDetailModel _$RedeemItemDetailModelFromJson(
        Map<String, dynamic> json) =>
    RedeemItemDetailModel(
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RedeemItemDetailModelToJson(
        RedeemItemDetailModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      id: json['id'] as int,
      categoryId: json['category_id'] as int,
      categoryName: json['category_name'] as String,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
      name: json['name'] as String,
      fullDesc: json['full_desc'] as String,
      status: json['status'] as int,
      amount: json['amount'] as int,
      createdAt: json['created_at'] as String,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'category_name': instance.categoryName,
      'image': instance.image,
      'name': instance.name,
      'full_desc': instance.fullDesc,
      'amount': instance.amount,
      'status': instance.status,
      'created_at': instance.createdAt,
    };
