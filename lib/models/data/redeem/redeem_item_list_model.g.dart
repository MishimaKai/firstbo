// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'redeem_item_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedeemItemListModel _$RedeemItemListModelFromJson(Map<String, dynamic> json) =>
    RedeemItemListModel(
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => RedeemItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      meta: MetaModel.fromJson(json['meta'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RedeemItemListModelToJson(
        RedeemItemListModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'meta': instance.meta,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
