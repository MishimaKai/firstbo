import 'package:json_annotation/json_annotation.dart';

import '../image/image_model.dart';

/// This allows the `RedeemItemModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'redeem_item_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RedeemItemModel {
  // constructor
  RedeemItemModel({
    required this.id,
    required this.categoryId,
    required this.categoryName,
    required this.image,
    required this.name,
    required this.fullDesc,
    required this.amount,
    required this.createdAt,
  });

  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'category_id')
  final int categoryId;
  @JsonKey(name: 'category_name')
  final String categoryName;
  @JsonKey(name: 'image')
  final Image image;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'full_desc')
  final String fullDesc;
  @JsonKey(name: 'amount')
  final int amount;
  @JsonKey(name: 'created_at')
  final String createdAt;

  // A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RedeemItemModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory RedeemItemModel.fromJson(Map<String, dynamic> json) =>
      _$RedeemItemModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RedeemItemModelToJson`.
  Map<String, dynamic> toJson() => _$RedeemItemModelToJson(this);
}
