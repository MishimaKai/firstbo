// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'redeem_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RedeemItemModel _$RedeemItemModelFromJson(Map<String, dynamic> json) =>
    RedeemItemModel(
      id: json['id'] as int,
      categoryId: json['category_id'] as int,
      categoryName: json['category_name'] as String,
      image: Image.fromJson(json['image'] as Map<String, dynamic>),
      name: json['name'] as String,
      fullDesc: json['full_desc'] as String,
      amount: json['amount'] as int,
      createdAt: json['created_at'] as String,
    );

Map<String, dynamic> _$RedeemItemModelToJson(RedeemItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'category_name': instance.categoryName,
      'image': instance.image,
      'name': instance.name,
      'full_desc': instance.fullDesc,
      'amount': instance.amount,
      'created_at': instance.createdAt,
    };
