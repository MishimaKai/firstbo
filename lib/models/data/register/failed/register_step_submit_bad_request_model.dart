// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

/// This allows the `RegisterBodyModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'register_step_submit_bad_request_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RegisterStepSubmitBadRequestModel {
  // constructor
  RegisterStepSubmitBadRequestModel({this.message, this.error});

  // variable parameters
  @JsonKey(name: 'message')
  final String? message;
  @JsonKey(name: 'error')
  final Error? error;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RegisterStepSubmitBadRequestModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory RegisterStepSubmitBadRequestModel.fromJson(
          Map<String, dynamic> json) =>
      _$RegisterStepSubmitBadRequestModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RegisterStepSubmitBadRequestModelToJson`.
  Map<String, dynamic> toJson() =>
      _$RegisterStepSubmitBadRequestModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Error {
  // constructor
  Error({this.code, this.message});

  // variable parameters
  @JsonKey(name: 'code')
  final int? code;
  @JsonKey(name: 'message')
  final Message? message;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$ErrorFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Error.fromJson(Map<String, dynamic> json) => _$ErrorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ErrorToJson`.
  Map<String, dynamic> toJson() => _$ErrorToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Message {
  // constructor
  Message({this.otp, this.email, this.phone});

  // variable parameters
  @JsonKey(name: 'otp')
  final List<String>? otp;
  @JsonKey(name: 'email')
  final List<String>? email;
  @JsonKey(name: 'phone')
  final List<String>? phone;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$MessageFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$MessageToJson`.
  Map<String, dynamic> toJson() => _$MessageToJson(this);
}
