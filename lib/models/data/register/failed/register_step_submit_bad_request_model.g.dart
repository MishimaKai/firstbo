// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_step_submit_bad_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterStepSubmitBadRequestModel _$RegisterStepSubmitBadRequestModelFromJson(
        Map<String, dynamic> json) =>
    RegisterStepSubmitBadRequestModel(
      message: json['message'] as String?,
      error: json['error'] == null
          ? null
          : Error.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RegisterStepSubmitBadRequestModelToJson(
        RegisterStepSubmitBadRequestModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'error': instance.error,
    };

Error _$ErrorFromJson(Map<String, dynamic> json) => Error(
      code: json['code'] as int?,
      message: json['message'] == null
          ? null
          : Message.fromJson(json['message'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ErrorToJson(Error instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
    };

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      otp: (json['otp'] as List<dynamic>?)?.map((e) => e as String).toList(),
      email:
          (json['email'] as List<dynamic>?)?.map((e) => e as String).toList(),
      phone:
          (json['phone'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'otp': instance.otp,
      'email': instance.email,
      'phone': instance.phone,
    };
