import 'package:json_annotation/json_annotation.dart';

/// This allows the `RegisterBodyModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'register_body_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RegisterBodyModel {
  // constructor
  RegisterBodyModel({
    required this.countryCode,
    required this.phone,
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.dob,
    required this.gender,
    required this.cityId,
    required this.address,
  });

  // variable parameters
  @JsonKey(name: 'country_code')
  final String countryCode;
  @JsonKey(name: 'phone')
  final String phone;
  @JsonKey(name: 'email')
  final String email;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'dob')
  final DateTime dob;
  @JsonKey(name: 'gender')
  final String gender;
  @JsonKey(name: 'city_id')
  final String cityId;
  @JsonKey(name: 'address')
  final String address;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RegisterBodyModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  // ignore: sort_constructors_first
  factory RegisterBodyModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterBodyModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RegisterBodyModelToJson`.
  Map<String, dynamic> toJson() => _$RegisterBodyModelToJson(this);
}
