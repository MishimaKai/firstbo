// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterBodyModel _$RegisterBodyModelFromJson(Map<String, dynamic> json) =>
    RegisterBodyModel(
      countryCode: json['country_code'] as String,
      phone: json['phone'] as String,
      email: json['email'] as String,
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      dob: DateTime.parse(json['dob'] as String),
      gender: json['gender'] as String,
      cityId: json['city_id'] as String,
      address: json['address'] as String,
    );

Map<String, dynamic> _$RegisterBodyModelToJson(RegisterBodyModel instance) =>
    <String, dynamic>{
      'country_code': instance.countryCode,
      'phone': instance.phone,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'dob': instance.dob.toIso8601String(),
      'gender': instance.gender,
      'city_id': instance.cityId,
      'address': instance.address,
    };
