// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../../client_info/client_info_model.dart';

/// This allows the `RegisterStepOtpSuccessDevModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'register_step_otp_success_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class RegisterStepOtpSuccessModel {
  // constructor
  RegisterStepOtpSuccessModel({
    required this.message,
    required this.data,
    required this.clientInfo,
  });

  // variable parameters
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'data')
  final Data data;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$RegisterStepOtpSuccessModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory RegisterStepOtpSuccessModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterStepOtpSuccessModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$RegisterStepOtpSuccessModelToJson`.
  Map<String, dynamic> toJson() => _$RegisterStepOtpSuccessModelToJson(this);
}

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class Data {
  // constructor
  Data({
    required this.memberAccess,
    required this.type,
    required this.exp,
  });

  // variable parameters
  @JsonKey(name: 'member_access')
  final String memberAccess;
  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'exp')
  final DateTime exp;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$DataFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$DataToJson`.
  Map<String, dynamic> toJson() => _$DataToJson(this);
}
