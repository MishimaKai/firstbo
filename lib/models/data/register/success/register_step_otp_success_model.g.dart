// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_step_otp_success_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterStepOtpSuccessModel _$RegisterStepOtpSuccessModelFromJson(
        Map<String, dynamic> json) =>
    RegisterStepOtpSuccessModel(
      message: json['message'] as String,
      data: Data.fromJson(json['data'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RegisterStepOtpSuccessModelToJson(
        RegisterStepOtpSuccessModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'data': instance.data,
      'client_info': instance.clientInfo,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      memberAccess: json['member_access'] as String,
      type: json['type'] as int,
      exp: DateTime.parse(json['exp'] as String),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'member_access': instance.memberAccess,
      'type': instance.type,
      'exp': instance.exp.toIso8601String(),
    };
