// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reset_device_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResetDeviceModel _$ResetDeviceModelFromJson(Map<String, dynamic> json) =>
    ResetDeviceModel(
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ResetDeviceModelToJson(ResetDeviceModel instance) =>
    <String, dynamic>{
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
