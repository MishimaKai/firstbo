// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../percentage_bar/percentage_bar_model.dart';
import '../stay_warning_box/stay_warning_box_model.dart';

part 'spending_power_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class SpendingPowerModel {
  // constructor
  SpendingPowerModel({
    required this.current,
    required this.minExistingMembership,
    required this.minNextMembership,
    required this.reqNextLevelMembership,
    required this.percentageBar,
    required this.stayWarningBox,
  });

  // variable parameters
  @JsonKey(name: 'current')
  final int current;
  @JsonKey(name: 'min_existing_membership')
  final int minExistingMembership;
  @JsonKey(name: 'min_next_membership')
  final int? minNextMembership;
  @JsonKey(name: 'req_next_level_membership')
  final int? reqNextLevelMembership;
  @JsonKey(name: 'percentage_bar')
  final PercentageBarModel percentageBar;
  @JsonKey(name: 'stay_warning_box')
  final StayWarningBoxModel stayWarningBox;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$SpendingPowerModelFromJson()` constructor.

  factory SpendingPowerModel.fromJson(Map<String, dynamic> json) =>
      _$SpendingPowerModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$SpendingPowerModelToJson`.

  Map<String, dynamic> toJson() => _$SpendingPowerModelToJson(this);
}
