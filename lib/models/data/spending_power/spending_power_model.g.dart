// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spending_power_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpendingPowerModel _$SpendingPowerModelFromJson(Map<String, dynamic> json) =>
    SpendingPowerModel(
      current: json['current'] as int,
      minExistingMembership: json['min_existing_membership'] as int,
      minNextMembership: json['min_next_membership'] as int?,
      reqNextLevelMembership: json['req_next_level_membership'] as int?,
      percentageBar: PercentageBarModel.fromJson(
          json['percentage_bar'] as Map<String, dynamic>),
      stayWarningBox: StayWarningBoxModel.fromJson(
          json['stay_warning_box'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SpendingPowerModelToJson(SpendingPowerModel instance) =>
    <String, dynamic>{
      'current': instance.current,
      'min_existing_membership': instance.minExistingMembership,
      'min_next_membership': instance.minNextMembership,
      'req_next_level_membership': instance.reqNextLevelMembership,
      'percentage_bar': instance.percentageBar,
      'stay_warning_box': instance.stayWarningBox,
    };
