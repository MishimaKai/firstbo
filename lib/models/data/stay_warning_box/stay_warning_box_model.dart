import 'package:json_annotation/json_annotation.dart';

part 'stay_warning_box_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class StayWarningBoxModel {
  // constructor
  StayWarningBoxModel({
    required this.show,
    required this.spendSp,
    required this.resetDate,
  });

  @JsonKey(name: 'show')
  final int show;
  @JsonKey(name: 'spend_sp')
  final int? spendSp;
  @JsonKey(name: 'reset_date')
  final String resetDate;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$StayWarningBoxModelFromJson()` constructor.

  // ignore: sort_constructors_first
  factory StayWarningBoxModel.fromJson(Map<String, dynamic> json) =>
      _$StayWarningBoxModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$StayWarningBoxModelToJson`.

  Map<String, dynamic> toJson() => _$StayWarningBoxModelToJson(this);
}
