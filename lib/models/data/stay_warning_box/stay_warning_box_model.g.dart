// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stay_warning_box_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StayWarningBoxModel _$StayWarningBoxModelFromJson(Map<String, dynamic> json) =>
    StayWarningBoxModel(
      show: json['show'] as int,
      spendSp: json['spend_sp'] as int?,
      resetDate: json['reset_date'] as String,
    );

Map<String, dynamic> _$StayWarningBoxModelToJson(
        StayWarningBoxModel instance) =>
    <String, dynamic>{
      'show': instance.show,
      'spend_sp': instance.spendSp,
      'reset_date': instance.resetDate,
    };
