// ignore_for_file: sort_constructors_first

import 'package:json_annotation/json_annotation.dart';

import '../percentage_bar/percentage_bar_model.dart';

part 'total_spending_model.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class TotalSpendingModel {
  // constructor
  TotalSpendingModel({
    required this.current,
    required this.displayCurrent,
    required this.minExistingMembership,
    required this.minNextMembership,
    required this.reqNextLevelMembership,
    required this.displayReqNextLevelMembership,
    required this.percentageBar,
  });

  // variable parameters
  @JsonKey(name: 'current')
  final int current;
  @JsonKey(name: 'display_current')
  final String displayCurrent;
  @JsonKey(name: 'min_existing_membership')
  final int minExistingMembership;
  @JsonKey(name: 'min_next_membership')
  final int? minNextMembership;
  @JsonKey(name: 'req_next_level_membership')
  final int? reqNextLevelMembership;
  @JsonKey(name: 'display_req_next_level_membership')
  final String? displayReqNextLevelMembership;
  @JsonKey(name: 'percentage_bar')
  final PercentageBarModel percentageBar;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$TotalSpendingModelFromJson()` constructor.

  factory TotalSpendingModel.fromJson(Map<String, dynamic> json) =>
      _$TotalSpendingModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$TotalSpendingModelToJson`.

  Map<String, dynamic> toJson() => _$TotalSpendingModelToJson(this);
}
