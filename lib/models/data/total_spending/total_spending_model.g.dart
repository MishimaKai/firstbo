// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'total_spending_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TotalSpendingModel _$TotalSpendingModelFromJson(Map<String, dynamic> json) =>
    TotalSpendingModel(
      current: json['current'] as int,
      displayCurrent: json['display_current'] as String,
      minExistingMembership: json['min_existing_membership'] as int,
      minNextMembership: json['min_next_membership'] as int?,
      reqNextLevelMembership: json['req_next_level_membership'] as int?,
      displayReqNextLevelMembership:
          json['display_req_next_level_membership'] as String?,
      percentageBar: PercentageBarModel.fromJson(
          json['percentage_bar'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TotalSpendingModelToJson(TotalSpendingModel instance) =>
    <String, dynamic>{
      'current': instance.current,
      'display_current': instance.displayCurrent,
      'min_existing_membership': instance.minExistingMembership,
      'min_next_membership': instance.minNextMembership,
      'req_next_level_membership': instance.reqNextLevelMembership,
      'display_req_next_level_membership':
          instance.displayReqNextLevelMembership,
      'percentage_bar': instance.percentageBar,
    };
