// ignore_for_file: sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

import '../outlet_item.dart';

/// This allows the `CategoryItemModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'category_item.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class CategoryItemModel {
  // constructor
  CategoryItemModel({
    required this.type,
    required this.group,
    required this.name,
    this.startTime,
    this.endTime,
    required this.amount,
    required this.formattedAmount,
    required this.outlet,
  });

  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'group')
  final String group;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'start_time')
  final String? startTime;
  @JsonKey(name: 'end_time')
  final String? endTime;
  @JsonKey(name: 'amount')
  final int amount;
  @JsonKey(name: 'formatted_amount')
  final String formattedAmount;
  @JsonKey(name: 'outlet')
  final List<OutletItemModel>? outlet;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$CategoryItemModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory CategoryItemModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryItemModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$CategoryItemModelToJson`.
  Map<String, dynamic> toJson() => _$CategoryItemModelToJson(this);
}
