// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryItemModel _$CategoryItemModelFromJson(Map<String, dynamic> json) =>
    CategoryItemModel(
      type: json['type'] as int,
      group: json['group'] as String,
      name: json['name'] as String,
      startTime: json['start_time'] as String?,
      endTime: json['end_time'] as String?,
      amount: json['amount'] as int,
      formattedAmount: json['formatted_amount'] as String,
      outlet: (json['outlet'] as List<dynamic>?)
          ?.map((e) => OutletItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoryItemModelToJson(CategoryItemModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'group': instance.group,
      'name': instance.name,
      'start_time': instance.startTime,
      'end_time': instance.endTime,
      'amount': instance.amount,
      'formatted_amount': instance.formattedAmount,
      'outlet': instance.outlet,
    };
