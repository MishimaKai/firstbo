// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryListModel _$CategoryListModelFromJson(Map<String, dynamic> json) =>
    CategoryListModel(
      data: (json['data'] as List<dynamic>)
          .map((e) => CategoryItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CategoryListModelToJson(CategoryListModel instance) =>
    <String, dynamic>{
      'data': instance.data,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
