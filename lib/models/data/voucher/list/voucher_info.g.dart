// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoucherInfoModel _$VoucherInfoModelFromJson(Map<String, dynamic> json) =>
    VoucherInfoModel(
      type: json['type'] as int,
      group: json['group'] as String,
      name: json['name'] as String,
      startTime: json['start_time'] as String?,
      endTime: json['end_time'] as String?,
      amount: json['amount'] as int,
      formattedAmount: json['formatted_amount'] as String,
      outlet: (json['outlet'] as List<dynamic>?)
          ?.map((e) => OutletItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$VoucherInfoModelToJson(VoucherInfoModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'group': instance.group,
      'name': instance.name,
      'start_time': instance.startTime,
      'end_time': instance.endTime,
      'amount': instance.amount,
      'formatted_amount': instance.formattedAmount,
      'outlet': instance.outlet,
    };
