// ignore_for_file: sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

/// This allows the `VoucherItemModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'voucher_item.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class VoucherItemModel {
  // constructor
  VoucherItemModel({
    required this.type,
    required this.typeName,
    required this.outletId,
    required this.outletName,
    required this.total,
    required this.used,
    required this.remaining,
    required this.isValid,
    required this.validStart,
    required this.validEnd,
    required this.textValidStart,
    required this.textValidEnd,
  });

  @JsonKey(name: 'type')
  final int type;
  @JsonKey(name: 'type_name')
  final String typeName;
  @JsonKey(name: 'outlet_id')
  final int outletId;
  @JsonKey(name: 'outlet_name')
  final String outletName;
  @JsonKey(name: 'total')
  final int total;
  @JsonKey(name: 'used')
  final int used;
  @JsonKey(name: 'remaining')
  final int remaining;
  @JsonKey(name: 'is_valid')
  final int isValid;
  @JsonKey(name: 'valid_start')
  final String validStart;
  @JsonKey(name: 'valid_end')
  final String validEnd;
  @JsonKey(name: 'text_valid_start')
  final String textValidStart;
  @JsonKey(name: 'text_valid_end')
  final String textValidEnd;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$VoucherItemModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory VoucherItemModel.fromJson(Map<String, dynamic> json) =>
      _$VoucherItemModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$VoucherItemModelToJson`.
  Map<String, dynamic> toJson() => _$VoucherItemModelToJson(this);
}
