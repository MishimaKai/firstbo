// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoucherItemModel _$VoucherItemModelFromJson(Map<String, dynamic> json) =>
    VoucherItemModel(
      type: json['type'] as int,
      typeName: json['type_name'] as String,
      outletId: json['outlet_id'] as int,
      outletName: json['outlet_name'] as String,
      total: json['total'] as int,
      used: json['used'] as int,
      remaining: json['remaining'] as int,
      isValid: json['is_valid'] as int,
      validStart: json['valid_start'] as String,
      validEnd: json['valid_end'] as String,
      textValidStart: json['text_valid_start'] as String,
      textValidEnd: json['text_valid_end'] as String,
    );

Map<String, dynamic> _$VoucherItemModelToJson(VoucherItemModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'type_name': instance.typeName,
      'outlet_id': instance.outletId,
      'outlet_name': instance.outletName,
      'total': instance.total,
      'used': instance.used,
      'remaining': instance.remaining,
      'is_valid': instance.isValid,
      'valid_start': instance.validStart,
      'valid_end': instance.validEnd,
      'text_valid_start': instance.textValidStart,
      'text_valid_end': instance.textValidEnd,
    };
