// ignore_for_file: sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

import '../../client_info/client_info_model.dart';
import '../../device/device_model.dart';
import '../../member_config/member_config_model.dart';
import '../../meta/meta_model.dart';
import 'voucher_info.dart';
import 'voucher_item.dart';

/// This allows the `VoucherListModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'voucher_list.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class VoucherListModel {
  // constructor
  VoucherListModel({
    required this.voucherInfo,
    required this.data,
    required this.meta,
    required this.memberConfig,
    required this.device,
    required this.clientInfo,
  });

  @JsonKey(name: 'voucher_info')
  final VoucherInfoModel voucherInfo;
  @JsonKey(name: 'data')
  final List<VoucherItemModel>? data;
  @JsonKey(name: 'meta')
  final MetaModel meta;
  @JsonKey(name: 'member_config')
  final MemberConfigModel memberConfig;
  @JsonKey(name: 'device')
  final Device device;
  @JsonKey(name: 'client_info')
  final ClientInfo clientInfo;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$VoucherListModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory VoucherListModel.fromJson(Map<String, dynamic> json) =>
      _$VoucherListModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$VoucherListModelToJson`.
  Map<String, dynamic> toJson() => _$VoucherListModelToJson(this);
}
