// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voucher_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoucherListModel _$VoucherListModelFromJson(Map<String, dynamic> json) =>
    VoucherListModel(
      voucherInfo: VoucherInfoModel.fromJson(
          json['voucher_info'] as Map<String, dynamic>),
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => VoucherItemModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      meta: MetaModel.fromJson(json['meta'] as Map<String, dynamic>),
      memberConfig: MemberConfigModel.fromJson(
          json['member_config'] as Map<String, dynamic>),
      device: Device.fromJson(json['device'] as Map<String, dynamic>),
      clientInfo:
          ClientInfo.fromJson(json['client_info'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VoucherListModelToJson(VoucherListModel instance) =>
    <String, dynamic>{
      'voucher_info': instance.voucherInfo,
      'data': instance.data,
      'meta': instance.meta,
      'member_config': instance.memberConfig,
      'device': instance.device,
      'client_info': instance.clientInfo,
    };
