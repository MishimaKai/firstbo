// ignore_for_file: sort_constructors_first
import 'package:json_annotation/json_annotation.dart';

/// This allows the `OutletItemModel` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'outlet_item.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class OutletItemModel {
  // constructor
  OutletItemModel({
    required this.id,
    required this.name,
    required this.amount,
    required this.formattedAmount,
  });

  // variable parameters
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'amount')
  final int amount;
  @JsonKey(name: 'formatted_amount')
  final String formattedAmount;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$OutletItemModelFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory OutletItemModel.fromJson(Map<String, dynamic> json) =>
      _$OutletItemModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$OutletItemModelToJson`.
  Map<String, dynamic> toJson() => _$OutletItemModelToJson(this);
}
