// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutletItemModel _$OutletItemModelFromJson(Map<String, dynamic> json) =>
    OutletItemModel(
      id: json['id'] as int,
      name: json['name'] as String,
      amount: json['amount'] as int,
      formattedAmount: json['formatted_amount'] as String,
    );

Map<String, dynamic> _$OutletItemModelToJson(OutletItemModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'amount': instance.amount,
      'formatted_amount': instance.formattedAmount,
    };
