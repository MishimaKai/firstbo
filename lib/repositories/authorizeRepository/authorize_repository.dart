import 'dart:io';

import '../../helpers/device_info.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'authorize_repository_interface.dart';

class AuthorizeRepository implements IAuthorizeRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getMembeProfile() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.memberProfile,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getMemberAccountInfo() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.memberAccountInfo,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postResendOTP(
    String action,
    String memberAccess,
    int type,
  ) async {
    Map<String, dynamic> body = <String, dynamic>{};

    if (Platform.isAndroid) {
      final String? androidInfoId = await DeviceInfo().getDeviceIdAndroid();
      body = <String, dynamic>{
        'action': action,
        'member_access': memberAccess,
        'type': type.toString(),
        'device_id': androidInfoId ?? '',
      };
    } else {
      final String? iosInfoId = await DeviceInfo().getDeviceIdIos();
      body = <String, dynamic>{
        'action': action,
        'member_access': memberAccess,
        'type': type.toString(),
        'device_id': iosInfoId ?? '',
      };
    }

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.resendOtp,
      body: body,
    );

    return result;
  }
}
