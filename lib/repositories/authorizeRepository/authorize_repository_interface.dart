abstract class IAuthorizeRepository {
  // get member profile
  Future<Map<String, dynamic>> getMembeProfile();
  // get member detail
  Future<Map<String, dynamic>> getMemberAccountInfo();
  // post resend OTP
  Future<Map<String, dynamic>> postResendOTP(
    String action,
    String memberAccess,
    int type,
  );
}
