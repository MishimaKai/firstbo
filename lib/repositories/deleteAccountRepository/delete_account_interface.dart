abstract class IDeleteAccount {
  // request otp
  Future<Map<String, dynamic>> postDeleteAccountOTP(
    String countryCode,
    String phone,
  );
  // request submit delete account
  Future<Map<String, dynamic>> postDeleteAccountSubmit(
    String countryCode,
    String phone,
    String otp,
  );
}
