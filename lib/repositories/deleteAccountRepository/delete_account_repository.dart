import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'delete_account_interface.dart';

class DeleteAccountRepository implements IDeleteAccount {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> postDeleteAccountOTP(
    String countryCode,
    String phone,
  ) async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.deleteAccountOTP,
      body: <String, dynamic>{
        'country_code': countryCode,
        'phone': phone,
      },
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postDeleteAccountSubmit(
    String countryCode,
    String phone,
    String otp,
  ) async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.deleteAccountSubmit,
      body: <String, dynamic>{
        'country_code': countryCode,
        'phone': phone,
        'otp': otp,
      },
      hasAccessToken: true,
    );

    return result;
  }
}
