import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'event_repository_interface.dart';

class EventRepository implements IEventRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getEventDetail(int id) async {
    final Map<String, dynamic> param = <String, dynamic>{'id': id};

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.eventDetail,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getEventList(int pageKey) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'page': pageKey,
      'per_page': 10
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.eventList,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }
}
