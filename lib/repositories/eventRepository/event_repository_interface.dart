abstract class IEventRepository {
  // request get event list
  Future<Map<String, dynamic>> getEventList(int pageKey);
  // request get evenr detail by id
  Future<Map<String, dynamic>> getEventDetail(int id);
}
