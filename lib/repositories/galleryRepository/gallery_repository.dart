import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'gallery_repository_interface.dart';

class GalleryRepository implements IGalleryRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getGalleryDetail(int id) async {
    final Map<String, dynamic> param = <String, dynamic>{'id': id};

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.galleryDetailPage,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getGalleryList(int pagekey) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'page': pagekey,
      'per_page': 10
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.galleryList,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }
}
