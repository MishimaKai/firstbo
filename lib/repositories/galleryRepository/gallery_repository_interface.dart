abstract class IGalleryRepository {
  // request get gallery list
  Future<Map<String, dynamic>> getGalleryList(int pageKey);
  // requerst get gallery detail
  Future<Map<String, dynamic>> getGalleryDetail(int id);
}
