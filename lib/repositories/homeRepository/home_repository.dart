import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'home_repository_interface.dart';

class HomeRepository extends IHomeRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getHome() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.home,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getMemberAccountInfo() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.memberHomeAccountInfo,
      hasAccessToken: true,
    );

    return result;
  }
}
