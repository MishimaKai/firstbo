abstract class IHomeRepository {
  // get home
  Future<Map<String, dynamic>> getHome();
  // get voucher, points, and notifications
  Future<Map<String, dynamic>> getMemberAccountInfo();
}
