import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'notification_repository_interface.dart';

class NotificationRepository implements INotificationRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getNotificationList(int pageKey) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'per_page': 10,
      'page': pageKey
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.notification,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }
}
