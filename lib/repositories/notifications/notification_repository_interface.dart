abstract class INotificationRepository {
  // get notification list
  Future<Map<String, dynamic>> getNotificationList(int pageKey);
}
