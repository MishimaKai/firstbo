import 'package:dio/dio.dart';

import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'payment_repository_interface.dart';

class PaymentRepository implements IPaymentRepository {
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> openBill(String qrCode) async {
    final Set<String> body = <String>{qrCode};

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.openBill,
      body: body,
      hasAccessToken: true,
      useLocation: true,
      contentType: Headers.textPlainContentType,
    );

    // log("raw body = $_body");

    return result;
  }
}
