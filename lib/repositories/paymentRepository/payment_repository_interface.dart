abstract class IPaymentRepository {
  Future<Map<String, dynamic>> openBill(String qrCode);
}
