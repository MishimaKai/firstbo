import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'point_repository_interface.dart';

class PointRepository implements IPointRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getPointHistory(
    String fromDate,
    String toDate,
    int pageKey,
  ) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'from_date': fromDate,
      'to_date': toDate,
      'page': pageKey,
      'per_page': 10,
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.pointHistory,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getPointDetail(int id) async {
    final Map<String, dynamic> param = <String, dynamic>{'id': id};

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.pointDetail,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }
}
