abstract class IPointRepository {
  // get point history
  Future<Map<String, dynamic>> getPointHistory(
    String fromDate,
    String toDate,
    int pageKey,
  );
  // get point detail
  Future<Map<String, dynamic>> getPointDetail(int id);
}
