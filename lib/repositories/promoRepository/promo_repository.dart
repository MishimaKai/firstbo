import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'promo_repository_interface.dart';

class PromoRepository implements IPromoRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getPromoDetail(int id) async {
    final Map<String, dynamic> param = <String, dynamic>{'id': id};

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.promoDetail,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getPromoList(int type, int pageKey) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'type': type,
      'page': pageKey,
      'per_page': 9
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.promoList,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }
}
