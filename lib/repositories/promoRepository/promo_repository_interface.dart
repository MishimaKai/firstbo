abstract class IPromoRepository {
  // request get promo list
  Future<Map<String, dynamic>> getPromoList(int type, int pageKey);
  // request get promo detail
  Future<Map<String, dynamic>> getPromoDetail(int id);
}
