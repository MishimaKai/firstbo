import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'province_city_repository_interface.dart';

class ProvinceCityRepository implements IProvinceCityRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getCity(int provinceId) async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.city,
      query: provinceId.toString(),
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getProvince() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.province,
    );

    return result;
  }
}
