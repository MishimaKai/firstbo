abstract class IProvinceCityRepository {
  // get province
  Future<Map<String, dynamic>> getProvince();
  // get city
  Future<Map<String, dynamic>> getCity(int provinceId);
}
