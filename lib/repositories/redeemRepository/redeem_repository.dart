import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'redeem_repository_interface.dart';

class RedeemRepository implements IRedeemRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getRedeemCategoryList() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.redeemCategoryList,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getRedeemItemList(
    int categoryId,
    int pageKey,
  ) async {
    final Map<String, dynamic> param = <String, dynamic>{
      'category_id': categoryId,
      'per_page': 12,
      'page': pageKey,
    };

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.redeemItemList,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getRedeemItemDetail(int id) async {
    final Map<String, dynamic> param = <String, dynamic>{'id': id};

    final String query = StringHelper.urlQueries(param);

    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.redeemItemDetailPage,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postRedeem(int id) async {
    final Map<String, dynamic> body = <String, dynamic>{'id': id};

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.redeem,
      body: body,
      hasAccessToken: true,
    );

    return result;
  }
}
