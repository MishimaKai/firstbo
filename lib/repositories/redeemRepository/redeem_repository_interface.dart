abstract class IRedeemRepository {
  // get item list
  Future<Map<String, dynamic>> getRedeemItemList(int categoryId, int pageKey);
  // get category list
  Future<Map<String, dynamic>> getRedeemCategoryList();
  // get item detail
  Future<Map<String, dynamic>> getRedeemItemDetail(int id);
  // post redeem item
  Future<Map<String, dynamic>> postRedeem(int id);
}
