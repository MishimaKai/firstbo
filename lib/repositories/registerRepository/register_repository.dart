import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'register_repository_interface.dart';

class RegisterRepository implements IRegisterRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> postRegisterOtp(
    int countryCode,
    String phoneNumber,
    String emailAddress,
    String firstName,
    String lastName,
    String dob,
    int gender,
    int cityId,
    String address,
  ) async {
    final Map<String, dynamic> body = <String, dynamic>{
      'country_code': countryCode.toString(),
      'phone': phoneNumber,
      'email': emailAddress,
      'first_name': firstName,
      'last_name': lastName,
      'dob': dob,
      'gender': gender.toString(),
      'city_id': cityId.toString(),
      'address': address,
    };

    // log('BODY POST = $body');

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.registerOTP,
      body: body,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postRegisterSubmit(
    int countryCode,
    String phoneNumber,
    String emailAddress,
    String firstName,
    String lastName,
    String dob,
    int gender,
    int cityId,
    String address,
    String otp,
  ) async {
    final Map<String, dynamic> body = <String, dynamic>{
      'country_code': countryCode.toString(),
      'phone': phoneNumber,
      'email': emailAddress,
      'first_name': firstName,
      'last_name': lastName,
      'dob': dob,
      'gender': gender.toString(),
      'city_id': cityId.toString(),
      'address': address,
      'otp': otp,
    };

    // log('BODY POST = $body');

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.registerSubmit,
      body: body,
    );

    return result;
  }
}
