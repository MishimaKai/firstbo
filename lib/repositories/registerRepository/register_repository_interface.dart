abstract class IRegisterRepository {
  // post register otp
  Future<Map<String, dynamic>> postRegisterOtp(
    int countryCode,
    String phoneNumber,
    String emailAddress,
    String firstName,
    String lastName,
    String dob,
    int gender,
    int cityId,
    String address,
  );

  // post register submit
  Future<Map<String, dynamic>> postRegisterSubmit(
    int countryCode,
    String phoneNumber,
    String emailAddress,
    String firstName,
    String lastName,
    String dob,
    int gender,
    int cityId,
    String address,
    String otp,
  );
}
