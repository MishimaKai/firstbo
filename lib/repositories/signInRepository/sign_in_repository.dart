import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'sign_in_repository_interface.dart';

class SignInRepository implements ISignInRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getCountryCode() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.countryCode,
    );

    return result;
  }
}
