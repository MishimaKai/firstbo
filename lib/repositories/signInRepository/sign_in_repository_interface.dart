abstract class ISignInRepository {
  // get country code
  Future<Map<String, dynamic>> getCountryCode();
}
