import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'terms_respository_interface.dart';

class TermsRepository implements ITermsRepository {
  final ApiRequest _apiRequest = ApiRequest();
  @override
  Future<Map<String, dynamic>> getPrivacyPolicy() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.privacyPolicy,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getTnC() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.tnc,
      hasAccessToken: true,
    );

    return result;
  }
}
