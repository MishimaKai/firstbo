abstract class ITermsRepository {
  // get tnc
  Future<Map<String, dynamic>> getTnC();
  // get privacy policy
  Future<Map<String, dynamic>> getPrivacyPolicy();
}
