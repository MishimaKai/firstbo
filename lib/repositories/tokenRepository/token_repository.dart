import 'dart:io';

import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../helpers/device_info.dart';
import '../../helpers/shared_pref.dart';
import '../../services/api.dart';
import '../../services/api_header.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'token_repository_interface.dart';

class TokenRepository implements ITokenRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();
  final SharedPref _sharedPref = SharedPref();

  @override
  Future<Map<String, dynamic>> postAccessToken(String authCode) async {
    Map<String, dynamic> body = <String, dynamic>{};

    if (Platform.isAndroid) {
      final String? androidInfoId = await DeviceInfo().getDeviceIdAndroid();
      final String? androidInfoOs = await DeviceInfo().getDeviceOSAndroid();
      body = <String, dynamic>{
        'authorization_code': authCode,
        'device_id': androidInfoId ?? '',
        'device_os': androidInfoOs ?? '',
      };
    } else {
      final String? iosInfoId = await DeviceInfo().getDeviceIdIos();
      final String? iosInfoOS = await DeviceInfo().getDeviceOSIos();
      body = <String, dynamic>{
        'authorization_code': authCode,
        'device_id': iosInfoId ?? '',
        'device_os': iosInfoOS ?? '',
      };
    }

    // log('BODY POST = $body');

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.accessToken,
      body: body,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postAuthorize(
    String memberAccess,
    int type,
    String otp,
  ) async {
    Map<String, dynamic> body = <String, dynamic>{};

    if (Platform.isAndroid) {
      final String? androidInfoId = await DeviceInfo().getDeviceIdAndroid();
      body = <String, dynamic>{
        'member_access': memberAccess,
        'type': type.toString(),
        'otp': otp,
        'device_id': androidInfoId ?? '',
      };
    } else {
      final String? iosInfoId = await DeviceInfo().getDeviceIdIos();
      body = <String, dynamic>{
        'member_access': memberAccess,
        'type': type.toString(),
        'otp': otp,
        'device_id': iosInfoId ?? '',
      };
    }

    // log('BODY POST = $body');

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.authorize,
      body: body,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postPreAuthorize(
    String memberAccess,
    int type,
  ) async {
    Map<String, dynamic> body = <String, dynamic>{};

    if (Platform.isAndroid) {
      final String? androidInfoId = await DeviceInfo().getDeviceIdAndroid();
      body = <String, dynamic>{
        'member_access': memberAccess,
        'type': type.toString(),
        'device_id': androidInfoId ?? '',
      };
    } else {
      final String? iosInfoId = await DeviceInfo().getDeviceIdIos();
      body = <String, dynamic>{
        'member_access': memberAccess,
        'type': type.toString(),
        'device_id': iosInfoId ?? '',
      };
    }

    // log('BODY POST = $body');

    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.preauthorize,
      body: body,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postRefreshToken() async {
    final API api = API();
    final Dio dio = Dio(
      BaseOptions(
        connectTimeout: 20000,
        receiveTimeout: 20000,
        baseUrl: API.host,
        contentType: ContentType.json.toString(),
      ),
    )..interceptors.add(PrettyDioLogger());

    final String? accessToken = await _sharedPref.readAccessToken();
    final String? refreshToken = await _sharedPref.readRefreshToken();

    final Map<String, dynamic> body = <String, dynamic>{
      'access_token': accessToken,
      'refresh_token': refreshToken,
    };

    // header
    final Map<String, dynamic> header = await ApiHeaders().result(
      doRefreshToken: true,
    );
    // endpoint api
    final String? endpoint = api.endPoint(EndPointName.refreshToken);

    try {
      final Response<dynamic> response = await dio.post(
        endpoint.toString(),
        data: body,
        options: Options(headers: header),
      );
      return response.data as Map<String, dynamic>;
    } on DioError catch (err) {
      // error handling in here
      Map<String, dynamic> error = <String, dynamic>{};
      if (err.type == DioErrorType.other) {
        if (err.message.contains('SocketException') ||
            err.message.contains('HttpException')) {
          error = <String, dynamic>{
            'message': 'No internet connection. #201',
          };
        } else {
          error = <String, dynamic>{'message': err.message};
        }
      } else if (err.type == DioErrorType.connectTimeout) {
        error = <String, dynamic>{
          'message': 'No internet connection. #202',
        };
      } else if (err.type == DioErrorType.receiveTimeout) {
        error = <String, dynamic>{
          'message': 'Unable to connect. #203',
        };
      } else {
        error = <String, dynamic>{
          'code': err.response?.statusCode,
          'message': err.response?.statusCode == 404
              ? '${err.response?.statusMessage}. #${err.response?.statusCode}'
              : err.response?.statusCode,
        };
      }

      return error;
    }
  }

  @override
  Future<Map<String, dynamic>> postLogout() async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.logout,
      body: <String, dynamic>{},
      hasAccessToken: true,
    );

    return result;
  }
}
