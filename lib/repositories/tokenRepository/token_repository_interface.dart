abstract class ITokenRepository {
  // post preauthorize
  Future<Map<String, dynamic>> postPreAuthorize(String memberAccess, int type);
  // post authorize for get authorize code
  Future<Map<String, dynamic>> postAuthorize(
    String memberAccess,
    int type,
    String otp,
  );
  // after post authorize then gain access token
  Future<Map<String, dynamic>> postAccessToken(String authCode);
  // refresh token
  Future<Map<String, dynamic>> postRefreshToken();
  // logout account user
  Future<Map<String, dynamic>> postLogout();
}
