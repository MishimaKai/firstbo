import 'dart:io';

import '../../helpers/device_info.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'verify_repository_interface.dart';

class VerifyRepository implements IVerifyRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> postVerifyRequest(File imageFile) async {
    final Map<String, dynamic> result = await _apiRequest.postFileRepository(
      endPoint: EndPointName.verificationRequest,
      imageFile: imageFile,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postUpdateProfile(
      String firstName,
      String lastName,
      String dob,
      String email,
      int gender,
      int cityId,
      String address,
      File? imageFile) async {
    final Map<String, dynamic> body = <String, dynamic>{
      'first_name': firstName,
      'last_name': lastName,
      'dob': dob,
      'email': email,
      'gender': gender,
      'city_id': cityId,
      'address': address,
      'image_file': imageFile,
    };

    if (imageFile == null) {
      body.remove('image_file');
    }

    final Map<String, dynamic> result = await _apiRequest.postFileRepository(
      endPoint: EndPointName.updateProfile,
      body: body,
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postResetDevice(
    int reasonId,
    String reason,
  ) async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.resetDevice,
      body: <String, dynamic>{'reason_id': reasonId, 'reason_text': reason},
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postResetDeviceLogin(
    int reasonId,
    String reason,
  ) async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.resetDeviceLogin,
      body: <String, dynamic>{
        'reason_id': reasonId,
        'reason_text': reason,
        'device_os': Platform.isAndroid
            ? await DeviceInfo().getDeviceSdkAndroid()
            : await DeviceInfo().getDeviceOSIos(),
      },
      hasAccessToken: true,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> postCancelResetDeviceLogin() async {
    final Map<String, dynamic> result = await _apiRequest.postRepository(
      endPoint: EndPointName.cancelResetDeviceLogin,
      body: <String, dynamic>{},
      hasAccessToken: true,
    );

    return result;
  }
}
