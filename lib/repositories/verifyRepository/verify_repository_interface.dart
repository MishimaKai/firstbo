import 'dart:io';

abstract class IVerifyRepository {
  // post verification request
  Future<Map<String, dynamic>> postVerifyRequest(File imageFile);
  // post update profile / member detail
  Future<Map<String, dynamic>> postUpdateProfile(
    String firstName,
    String lastName,
    String dob,
    String email,
    int gender,
    int cityId,
    String address,
    File? imageFile,
  );
  // post reset device
  Future<Map<String, dynamic>> postResetDevice(int reasonId, String reason);
  // post reset device will login
  Future<Map<String, dynamic>> postResetDeviceLogin(
    int reasonId,
    String reason,
  );
  // post cancel reset device
  Future<Map<String, dynamic>> postCancelResetDeviceLogin();
}
