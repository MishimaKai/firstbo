import '../../helpers/string_helper.dart';
import '../../services/api_request.dart';
import '../../utils/enums.dart';
import 'voucher_repository_interface.dart';

class VoucherRepository implements IVoucherRepository {
  // initiate
  final ApiRequest _apiRequest = ApiRequest();

  @override
  Future<Map<String, dynamic>> getVoucherList(
      int type, int? outletId, int pageKey) async {
    Map<String, dynamic> param = <String, dynamic>{};
    if (outletId != null) {
      param = <String, dynamic>{
        'type': type,
        'outlet_id': outletId,
        'page': pageKey,
        'per_page': 10,
      };
    } else {
      param = <String, dynamic>{
        'type': type,
        'page': pageKey,
        'per_page': 10,
      };
    }

    final String query = StringHelper.urlQueries(param);
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.voucherList,
      hasAccessToken: true,
      query: query,
    );

    return result;
  }

  @override
  Future<Map<String, dynamic>> getVoucherCategory() async {
    final Map<String, dynamic> result = await _apiRequest.getRepository(
      endPoint: EndPointName.voucherCategory,
      hasAccessToken: true,
    );

    return result;
  }
}
