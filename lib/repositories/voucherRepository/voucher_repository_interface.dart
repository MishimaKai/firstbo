abstract class IVoucherRepository {
  // request get voucher category
  Future<Map<String, dynamic>> getVoucherCategory();
  // request get voucher list
  // Future<Map<String, dynamic>> getVoucherList(int categoryId, int pageKey);
  Future<Map<String, dynamic>> getVoucherList(
    int type,
    int? outletId,
    int pageKey,
  );
}
