import 'package:get/route_manager.dart';

import '../views/authorize/authorize_page.dart';
import '../views/bottle/bottle_item_detail_page.dart';
import '../views/bottle/bottle_page.dart';
import '../views/change_device/change_device_page.dart';
import '../views/events/event_detail_page.dart';
import '../views/food/food_item_detail_page.dart';
import '../views/food/food_page.dart';
import '../views/gallery/pages/gallery_detail_page.dart';
import '../views/gallery/pages/gallery_page.dart';
import '../views/general_calendar/pages/general_calendar_page.dart';
import '../views/home/pages/home_page.dart';
import '../views/login/pages/sign_in_page.dart';
import '../views/login/pages/verification_otp_page.dart';
import '../views/my_points/pages/my_points_detail_page.dart';
import '../views/my_points/pages/my_points_page.dart';
import '../views/notification/pages/notification_page.dart';
import '../views/profile/pages/delete_account/delete_my_account_page.dart';
import '../views/profile/pages/delete_account/delete_verify_my_account.dart';
import '../views/profile/pages/edit_profile/edit_profile_page.dart';
import '../views/profile/pages/personal_info/personal_info_page.dart';
import '../views/profile/pages/profile_page.dart';
import '../views/profile/pages/term_page.dart';
import '../views/profile/widgets/id_card_picker_page.dart';
import '../views/qr_code_view/qr_code_page.dart';
import '../views/redeem/redeem_item_detail_page.dart';
import '../views/redeem/redeem_page.dart';
import '../views/register/register_page.dart';
import '../views/voucher/pages/voucher_category_page.dart';
import '../views/voucher/pages/voucher_list_page.dart';
import '../views/widgets/frame_view/frame_view.dart';
import 'route_names.dart';

class AppRoutes {
  AppRoutes._();
  static final List<GetPage<dynamic>> routes = <GetPage<dynamic>>[
    GetPage<dynamic>(
      name: RouteNames.frameRoute,
      page: () => const FrameView(),
    ),
    GetPage<dynamic>(
      name: RouteNames.authortizeRoute,
      page: () => const AuthorizeView(),
    ),
    GetPage<dynamic>(
      name: RouteNames.registerRoute,
      page: () => const RegisterPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.signInRoute,
      page: () => const SignInView(),
    ),
    GetPage<dynamic>(
      name: RouteNames.homeRoute,
      page: () => const HomePage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.voucherCategoryRoute,
      page: () => const VoucherCategoryPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.voucherListRoute,
      page: () => const VoucherListPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.redeemRoute,
      page: () => const RedeemPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.profileRoute,
      page: () => const ProfilePage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.galleryRoute,
      page: () => const GalleryPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.verificationOtpRoute,
      page: () => const VerificationPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.personalInfoRoute,
      page: () => const PersonalInfoPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.editProfileRoute,
      page: () => const EditProfilePage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.deleteMyAccountPage,
      page: () => const DeleteMyAccountPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.deleteVerifyMyAccountPage,
      page: () => const DeleteVerifyMyAccountPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.scanQRCode,
      page: () => const QRCodePage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.myPointsPage,
      page: () => const MyPointsPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.myPointsDetailPage,
      page: () => const MyPointsDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.generalCalendar,
      page: () => GeneralCalendar(),
    ),
    GetPage<dynamic>(
      name: RouteNames.bottlePackages,
      page: () => const BottlePage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.foodPackages,
      page: () => const FoodPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.eventsDetailPage,
      page: () => const EventsDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.foodItemDetailPage,
      page: () => const FoodItemDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.bottleItemDetailPage,
      page: () => const BottleItemDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.redeemItemDetailPage,
      page: () => const RedeemItemDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.galleryItemDetail,
      page: () => const GalleryDetailPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.notification,
      page: () => const NotificationPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.termsRoute,
      page: () => const TermsView(),
    ),
    GetPage<dynamic>(
      name: RouteNames.idCardRoute,
      page: () => const IDCardPickerPage(),
    ),
    GetPage<dynamic>(
      name: RouteNames.changeDeviceRoute,
      page: () => const ChangeDevicePage(),
    ),
  ];
}
