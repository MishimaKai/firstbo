import '../utils/enums.dart';

// API Class for initialize url api, client id and client secret
class API {
  // url api for all request data from server
  // demo api:
  // static String host = 'https://demo.dreite.co.id/blackowl/api/web';

  // live api:
  static String host = 'https://membership.blackowl.id/api/web';

  // client id & client secret: used for header data validation
  // demo
  // static String clientIdAndroid = '0bf6beb5c9550daf16fa4dbdd6cba0f2';
  // static String clientSecretAndroid =
  //     'COCg9rMxExfggnG_ScTyOBwpxCjJEmKEQ7vBI95pnKNuXyKXE5';

  // static String clientIdiOS = 'ea694136947172362a49f77356b6a9fe';
  // static String clientSecretiOS =
  //     'OnWB412J6v8_y_8YNTPL1YLsWGdQufr64BSmOe27ot_hPY3rbj';

  // live
  // android
  static String clientIdAndroid = '5f18e8b13f99239c66ef42dc7fd35dd8';
  static String clientSecretAndroid =
      '_NoGxfuUa-S19U53cKEcY3LYyye2E34hp_lyfok9n3RLBDVkEx';

  // iOS
  static String clientIdiOS = 'a0af208c7acf662b7fe3d0212fd1aff6';
  static String clientSecretiOS =
      '9RI9XP0wxUC9g0yUNaJvT8rPe5yyMKp-nizv3eTmek8TkTw0Wi';

  // API endpoint
  String? endPoint(EndPointName endPointName, [String? query]) {
    final Map<EndPointName, String> urlAPI = <EndPointName, String>{
      //? POST METHOD
      // ###################################################################
      //register
      EndPointName.registerOTP: '/v1/member/register?step=otp',
      EndPointName.registerSubmit: '/v1/member/register?step=submit',
      // request post pre-authorize
      EndPointName.preauthorize: '/v1/member/preauthorize',
      // authorize, access token and refresh token
      EndPointName.authorize: '/v1/member/authorize',
      EndPointName.accessToken: '/v1/member/access-token',
      EndPointName.refreshToken: '/v1/member/refresh-token',
      // resend otp
      EndPointName.resendOtp: '/v1/member/resend-otp',
      // logout
      EndPointName.logout: '/v1/member/logout',
      // verification request
      EndPointName.verificationRequest: '/v1/member/verification-request',
      // open bill
      EndPointName.openBill: '/v1/member/open-bill',
      // update profile / member detail
      EndPointName.updateProfile: '/v1/member/update-profile',
      // redeem
      EndPointName.redeem: '/v1/redeem/request',
      // reset device when logged in
      EndPointName.resetDevice: '/v1/member/reset-device-request',
      // reset device when will log in
      EndPointName.resetDeviceLogin: '/v1/member/reset-device-login-request',
      // cancel reset device
      EndPointName.cancelResetDeviceLogin:
          '/v1/member/cancel-reset-device-login-request',
      // delete account
      EndPointName.deleteAccountOTP:
          '/v1/member/request-delete-account?step=otp',
      EndPointName.deleteAccountSubmit:
          '/v1/member/request-delete-account?step=submit',
      //? GET METHOD
      // ###################################################################
      // member detail
      EndPointName.memberDetail: '/v1/member/detail', // old API
      // profile
      EndPointName.memberProfile: '/v1/member/profile', // new API
      // memberAccountInfo
      EndPointName.memberAccountInfo: '/v1/member/account-info',
      // memberHomeAccountInfo
      EndPointName.memberHomeAccountInfo: '/v1/member/home-account-info',
      // history
      EndPointName.history: '/v1/member-point/history',
      // province
      EndPointName.province: '/v1/province/data-select',
      // city
      EndPointName.city: '/v1/city/data-select?province_id=$query',
      // home
      EndPointName.home: '/v1/member/home',
      // event list
      EndPointName.eventList: '/v1/event/list?$query',
      // event detail
      EndPointName.eventDetail: '/v1/event/detail?$query',
      // promo/package list
      EndPointName.promoList: '/v1/promo/list?$query',
      // promo/package detail
      EndPointName.promoDetail: '/v1/promo/detail?$query',
      // gallery list
      EndPointName.galleryList: '/v1/gallery/list?$query',
      // gallery detail
      EndPointName.galleryDetailPage: '/v1/gallery/detail?$query',
      // voucher category
      // EndPointName.voucherCategory: '/v1/member-voucher/category',
      EndPointName.voucherCategory: '/v2/member-voucher/category',
      // voucher list
      // EndPointName.voucherList: '/v1/member-voucher/list?$query',
      EndPointName.voucherList: '/v2/member-voucher/list?$query',
      // point history
      EndPointName.pointHistory: '/v1/member-point/history?$query',
      // point detail
      EndPointName.pointDetail: '/v1/member-point/detail?$query',
      // redeem item list
      EndPointName.redeemItemList: '/v1/redeem/item-list?$query',
      // redeem category list
      EndPointName.redeemCategoryList: '/v1/redeem/category-list?$query',
      // redeem item detail
      EndPointName.redeemItemDetailPage: '/v1/redeem/item-detail?$query',
      // Notification
      EndPointName.notification: '/v1/notification/list?$query',
      // tNc
      EndPointName.tnc: '/v1/tnc/info',
      // privacy policy
      EndPointName.privacyPolicy: '/v1/privacy-policy/info',
      // Country Code
      EndPointName.countryCode: '/v1/country-code/data-select'
      // ###################################################################
    };

    // return endpoint
    return urlAPI[endPointName];
  }
}
