// Header Class for handle header data when request to url api

import 'dart:io';

import '../helpers/device_info.dart';
import '../helpers/shared_pref.dart';
import 'api.dart';

class ApiHeaders {
  /// This method will generate Map data for header
  Future<Map<String, String>> result({
    bool hasAccessToken = true,
    bool doRefreshToken = false,
    bool useLocation = false,
  }) async {
    /// Initialize box for get storage value
    final SharedPref sharedPref = SharedPref();

    /// get detail device
    final String? packageInfoVersion = await DeviceInfo().getVersionApp();

    /// Call getIp address value
    // final ipAddress = await _getLocalIpAddress();

    final Map<String, String> header = <String, String>{
      'Auth-Client-Id':
          Platform.isAndroid ? API.clientIdAndroid : API.clientIdiOS,
      'Auth-Client-Secret':
          Platform.isAndroid ? API.clientSecretAndroid : API.clientSecretiOS,
      // 'Req-Ip': ipAddress.toString(),
      'X-Client-Version': packageInfoVersion.toString(),
    };

    if (useLocation) {
      // call location
      // Location _location = Location();
      // List<double> _getCurrentLongLat = await _location.getCurrentLocation();

      if (Platform.isAndroid) {
        final String? deviceIdAndroid = await DeviceInfo().getDeviceIdAndroid();

        if (hasAccessToken) {
          final String? accessToken = await sharedPref.readAccessToken();
          header.addAll(<String, String>{
            'Auth-Access-Token': accessToken ?? '',
            'Auth-Device-Id': deviceIdAndroid!,
          });
        }
        if (doRefreshToken) {
          header.addAll(<String, String>{'Auth-Device-Id': deviceIdAndroid!});
        }

        if (useLocation) {
          header.addAll(<String, String>{
            // 'Device-Geo-Long': _getCurrentLongLat[0].toString(),
            'Device-Geo-Long': '0.0',
            // 'Device-Geo-Lat': _getCurrentLongLat[1].toString(),
            'Device-Geo-Lat': '0.0'
          });
        }
      } else {
        final String? deviceIdIos = await DeviceInfo().getDeviceIdIos();
        if (hasAccessToken) {
          final String? accessToken = await sharedPref.readAccessToken();
          header.addAll(<String, String>{
            'Auth-Access-Token': accessToken ?? '',
            'Auth-Device-Id': deviceIdIos!,
          });
        }

        if (doRefreshToken) {
          header.addAll(<String, String>{'Auth-Device-Id': deviceIdIos!});
        }

        if (useLocation) {
          header.addAll(<String, String>{
            // 'Device-Geo-Long': _getCurrentLongLat[0].toString(),
            'Device-Geo-Long': '0.0',
            // 'Device-Geo-Lat': _getCurrentLongLat[1].toString(),
            'Device-Geo-Lat': '0.0'
          });
        }
      }
    } else {
      if (Platform.isAndroid) {
        final String? deviceIdAndroid = await DeviceInfo().getDeviceIdAndroid();
        if (hasAccessToken) {
          final String? accessToken = await sharedPref.readAccessToken();
          header.addAll(<String, String>{
            'Auth-Access-Token': accessToken ?? '',
            'Auth-Device-Id': deviceIdAndroid!,
          });
        }

        if (doRefreshToken) {
          header.addAll(<String, String>{'Auth-Device-Id': deviceIdAndroid!});
        }
      } else {
        final String? deviceIdIos = await DeviceInfo().getDeviceIdIos();
        if (hasAccessToken) {
          final String? accessToken = await sharedPref.readAccessToken();
          header.addAll(<String, String>{
            'Auth-Access-Token': accessToken ?? '',
            'Auth-Device-Id': deviceIdIos!,
          });
        }

        if (doRefreshToken) {
          header.addAll(<String, String>{'Auth-Device-Id': deviceIdIos!});
        }
      }
    }

    return header;
  }

  /// This method for get ip address from device
  /// Will return ip address value as string
  // Future<String?> _getLocalIpAddress() async {
  //   final interfaces = await NetworkInterface.list(
  //     type: InternetAddressType.IPv4,
  //     includeLinkLocal: true,
  //   );

  //   try {
  //     // Try VPN connection first
  //     final NetworkInterface vpnInterface =
  //         interfaces.firstWhere((element) => element.name == 'tun0');
  //     return vpnInterface.addresses.first.address;
  //     // ignore: avoid_catching_errors
  //   } on StateError {
  //     // Try wlan connection next
  //     try {
  //       final NetworkInterface interface =
  //           interfaces.firstWhere((element) => element.name == 'wlan0');
  //       return interface.addresses.first.address;
  //     } catch (ex) {
  //       // Try any other connection next
  //       try {
  //         final NetworkInterface interface = interfaces.firstWhere(
  //           (element) => !(element.name == 'tun0' || element.name == 'wlan0'),
  //         );
  //         return interface.addresses.first.address;
  //       } catch (ex) {
  //         return null;
  //       }
  //     }
  //   }
  // }
}
