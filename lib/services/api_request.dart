import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart' as getx;
import 'package:http_parser/http_parser.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../middleware/error_handler.dart';
import '../routes/route_names.dart';
import '../utils/app_colors.dart';
import '../utils/enums.dart';
import '../utils/status_code.dart';
import 'api.dart';
import 'api_header.dart';

class ApiRequest {
  final API api = API();
  static final Dio _dio = Dio(
    BaseOptions(
      connectTimeout: 20000,
      receiveTimeout: 20000,
      baseUrl: API.host,
      contentType: ContentType.json.toString(),
    ),
  )..interceptors.addAll(<Interceptor>[
      PrettyDioLogger(),
      InterceptorsWrapper(
        onResponse: (Response<dynamic> response,
            ResponseInterceptorHandler handler) async {
          final ErrorHandler errorHandler = ErrorHandler();

          final Map<String, dynamic> containData =
              response.data as Map<String, dynamic>;

          final Map<String, dynamic> messageAndErrorCode =
              errorHandler.messageErrorCodeHandler(containData);

          if (messageAndErrorCode['errorCode'] ==
              StatusCode.appCodeTokenInvalid) {
            final bool result = await errorHandler.refreshToken();
            if (result) {
              return handler.next(response);
            }
          } else if (messageAndErrorCode['errorCode'] ==
              StatusCode.appCodeDeviceNotRegistered) {
            CancelToken().cancel();
            getx.Get.offAllNamed(
              RouteNames.changeDeviceRoute,
              arguments: containData,
            );
          } else if (messageAndErrorCode['errorCode'] ==
              StatusCode.appCodeFailedSendingEmail) {
            getx.Get.snackbar(
              'label_failed'.tr,
              messageAndErrorCode['errorMessage'].toString(),
              backgroundColor: AppColors.redAlert,
              colorText: AppColors.whiteBackground,
              snackPosition: getx.SnackPosition.BOTTOM,
            );
            CancelToken().cancel();
            errorHandler.userLogout(
              isDirectToLogin: true,
              isUsePostLogout: false,
            );
          } else {
            return handler.next(response);
          }
        },
        onError: (DioError error, ErrorInterceptorHandler handler) async {
          if (error.response?.statusCode == StatusCode.codeUnauthorized) {}
        },
      ),
    ]);

  Future<Map<String, dynamic>> postFileRepository({
    required EndPointName endPoint,
    Map<String, dynamic>? body,
    String? type,
    File? imageFile,
    bool hasAccessToken = false,
  }) async {
    // header
    final Map<String, dynamic> header = await ApiHeaders().result(
      hasAccessToken: hasAccessToken,
    );
    // endpoint api
    final String? endpoint = api.endPoint(endPoint, type);

    final dynamic bodyMultiPart;

    if (imageFile != null) {
      final Map<String, dynamic> containMap = <String, dynamic>{
        'image_file': await MultipartFile.fromFile(
          imageFile.path,
          filename: imageFile.path.split('/').last,
          contentType: MediaType(
            'image',
            imageFile.path.split('.').last,
          ),
        ),
      };
      body?.forEach((String key, dynamic value) {
        containMap[key] = value;
      });
      bodyMultiPart = FormData.fromMap(containMap);
    } else {
      final Map<String, dynamic> tempMap = body!;
      if (tempMap.containsKey('image_file')) {
        tempMap['image_file'] = await MultipartFile.fromFile(
          (tempMap['image_file'] as File).path,
          filename: (tempMap['image_file'] as File).path.split('/').last,
          contentType: MediaType(
            'image',
            (tempMap['image_file'] as File).path.split('.').last,
          ),
        );
        bodyMultiPart = FormData.fromMap(tempMap);
      } else {
        bodyMultiPart = FormData.fromMap(body);
      }
    }

    try {
      final Response<dynamic> response = await _dio.post(
        endpoint.toString(),
        data: bodyMultiPart,
        options: Options(headers: header),
      );
      return response.data as Map<String, dynamic>;
    } on DioError catch (err) {
      // error handling in here
      return _whenReceiveError(err);
    }
  }

  Future<Map<String, dynamic>> postRepository({
    required EndPointName endPoint,
    required dynamic body,
    bool hasAccessToken = false,
    bool doRefreshtoken = false,
    bool useLocation = false,
    String? contentType,
  }) async {
    // header
    final Map<String, dynamic> header = await ApiHeaders().result(
      hasAccessToken: hasAccessToken,
      doRefreshToken: doRefreshtoken,
      useLocation: useLocation,
    );
    // endpoint api
    final String? endpoint = api.endPoint(endPoint);

    try {
      final Response<dynamic> response = await _dio.post(
        endpoint.toString(),
        data: body,
        options: Options(
          headers: header,
          contentType: contentType,
        ),
      );
      return response.data as Map<String, dynamic>;
    } on DioError catch (err) {
      // error handling in here
      return _whenReceiveError(err);
    }
  }

  Future<Map<String, dynamic>> getRepository({
    required EndPointName endPoint,
    bool hasAccessToken = false,
    String? query = '',
  }) async {
    // header
    final Map<String, dynamic> header = await ApiHeaders().result(
      hasAccessToken: hasAccessToken,
    );
    // endpoint api
    final String? endpoint = api.endPoint(endPoint, query);

    try {
      final Response<dynamic> response = await _dio.get(
        endpoint.toString(),
        options: Options(
          followRedirects: false,
          headers: header,
        ),
      );
      return response.data as Map<String, dynamic>;
    } on DioError catch (err) {
      // error handling in here
      return _whenReceiveError(err);
    }
  }

  Map<String, dynamic> _whenReceiveError(DioError err) {
    // error handling in here
    Map<String, dynamic> error = <String, dynamic>{};
    if (err.type == DioErrorType.other) {
      if (err.message.contains('SocketException') ||
          err.message.contains('HttpException')) {
        error = <String, dynamic>{
          'message': 'No internet connection. #201',
        };
      } else {
        error = <String, dynamic>{'message': err.message};
      }
    } else if (err.type == DioErrorType.connectTimeout) {
      error = <String, dynamic>{
        'message': 'No internet connection. #202',
      };
    } else if (err.type == DioErrorType.receiveTimeout) {
      error = <String, dynamic>{
        'message': 'Unable to connect. #203',
      };
    } else {
      error = <String, dynamic>{
        'code': err.response?.statusCode,
        'message': err.response?.statusCode == 404
            ? '${err.response?.statusMessage}. #${err.response?.statusCode}'
            : err.response?.statusCode,
      };
    }

    return error;
  }
}
