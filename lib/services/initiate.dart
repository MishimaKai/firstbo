import 'package:intl/date_symbol_data_local.dart';

import '../helpers/translations.dart';

dynamic initiate() async {
  final BlackOwlTranslations translations = BlackOwlTranslations();
  // date formating
  await initializeDateFormatting();
  // initiate localization
  translations.initLocale();
}
