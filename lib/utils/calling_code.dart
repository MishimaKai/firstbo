class CallingCode {
  static const int bruneiCode = 673;
  static const int cambodiaCode = 855;
  static const int indoCode = 62;
  static const int laosCode = 856;
  static const int malaysiaCode = 60;
  static const int myanmarCode = 95;
  static const int philipinesCode = 63;
  static const int singaporeCode = 65;
  static const int thailandCode = 66;
  static const int timorCode = 670;
  static const int vietnameCode = 84;
}
