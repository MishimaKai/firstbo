import 'calling_code.dart';

class CountryIdentifier {
  CountryIdentifier._();
  static final List<Map<String, dynamic>> countryList = <Map<String, dynamic>>[
    <String, dynamic>{
      'name': 'Brunei Darussalam',
      'flag': 'flags/brn.png',
      'code': CallingCode.bruneiCode
    },
    <String, dynamic>{
      'name': 'Cambodia',
      'flag': 'flags/khm.png',
      'code': CallingCode.cambodiaCode,
    },
    <String, dynamic>{
      'name': 'Indonesia',
      'flag': 'flags/idn.png',
      'code': CallingCode.indoCode,
    },
    <String, dynamic>{
      'name': 'Laos',
      'flag': 'flags/lao.png',
      'code': CallingCode.laosCode,
    },
    <String, dynamic>{
      'name': 'Malaysia',
      'flag': 'flags/mys.png',
      'code': CallingCode.malaysiaCode,
    },
    <String, dynamic>{
      'name': 'Myanmar',
      'flag': 'flags/mmr.png',
      'code': CallingCode.myanmarCode,
    },
    <String, dynamic>{
      'name': 'Philipines',
      'flag': 'flags/phl.png',
      'code': CallingCode.philipinesCode,
    },
    <String, dynamic>{
      'name': 'Singapore',
      'flag': 'flags/sgp.png',
      'code': CallingCode.singaporeCode,
    },
    <String, dynamic>{
      'name': 'Thailand',
      'flag': 'flags/tha.png',
      'code': CallingCode.thailandCode,
    },
    <String, dynamic>{
      'name': 'Timor Leste',
      'flag': 'flags/tls.png',
      'code': CallingCode.timorCode,
    },
    <String, dynamic>{
      'name': 'Vietnam',
      'flag': 'flags/vnm.png',
      'code': CallingCode.vietnameCode,
    },
  ];
}
