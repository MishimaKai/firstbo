// this class represent for shared preferences key
class PreferencesKey {
  // authorize
  static const String keyAccessToken = 'access_token';
  static const String keyRefreshToken = 'refresh_token';
  // language
  static const String keyCountry = 'country';
  static const String keyLanguage = 'language';
  // redeem category id
  static const String keyRedeemCategoryId = 'redeem_category_id';
  // change device status
  static const String keyChangeDeviceStatus = 'change_device_status';
  // profile
  static const String keyUserProfileFirstName = 'first_name';
  static const String keyUserProfileLastName = 'last_name';
  static const String keyUserProfilePhoneNumber = 'phone_number';
  static const String keyUserProfileEmailAddress = 'email_address';
  static const String keyUserProfileGender = 'gender';
  static const String keyUserProfileDob = 'dob';
  static const String keyUserProfileCityNumber = 'city_number';
  static const String keyUserProfileStateNumber = 'state_number';
  static const String keyUserProfileAddress = 'address';
  static const String keyUserProfileCountryNumber = 'country_number';
  static const String keyUserProfileCountryFlag = 'country_flag';
  static const String keyUserProfileMembership = 'membership';
  static const String keyUserProfileMembershipBenefit = 'membership_benefit';
  static const String keyUserPicture = 'user_picture';
  static const String keyUserProfileVerified = 'verified';
  static const String keyUserProfileVerifiedRequest = 'verified_request';
  static const String keyUserProfileResetDeviceRequest = 'reset_device_request';
  static const String keyCompanyProfileInstagram = 'company_instagram';
  static const String keyCompanyProfileWhatsapp = 'company_whatsapp';
  // my membership
  static const String keyCurrentMembership = 'current_membership';
  static const String keyNextMembership = 'next_membership';
  static const String keyIsReachedTransaction = 'is_reached_trx';
  static const String keyCurrentTransaction = 'current_trx';
  static const String keyMinTransaction = 'min_trx';
  // my membership -- spending power
  static const String keySpendingPowerCurrent = 'spending_power_current';
  static const String keySpendingPowerMinExistingMembership =
      'spending_power_min_existing_membership';
  static const String keySpendingPowerMinNextMembership =
      'spending_power_min_next_membership';
  static const String keySpendingPowerReqNextLevelMembership =
      'spending_power_req_next_membership';
  // spending power -- percentage
  static const String keySpendingPowerPercentage = 'spending_power_percentage';
  // spending power -- stay warning box
  static const String keySpendingPowerStayWarningBox =
      'spending_power_stay_warning_box';
  // my membership -- total spending
  static const String keyTotalSpendingCurrent = 'total_spending_current';
  static const String keyTotalSpendingDisplayCurrent =
      'total_spending_display_current';
  static const String keyTotalSpendingMinExistingMembership =
      'total_spending_min_existing_membership';
  static const String keyTotalSpendingMinNextMembership =
      'total_spending_min_next_membership';
  static const String keyTotalSpendingReqNextLevelMembership =
      'total_spending_req_next_level_membership';
  static const String keyTotalSpendingDisplayReqNextLevelMembership =
      'total_spending_display_req_next_level_membership';
  // total spending -- percentage
  static const String keyTotalSpendingPercentage = 'total_spending_percentage';
  // my membership -- reset
  static const String keyMembershipReset = 'membership_reset';
  // my membership -- rank
  static const String keyMembershipRank = 'membership_rank';
  // change user profile data
  static const String keyUserProfileIsUserChanged = 'is_user_profile_changed';
  // FCM topic
  static const String keyFCMTopicList = 'fcm_topic_name_list';
  // Pop Up
  static const String keyPopUpId = 'pop_up_id';
  // detect fold device for snackbar
  static const String keyFoldDevice = 'fold_device';
}
