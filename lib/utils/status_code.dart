// this class represent for definded status code
class StatusCode {
  static const int appCodeUserInvalid = 1000;
  static const int appCodeTokenInvalid = 1001;
  static const int appCodeAuthInvalid = 1002;
  static const int appCodeValidateData = 1003;
  static const int appCodeFailedSendingEmail = 1004;
  static const int appCodeFailedSavedData = 1005;
  static const int appCodeRequiredField = 1006;
  static const int appCodeDeviceInvalid = 1007;
  static const int appCodeFailedGetData = 1008;
  static const int appCodeDeviceNotRegistered = 1009;
  static const int appCodeEmptyBody = 1011;
  static const int appCodeDataExist = 1202;
  static const int appCodeServerMaintenance = 2000;
  static const int appCodeFailedProcessBill = 1110;

  // Error code commonly used;
  static const int codeOk = 200;
  static const int codeUnknownAction = 301;
  static const int codeParamEmpty = 302;
  static const int codeBadRequest = 400;
  static const int codeUnauthorized = 401;
  static const int codePaymentRequired = 402;
  static const int codeForbidden = 403;
  static const int codeNotFound = 404;
  static const int codeIntServerError = 500;
  static const int codeNotImplemented = 501;
}
