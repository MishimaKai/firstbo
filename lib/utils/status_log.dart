// this class represent log category
class StatusLog {
  static const String logNetworking = 'NETWORKING';
  static const String logResponse = 'RESPONSE';
  static const String logEndPoint = 'ENDPOINT';
  static const String logHeader = 'HEADER';
  static const String logParameter = 'PARAMETER';
  static const String logIntercept = 'INTERCEPTOR';
}
