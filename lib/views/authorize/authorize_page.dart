import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/account/authorize_controller.dart';
import '../../helpers/notification_service.dart';
import '../../models/UI/notifications/received_notification_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/assets_list.dart';
import '../../utils/enums.dart';
import '../../utils/size_config.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/text/montserrat_text_view.dart';

class AuthorizeView extends StatefulWidget {
  // constructor
  const AuthorizeView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => AuthorizeViewState();
}

class AuthorizeViewState extends State<AuthorizeView> {
  // variable
  late Image _imageLogo;

  @override
  void initState() {
    super.initState();
    // for iOS 10 or older
    NotificationService.configureDidReceiveLocalNotification(
      context: context,
      onPressed: (ReceivedNotification event) async {
        await Get.toNamed(event.toString());
      },
    );
    // assign image to variable
    _imageLogo = Image.asset(Assets.imageBlackOwlTransparent);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_imageLogo.image, context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        view: _builderContent(),
      ),
    );
  }

  Widget _builderContent() {
    return GetBuilder<AuthorizeController>(
      init: AuthorizeController(pageContext: context),
      builder: (AuthorizeController controller) =>
          _containerWrapper(controller),
    );
  }

  Widget _containerWrapper(AuthorizeController controller) {
    return Container(
      color: AppColors.darkBackground,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _splashScreenLogo()),
          ResponsiveRowColumnItem(child: _infoAppContent(controller))
        ],
      ),
    );
  }

  Widget _infoAppContent(AuthorizeController controller) {
    return Obx(
      () => _infoAppWrapper(
        version: '${controller.versionApp.value}',
        year: controller.year.value,
        appname: '${controller.appName.value}',
      ),
    );
  }

  Widget _splashScreenLogo() {
    return Expanded(
      child: Align(
        child: Image.asset(
          Assets.imageBlackOwlTransparent,
          width: SizeConfig.horizontal(50),
          height: SizeConfig.horizontal(50),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _infoAppWrapper({
    required String version,
    required int year,
    required String appname,
  }) {
    return SizedBox(
      height: SizeConfig.vertical(12),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _versionNumber(version)),
          ResponsiveRowColumnItem(child: _applicationName(year, appname)),
        ],
      ),
    );
  }

  Widget _versionNumber(String version) {
    return MontserratTextView(
      value: 'Version $version',
      color: AppColors.darkGold,
    );
  }

  Widget _applicationName(int year, String name) {
    return MontserratTextView(
      value: '©$year. $name',
      alignText: AlignTextType.center,
      color: AppColors.darkGold,
    );
  }
}
