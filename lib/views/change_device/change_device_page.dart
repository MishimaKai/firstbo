import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/account/change_device_controller.dart';
import '../../helpers/scroll_config.dart';
import '../../utils/app_colors.dart';
import '../../utils/assets_list.dart';
import '../../utils/enums.dart';
import '../../utils/size_config.dart';
import '../widgets/buttons/custom_flat_buttons.dart';
import '../widgets/buttons/montserrat_button.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/layouts/under_line.dart';
import '../widgets/text/montserrat_text_view.dart';
import 'widgets/reason_sheet.dart';

class ChangeDevicePage extends StatefulWidget {
  const ChangeDevicePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ChangeDevicePageState();
}

class ChangeDevicePageState extends State<ChangeDevicePage> {
  late ChangeDeviceController controller;

  @override
  void initState() {
    controller = Get.put(ChangeDeviceController(pageContext: context));
    SchedulerBinding.instance.addPostFrameCallback((_) {
      controller.initiateArgs(context);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // intiate size config device
    SizeConfig().init(context);
    return KeyboardSizeProvider(
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        avoidBottomInset: true,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        view: _annotatedWrapper(context, controller),
      ),
    );
  }

  Widget _annotatedWrapper(
    BuildContext context,
    ChangeDeviceController controller,
  ) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: Container(
        color: AppColors.darkBackground,
        child: Consumer<ScreenHeight>(
          builder: (BuildContext context, ScreenHeight res, Widget? child) =>
              _mainGestureWrapper(context, res, controller),
        ),
      ),
    );
  }

  Widget _mainGestureWrapper(
    BuildContext context,
    ScreenHeight res,
    ChangeDeviceController controller,
  ) {
    return Obx(() {
      if (controller.isChangeDeviceProceed.isFalse) {
        return GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
            if (res.isSmall) {
              ScrollConfig.resetScrollView(controller.scrollController);
            }
          },
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnMainAxisSize: MainAxisSize.min,
            columnMainAxisAlignment: MainAxisAlignment.center,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Expanded(
                  child: CustomScrollView(
                    controller: controller.scrollController,
                    physics: ScrollConfig.isUseScroll(res.isOpen),
                    slivers: <Widget>[
                      SliverFillRemaining(
                        hasScrollBody: false,
                        child: _mainViewWrapper(context, controller),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      } else {
        return RefreshIndicator(
          triggerMode: RefreshIndicatorTriggerMode.anywhere,
          child: Stack(
            children: <Widget>[
              ListView(),
              _mainViewWrapper(context, controller),
            ],
          ),
          onRefresh: () async {
            await controller.onRefresh();
          },
        );
      }
    });
  }

  Widget _mainViewWrapper(
    BuildContext context,
    ChangeDeviceController controller,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisAlignment: MainAxisAlignment.center,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _logoApp()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _proceedWrapper()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _textButton()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
      ],
    );
  }

  Widget _proceedWrapper() {
    return Obx(() {
      if (controller.isChangeDeviceProceed.isFalse) {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnMainAxisAlignment: MainAxisAlignment.center,
          columnMainAxisSize: MainAxisSize.min,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: _instruction()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2.5)),
            ResponsiveRowColumnItem(child: _reasonButton()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2.5)),
            ResponsiveRowColumnItem(child: _reasonTextField()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2.5)),
            ResponsiveRowColumnItem(child: _button()),
          ],
        );
      } else {
        return MontserratTextView(
          value: 'Waiting for Black Owl Approval',
          size: SizeConfig.safeHorizontal(4),
          fontWeight: FontWeight.w300,
          alignText: AlignTextType.center,
          color: Colors.white,
        );
      }
    });
  }

  // logo company
  Widget _logoApp() {
    return Align(
      child: Image.asset(
        Assets.imageBlackOwlTransparent,
        width: SizeConfig.horizontal(45),
        height: SizeConfig.horizontal(45),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _instruction() {
    return SizedBox(
      width: SizeConfig.horizontal(80),
      child: MontserratTextView(
        value: 'change_device_instruction'.tr,
        size: SizeConfig.safeHorizontal(4),
        fontWeight: FontWeight.w300,
        alignText: AlignTextType.center,
        color: Colors.white,
      ),
    );
  }

  Widget _reasonButton() {
    return SizedBox(
      width: SizeConfig.horizontal(80),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Reason',
              color: AppColors.whiteBackground,
              fontWeight: FontWeight.w700,
              size: SizeConfig.safeHorizontal(3),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Get.bottomSheet(
                    ReasonSheet(controller: controller),
                    persistent: false,
                    isScrollControlled: false,
                  );
                },
                child: ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.ROW,
                  rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(
                      child: Obx(
                        () => MontserratTextView(
                          value: controller.reasonSelection.value ??
                              'Please select',
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          size: SizeConfig.horizontal(3.5),
                        ),
                      ),
                    ),
                    const ResponsiveRowColumnItem(
                      child: Icon(Icons.arrow_drop_down, color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: Underline()),
        ],
      ),
    );
  }

  Widget _reasonTextField() {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return Obx(() {
      if (controller.isManualInput.isTrue) {
        return SizedBox(
          width: SizeConfig.horizontal(80),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnCrossAxisAlignment: CrossAxisAlignment.start,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: MontserratTextView(
                  value: 'Type your reason',
                  color: AppColors.whiteBackground,
                  fontWeight: FontWeight.w700,
                  size: SizeConfig.safeHorizontal(3),
                ),
              ),
              ResponsiveRowColumnItem(
                child: TextField(
                  controller: controller.reasonInputController,
                  cursorColor: AppColors.darkGold,
                  // maxLines: 1,
                  keyboardType: TextInputType.name,
                  textAlign: TextAlign.left,
                  // cursorHeight: SizeConfig.vertical(2),
                  cursorHeight: 14,
                  style: montserratStyle.registerFieldWhite(3),
                  decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    hintText: 'Type here..',
                    hintStyle: montserratStyle.registerFieldGrey(3),
                  ),
                ),
              ),
              const ResponsiveRowColumnItem(child: Underline()),
              // ResponsiveRowColumnItem(child: MontserratTextError(value: '')),
            ],
          ),
        );
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _button() {
    return Obx(
      () => Container(
        margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
        width: SizeConfig.screenWidth,
        child: CustomFlatButton(
          loading: controller.idSelection.value == null,
          onTap: () {
            controller.showDialogConfirm(context);
          },
          text: 'Change Device',
        ),
      ),
    );
  }

  Widget _textButton() {
    return MontserratButton(
      onTap: () {
        controller.showDialogCancel(context);
      },
      value: 'Cancel Change Device',
    );
  }
}
