import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class ConfirmationDialog extends StatelessWidget {
  const ConfirmationDialog({
    Key? key,
    required this.content,
    this.isDualScreen = false,
    required this.onPress,
    required this.isLoading,
  }) : super(key: key);

  final String content;
  final bool isDualScreen;
  final Function() onPress;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal(context);
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(80),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogNormal(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: SizedBox(
        width: SizeConfig.horizontal(80),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogWrapper(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(
            vertical: SizeConfig.vertical(1.5),
            horizontal: SizeConfig.horizontal(2.5),
          ),
          decoration: _dialogDecor(),
          child: _dialogContent(context),
        ),
      ],
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
          // AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _dialogContent(BuildContext context) {
    return Column(
      children: <Widget>[
        Image.asset(Assets.iconAlertCircle, height: SizeConfig.horizontal(15)),
        const SpaceSizer(vertical: 1),
        MontserratTextView(
          value: 'Attention',
          alignText: AlignTextType.center,
          color: AppColors.darkBackground,
          fontWeight: FontWeight.bold,
          size: SizeConfig.safeHorizontal(4),
        ),
        const SpaceSizer(vertical: 1),
        SizedBox(
          height: SizeConfig.horizontal(35),
          child: Scrollbar(
            child: ListView(
              children: <Widget>[
                const SpaceSizer(vertical: 0.5),
                Html(data: content),
                const SpaceSizer(vertical: 0.5),
              ],
            ),
          ),
        ),
        // Html(data: content),
        const SpaceSizer(vertical: 2),
        MontserratTextView(
          value: 'Are you sure want to change device?',
          alignText: AlignTextType.center,
          color: AppColors.darkBackground,
          size: SizeConfig.safeHorizontal(3),
        ),
        const SpaceSizer(vertical: 2),
        _noButton(context),
        const SpaceSizer(vertical: 2),
        _yesButton(),
      ],
    );
  }

  Widget _noButton(BuildContext context) {
    return Container(
      width: SizeConfig.horizontal(80),
      height: isDualScreen ? SizeConfig.horizontal(10) : SizeConfig.vertical(5),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.lighterGold),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
        color: Colors.transparent,
      ),
      child: RippleButton(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: SizedBox(
          width: SizeConfig.horizontal(80),
          height:
              isDualScreen ? SizeConfig.horizontal(10) : SizeConfig.vertical(5),
          child: Center(
            child: MontserratTextView(
              value: 'No',
              alignText: AlignTextType.center,
              color: AppColors.whiteBackground,
              size: SizeConfig.safeHorizontal(3.5),
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }

  Widget _yesButton() {
    return CustomFlatButton(
      onTap: () {
        onPress();
      },
      height: isDualScreen ? SizeConfig.horizontal(10) : null,
      useCircularLoading: true,
      loading: isLoading,
      text: 'Yes',
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }
}
