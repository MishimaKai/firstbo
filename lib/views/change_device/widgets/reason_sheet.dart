import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/account/change_device_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/frame/frame_sheet.dart';
import '../../widgets/layouts/under_line.dart';
import '../../widgets/text/montserrat_text_view.dart';

class ReasonSheet extends StatelessWidget {
  const ReasonSheet({Key? key, required this.controller}) : super(key: key);

  final ChangeDeviceController controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameSheet(
      height: SizeConfig.vertical(32),
      backgroundColor: AppColors.flatGold,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          SizeConfig.horizontal(2.5),
          0,
          SizeConfig.horizontal(2.5),
          SizeConfig.vertical(1),
        ),
        child: _listWrapper(),
      ),
    );
  }

  Widget _listWrapper() {
    return ListView.builder(
      itemCount: controller.reasonList.length,
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      itemBuilder: (BuildContext context, int index) {
        return Obx(
          () => _item(
            context: context,
            id: controller.reasonList[index].id,
            reason: controller.reasonList[index].title,
          ),
        );
      },
    );
  }

  Widget _item({
    required BuildContext context,
    required int id,
    required String reason,
  }) {
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(1.5)),
      child: RippleButton(
        onTap: () {
          controller.changeReason(id, context);
        },
        radius: 0,
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnMainAxisSize: MainAxisSize.min,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: _content(context: context, id: id, reason: reason),
            ),
            ResponsiveRowColumnItem(
              child: Underline(lineColor: AppColors.darkBackground),
            ),
          ],
        ),
      ),
    );
  }

  Widget _content({
    required BuildContext context,
    required int id,
    required String reason,
  }) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: reason,
            fontWeight: FontWeight.w400,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
        ResponsiveRowColumnItem(
          child: Radio<int?>(
            value: id,
            groupValue: controller.idSelection.value,
            activeColor: AppColors.darkBackground,
            fillColor: MaterialStateProperty.all(AppColors.darkBackground),
            onChanged: (Object? value) {
              controller.changeReason(id, context);
            },
          ),
        ),
      ],
    );
  }
}
