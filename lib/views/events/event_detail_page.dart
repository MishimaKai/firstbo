import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../controllers/events/event_detail_controller.dart';
import '../../models/data/outlet_info/outlet_info_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/page_names.dart';
import '../../utils/size_config.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/frame_view/frame_action_wrapper.dart';
import '../widgets/layouts/shimmer_placeholder.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/shimmers/badge_shimmer.dart';
import '../widgets/shimmers/item_detail_shimmer.dart';
import '../widgets/text/montserrat_text_view.dart';

class EventsDetailPage extends StatelessWidget {
  // constructor
  const EventsDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: _frameAction(),
      view: _builderContent(context),
    );
  }

  Widget _frameAction() {
    return FrameActionWrapper(
      title: PageNames.eventPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        Get.back();
      },
    );
  }

  Widget _builderContent(BuildContext context) {
    return GetBuilder<EventDetailController>(
      init: EventDetailController(pageContext: context),
      builder: (EventDetailController controller) =>
          AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          systemNavigationBarColor: AppColors.darkBackground,
          systemNavigationBarIconBrightness: Brightness.light,
        ),
        child: _obxWrapper(controller),
      ),
    );
  }

  Widget _obxWrapper(EventDetailController controller) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const ItemDetailShimmer();
      } else {
        return Container(
          color: AppColors.darkBackground,
          margin: EdgeInsets.only(bottom: SizeConfig.vertical(1)),
          child: _eventsDetailWrapper(controller),
        );
      }
    });
  }

  Widget _eventsDetailWrapper(EventDetailController controller) {
    return SingleChildScrollView(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _bannerImageContent(controller)),
          ResponsiveRowColumnItem(child: _descriptionEvents(controller)),
        ],
      ),
    );
  }

  Widget _bannerImageContent(EventDetailController controller) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: CachedNetworkImage(
        imageUrl: controller.getImageEvent(),
        fit: BoxFit.fill,
        placeholder: (BuildContext context, String url) =>
            ShimmerPlaceholder(width: SizeConfig.screenWidth),
        errorWidget: (BuildContext context, String url, dynamic error) =>
            const SizedBox.shrink(),
      ),
    );
  }

  Widget _descriptionEvents(EventDetailController controller) {
    return Container(
      color: AppColors.darkBackground,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.horizontal(2.5),
                right: SizeConfig.horizontal(2.5),
                top: SizeConfig.vertical(2),
              ),
              child: _titleAndOutlet(controller),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
              ),
              child: MontserratTextView(
                value: controller.getWrappedDate(),
                color: AppColors.whiteBackground,
                size: SizeConfig.safeHorizontal(4),
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(0.5),
              ),
              child: Html(
                data: controller.getDescEvent(),
                style: <String, Style>{
                  'p': Style(color: AppColors.whiteBackground)
                },
                onLinkTap: (String? url, _, __, ___) async {
                  // convert url string to Uri
                  final Uri uriSouce = Uri.parse(url!);
                  // then launch url
                  await launchUrl(
                    uriSouce,
                    mode: LaunchMode.externalApplication,
                  );
                },
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
              ),
              child: _outletList(controller.getListOutlet()),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10))
        ],
      ),
    );
  }

  Widget _titleAndOutlet(EventDetailController controller) {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: RichText(
            text: TextSpan(
              text: controller.getEventTitle(),
              style: montserratStyle.whiteTitleEvent6(),
            ),
          ),
        ),
      ],
    );
  }

  Widget _outletList(List<OutletInfoModel> outletList) {
    if (outletList.isNotEmpty) {
      return _whenOutletExist(outletList);
    } else {
      return _whenOutletEmpty(outletList);
    }
  }

  Widget _whenOutletExist(List<OutletInfoModel> outletList) {
    return Wrap(
      children: List<Widget>.generate(
        outletList.length,
        (int index) {
          if (outletList[index].name != '') {
            return _containerOutlet(outletList[index].name);
          } else {
            return const BadgeShimmer();
          }
        },
      ),
    );
  }

  Widget _whenOutletEmpty(List<OutletInfoModel> outletList) {
    if (outletList.isEmpty) {
      return const SizedBox.shrink();
    } else {
      return Wrap(
        children: List<Widget>.generate(
          2,
          (int index) => const BadgeShimmer(),
        ),
      );
    }
  }

  Widget _containerOutlet(String outletName) {
    return Container(
      margin: EdgeInsets.only(
        right: SizeConfig.horizontal(2),
        top: SizeConfig.vertical(0.5),
        bottom: SizeConfig.vertical(0.5),
      ),
      padding: EdgeInsets.all(SizeConfig.horizontal(1)),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.flatGold),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      ),
      child: _outletNameWrapper(outletName),
    );
  }

  Widget _outletNameWrapper(String param) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: param,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
            color: AppColors.whiteBackground,
          ),
        ),
      ],
    );
  }
}
