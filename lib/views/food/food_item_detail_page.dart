import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/promo/promo_detail_controller.dart';
import '../../helpers/string_helper.dart';
import '../../models/data/outlet_info/outlet_info_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/page_names.dart';
import '../../utils/size_config.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/frame_view/frame_action_wrapper.dart';
import '../widgets/layouts/shimmer_placeholder.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/layouts/under_line.dart';
import '../widgets/shimmers/badge_shimmer.dart';
import '../widgets/shimmers/item_detail_shimmer.dart';
import '../widgets/text/montserrat_text_view.dart';

class FoodItemDetailPage extends StatelessWidget {
  // constructor
  const FoodItemDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: _frameAction(),
      view: _builderContent(context),
    );
  }

  Widget _frameAction() {
    return FrameActionWrapper(
      title: PageNames.foodPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        Get.back();
      },
    );
  }

  Widget _builderContent(BuildContext context) {
    return GetBuilder<PromoDetailController>(
      init: PromoDetailController(pageContext: context),
      builder: (PromoDetailController controller) =>
          _annotateWrapper(controller),
    );
  }

  Widget _annotateWrapper(PromoDetailController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _obxWrapper(controller),
    );
  }

  Widget _obxWrapper(PromoDetailController controller) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const ItemDetailShimmer();
      } else {
        return Container(
          color: AppColors.darkBackground,
          margin: EdgeInsets.only(
            bottom: SizeConfig.vertical(1),
          ),
          child: _foodItemDetailWrapper(controller),
        );
      }
    });
  }

  Widget _foodItemDetailWrapper(PromoDetailController controller) {
    return SingleChildScrollView(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _bannerImageContent(controller)),
          ResponsiveRowColumnItem(child: _descriptionFoodItem(controller)),
        ],
      ),
    );
  }

  Widget _bannerImageContent(PromoDetailController controller) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: CachedNetworkImage(
        imageUrl: controller.getPromoImage(),
        fit: BoxFit.fill,
        placeholder: (BuildContext context, String url) =>
            ShimmerPlaceholder(width: SizeConfig.screenWidth),
        errorWidget: (BuildContext context, String url, dynamic error) =>
            const SizedBox.shrink(),
      ),
    );
  }

  Widget _descriptionFoodItem(PromoDetailController controller) {
    return Container(
      width: SizeConfig.screenWidth,
      color: AppColors.darkBackground,
      padding: EdgeInsets.only(top: SizeConfig.vertical(2)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          _titleFoodItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _priceFoodItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _descriptionDetailFoodItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: _dateDisplay(controller.getWrappedDate()),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
              ),
              child: _outletList(controller.getListOutlet()),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
        ],
      ),
    );
  }

  Widget _dateDisplay(String param) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      child: MontserratTextView(
        value: param,
        color: AppColors.whiteBackground,
        size: SizeConfig.safeHorizontal(4),
      ),
    );
  }

  ResponsiveRowColumnItem _titleFoodItem(PromoDetailController controller) {
    return ResponsiveRowColumnItem(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
        child: MontserratTextView(
          value: controller.getPromoName(),
          color: AppColors.whiteBackground,
          size: SizeConfig.safeHorizontal(5),
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  ResponsiveRowColumnItem _descriptionDetailFoodItem(
    PromoDetailController controller,
  ) {
    return ResponsiveRowColumnItem(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(0.5),
        ),
        child: Html(
          data: controller.getPromoDetail(),
          style: <String, Style>{'p': Style(color: AppColors.whiteBackground)},
        ),
      ),
    );
  }

  ResponsiveRowColumnItem _priceFoodItem(PromoDetailController controller) {
    if (controller.getPromoPrice() != 0) {
      return _priceDisplay(controller);
    } else {
      return const ResponsiveRowColumnItem(child: SizedBox.shrink());
    }
  }

  ResponsiveRowColumnItem _priceDisplay(PromoDetailController controller) {
    return ResponsiveRowColumnItem(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _price(controller)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
              ),
              child: const Underline(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _price(PromoDetailController controller) {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      child: RichText(
        text: TextSpan(
          text: 'RP ',
          style: montserratStyle.flatGoldDescriptionPriceItem4(),
          children: <TextSpan>[
            TextSpan(
              text: StringHelper.convertToRupiah(controller.getPromoPrice()),
              style: montserratStyle.flatGoldDescriptionPriceItem6(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _outletList(List<OutletInfoModel> outletList) {
    if (outletList.isNotEmpty) {
      return _whenOutletExist(outletList);
    } else {
      return _whenOutletEmpty(outletList);
    }
  }

  Widget _whenOutletExist(List<OutletInfoModel> outletList) {
    return Wrap(
      children: List<Widget>.generate(
        outletList.length,
        (int index) {
          if (outletList[index].name != '') {
            return _containerOutlet(outletList[index].name);
          } else {
            return const BadgeShimmer();
          }
        },
      ),
    );
  }

  Widget _whenOutletEmpty(List<OutletInfoModel> outletList) {
    if (outletList.isEmpty) {
      return const SizedBox.shrink();
    } else {
      return Wrap(
        children: List<Widget>.generate(
          2,
          (int index) => const BadgeShimmer(),
        ),
      );
    }
  }

  Widget _containerOutlet(String outletName) {
    return Container(
      margin: EdgeInsets.only(
        right: SizeConfig.horizontal(2),
        top: SizeConfig.vertical(0.5),
        bottom: SizeConfig.vertical(0.5),
      ),
      padding: EdgeInsets.all(SizeConfig.horizontal(1)),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.flatGold),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      ),
      child: _outletNameWrapper(outletName),
    );
  }

  Widget _outletNameWrapper(String param) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: param,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
            color: AppColors.whiteBackground,
          ),
        ),
      ],
    );
  }
}
