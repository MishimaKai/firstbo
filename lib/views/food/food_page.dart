import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' as getx;
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/promo/promo_controller.dart';
import '../../models/data/package/package_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/enums.dart';
import '../../utils/page_names.dart';
import '../../utils/size_config.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/frame_view/frame_action_wrapper.dart';
import '../widgets/layouts/custom_empty_state.dart';
import '../widgets/layouts/price_item.dart';
import '../widgets/shimmers/price_shimmer.dart';

class FoodPage extends StatelessWidget {
  // constuctor
  const FoodPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: FrameActionWrapper(
        title: PageNames.foodPage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          getx.Get.back();
        },
      ),
      view: _builderContent(context),
    );
  }

  Widget _builderContent(BuildContext context) {
    return getx.GetBuilder<PromoController>(
      init: PromoController(pageContext: context),
      builder: (PromoController controller) => _annotateWrapper(controller),
    );
  }

  Widget _annotateWrapper(PromoController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _streamWrapper(controller),
    );
  }

  Widget _streamWrapper(PromoController controller) {
    return RefreshIndicator(
      child: Container(
        color: AppColors.darkBackground,
        padding: EdgeInsets.only(
          left: SizeConfig.horizontal(2.5),
          right: SizeConfig.horizontal(2.5),
        ),
        child: _foodListWrapper(
          paramList: controller.promoList,
          controller: controller,
        ),
      ),
      onRefresh: () async {
        await controller.onRefreshPage();
      },
    );
  }

  Widget _foodListWrapper({
    required List<PackageModel?> paramList,
    required PromoController controller,
  }) {
    return Container(
      width: SizeConfig.screenWidth,
      alignment: Alignment.topCenter,
      child: PagedGridView<int, PackageModel>(
        pagingController: controller.promoListController,
        showNewPageProgressIndicatorAsGridChild: false,
        builderDelegate: PagedChildBuilderDelegate<PackageModel>(
          itemBuilder: (BuildContext context, PackageModel promo, int index) =>
              SizedBox(
            height: SizeConfig.horizontal(50),
            child: PriceItem(
              id: promo.id ?? 0,
              image: promo.image?.original ?? '',
              productName: promo.title ?? '',
              price: promo.price ?? 0,
              lengthItem: controller.promoList.length,
              index: index,
              typeShowPriceItem: TypeShowPriceItem.isDisplayForPackages,
              typePriceItem: TypePriceItem.isPriceCurrency,
              navigatePriceItem: NavigatePriceItem.toFoodPackages,
            ),
          ),
          firstPageProgressIndicatorBuilder: (BuildContext context) => SizedBox(
            height: SizeConfig.screenHeight,
            child: const PriceShimmer(length: 12),
          ),
          newPageProgressIndicatorBuilder: (BuildContext context) => SizedBox(
            height: ResponsiveValue<double>(
              context,
              defaultValue: SizeConfig.vertical(22),
              valueWhen: <Condition<double>>[
                Condition<double>.equals(
                  name: MOBILE,
                  value: SizeConfig.vertical(25),
                ),
              ],
            ).value,
            child: const PriceShimmer(length: 3),
          ),
          noItemsFoundIndicatorBuilder: (BuildContext context) =>
              const CustomEmptyState(
            caption: 'No item in this category',
            height: 100,
          ),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 4 / 6.5,
          mainAxisSpacing: SizeConfig.vertical(2),
          crossAxisSpacing: SizeConfig.horizontal(2.5),
          crossAxisCount: 3,
        ),
      ),
    );
  }
}
