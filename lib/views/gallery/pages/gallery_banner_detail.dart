import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../../widgets/layouts/shimmer_placeholder.dart';

class GalleryBannerDetail extends StatelessWidget {
  // constructor
  const GalleryBannerDetail({
    Key? key,
    required this.tagHero,
    required this.image,
    required this.onTap,
  }) : super(key: key);

  final String tagHero;
  final String image;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1)),
      child: GestureDetector(onTap: onTap, child: _heroWrapper()),
    );
  }

  Widget _heroWrapper() {
    return Hero(
      tag: tagHero,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        child: _imageWrapper(),
      ),
    );
  }

  Widget _imageWrapper() {
    return CachedNetworkImage(
      imageUrl: image,
      fit: BoxFit.cover,
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(25),
      placeholder: (BuildContext context, String url) => ShimmerPlaceholder(
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(20),
        borderRadius: BorderRadius.circular(
          SizeConfig.horizontal(2),
        ),
      ),
      errorWidget: (BuildContext context, String url, dynamic error) =>
          const SizedBox.shrink(),
    );
  }
}
