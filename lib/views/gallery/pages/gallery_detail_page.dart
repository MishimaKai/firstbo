import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/gallery/gallery_detail_controller.dart';
import '../../../helpers/date_helper.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/gallery_item_detail.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/item_detail_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';
import 'gallery_banner_detail.dart';

class GalleryDetailPage extends StatelessWidget {
  // constructor
  const GalleryDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 45.0,
        elevation: 0,
        avoidBottomInset: true,
        isUseLeading: false,
        isImplyLeading: false,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        action: _frameAction(),
        view: _builderContent(context),
      ),
    );
  }

  Widget _frameAction() {
    return FrameActionWrapper(
      title: PageNames.galleryPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        Get.back();
      },
    );
  }

  Widget _builderContent(BuildContext context) {
    return GetBuilder<GalleryDetailController>(
      init: GalleryDetailController(pageContext: context),
      builder: (GalleryDetailController controller) =>
          _obxWrapper(controller, context),
    );
  }

  Widget _obxWrapper(GalleryDetailController controller, BuildContext context) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return ItemDetailShimmer(
          type: TypeDetailShimmer.specify,
          borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
          height: SizeConfig.vertical(25),
        );
      } else {
        return _contentWrapper(controller, context);
      }
    });
  }

  Widget _contentWrapper(
    GalleryDetailController controller,
    BuildContext context,
  ) {
    return Container(
      color: AppColors.darkBackground,
      padding: EdgeInsets.only(
        left: SizeConfig.horizontal(1),
        right: SizeConfig.horizontal(1),
        top: SizeConfig.vertical(1),
      ),
      child: _galleryItemDetailWrapper(context, controller),
    );
  }

  Widget _galleryItemDetailWrapper(
    BuildContext context,
    GalleryDetailController controller,
  ) {
    return CustomScrollView(
      slivers: <Widget>[
        _bannerOutletAndDateWrapper(context, controller),
        _imageGridList(controller),
        _sliverSpace(),
      ],
    );
  }

  Widget _bannerOutletAndDateWrapper(
    BuildContext context,
    GalleryDetailController controller,
  ) {
    return SliverList(
      delegate: SliverChildListDelegate(
        <Widget>[_sliverListContent(context, controller)],
      ),
    );
  }

  Widget _sliverListContent(
    BuildContext context,
    GalleryDetailController controller,
  ) {
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2),
              ),
              child: _bannerOutletAndDate(context, controller),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(child: _detail(controller)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ],
      ),
    );
  }

  Widget _detail(GalleryDetailController controller) {
    return Html(
      data: controller.getDescDetail(),
      style: <String, Style>{'p': Style(color: AppColors.whiteBackground)},
    );
  }

  Widget _imageGridList(GalleryDetailController controller) {
    return SliverGrid(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) => GalleryItemThumbnail(
          displayOrder: controller.imageDetailList[index].displayOrder ?? 0,
          image: controller.imageDetailList[index].image?.original ?? '',
          onTap: () {
            controller.openImages(context, index);
          },
        ),
        childCount: controller.imageDetailList.length,
      ),
    );
  }

  Widget _sliverSpace() {
    return SliverList(
      delegate: SliverChildListDelegate(
        <Widget>[
          SizedBox(
            width: SizeConfig.screenWidth,
            height: SizeConfig.vertical(10),
          ),
        ],
      ),
    );
  }

  Widget _bannerOutletAndDate(
    BuildContext context,
    GalleryDetailController controller,
  ) {
    return SizedBox(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: GalleryBannerDetail(
              tagHero: controller.tagImageBanner,
              image: controller.getImageDetail(),
              onTap: () => controller.openImagesBanner(context),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(child: _titleAndDateWrapper(controller)),
        ],
      ),
    );
  }

  Widget _titleAndDateWrapper(GalleryDetailController controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _titleDisplay(controller)),
        ResponsiveRowColumnItem(child: _datedisplay(controller)),
      ],
    );
  }

  Widget _titleDisplay(GalleryDetailController controller) {
    return MontserratTextView(
      value: controller.getTitleDetail(),
      color: AppColors.whiteBackground,
      size: SizeConfig.safeHorizontal(3.5),
      fontWeight: FontWeight.w700,
    );
  }

  Widget _datedisplay(GalleryDetailController controller) {
    return MontserratTextView(
      value:
          '${DateHelper.getDayDisplayer(controller.getDateDetail())}, ${DateHelper.getEventOfDateDisplayer(controller.getDateDetail())}',
      color: AppColors.whiteBackground,
      size: SizeConfig.safeHorizontal(3),
      fontWeight: FontWeight.w300,
    );
  }
}
