import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/gallery/gallery_controller.dart';
import '../../../helpers/date_helper.dart';
import '../../../models/data/gallery/gallery_model.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/layouts/shimmer_placeholder.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/gallery_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class GalleryPage extends StatefulWidget {
  // constructor
  const GalleryPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => GalleryViewState();
}

class GalleryViewState extends State<GalleryPage> {
  GalleryController? controller;

  @override
  void initState() {
    super.initState();
    controller = Get.put(GalleryController(pageContext: context));
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return RefreshIndicator(
      child: Container(
        color: AppColors.darkBackground,
        padding: EdgeInsets.only(
          left: SizeConfig.horizontal(2.5),
          right: SizeConfig.horizontal(2.5),
          bottom: SizeConfig.vertical(3),
        ),
        child: _galleriesListWrapper(controller),
      ),
      onRefresh: () async {
        await controller?.onRefreshPage();
      },
    );
  }

  Widget _galleriesListWrapper(GalleryController? controller) {
    return PagedListView<int, GalleryModel>(
      pagingController: controller!.galleryListController,
      padding: EdgeInsets.only(bottom: SizeConfig.vertical(20)),
      builderDelegate: PagedChildBuilderDelegate<GalleryModel>(
        itemBuilder: (BuildContext context, GalleryModel gallery, int index) =>
            _listItemGalleries(
          index: index,
          id: gallery.id ?? 0,
          image: gallery.image?.original ?? '',
          title: gallery.title ?? '',
          date: gallery.galleryDate ?? '',
        ),
        firstPageProgressIndicatorBuilder: (BuildContext context) =>
            const GalleryShimmer(length: 3),
        newPageProgressIndicatorBuilder: (BuildContext context) =>
            const GalleryShimmer(length: 1),
        noItemsFoundIndicatorBuilder: (BuildContext context) =>
            const CustomEmptyState(
          caption: 'No photos available',
          height: 100,
        ),
      ),
    );
  }

  Widget _listItemGalleries({
    required int index,
    required int id,
    required String image,
    required String title,
    required String date,
  }) {
    return Container(
      padding: EdgeInsets.only(
        top: index == 0 ? SizeConfig.vertical(1) : 0,
        bottom: SizeConfig.vertical(2),
      ),
      child: RippleButton(
        onTap: () => Get.toNamed(
          RouteNames.galleryItemDetail,
          arguments: id,
        ),
        radius: 2,
        child: SizedBox(
          width: SizeConfig.screenWidth,
          child: _itemGallery(image, title, date),
        ),
      ),
    );
  }

  Widget _itemGallery(String image, String title, String date) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.cover,
              height: SizeConfig.vertical(25),
              width: SizeConfig.screenWidth,
              placeholder: (BuildContext context, String url) =>
                  ShimmerPlaceholder(
                width: SizeConfig.screenWidth,
                height: SizeConfig.vertical(20),
              ),
              errorWidget: (
                BuildContext context,
                String url,
                dynamic error,
              ) =>
                  const SizedBox.shrink(),
            ),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnCrossAxisAlignment: CrossAxisAlignment.start,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: MontserratTextView(
                  value: title,
                  color: AppColors.whiteBackground,
                  size: SizeConfig.safeHorizontal(3.5),
                  fontWeight: FontWeight.w700,
                ),
              ),
              ResponsiveRowColumnItem(
                child: MontserratTextView(
                  value:
                      '${DateHelper.getDayDisplayer(date)}, ${DateHelper.getEventOfDateDisplayer(date)}',
                  color: AppColors.whiteBackground,
                  size: SizeConfig.safeHorizontal(3),
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
      ],
    );
  }
}
