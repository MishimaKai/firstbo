import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' as getx;
import 'package:intl/intl.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/calendar/general_calendar_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/layouts/under_line.dart';
import '../../widgets/text/montserrat_text_view.dart';
import '../widgets/custom_general_table_calendar.dart';

class GeneralCalendar extends StatelessWidget {
  // constructor
  GeneralCalendar({
    Key? key,
  }) : super(key: key);

  final GeneralCalendarController _calendarController = getx.Get.put(
    GeneralCalendarController(),
  );

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return getx.GetBuilder<GeneralCalendarController>(
      init: GeneralCalendarController(),
      id: 'generalCalendar',
      builder: (_) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(
            systemNavigationBarColor: AppColors.darkBackground,
            systemNavigationBarIconBrightness: Brightness.light,
          ),
          child: FrameScaffold(
            heightBar: 45.0,
            elevation: 0,
            avoidBottomInset: true,
            isUseLeading: false,
            isImplyLeading: false,
            color: Platform.isIOS ? AppColors.darkBackground : null,
            statusBarColor: AppColors.darkBackground,
            statusBarIconBrightness: Brightness.light,
            statusBarBrightness: Brightness.dark,
            colorScaffold: AppColors.darkBackground,
            action: FrameActionWrapper(
              title: _calendarController.titlePage,
              fontWeight: FontWeight.bold,
              onPressed: () {
                getx.Get.back();
              },
            ),
            view: Container(
              color: AppColors.darkBackground,
              padding: EdgeInsets.only(
                left: SizeConfig.horizontal(2.5),
                right: SizeConfig.horizontal(2.5),
                top: SizeConfig.vertical(1),
              ),
              child: _generalCalendarPageWrapper(context),
            ),
          ),
        );
      },
    );
  }

  Widget _generalCalendarPageWrapper(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _headerDateWrapper(context)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        const ResponsiveRowColumnItem(child: Underline()),
        ResponsiveRowColumnItem(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: SizeConfig.vertical(3),
            ),
            child: MontserratTextView(
              value: DateFormat('MMMM y').format(
                _calendarController.focusedDayGeneralCalendar,
              ),
              alignText: AlignTextType.center,
              color: AppColors.whiteBackground,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        ResponsiveRowColumnItem(
          child: Expanded(
            child: CustomGeneralTableCalendar(controller: _calendarController),
          ),
        ),
      ],
    );
  }

  // wrapper date picker
  Widget _headerDateWrapper(BuildContext context) {
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _startDateDisplay(context)),
          ResponsiveRowColumnItem(child: _endDateDisplay(context)),
        ],
      ),
    );
  }

  // custom start date button
  Widget _startDateDisplay(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1)),
      width: SizeConfig.horizontal(40),
      height: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.vertical(7),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.vertical(8),
          ),
        ],
      ).value,
      child: ResponsiveRowColumn(
        columnMainAxisAlignment: MainAxisAlignment.center,
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'From',
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w700,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: _calendarController.rangeStartGeneralCalendar != null
                  ? DateFormat('dd MMMM y')
                      .format(_calendarController.rangeStartGeneralCalendar!)
                  : 'Calendar',
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(3.5),
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  // custom end date button
  Widget _endDateDisplay(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1)),
      width: SizeConfig.horizontal(40),
      height: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.vertical(7),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.vertical(8),
          ),
        ],
      ).value,
      child: ResponsiveRowColumn(
        columnMainAxisAlignment: MainAxisAlignment.center,
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'To',
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w700,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: _calendarController.rangeEndGeneralCalendar != null
                  ? DateFormat('dd MMMM y')
                      .format(_calendarController.rangeEndGeneralCalendar!)
                  : 'Calendar',
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(3.5),
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}
