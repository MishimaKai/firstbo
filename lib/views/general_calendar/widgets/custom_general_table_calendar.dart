import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../controllers/calendar/general_calendar_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/text/montserrat_text_view.dart';

class CustomGeneralTableCalendar extends StatelessWidget {
  // constructor
  const CustomGeneralTableCalendar({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final GeneralCalendarController controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return TableCalendar<dynamic>(
      locale: 'en_US',
      focusedDay: controller.focusedDayGeneralCalendar,
      firstDay: controller.firstDayCalendar,
      lastDay: controller.lastDayCalendar,
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        outsideTextStyle: montserratStyle.greyCalendar(),
      ),
      rangeStartDay: controller.rangeStartGeneralCalendar,
      rangeEndDay: controller.rangeEndGeneralCalendar,
      rangeSelectionMode: controller.onSetSelectionMode(),
      headerVisible: false,
      calendarFormat: controller.calendarFormat,
      selectedDayPredicate: (DateTime day) => isSameDay(
        controller.selectedDayGeneralCalendar,
        day,
      ),
      onDaySelected: (DateTime selectedDay, DateTime focusedDay) =>
          controller.daySelectedGeneralCalendar(selectedDay, focusedDay),
      onFormatChanged: (CalendarFormat format) =>
          controller.formatChangeGeneralCalendar(format),
      onPageChanged: (DateTime focusedDay) =>
          controller.pageChangeGeneralCalendar(focusedDay),
      onRangeSelected: (DateTime? start, DateTime? end, DateTime focusedDay) =>
          controller.onRangeSelectedGeneralCalendar(start, end, focusedDay),
      calendarBuilders: CalendarBuilders<dynamic>(
        dowBuilder: (BuildContext context, DateTime day) =>
            customDowbuilder(context, day),
        defaultBuilder: (
          BuildContext context,
          DateTime day,
          DateTime focusedDay,
        ) =>
            customDefaultBuilder(
          context,
          day,
          focusedDay,
        ),
        rangeStartBuilder: (
          BuildContext context,
          DateTime day,
          DateTime focusedDay,
        ) {
          return customRangeStartBuilder(day);
        },
        rangeEndBuilder: (
          BuildContext context,
          DateTime day,
          DateTime focusedDay,
        ) {
          return customRangeEndBuilder(day);
        },
        withinRangeBuilder: (
          BuildContext context,
          DateTime day,
          DateTime focusedDay,
        ) {
          return customWithinRangeBuilder(day);
        },
      ),
    );
  }

  // Custom day name
  Widget customDowbuilder(BuildContext context, DateTime day) {
    final String weekday = DateFormat.E('en_US').format(day);
    return Center(
      child: ExcludeSemantics(
        child: MontserratTextView(
          value: weekday.substring(0, 1),
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w400,
          size: SizeConfig.safeHorizontal(4.5),
        ),
      ),
    );
  }

  // Custom default builder
  Widget customDefaultBuilder(
      BuildContext context, DateTime day, DateTime focusedDay) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      margin: const EdgeInsets.all(2.0),
      decoration: const BoxDecoration(shape: BoxShape.circle),
      alignment: Alignment.center,
      child: MontserratTextView(
        value: day.day.toString(),
        color: AppColors.flatGold,
        size: SizeConfig.safeHorizontal(4.5),
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget customRangeStartBuilder(DateTime day) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: AppColors.darkBackground,
        shape: BoxShape.circle,
        border: Border.all(color: AppColors.flatGold, width: 0.5),
      ),
      alignment: Alignment.center,
      child: MontserratTextView(
        value: day.day.toString(),
        color: AppColors.flatGold,
        size: SizeConfig.safeHorizontal(4.5),
        fontWeight: FontWeight.w400,
      ),
    );
  }

  Widget customRangeEndBuilder(DateTime day) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: AppColors.darkBackground,
        shape: BoxShape.circle,
        border: Border.all(color: AppColors.flatGold, width: 0.5),
      ),
      alignment: Alignment.center,
      child: MontserratTextView(
        value: day.day.toString(),
        color: AppColors.flatGold,
        size: SizeConfig.safeHorizontal(4.5),
        fontWeight: FontWeight.w400,
      ),
    );
  }

  // Custom cell calendar
  Widget customWithinRangeBuilder(DateTime day) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      margin: const EdgeInsets.all(2.0),
      alignment: Alignment.center,
      decoration: const BoxDecoration(shape: BoxShape.circle),
      child: MontserratTextView(
        value: day.day.toString(),
        color: AppColors.darkBackground,
        size: SizeConfig.safeHorizontal(4.5),
        fontWeight: FontWeight.w400,
      ),
    );
  }
}
