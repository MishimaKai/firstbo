import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;
import 'package:responsive_framework/responsive_framework.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../controllers/home/home_controller.dart';
import '../../../controllers/home/member_account_info_controller.dart';
import '../../../models/data/event/event_model.dart';
import '../../../models/data/package/package_model.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/dialogs/custom_dialog.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/layouts/event_item.dart';
import '../../widgets/layouts/price_item.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/home_shimmer.dart';
import '../widgets/banner_title.dart';
import '../widgets/member_info_wrapper.dart';
import '../widgets/section_label.dart';

class HomePage extends StatefulWidget {
  // constructor
  const HomePage({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  // initiate home controller with default null
  HomeController? controller;
  // initiate account info controller
  final MemberAccountInfoController _infoController = getx.Get.find();

  @override
  void initState() {
    super.initState();
    // assign home conroller with default param BuildContext
    controller = getx.Get.put(HomeController(pageContext: context));
    // listen first time dialog status
    controller?.isShowDialog.listen((bool value) {
      if (value) {
        _showPopUpLaunch(context);
      }
    });
  }

  // show pop up when launch app
  dynamic _showPopUpLaunch(BuildContext context) {
    return showDialog(
      barrierColor: Colors.transparent.withOpacity(0.1),
      context: context,
      builder: (BuildContext context) => CustomDialog(
        isDualScreen: controller!.isDualScreen.value,
        onPressedInDialog: () async {
          // if content type 1 or image
          // otherwise video from youtube
          if (controller!.type == 1) {
            Navigator.pop(context);
          } else {
            // convert url string to Uri
            final Uri uriSouce = Uri.parse(controller!.source!);
            // then launch url
            await launchUrl(uriSouce);
            // ignore: use_build_context_synchronously
            Navigator.pop(context);
          }
        },
        image:
            controller!.type == 1 ? controller!.image : controller!.thumbnail,
        typeDialog: TypeDialog.promoEvent,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // intiate size config device
    SizeConfig().init(context);

    /// and wrap with [RefreshIndicator] with trigger mode [RefreshIndicatorTriggerMode.anywhere]
    return RefreshIndicator(
      triggerMode: RefreshIndicatorTriggerMode.anywhere,
      child: _mainViewWrapper(),
      onRefresh: () async {
        // request data for home page except balance point, voucher, name and notifications
        await controller?.getRequestHome();
        await _infoController.getRequestMemberAccountInfo();
      },
    );
  }

  // normal UI
  Widget _mainViewWrapper() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              0, SizeConfig.vertical(3), 0, SizeConfig.vertical(6)),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              const ResponsiveRowColumnItem(child: BannerTitle()),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
              ResponsiveRowColumnItem(child: _wrapper(context, controller)),
            ],
          ),
        ),
      ),
    );
  }

  // wrapper loading skeleton loading for normal UI
  Widget _wrapper(BuildContext context, HomeController? controller) {
    return getx.Obx(() {
      if (controller?.isLoading.isTrue ?? true) {
        return const HomeShimmer();
      } else {
        return _contentWrapper(context, controller!);
      }
    });
  }

  // content for UI normal device
  Widget _contentWrapper(BuildContext context, HomeController controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        const ResponsiveRowColumnItem(child: MemberInfoWrapper()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: _upComingEventWrapper(
            context: context,
            paramList: controller.getEventList(),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: _bottlePackagesWrapper(
            context: context,
            paramList: controller.getBottlePackages(),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: _foodPackageWrapper(
            context: context,
            paramList: controller.getFoodPackages(),
          ),
        ),
      ],
    );
  }

  /// event wrapper with param [context] for BuildContext,[paramList] as data list
  Widget _upComingEventWrapper({
    required BuildContext context,
    required List<EventModel?> paramList,
  }) {
    if (paramList.isEmpty) {
      return _upComingEventEmpty();
    } else {
      return _upComingEventContent(context: context, paramList: paramList);
    }
  }

  /// when event are empty
  Widget _upComingEventEmpty() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'events_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.eventsRoute);
            },
            typeNavigationSectionLabel: TypeNavigationSectionLabel.none,
          ),
        ),
        ResponsiveRowColumnItem(
          child: CustomEmptyState(caption: 'events_empty_label'.tr, height: 37),
        ),
      ],
    );
  }

  /// when event are exist with param [context] for BuildContext,[paramList] as data list
  Widget _upComingEventContent({
    required BuildContext context,
    required List<EventModel?> paramList,
  }) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'events_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.eventsRoute);
            },
            typeNavigationSectionLabel: TypeNavigationSectionLabel.none,
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _upComingEventSlider(paramList)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: _upComingEventSliderIndicator(paramList),
        ),
      ],
    );
  }

  /// event content with [paramList] as data list
  Widget _upComingEventSlider(List<EventModel?> paramList) {
    return CarouselSlider.builder(
      itemCount: paramList.length,
      itemBuilder: (BuildContext context, int index, int realIndex) {
        return EventItem(
          id: paramList[index]!.id!,
          image: paramList[index]?.image?.original ?? '',
          // ignore: always_specify_types
          outletInfo: paramList[index]?.outletInfo ?? [],
          startDate: paramList[index]?.startDate ?? '',
          lengthItem: paramList.length,
          index: index,
          typeEvent: TypeEventItem.isDisplayForHome,
        );
      },
      options: CarouselOptions(
        autoPlay: true,
        enlargeCenterPage: true,
        aspectRatio: 1 / 0.8,
        onPageChanged: (int index, CarouselPageChangedReason reason) {
          controller!.currentEvent.value = index;
        },
      ),
    );
  }

  /// indicator slider with [paramList] as data list
  Widget _upComingEventSliderIndicator(List<EventModel?> paramList) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: paramList.asMap().entries.map((
        MapEntry<int, EventModel?> entry,
      ) {
        return Container(
          width: SizeConfig.horizontal(2.5),
          height: SizeConfig.horizontal(2.5),
          margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: (Theme.of(context).brightness == Brightness.dark
                    ? AppColors.darkBackground
                    : AppColors.flatGold)
                .withOpacity(
              // ignore: unrelated_type_equality_checks
              controller!.currentEvent == entry.key ? 0.9 : 0.4,
            ),
          ),
        );
      }).toList(),
    );
  }

  /// bottle wrapper with param [context] for BuildContext, [paramList] as data list
  Widget _bottlePackagesWrapper({
    required BuildContext context,
    required List<PackageModel?> paramList,
  }) {
    if (paramList.isEmpty) {
      return _bottlePackagesEmpty();
    } else {
      return _bottlePackageContent(paramList);
    }
  }

  /// when bottle are empty
  Widget _bottlePackagesEmpty() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'bottle_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.bottlePackages, arguments: 2);
            },
          ),
        ),
        ResponsiveRowColumnItem(
          child: CustomEmptyState(caption: 'bottle_empty_label'.tr, height: 22),
        ),
      ],
    );
  }

  /// when bottle are exist with param [paramList] as data list
  Widget _bottlePackageContent(List<PackageModel?> paramList) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'bottle_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.bottlePackages, arguments: 2);
            },
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _packageList(paramList)),
      ],
    );
  }

  /// food list with param [context] for BuildContext,  [paramList] as data list
  Widget _foodPackageWrapper({
    required BuildContext context,
    required List<PackageModel?> paramList,
  }) {
    if (paramList.isEmpty) {
      return _foodPackageEmpty();
    } else {
      return _foodPackageContent(paramList);
    }
  }

  /// when food are empty
  Widget _foodPackageEmpty() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'food_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.foodPackages, arguments: 1);
            },
          ),
        ),
        ResponsiveRowColumnItem(
          child: CustomEmptyState(caption: 'food_empty_label'.tr, height: 22),
        ),
      ],
    );
  }

  /// when food are exist with param [paramList] as data list
  Widget _foodPackageContent(List<PackageModel?> paramList) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SectionLabel(
            label: 'food_title'.tr,
            onTap: () {
              getx.Get.toNamed(RouteNames.foodPackages, arguments: 1);
            },
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _packageList(paramList)),
      ],
    );
  }

  /// content with param [paramList] as data list
  Widget _packageList(List<PackageModel?> paramList) {
    return SizedBox(
      height: SizeConfig.horizontal(50),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: paramList.length,
        itemBuilder: (BuildContext context, int index) {
          return PriceItem(
            id: paramList[index]?.id ?? 0,
            image: paramList[index]?.image?.original ?? '',
            productName: paramList[index]?.title ?? '',
            price: paramList[index]?.price ?? 0,
            lengthItem: paramList.length,
            index: index,
            typeShowPriceItem: TypeShowPriceItem.isDisplayForHome,
            typePriceItem: TypePriceItem.isPriceCurrency,
            navigatePriceItem: NavigatePriceItem.toBottlePackages,
          );
        },
      ),
    );
  }
}
