import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../helpers/string_helper.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/text/montserrat_text_view.dart';

class BalanceCard extends StatelessWidget {
  const BalanceCard({
    required this.balanceVoucher,
    required this.balancePoint,
    required this.typeMemberForBalanceCard,
    required this.onTapVoucher,
    required this.onTapPoints,
    Key? key,
  }) : super(key: key);

  final int balanceVoucher;
  final int balancePoint;
  final TypeMemberForBalanceCard typeMemberForBalanceCard;
  final Function() onTapVoucher;
  final Function() onTapPoints;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.screenWidth,
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(1),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[AppColors.flatGold, AppColors.darkFlatGold],
        ),
      ),
      child: _wrapper(),
    );
  }

  Widget _wrapper() {
    // non associate
    if (typeMemberForBalanceCard == TypeMemberForBalanceCard.other) {
      return IntrinsicHeight(
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.ROW,
          rowMainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RippleButton(
                onTap: onTapVoucher,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal(2),
                  ),
                  child: _balanceVoucher(),
                ),
              ),
            ),
            ResponsiveRowColumnItem(
              child: VerticalDivider(
                width: SizeConfig.horizontal(5),
                thickness: 1.5,
                indent: 20,
                endIndent: 0,
                color: AppColors.darkBackground,
              ),
            ),
            ResponsiveRowColumnItem(
              child: RippleButton(
                onTap: onTapPoints,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal(2),
                  ),
                  child: _balancePoints(),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return IntrinsicHeight(
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.ROW,
          rowPadding: EdgeInsets.only(
            left: SizeConfig.horizontal(1),
          ),
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: RippleButton(
                // onTap: onTapVoucher,
                onTap: () {},
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal(2),
                  ),
                  child: _balanceVoucherAssociate(),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _balanceVoucher() {
    return SizedBox(
      width: SizeConfig.horizontal(35),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _balanceLabel(
              icon: Assets.iconVoucher,
              balanceName: 'Voucher',
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.vertical(1)),
          ),
          ResponsiveRowColumnItem(
            child: RichText(
              text: TextSpan(
                style: GoogleFonts.montserrat(
                  color: AppColors.basicDark,
                  fontSize: SizeConfig.safeHorizontal(4),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Rp ',
                    style: TextStyle(fontSize: SizeConfig.safeHorizontal(3.5)),
                  ),
                  TextSpan(text: StringHelper.convertToRupiah(balanceVoucher)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _balanceVoucherAssociate() {
    return SizedBox(
      width: SizeConfig.horizontal(80),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _balanceLabel(
              icon: Assets.iconVoucher,
              balanceName: 'Voucher',
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.vertical(1)),
          ),
          ResponsiveRowColumnItem(
            child: RichText(
              text: TextSpan(
                style: GoogleFonts.montserrat(
                  color: AppColors.basicDark,
                  fontSize: SizeConfig.safeHorizontal(4),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Rp ',
                    style: TextStyle(fontSize: SizeConfig.safeHorizontal(3.5)),
                  ),
                  TextSpan(text: StringHelper.convertToRupiah(balanceVoucher)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _balancePoints() {
    return SizedBox(
      width: SizeConfig.horizontal(35),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _balanceLabel(
              icon: Assets.iconPoint,
              balanceName: 'Points',
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.vertical(1)),
          ),
          ResponsiveRowColumnItem(
            child: RichText(
              text: TextSpan(
                style: GoogleFonts.montserrat(
                  color: AppColors.basicDark,
                  fontSize: SizeConfig.safeHorizontal(4),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                ),
                children: <TextSpan>[
                  TextSpan(text: StringHelper.convertToRupiah(balancePoint)),
                  TextSpan(
                    text: ' points',
                    style: TextStyle(fontSize: SizeConfig.safeHorizontal(3.5)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _balanceLabel({required String icon, required String balanceName}) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            icon,
            width: SizeConfig.horizontal(6),
            height: SizeConfig.horizontal(6),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2)),
        ),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: balanceName,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
      ],
    );
  }
}
