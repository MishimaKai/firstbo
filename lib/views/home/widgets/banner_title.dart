import 'package:flutter/material.dart';

import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';

class BannerTitle extends StatelessWidget {
  const BannerTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(5),
      ),
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(5)),
      child: Image.asset(
        Assets.imageBlackOwlFull,
        fit: BoxFit.contain,
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(12),
      ),
    );
  }
}
