import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/frame/frame_widget_controller.dart';
import '../../widgets/text/montserrat_text_view.dart';

class MemberInfo extends StatelessWidget {
  const MemberInfo({
    required this.membershipType,
    required this.imageProfile,
    required this.firstName,
    required this.lastName,
    required this.membershipImage,
    required this.memberhipName,
    required this.totalNotifications,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  final int membershipType;
  final String imageProfile;
  final String firstName;
  final String lastName;
  final String membershipImage;
  final String memberhipName;
  final String? totalNotifications;
  final Function() onTap;

  static final FrameController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Flexible(child: _memberAvatarWrapper()),
          ),
          ResponsiveRowColumnItem(child: _whenNotificationEmpty())
        ],
      ),
    );
  }

  Widget _memberAvatarWrapper() {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(SizeConfig.horizontal(3)),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: AppColors.rippleColor,
          onTap: () {
            return controller.onTapNavigation(4);
          },
          child: Ink(
            color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2),
                vertical: SizeConfig.vertical(1),
              ),
              child: _memberAvatar(),
            ),
          ),
        ),
      ),
    );
  }

  Widget _memberAvatar() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Container(
            width: SizeConfig.horizontal(12),
            height: SizeConfig.horizontal(12),
            decoration: BoxDecoration(
              color: AppColors.whiteBackground,
              shape: BoxShape.circle,
            ),
            child: _imageWrapper(image: imageProfile),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2.5)),
        ),
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnCrossAxisAlignment: CrossAxisAlignment.start,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                  child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: MontserratTextView(
                      value: '$firstName $lastName',
                      color: AppColors.whiteBackground,
                      maxLines: 2,
                      size: SizeConfig.safeHorizontal(4),
                    ),
                  ),
                ],
              )),
              ResponsiveRowColumnItem(
                child: SizedBox(height: SizeConfig.vertical(1)),
              ),
              ResponsiveRowColumnItem(child: _membershipWrapper()),
            ],
          ),
        ),
      ],
    );
  }

  Widget _membershipWrapper() {
    return MontserratTextView(
      value: memberhipName,
      color: AppColors.flatGold,
      fontWeight: FontWeight.w300,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _imageWrapper({required String image, double? width, double? height}) {
    if (image.isNotEmpty) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(12)),
        child: CachedNetworkImage(
          width: width,
          height: height,
          imageUrl: image,
          fit: BoxFit.cover,
          placeholder: (BuildContext context, String url) =>
              const CircularProgressIndicator(),
          errorWidget: (BuildContext context, String url, dynamic error) =>
              const SizedBox.shrink(),
        ),
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget _whenNotificationEmpty() {
    if (totalNotifications == null) {
      return RippleButton(
        onTap: onTap,
        child: Image.asset(
          Assets.iconNotification,
          width: SizeConfig.horizontal(8),
          height: SizeConfig.horizontal(8),
          color: AppColors.lightDark,
        ),
      );
    } else {
      return RippleButton(
        onTap: onTap,
        child: SizedBox(
          width: SizeConfig.horizontal(8),
          height: SizeConfig.horizontal(8),
          child: Stack(
            children: <Widget>[_imageNotifWrapper(), _badgeNumber()],
          ),
        ),
      );
    }
  }

  Widget _imageNotifWrapper() {
    return Image.asset(
      Assets.iconNotification,
      width: SizeConfig.horizontal(8),
      height: SizeConfig.horizontal(8),
      color: AppColors.whiteBackground,
    );
  }

  Widget _badgeNumber() {
    return Positioned(
      top: 0,
      right: 0,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(0.1),
          horizontal: SizeConfig.horizontal(0.5),
        ),
        decoration: BoxDecoration(
          color: AppColors.redAlert,
          borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
        ),
        constraints: BoxConstraints(
          minWidth: SizeConfig.horizontal(5),
          minHeight: SizeConfig.horizontal(5),
        ),
        child: Center(
          child: MontserratTextView(
            value: totalNotifications.toString(),
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.bold,
            size: SizeConfig.safeHorizontal(2.5),
            alignText: AlignTextType.center,
          ),
        ),
      ),
    );
  }
}
