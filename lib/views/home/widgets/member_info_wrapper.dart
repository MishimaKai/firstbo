import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/home/member_account_info_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/layouts/shimmer_placeholder.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';
import 'balance_card.dart';
import 'member_info.dart';

class MemberInfoWrapper extends StatelessWidget {
  // constructor
  const MemberInfoWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<MemberAccountInfoController>(
      init: MemberAccountInfoController(pageContext: context),
      builder: (MemberAccountInfoController controller) {
        return Obx(() {
          if (controller.isLoadingAmount.isTrue) {
            return _whenLoading();
          } else {
            return ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: MemberInfo(
                    membershipType: controller.membershipType,
                    imageProfile: controller.getImageProfile(),
                    firstName: controller.getFirstName(),
                    lastName: controller.getLastName(),
                    membershipImage: controller.getMembershipBadge(),
                    memberhipName: controller.getMemberhipName(),
                    totalNotifications: controller.getTotalNotification(),
                    onTap: () {
                      controller.navigateToNotifcation();
                    },
                  ),
                ),
                const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
                ResponsiveRowColumnItem(
                  child: BalanceCard(
                    balanceVoucher: controller.getBalanceVoucher(),
                    balancePoint: controller.getBalancePoint(),
                    typeMemberForBalanceCard: controller.getTypeMember(),
                    onTapVoucher: () => controller.navigateToVoucher(),
                    onTapPoints: () => controller.navigateToPoints(),
                  ),
                ),
              ],
            );
          }
        });
      },
    );
  }

  Widget _whenLoading() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2.5),
            ),
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _personShimmer()),
                ResponsiveRowColumnItem(
                  child: Image.asset(
                    Assets.iconNotification,
                    width: SizeConfig.horizontal(8),
                    height: SizeConfig.horizontal(8),
                    color: AppColors.lightDark,
                  ),
                )
              ],
            ),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(
          child: Container(
            width: SizeConfig.screenWidth,
            margin: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2.5),
            ),
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2),
              vertical: SizeConfig.vertical(1),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[AppColors.flatGold, AppColors.darkFlatGold],
              ),
            ),
            child: _wrapper(),
          ),
        ),
      ],
    );
  }

  Widget _personShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(12),
            height: SizeConfig.horizontal(12),
            shape: BoxShape.circle,
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2.5)),
        ),
        ResponsiveRowColumnItem(child: _personShimmerContent()),
      ],
    );
  }

  Widget _personShimmerContent() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(15),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(1)),
        ),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(12),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
      ],
    );
  }

  Widget _wrapper() {
    return IntrinsicHeight(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _balanceShimmer('Voucher')),
          ResponsiveRowColumnItem(
            child: VerticalDivider(
              width: SizeConfig.horizontal(5),
              thickness: 1.5,
              indent: 20,
              endIndent: 0,
              color: AppColors.darkBackground,
            ),
          ),
          ResponsiveRowColumnItem(child: _balanceShimmer('Points')),
          // ResponsiveRowColumnItem(child: _balancePoints()),
        ],
      ),
    );
  }

  Widget _balanceShimmer(String balanceName) {
    return SizedBox(
      width: SizeConfig.horizontal(35),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _balanceLabel(
              icon: balanceName == 'Voucher'
                  ? Assets.iconVoucher
                  : Assets.iconPoint,
              balanceName: balanceName,
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(height: SizeConfig.vertical(1)),
          ),
          ResponsiveRowColumnItem(child: _balanceValue(balanceName)),
        ],
      ),
    );
  }

  Widget _balanceValue(String balanceName) {
    if (balanceName == 'Voucher') {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Rp ',
              color: AppColors.basicDark,
              fontWeight: FontWeight.bold,
              size: SizeConfig.safeHorizontal(3.5),
              fontStyle: FontStyle.normal,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
          ResponsiveRowColumnItem(
            child: ShimmerPlaceholder(
              width: SizeConfig.horizontal(15),
              borderRadius: BorderRadius.circular(
                SizeConfig.horizontal(2.5),
              ),
            ),
          ),
        ],
      );
    } else {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: ShimmerPlaceholder(
              width: SizeConfig.horizontal(15),
              borderRadius: BorderRadius.circular(
                SizeConfig.horizontal(2.5),
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: ' Points',
              color: AppColors.basicDark,
              fontWeight: FontWeight.bold,
              size: SizeConfig.safeHorizontal(3.5),
              fontStyle: FontStyle.normal,
            ),
          ),
        ],
      );
    }
  }

  Widget _balanceLabel({required String icon, required String balanceName}) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            icon,
            width: SizeConfig.horizontal(6),
            height: SizeConfig.horizontal(6),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2)),
        ),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: balanceName,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
      ],
    );
  }
}
