import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/montserrat_button.dart';
import '../../widgets/text/montserrat_text_view.dart';

class SectionLabel extends StatelessWidget {
  const SectionLabel({
    required this.label,
    required this.onTap,
    this.typeNavigationSectionLabel = TypeNavigationSectionLabel.navigation,
    Key? key,
  }) : super(key: key);

  final String label;
  final Function() onTap;
  final TypeNavigationSectionLabel typeNavigationSectionLabel;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(1.5)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: label,
              color: AppColors.whiteBackground,
              fontWeight: FontWeight.bold,
              size: SizeConfig.safeHorizontal(4.5),
            ),
          ),
          ResponsiveRowColumnItem(
            child: typeNavigationSectionLabel == TypeNavigationSectionLabel.none
                ? const SizedBox.shrink()
                : MontserratButton(
                    onTap: onTap,
                    value: 'see all',
                    color: AppColors.darkGold,
                    isUnderLine: false,
                    isUseRipple: true,
                    customPadding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.horizontal(2),
                      vertical: SizeConfig.vertical(0.5),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
