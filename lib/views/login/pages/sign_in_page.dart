import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/account/sign_in_controller.dart';
import '../../../helpers/scroll_config.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/buttons/montserrat_button.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/layouts/under_line.dart';
import '../../widgets/text/montserrat_text_view.dart';
import '../widgets/form_login_email_address.dart';
import '../widgets/form_login_phone_number.dart';

/// [SignView] represent user login
/// user can login using [phone number] or [email address]
/// if user account doesn't exist, page will be navigate to register form
/// otherwise user will navigate to verification page, the verification
/// will sent to phone number via whatsapp or email address
class SignInView extends StatelessWidget {
  // constructor
  const SignInView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // intiate size config device
    SizeConfig().init(context);

    /// wrap UI with [keyboardProvider] widget for handle virtual keyboard
    /// and [FrameScaffold] for handle UI theme, also register controller
    /// using [GetBuilder]
    return KeyboardSizeProvider(
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        avoidBottomInset: true,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        view: GetBuilder<SignInController>(
          init: SignInController(pageContext: context),
          builder: (SignInController controller) =>
              _annotatedWrapper(controller),
        ),
      ),
    );
  }

  /// handle system UI using [AnnotatedRegion] and set background color
  Widget _annotatedWrapper(SignInController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: Container(
        color: AppColors.darkBackground,
        child: Consumer<ScreenHeight>(
          builder: (BuildContext context, ScreenHeight res, Widget? child) =>
              _mainGestureWrapper(context, res, controller),
        ),
      ),
    );
  }

  /// [GestureDetector] wrapper for dismiss virtual keyboard
  /// and when user dismissed the keyboard by gesture,
  /// then reset scroll using [ScrollController]
  Widget _mainGestureWrapper(
    BuildContext context,
    ScreenHeight res,
    SignInController signInController,
  ) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        if (res.isSmall) {
          ScrollConfig.resetScrollView(signInController.scrollController);
        }
      },
      child: _mainViewWrapper(context, res, signInController),
    );
  }

  /// wrap main view with responsive column, [res] param
  /// from [Consumer] builder
  Widget _mainViewWrapper(
    BuildContext context,
    ScreenHeight res,
    SignInController signInController,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnMainAxisAlignment: MainAxisAlignment.center,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Expanded(
            child: CustomScrollView(
              controller: signInController.scrollController,
              physics: ScrollConfig.isUseScroll(res.isSmall),
              slivers: <Widget>[_sliver(context, signInController)],
            ),
          ),
        ),
      ],
    );
  }

  /// Implement sliver widget with responsive column and items
  Widget _sliver(BuildContext context, SignInController signInController) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          // logo header and text
          ResponsiveRowColumnItem(child: _logoApp()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'sign_in_motto_label'.tr,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w300,
              color: Colors.white,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
          // body section with form and login button
          ResponsiveRowColumnItem(
            child: _formWrapper(
              context: context,
              signInController: signInController,
            ),
          ),
          ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
          ResponsiveRowColumnItem(
            child: _loginButton(
              signInController: signInController,
              onTap: () {
                signInController.onValidateLogin(context);
              },
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          // footer with text button and version number
          ResponsiveRowColumnItem(
            child: Obx(
              () => _loginWithEmailTextButton(
                isPhoneNumber: signInController.isUsePhoneNumber.value,
                onTap: () {
                  signInController.changeFormType();
                },
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: Obx(
              () => _versionNumber(signInController.versionApp.value ?? ''),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
        ],
      ),
    );
  }

  // logo company
  Widget _logoApp() {
    return Align(
      child: Image.asset(
        Assets.imageBlackOwlTransparent,
        width: SizeConfig.horizontal(45),
        height: SizeConfig.horizontal(45),
        fit: BoxFit.cover,
      ),
    );
  }

  // form wrapper for login using phone number and email
  // dialog context following current page context
  Widget _formWrapper({
    required BuildContext context,
    required SignInController signInController,
  }) {
    return Obx(() {
      if (signInController.isUsePhoneNumber.value) {
        return FormLoginPhoneNumber(
          pageContext: context,
          flagName: signInController.flagName,
          callingCode: signInController.defaultCountryCallingCode,
          phoneNumberController: signInController.phoneNumberController,
          onTap: (String flagName, String countryName, int countryCode) {
            signInController.updatePhoneIdentify(
              flagName,
              countryName,
              countryCode,
            );
            Navigator.of(context).pop();
          },
          countryList: signInController.countryCodeList,
          isDualScreen: signInController.isDualScreen.value,
        );
      } else {
        return FormLoginEmailAddress(
          emailAddressController: signInController.emailAddressController,
        );
      }
    });
  }

  // under line for phone number form
  Widget _underlineFormPhoneNumber() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
      child: const Underline(),
    );
  }

  // login button navigate to OTP page
  Container _loginButton({
    required Function() onTap,
    required SignInController signInController,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
      width: SizeConfig.screenWidth,
      child: Obx(
        () => CustomFlatButton(
          onTap: onTap,
          text: 'next_label'.tr,
          useCircularLoading: true,
          loading: signInController.isLoading.value,
        ),
      ),
    );
  }

  // text button for optional login using email address
  Widget _loginWithEmailTextButton({
    required bool isPhoneNumber,
    required Function() onTap,
  }) {
    return MontserratButton(
      onTap: onTap,
      value: isPhoneNumber
          ? 'sign_in_with_email'.tr
          : 'sign_in_with_phone_number'.tr,
    );
  }

  // version number of the app
  Widget _versionNumber(String version) {
    return MontserratTextView(
      value: 'Version $version',
      color: AppColors.darkGold,
    );
  }
}
