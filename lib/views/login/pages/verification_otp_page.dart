import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/account/verification_otp_controller.dart';
import '../../../helpers/scroll_config.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/buttons/montserrat_button.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

/// [VerificationPage] represent user verification
/// after user registered their data or login using [phone number] or
/// [email address], user will receive verification code, the verification
/// will be expired in few minutes and will be validate within from APi
class VerificationPage extends StatelessWidget {
  // connstuctor
  const VerificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // intiate size config device
    SizeConfig().init(context);

    /// wrap UI with [keyboardProvider] widget for handle virtual keyboard
    /// and [FrameScaffold] for handle UI theme, also register controller
    /// using [GetBuilder]
    return KeyboardSizeProvider(
      child: FrameScaffold(
        heightBar: 0,
        elevation: 0,
        avoidBottomInset: true,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        view: GetBuilder<VerificationOTPController>(
          init: VerificationOTPController(pageContext: context),
          builder: (VerificationOTPController controller) =>
              _annotatedWrapper(controller),
        ),
      ),
    );
  }

  /// handle system UI using [AnnotatedRegion] and set background color
  Widget _annotatedWrapper(VerificationOTPController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: Container(
        color: AppColors.darkBackground,
        child: Consumer<ScreenHeight>(
          builder: (BuildContext context, ScreenHeight res, Widget? child) =>
              _mainGestureWrapper(context, res, controller),
        ),
      ),
    );
  }

  /// [GestureDetector] wrapper for dismiss virtual keyboard
  /// and when user dismissed the keyboard by gesture,
  /// then reset scroll using [ScrollController]
  Widget _mainGestureWrapper(
    BuildContext context,
    ScreenHeight res,
    VerificationOTPController controller,
  ) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        if (res.isSmall) {
          ScrollConfig.resetScrollView(controller.scrollController);
        }
      },
      child: _mainViewWrapper(context, res, controller),
    );
  }

  /// wrap main view with responsive column, [res] param
  /// from [Consumer] builder
  Widget _mainViewWrapper(
    BuildContext context,
    ScreenHeight res,
    VerificationOTPController controller,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnMainAxisAlignment: MainAxisAlignment.center,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Expanded(
            child: CustomScrollView(
              controller: controller.scrollController,
              physics: ScrollConfig.isUseScroll(res.isSmall),
              slivers: <Widget>[_sliver(context, controller)],
            ),
          ),
        ),
      ],
    );
  }

  /// Implement sliver widget with responsive column and items
  Widget _sliver(
    BuildContext context,
    VerificationOTPController controller,
  ) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _logoApp()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
          ResponsiveRowColumnItem(
            child: _descriptionTop(controller: controller),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _memberAccessDisplay(controller: controller),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 8)),
          ResponsiveRowColumnItem(
            child: _formWithPlugin(context: context, controller: controller),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(child: _buttonSignIn(controller: controller)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: _validationCountdownOrResend(controller: controller),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: _textBackToLogin(controller: controller),
          ),
        ],
      ),
    );
  }

  // logo company
  Widget _logoApp() {
    return Align(
      child: Image.asset(
        Assets.imageBlackOwlTransparent,
        width: SizeConfig.horizontal(45),
        height: SizeConfig.horizontal(45),
        fit: BoxFit.cover,
      ),
    );
  }

  // instruction for user
  /// with [size] param with default value is 4
  Widget _descriptionTop({required VerificationOTPController controller}) {
    return Obx(
      () => MontserratTextView(
        value: '${controller.otpInstruction}',
        size: SizeConfig.safeHorizontal(4),
        fontWeight: FontWeight.w300,
        alignText: AlignTextType.center,
        color: Colors.white,
      ),
    );
  }

  // show phone number or email user
  Widget _memberAccessDisplay({required VerificationOTPController controller}) {
    return Obx(
      () => MontserratTextView(
        value: '${controller.memberAccess}',
        size: SizeConfig.safeHorizontal(4),
        fontWeight: FontWeight.w300,
        alignText: AlignTextType.center,
        color: Colors.white,
      ),
    );
  }

  // OTP form
  /// for handle width form use [marginHorizontal] param with default value 7
  /// set [autoDisposeControllers] to false for prevent form disposable after screen folded
  Widget _formWithPlugin({
    required BuildContext context,
    required VerificationOTPController controller,
  }) {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(7)),
      child: PinCodeTextField(
        appContext: context,
        textStyle: montserratStyle.otpFieldGrey(),
        length: 6,
        key: key,
        showCursor: false,
        controller: controller.otpNumber,
        cursorColor: AppColors.lightDark,
        keyboardType: TextInputType.number,
        autoDisposeControllers: false,
        pinTheme: PinTheme(
          borderRadius: BorderRadius.circular(10),
          shape: PinCodeFieldShape.box,
          activeColor: AppColors.flatGold,
          selectedColor: AppColors.lightDark,
          inactiveColor: AppColors.lightDark,
        ),
        onChanged: (String value) {},
      ),
    );
  }

  // sign in button
  Widget _buttonSignIn({required VerificationOTPController controller}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
      width: SizeConfig.screenWidth,
      child: Obx(
        () => CustomFlatButton(
          loading: controller.isLoadingButton.value,
          onTap: () {
            controller.onPressSignIn();
          },
          text: 'sign_in_label'.tr,
          useCircularLoading: true,
        ),
      ),
    );
  }

  // check if user already pressed resend OTP or not
  Widget _validationCountdownOrResend({
    required VerificationOTPController controller,
  }) {
    return Obx(() {
      if (controller.isLoadingResend.isTrue) {
        return CircularProgressIndicator(color: AppColors.whiteBackground);
      } else {
        if (controller.isResendCode.isTrue) {
          return _textResendCode(controller: controller);
        } else {
          return _textCountDown(controller: controller);
        }
      }
    });
  }

  /// param [size] fwith default value 4 for font size
  Widget _textCountDown({required VerificationOTPController controller}) {
    return Obx(
      () => MontserratTextView(
        value: controller.timerText.value,
        color: AppColors.whiteBackground,
        size: SizeConfig.safeHorizontal(4),
      ),
    );
  }

  /// param [size] fwith default from custom widget for font size
  Widget _textResendCode({required VerificationOTPController controller}) {
    return MontserratButton(
      onTap: () {
        controller.resendOTPCode();
      },
      value: 'resend_otp_label'.tr,
    );
  }

  /// param [size] fwith default from custom widget for font size
  Widget _textBackToLogin({required VerificationOTPController controller}) {
    return MontserratButton(
      onTap: () {
        controller.isLoading.value = false;
        controller.isLoadingButton.value = false;
        GetInstance().resetInstance();
        Get.offAllNamed(RouteNames.signInRoute);
      },
      value: 'back_to_sign_in_label'.tr,
    );
  }
}
