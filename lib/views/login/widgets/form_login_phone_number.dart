import 'package:country_calling_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../models/data/country_code/country_code_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/dialogs/country_dialog.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class FormLoginPhoneNumber extends StatelessWidget {
  const FormLoginPhoneNumber({
    this.marginHorizontal,
    required this.pageContext,
    required this.flagName,
    required this.callingCode,
    required this.phoneNumberController,
    required this.onTap,
    required this.countryList,
    this.onChanged,
    this.isDualScreen = false,
    Key? key,
  }) : super(key: key);

  final double? marginHorizontal;
  final BuildContext pageContext;
  final Rx<String> flagName;
  final Rx<String> callingCode;
  final TextEditingController phoneNumberController;
  final Function(String flagName, String countryName, int countryCode) onTap;
  final Function(String)? onChanged;
  final RxList<CountryIdentity> countryList;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(marginHorizontal ?? 10),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _countryFlagButton(pageContext)),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 1)),
          ResponsiveRowColumnItem(child: _phoneNumberForm()),
        ],
      ),
    );
  }

  Widget _countryFlagButton(BuildContext context) {
    return RippleButton(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            FocusManager.instance.primaryFocus?.unfocus();
            return CountryDialog(
              onTap: onTap,
              countryList: countryList,
              isDualScreen: isDualScreen,
            );
          },
        );
      },
      child: _flagWrapper(),
    );
  }

  Widget _flagWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _flag()),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 1)),
        ResponsiveRowColumnItem(child: _callingCode()),
        ResponsiveRowColumnItem(
          child: Icon(
            Icons.arrow_drop_down_outlined,
            color: AppColors.whiteBackground,
            size: SizeConfig.horizontal(6),
          ),
        )
      ],
    );
  }

  Widget _flag() {
    return Obx(
      () => SizedBox(
        width: SizeConfig.horizontal(7.5),
        height: SizeConfig.horizontal(7.5),
        child: Image.asset(
          flagName.value,
          package: countryCodePackageName,
        ),
      ),
    );
  }

  Widget _callingCode() {
    return Obx(
      () => MontserratTextView(
        value: callingCode.value,
        size: SizeConfig.safeHorizontal(5),
        color: Colors.white,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.w300,
      ),
    );
  }

  Widget _phoneNumberForm() {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return Expanded(
      child: TextField(
        controller: phoneNumberController,
        onChanged: onChanged,
        keyboardType: TextInputType.phone,
        style: montserratStyle.white5(),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: SizeConfig.vertical(0.1)),
          hintText: 'sign_in_hint_phone_number'.tr,
          hintStyle: montserratStyle.grey5(),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
