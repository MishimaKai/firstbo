import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' as getx;
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/balances/points/my_points_detail_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/price_item.dart';
import '../../widgets/layouts/shimmer_placeholder.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../widgets/detail_info.dart';

class MyPointsDetailPage extends StatelessWidget {
  // constructor
  const MyPointsDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 45.0,
        elevation: 0,
        avoidBottomInset: true,
        isUseLeading: false,
        isImplyLeading: false,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        action: _frameActionWrapper(),
        view: _getBuilderWrapper(context),
      ),
    );
  }

  Widget _frameActionWrapper() {
    return FrameActionWrapper(
      title: PageNames.recentTransactionPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        getx.Get.back();
      },
    );
  }

  Widget _getBuilderWrapper(BuildContext context) {
    return getx.GetBuilder<MyPointsDetailController>(
      init: MyPointsDetailController(pageContext: context),
      builder: (MyPointsDetailController controller) {
        return RefreshIndicator(
          triggerMode: RefreshIndicatorTriggerMode.anywhere,
          child: Container(
            color: AppColors.darkBackground,
            child: _mainViewWrapper(context: context, controller: controller),
          ),
          onRefresh: () async {
            await controller.getPointDetail();
          },
        );
      },
    );
  }

  Widget _mainViewWrapper({
    required BuildContext context,
    required MyPointsDetailController controller,
  }) {
    return Container(
      padding: EdgeInsets.only(
        top: SizeConfig.vertical(2),
        left: SizeConfig.horizontal(7),
        right: SizeConfig.horizontal(7),
      ),
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: getx.Obx(() {
                if (controller.isLoading.isTrue) {
                  return _contentOnLoad(context);
                } else {
                  return _detailTransactionWrapper(controller, context);
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _contentOnLoad(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        const ResponsiveRowColumnItem(
          child: ItemShimmer(length: 1, height: 40),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
        ResponsiveRowColumnItem(
          child: Container(
            padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
            child: ShimmerPlaceholder(
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
              width: SizeConfig.horizontal(35),
              height: ResponsiveValue<double>(
                context,
                defaultValue: SizeConfig.vertical(22),
                valueWhen: <Condition<double>>[
                  Condition<double>.equals(
                    name: MOBILE,
                    value: SizeConfig.vertical(25),
                  ),
                ],
              ).value,
            ),
          ),
        ),
      ],
    );
  }

  ResponsiveRowColumn _detailTransactionWrapper(
    MyPointsDetailController controller,
    BuildContext context,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: DetailInfo(controller: controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: SizeConfig.horizontal(50),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return PriceItem(
                  id: 0,
                  image: controller.getItemImage(),
                  productName: controller.getItemName(),
                  price: controller.getItemAmount(),
                  lengthItem: 3,
                  index: 0,
                  typeShowPriceItem: TypeShowPriceItem.isDisplayForPackages,
                  typePriceItem: TypePriceItem.isPriceCurrency,
                  navigatePriceItem: NavigatePriceItem.none,
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
