import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart' as getx;
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:intl/intl.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/balances/points/my_points_controller.dart';
import '../../../helpers/date_helper.dart';
import '../../../models/data/points/points_model.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class MyPointsPage extends StatelessWidget {
  const MyPointsPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return getx.GetBuilder<MyPointsController>(
      init: MyPointsController(pageContext: context),
      builder: (MyPointsController controller) {
        return FrameScaffold(
          heightBar: 45.0,
          elevation: 0,
          avoidBottomInset: true,
          isUseLeading: false,
          isImplyLeading: false,
          color: Platform.isIOS ? AppColors.darkBackground : null,
          statusBarColor: AppColors.darkBackground,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
          colorScaffold: AppColors.darkBackground,
          action: FrameActionWrapper(
            title: PageNames.pointsPage,
            fontWeight: FontWeight.bold,
            onPressed: () {
              getx.Get.back(result: <String, bool>{'update_balance': true});
            },
          ),
          view: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
              systemNavigationBarColor: AppColors.darkBackground,
              systemNavigationBarIconBrightness: Brightness.light,
            ),
            child: Container(
              color: AppColors.darkBackground,
              padding: EdgeInsets.only(
                left: SizeConfig.horizontal(2.5),
                right: SizeConfig.horizontal(2.5),
                top: SizeConfig.vertical(1),
              ),
              child: _mainViewWrapper(context: context, controller: controller),
            ),
          ),
        );
      },
    );
  }

  Widget _mainViewWrapper({
    required BuildContext context,
    required MyPointsController controller,
  }) {
    return RefreshIndicator(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: getx.Obx(
              () => _dateWrapper(context, controller),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Flexible(child: _pointList(controller)),
          )
        ],
      ),
      onRefresh: () async {
        await controller.onRefreshPage();
      },
    );
  }

  // wrapper date picker
  Widget _dateWrapper(BuildContext context, MyPointsController controller) {
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: RippleButton(
              onTap: () => controller.onOpenCalendar(),
              child: _dateButton(
                title: 'From',
                format: controller.rangeStartGeneralCalendar.value,
                context: context,
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            child: RippleButton(
              onTap: () => controller.onOpenCalendar(),
              child: _dateButton(
                title: 'To',
                format: controller.rangeEndGeneralCalendar.value,
                context: context,
              ),
            ),
          ),
        ],
      ),
    );
  }

  // custom date button
  Widget _dateButton({
    required String title,
    required DateTime format,
    required BuildContext context,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1)),
      width: SizeConfig.horizontal(40),
      height: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.vertical(7),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.vertical(8),
          ),
        ],
      ).value,
      child: ResponsiveRowColumn(
        columnMainAxisAlignment: MainAxisAlignment.center,
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: title,
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w700,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: DateFormat('dd MMMM y').format(format),
              alignText: AlignTextType.center,
              color: AppColors.flatGold,
              size: SizeConfig.safeHorizontal(3.5),
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }

  Widget _pointList(MyPointsController controller) {
    return PagedListView<int, PointsModel>(
      shrinkWrap: true,
      pagingController: controller.pointsHistoryController,
      builderDelegate: PagedChildBuilderDelegate<PointsModel>(
        itemBuilder: (BuildContext context, PointsModel points, int index) {
          return _tabItemPoint(
            id: points.id,
            amount: points.amount ?? 0,
            title: points.title ?? '',
            createdAt: points.createdAt!,
            references: points.refTable ?? '',
            // ignore: avoid_bool_literals_in_conditional_expressions
            isValid: points.viewDetail == 0 ? false : true,
          );
        },
        firstPageProgressIndicatorBuilder: (BuildContext context) => Container(
          margin: EdgeInsets.only(top: SizeConfig.vertical(2)),
          child: const ItemShimmer(length: 4, height: 14),
        ),
        newPageProgressIndicatorBuilder: (BuildContext context) => Container(
          margin: EdgeInsets.only(top: SizeConfig.vertical(2)),
          child: const ItemShimmer(length: 1, height: 14),
        ),
        noItemsFoundIndicatorBuilder: (BuildContext context) =>
            const CustomEmptyState(
          caption: 'No points available',
          height: 55,
        ),
      ),
    );
  }

  ResponsiveRowColumnItem _tabItemPoint({
    required int id,
    required int amount,
    required String title,
    required String createdAt,
    required String references,
    required bool isValid,
  }) {
    return ResponsiveRowColumnItem(
      child: Container(
        margin: EdgeInsets.only(top: SizeConfig.vertical(2)),
        width: SizeConfig.screenWidth,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
          color: AppColors.flatGold,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.horizontal(2)),
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              splashColor: AppColors.rippleColor,
              onTap: () {
                if (isValid) {
                  getx.Get.toNamed(
                    RouteNames.myPointsDetailPage,
                    arguments: id,
                  );
                } else {
                  _emptyAction();
                }
              },
              child: Ink(
                color: Colors.transparent,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: isValid
                        ? SizeConfig.vertical(1.5)
                        : SizeConfig.vertical(2),
                    bottom: SizeConfig.vertical(2),
                    left: SizeConfig.horizontal(4),
                    right: SizeConfig.horizontal(4),
                  ),
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    columnCrossAxisAlignment: CrossAxisAlignment.start,
                    columnMainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(
                        child: _dateTimeAndCodePoints(
                          createdAt,
                          references,
                          isValid,
                        ),
                      ),
                      const ResponsiveRowColumnItem(
                        child: SpaceSizer(vertical: 2),
                      ),
                      ResponsiveRowColumnItem(
                        child: _descriptionPoints(amount, title),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  dynamic _emptyAction() {
    return null;
  }

  ResponsiveRowColumn _dateTimeAndCodePoints(
    String createdAt,
    String refeference,
    bool isValid,
  ) {
    final String day = DateHelper.getDayDisplayer(createdAt);
    final String date = DateHelper.getDateDisplayer(createdAt);
    final String month = DateHelper.getMonthDisplayer(createdAt, false);
    final String year = DateHelper.getYearDisplayer(createdAt);

    return ResponsiveRowColumn(
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: '$day, $date $month $year',
            color: AppColors.darkBackground,
            size: SizeConfig.safeHorizontal(2.5),
            fontWeight: FontWeight.w700,
          ),
        ),
        ResponsiveRowColumnItem(
          child: isValid
              ? Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal(3.5),
                    vertical: SizeConfig.vertical(0.5),
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.darkBackground,
                    borderRadius:
                        BorderRadius.circular(SizeConfig.horizontal(2)),
                  ),
                  child: MontserratTextView(
                    value: 'View Details',
                    color: AppColors.flatGold,
                    size: SizeConfig.safeHorizontal(2.5),
                  ),
                )
              : const SizedBox.shrink(),
        ),
      ],
    );
  }

  ResponsiveRowColumn _descriptionPoints(int amount, String title) {
    return ResponsiveRowColumn(
      // rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SizedBox(
            child: Center(
              child: Image.asset(
                Assets.iconPoint,
                fit: BoxFit.fill,
                width: SizeConfig.horizontal(6),
                height: SizeConfig.horizontal(6),
              ),
            ),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SpaceSizer(horizontal: SizeConfig.horizontal(1)),
        ),
        ResponsiveRowColumnItem(
          child: Expanded(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              columnCrossAxisAlignment: CrossAxisAlignment.start,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: '$amount points',
                    color:
                        amount < 0 ? AppColors.redWeekend : AppColors.darkBlue,
                    size: SizeConfig.safeHorizontal(4),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: title,
                    alignText: AlignTextType.left,
                    color: AppColors.darkBackground,
                    size: SizeConfig.safeHorizontal(3.5),
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
