import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';

import '../../../../helpers/string_helper.dart';
import '../../../../utils/app_colors.dart';
import '../../../../utils/size_config.dart';
import '../../../controllers/balances/points/my_points_detail_controller.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class DetailInfo extends StatelessWidget {
  const DetailInfo({Key? key, required this.controller}) : super(key: key);

  final MyPointsDetailController controller;

  @override
  Widget build(BuildContext context) {
    return ResponsiveRowColumnItem(
      child: Container(
        width: SizeConfig.screenWidth,
        padding: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(3),
          horizontal: SizeConfig.horizontal(2),
        ),
        decoration: BoxDecoration(
          color: AppColors.flatGold,
          borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        ),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnCrossAxisAlignment: CrossAxisAlignment.start,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value: controller.getDatePoints(),
                color: AppColors.darkBackground,
                fontWeight: FontWeight.w700,
                size: SizeConfig.safeHorizontal(3.5),
              ),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value: 'Deducted Points',
                color: AppColors.darkBackground,
                fontWeight: FontWeight.w700,
                size: SizeConfig.safeHorizontal(3.5),
              ),
            ),
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value:
                    '${StringHelper.convertToRupiah(controller.getDeductedPoints())} points',
                color: AppColors.darkBackground,
                fontWeight: FontWeight.w300,
                size: SizeConfig.safeHorizontal(3.5),
              ),
            ),
            _itemTypeWrapper(),
          ],
        ),
      ),
    );
  }

  ResponsiveRowColumnItem _itemTypeWrapper() {
    return ResponsiveRowColumnItem(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Redeemed Item',
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: controller.getItemName(),
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Status',
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: controller.getStatus(),
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _rowStatusList(),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Note',
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: controller.getNote(),
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
        ],
      ),
    );
  }

  ResponsiveRowColumnItem _rowStatusList() {
    return ResponsiveRowColumnItem(
      child: SizedBox(
        width: SizeConfig.screenWidth,
        child: Row(
          children: List<Widget>.generate(
            controller.getStatusList().length,
            (int index) {
              return _itemWrapper(index);
            },
          ),
        ),
      ),
    );
  }

  Widget _itemWrapper(int index) {
    return SizedBox(
      width: SizeConfig.horizontal(18),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  height: SizeConfig.vertical(1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: index == 0
                          ? Radius.circular(SizeConfig.horizontal(10))
                          : Radius.zero,
                      bottomLeft: index == 0
                          ? Radius.circular(SizeConfig.horizontal(10))
                          : Radius.zero,
                      topRight: index == controller.getStatusList().length - 1
                          ? Radius.circular(SizeConfig.horizontal(10))
                          : Radius.zero,
                      bottomRight:
                          index == controller.getStatusList().length - 1
                              ? Radius.circular(SizeConfig.horizontal(10))
                              : Radius.zero,
                    ),
                    border: Border.all(
                      width: 0,
                      color: AppColors.darkBackground,
                    ),
                    color: AppColors.darkBackground,
                  ),
                ),
                Container(
                  width: SizeConfig.vertical(3),
                  height: SizeConfig.vertical(3),
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: AppColors.darkBackground,
                        width: SizeConfig.horizontal(1.5)),
                    shape: BoxShape.circle,
                    color: controller.getStatusFromList(index)
                        ? AppColors.flatGold
                        : AppColors.darkBackground,
                  ),
                ),
              ],
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: controller.getStatusNameFromList(index),
              color: AppColors.darkBackground,
              size: SizeConfig.safeHorizontal(2.5),
            ),
          ),
        ],
      ),
    );
  }
}
