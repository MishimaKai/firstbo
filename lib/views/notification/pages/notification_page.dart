import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../../../controllers/notifications/notification_controller.dart';
import '../../../models/data/notifications/notification_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../widgets/item_notification.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<NotificationController>(
      init: NotificationController(pageContext: context),
      builder: (NotificationController controller) {
        return FrameScaffold(
          heightBar: 65.0,
          elevation: 0,
          avoidBottomInset: true,
          isUseLeading: false,
          isImplyLeading: false,
          color: Platform.isIOS ? AppColors.darkBackground : null,
          statusBarColor: AppColors.darkBackground,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
          colorScaffold: AppColors.darkBackground,
          action: FrameActionWrapper(
            title: 'Notifications',
            onPressed: () {
              // _controller.onBackNavigate();
              Get.back(result: <String, bool>{'update_balance': true});
            },
          ),
          view: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
              systemNavigationBarColor: AppColors.darkBackground,
              systemNavigationBarIconBrightness: Brightness.light,
            ),
            child: RefreshIndicator(
              child: Container(
                color: AppColors.darkBackground,
                padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(2.5),
                  right: SizeConfig.horizontal(2.5),
                ),
                child: _notificationWrapper(controller),
              ),
              onRefresh: () async {
                await controller.onRefreshPage();
              },
            ),
          ),
        );
      },
    );
  }

  Widget _notificationWrapper(NotificationController controller) {
    return Container(
      height: SizeConfig.vertical(90),
      width: SizeConfig.screenWidth,
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(1)),
      child: PagedListView<int, NotificationModel>(
        pagingController: controller.notificationListController,
        builderDelegate: PagedChildBuilderDelegate<NotificationModel>(
          itemBuilder: (
            BuildContext context,
            NotificationModel notification,
            int index,
          ) {
            return ItemNotification(
              title: notification.title,
              subtitle: notification.subtitle,
            );
          },
          firstPageProgressIndicatorBuilder: (BuildContext context) =>
              const ItemShimmer(
            length: 5,
            height: 10,
          ),
          newPageProgressIndicatorBuilder: (BuildContext context) =>
              const ItemShimmer(
            length: 1,
            height: 10,
          ),
          noItemsFoundIndicatorBuilder: (BuildContext context) =>
              const CustomEmptyState(
            caption: 'No notifications available',
            height: 100,
          ),
        ),
      ),
    );
  }
}
