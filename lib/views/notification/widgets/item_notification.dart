import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class ItemNotification extends StatelessWidget {
  const ItemNotification({
    required this.title,
    required this.subtitle,
    Key? key,
  }) : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.screenWidth,
      // height: SizeConfig.vertical(10),
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(1.5),
      ),
      margin: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1)),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        color: AppColors.flatGold,
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              Assets.iconNotification,
              fit: BoxFit.fill,
              height: SizeConfig.horizontal(7),
              width: SizeConfig.horizontal(7),
              color: AppColors.darkBackground,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
          ResponsiveRowColumnItem(
            child: Expanded(
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                columnCrossAxisAlignment: CrossAxisAlignment.start,
                columnMainAxisAlignment: MainAxisAlignment.center,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: MontserratTextView(
                      value: title,
                      size: SizeConfig.safeHorizontal(4),
                      fontWeight: FontWeight.w700,
                      color: AppColors.darkBackground,
                      alignText: AlignTextType.left,
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: MontserratTextView(
                      value: subtitle,
                      size: SizeConfig.safeHorizontal(3.5),
                      fontWeight: FontWeight.w300,
                      color: AppColors.darkBackground,
                      alignText: AlignTextType.left,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
