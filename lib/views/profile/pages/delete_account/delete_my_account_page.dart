import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../../controllers/profile/delete_account/delete_my_account_controller.dart';
import '../../../../utils/app_colors.dart';
import '../../../../utils/assets_list.dart';
import '../../../../utils/enums.dart';
import '../../../../utils/size_config.dart';

import '../../../login/widgets/form_login_phone_number.dart';
import '../../../widgets/buttons/custom_flat_buttons.dart';
import '../../../widgets/frame/frame_scaffold.dart';
import '../../../widgets/layouts/space_sizer.dart';
import '../../../widgets/layouts/under_line.dart';
import '../../../widgets/text/montserrat_text_error.dart';
import '../../../widgets/text/montserrat_text_view.dart';

class DeleteMyAccountPage extends StatelessWidget {
  // constructor
  const DeleteMyAccountPage({Key? key}) : super(key: key);

  /// detect keyboard is visible, [isKeyboardVisible] are true
  /// then show bezier curve
  Widget _isDisplayInfo(
    DeleteMyAccountController controller,
    bool isKeyboardVisible,
  ) {
    // log("screen height = ${SizeConfig.screenHeight} screen width = ${SizeConfig.screenWidth}");
    if (!isKeyboardVisible) {
      return _cardInfo();
    } else {
      return const SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<DeleteMyAccountController>(
      init: DeleteMyAccountController(pageContext: context),
      builder: (DeleteMyAccountController controller) {
        return _mainViewWrapper(controller);
      },
    );
  }

  Widget _mainViewWrapper(DeleteMyAccountController controller) {
    return KeyboardSizeProvider(
      child: Consumer<ScreenHeight>(
        builder: (BuildContext context, ScreenHeight res, Widget? child) {
          return FrameScaffold(
            elevation: 0,
            avoidBottomInset: false,
            color: res.isOpen ? AppColors.darkBackground : AppColors.flatGold,
            statusBarColor:
                res.isOpen ? AppColors.darkBackground : AppColors.flatGold,
            statusBarIconBrightness:
                res.isOpen ? Brightness.light : Brightness.dark,
            statusBarBrightness:
                res.isOpen ? Brightness.dark : Brightness.light,
            colorScaffold: AppColors.darkBackground,
            isUseLeading: true,
            customLeading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back,
                color: res.isOpen
                    ? AppColors.whiteBackground
                    : AppColors.darkBackground,
              ),
            ),
            view: _annotateWrapper(controller, context, res),
          );
        },
      ),
    );
  }

  Widget _annotateWrapper(
    DeleteMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _mainGestureWrapper(controller, context, res),
    );
  }

  // gesture wrapper for listening keyboard size
  Widget _mainGestureWrapper(
    DeleteMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        // if (res.isSmall) {
        //   controller.resetScrollView();
        // }
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _backgroundSpacer(res),
          _content(controller, context, res),
        ],
      ),
    );
  }

  Widget _backgroundSpacer(ScreenHeight res) {
    if (res.isOpen) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Expanded(
              child: Container(
                color: AppColors.darkBackground,
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
        ],
      );
    } else {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Container(
              color: AppColors.flatGold,
              width: SizeConfig.screenWidth,
              height: SizeConfig.vertical(25),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Expanded(
              child: Container(
                color: AppColors.darkBackground,
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
        ],
      );
    }
  }

  Widget _content(
    DeleteMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _isDisplayInfo(controller, res.isOpen)),
        ResponsiveRowColumnItem(
          child: SpaceSizer(vertical: res.isOpen ? 2 : 5),
        ),
        ResponsiveRowColumnItem(child: _formWrapper(controller, context)),
        ResponsiveRowColumnItem(child: _buttonDisplay(controller, res)),
        ResponsiveRowColumnItem(
          child: SpaceSizer(vertical: res.isOpen ? 0 : 10),
        ),
      ],
    );
  }

  Widget _buttonDisplay(
    DeleteMyAccountController controller,
    ScreenHeight res,
  ) {
    if (res.isOpen) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(child: _buttonWrapper(controller)),
        ],
      );
    } else {
      return Expanded(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: _buttonWrapper(controller),
        ),
      );
    }
  }

  Widget _cardInfo() {
    return Container(
      padding: EdgeInsets.fromLTRB(
        SizeConfig.horizontal(2),
        0,
        SizeConfig.horizontal(2),
        SizeConfig.vertical(2),
      ),
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(
        color: AppColors.flatGold,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(SizeConfig.horizontal(2)),
          bottomRight: Radius.circular(SizeConfig.horizontal(2)),
        ),
        border: Border.all(color: AppColors.flatGold, width: 0),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              Assets.iconAlertCircle,
              width: SizeConfig.horizontal(35),
              height: SizeConfig.horizontal(35),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Delete My Account',
              fontWeight: FontWeight.bold,
              size: SizeConfig.safeHorizontal(4.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'There is no undo, so please be careful!',
              fontWeight: FontWeight.bold,
              alignText: AlignTextType.center,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
          ResponsiveRowColumnItem(child: _cardInfoDetail()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ],
      ),
    );
  }

  Widget _cardInfoDetail() {
    return SizedBox(
      width: SizeConfig.horizontal(60),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Flexible(
              child: MontserratTextView(
                value:
                    'You will not be able recover your account, or any information on it once you go through with deletion',
                alignText: AlignTextType.center,
                fontWeight: FontWeight.w300,
                size: SizeConfig.safeHorizontal(3.2),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _formWrapper(
    DeleteMyAccountController controller,
    BuildContext context,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _formInfo()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
        ResponsiveRowColumnItem(
          child: _formForPhoneNumber(context, controller),
        ),
      ],
    );
  }

  Widget _formInfo() {
    return SizedBox(
      width: SizeConfig.horizontal(60),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Flexible(
              child: MontserratTextView(
                value:
                    'Type your phone number to get OTP code to proceed to the next step',
                color: AppColors.whiteBackground,
                alignText: AlignTextType.center,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _formForPhoneNumber(
    BuildContext context,
    DeleteMyAccountController controller,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: FormLoginPhoneNumber(
            pageContext: context,
            flagName: controller.flagName,
            callingCode: controller.defaultCountryCallingCode,
            phoneNumberController: controller.phoneNumberController,
            onTap: (
              String flagName,
              String countryName,
              int countryCode,
            ) {
              controller.updatePhoneIdentify(
                flagName,
                countryName,
                countryCode,
              );
              Navigator.of(context).pop();
            },
            countryList: controller.countryCodeList,
            isDualScreen: controller.isDualScreen.value,
          ),
        ),
        ResponsiveRowColumnItem(child: _underlineFormPhoneNumber()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _validatetextView(controller)),
      ],
    );
  }

  // under line for phone number form
  Widget _underlineFormPhoneNumber() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
      child: const Underline(),
    );
  }

  Widget _validatetextView(DeleteMyAccountController controller) {
    return Obx(() {
      if (controller.textValidate.value == null) {
        return const SizedBox.shrink();
      } else {
        return Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.horizontal(10),
          ),
          child: Align(
            alignment: Alignment.centerLeft,
            child: MontserratTextError(
              value: controller.textValidate.toString(),
            ),
          ),
        );
      }
    });
  }

  Widget _buttonWrapper(DeleteMyAccountController controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: CustomFlatButton(
            onTap: () {
              Get.back();
            },
            text: 'Cancel',
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: Obx(
            () => CustomFlatButton(
              loading: controller.isLoading.value,
              onTap: () {
                controller.navigateToVerify();
              },
              text: 'Next',
              useCircularLoading: true,
            ),
          ),
        ),
      ],
    );
  }
}
