import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../../controllers/profile/delete_account/delete_verify_my_account_controller.dart';
import '../../../../routes/route_names.dart';
import '../../../../utils/app_colors.dart';
import '../../../../utils/assets_list.dart';
import '../../../../utils/enums.dart';
import '../../../../utils/size_config.dart';
import '../../../widgets/buttons/custom_flat_buttons.dart';
import '../../../widgets/buttons/montserrat_button.dart';
import '../../../widgets/frame/frame_scaffold.dart';
import '../../../widgets/layouts/space_sizer.dart';
import '../../../widgets/text/montserrat_text_view.dart';

class DeleteVerifyMyAccountPage extends StatelessWidget {
  // constructor
  const DeleteVerifyMyAccountPage({Key? key}) : super(key: key);

  /// detect keyboard is visible, [isKeyboardVisible] are true
  /// then show bezier curve
  Widget _isDisplayInfo(
    DeleteVerifyMyAccountController controller,
    bool isKeyboardVisible,
  ) {
    // log("screen height = ${SizeConfig.screenHeight} screen width = ${SizeConfig.screenWidth}");
    if (!isKeyboardVisible) {
      return _cardInfo();
    } else {
      return const SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GetBuilder<DeleteVerifyMyAccountController>(
      init: DeleteVerifyMyAccountController(pageContext: context),
      builder: (DeleteVerifyMyAccountController controller) {
        return _mainViewWrapper(controller);
      },
    );
  }

  Widget _mainViewWrapper(DeleteVerifyMyAccountController controller) {
    return KeyboardSizeProvider(
      child: Consumer<ScreenHeight>(
        builder: (BuildContext context, ScreenHeight res, Widget? child) {
          return FrameScaffold(
            elevation: 0,
            avoidBottomInset: false,
            color: res.isOpen ? AppColors.darkBackground : AppColors.flatGold,
            statusBarColor:
                res.isOpen ? AppColors.darkBackground : AppColors.flatGold,
            statusBarIconBrightness:
                res.isOpen ? Brightness.light : Brightness.dark,
            statusBarBrightness:
                res.isOpen ? Brightness.dark : Brightness.light,
            colorScaffold: AppColors.darkBackground,
            isUseLeading: true,
            customLeading: IconButton(
              onPressed: () {
                Get.offNamed(RouteNames.deleteMyAccountPage);
              },
              icon: Icon(
                Icons.arrow_back,
                color: res.isOpen
                    ? AppColors.whiteBackground
                    : AppColors.darkBackground,
              ),
            ),
            view: _annotateWrapper(controller, context, res),
          );
        },
      ),
    );
  }

  Widget _annotateWrapper(
    DeleteVerifyMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _mainGestureWrapper(controller, context, res),
    );
  }

  // gesture wrapper for listening keyboard size
  Widget _mainGestureWrapper(
    DeleteVerifyMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _backgroundSpacer(res),
          _content(controller, context, res),
        ],
      ),
    );
  }

  Widget _backgroundSpacer(ScreenHeight res) {
    if (res.isOpen) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Expanded(
              child: Container(
                color: AppColors.darkBackground,
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
        ],
      );
    } else {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Container(
              color: AppColors.flatGold,
              width: SizeConfig.screenWidth,
              height: SizeConfig.vertical(25),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Expanded(
              child: Container(
                color: AppColors.darkBackground,
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
        ],
      );
    }
  }

  Widget _content(
    DeleteVerifyMyAccountController controller,
    BuildContext context,
    ScreenHeight res,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _isDisplayInfo(controller, res.isOpen)),
        ResponsiveRowColumnItem(
          child: SpaceSizer(vertical: res.isOpen ? 0 : 3),
        ),
        ResponsiveRowColumnItem(child: _formWrapper(controller, context)),
        ResponsiveRowColumnItem(child: _buttonDisplay(controller, res)),
        ResponsiveRowColumnItem(
          child: SpaceSizer(vertical: res.isOpen ? 0 : 10),
        ),
      ],
    );
  }

  Widget _buttonDisplay(
    DeleteVerifyMyAccountController controller,
    ScreenHeight res,
  ) {
    if (res.isOpen) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(child: _buttonConfirmWrapper(controller)),
        ],
      );
    } else {
      return Expanded(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: _buttonConfirmWrapper(controller),
        ),
      );
    }
  }

  Widget _cardInfo() {
    return Container(
      padding: EdgeInsets.fromLTRB(
        SizeConfig.horizontal(2),
        0,
        SizeConfig.horizontal(2),
        SizeConfig.vertical(2),
      ),
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(
        color: AppColors.flatGold,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(SizeConfig.horizontal(2)),
          bottomRight: Radius.circular(SizeConfig.horizontal(2)),
        ),
        border: Border.all(color: AppColors.flatGold, width: 0),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              Assets.iconAlertCircle,
              width: SizeConfig.horizontal(35),
              height: SizeConfig.horizontal(35),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Delete My Account',
              fontWeight: FontWeight.bold,
              size: SizeConfig.safeHorizontal(4.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'There is no undo, so please be careful!',
              fontWeight: FontWeight.bold,
              alignText: AlignTextType.center,
              size: SizeConfig.safeHorizontal(3.5),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
          ResponsiveRowColumnItem(child: _cardInfoDetail()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ],
      ),
    );
  }

  Widget _cardInfoDetail() {
    return SizedBox(
      width: SizeConfig.horizontal(60),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Flexible(
              child: MontserratTextView(
                value:
                    'You will not be able recover your account, or any information on it once you go through with deletion',
                alignText: AlignTextType.center,
                fontWeight: FontWeight.w300,
                size: SizeConfig.safeHorizontal(3.2),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _formWrapper(
    DeleteVerifyMyAccountController controller,
    BuildContext context,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _formInfo(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
        ResponsiveRowColumnItem(child: _formForOTP(context, controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
        ResponsiveRowColumnItem(
          child: Obx(() {
            if (controller.isLoadingResendOTP.isTrue) {
              return SizedBox(
                width: SizeConfig.horizontal(4.5),
                height: SizeConfig.horizontal(4.5),
                child: CircularProgressIndicator(
                  color: AppColors.whiteBackground,
                ),
              );
            } else {
              if (controller.isResendCode.isTrue) {
                return MontserratButton(
                  onTap: () async {
                    await controller.resendOTP();
                  },
                  value: 'Resend OTP Code',
                );
              } else {
                return MontserratTextView(
                  value: controller.timerText.value,
                  color: AppColors.whiteBackground,
                  size: SizeConfig.safeHorizontal(4),
                );
              }
            }
          }),
        ),
      ],
    );
  }

  Widget _formInfo(DeleteVerifyMyAccountController controller) {
    return SizedBox(
      width: SizeConfig.horizontal(60),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Flexible(
              child: MontserratTextView(
                value:
                    'The OTP has been sent to your mobile number ${controller.phoneNumber}',
                color: AppColors.whiteBackground,
                alignText: AlignTextType.center,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _formForOTP(
    BuildContext context,
    DeleteVerifyMyAccountController controller,
  ) {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(7)),
      child: PinCodeTextField(
        appContext: context,
        textStyle: montserratStyle.otpFieldGrey(),
        length: 6,
        key: key,
        showCursor: false,
        autoDisposeControllers: false,
        controller: controller.otpNumber,
        cursorColor: AppColors.lightDark,
        keyboardType: TextInputType.number,
        pinTheme: PinTheme(
          borderRadius: BorderRadius.circular(10),
          shape: PinCodeFieldShape.box,
          activeColor: AppColors.flatGold,
          selectedColor: AppColors.lightDark,
          inactiveColor: AppColors.lightDark,
        ),
        onChanged: (String value) {},
      ),
    );
  }

  Widget _buttonConfirmWrapper(DeleteVerifyMyAccountController controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Do you really want to delete the account?',
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.bold,
            alignText: AlignTextType.center,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: CustomFlatButton(
            onTap: () {
              Get.offNamed(RouteNames.personalInfoRoute);
            },
            text: 'No Take Me Back',
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
        ResponsiveRowColumnItem(child: _deleteButton(controller)),
      ],
    );
  }

  Widget _deleteButton(DeleteVerifyMyAccountController controller) {
    return Container(
      width: SizeConfig.horizontal(80),
      height: SizeConfig.vertical(5),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: AppColors.darkBackground,
        border: Border.all(color: AppColors.flatGold),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
      ),
      child: Obx(
        () => CustomFlatButton(
          useCircularLoading: true,
          loading: controller.isLoadingSubmit.value,
          radius: 5,
          onTap: () {
            controller.onSubmit();
          },
          text: 'Yes, Delete My Account',
        ),
      ),
    );
  }
}
