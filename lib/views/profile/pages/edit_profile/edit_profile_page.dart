import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../../controllers/profile/edit_profile/edit_profile_controller.dart';
import '../../../../utils/app_colors.dart';
import '../../../../utils/enums.dart';
import '../../../../utils/page_names.dart';
import '../../../../utils/size_config.dart';
import '../../../widgets/buttons/custom_flat_buttons.dart';
import '../../../widgets/buttons/ripple_button.dart';
import '../../../widgets/form/address.dart';
import '../../../widgets/form/birth_date.dart';
import '../../../widgets/form/city_drop_down.dart';
import '../../../widgets/form/email_address.dart';
import '../../../widgets/form/first_name.dart';
import '../../../widgets/form/gender.dart';
import '../../../widgets/form/last_name.dart';
import '../../../widgets/form/phone_number.dart';
import '../../../widgets/form/province_drop_down.dart';
import '../../../widgets/frame/frame_scaffold.dart';
import '../../../widgets/frame_view/frame_action_wrapper.dart';
import '../../../widgets/layouts/space_sizer.dart';
import '../../../widgets/text/montserrat_text_error.dart';
import '../../../widgets/text/montserrat_text_view.dart';
import '../../widgets/dialog_change_picture.dart';

class EditProfilePage extends StatelessWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 45.0,
        elevation: 0,
        avoidBottomInset: true,
        isUseLeading: false,
        isImplyLeading: false,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        action: FrameActionWrapper(
          title: PageNames.editProfilePage,
          fontWeight: FontWeight.bold,
          onPressed: () {
            final FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            Get.back();
          },
        ),
        view: GetBuilder<EditProfileController>(
            init: EditProfileController(pageContext: context),
            builder: (EditProfileController controller) {
              return Container(
                color: AppColors.darkBackground,
                padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(2.5),
                  right: SizeConfig.horizontal(2.5),
                  top: SizeConfig.vertical(1),
                ),
                child: SingleChildScrollView(
                  child: _formEditProfileWrapper(context, controller),
                ),
              );
            }),
      ),
    );
  }

  Widget _formEditProfileWrapper(
    BuildContext context,
    EditProfileController controller,
  ) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SizedBox(
            width: SizeConfig.screenWidth,
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              rowMainAxisAlignment: MainAxisAlignment.center,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: _imageMemberProfile(
                    context,
                    controller,
                  ),
                ),
                const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 5)),
                ResponsiveRowColumnItem(
                  child: Container(
                    width: SizeConfig.horizontal(40),
                    height: SizeConfig.vertical(5),
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColors.flatGold),
                      borderRadius: BorderRadius.circular(
                        SizeConfig.horizontal(4),
                      ),
                    ),
                    child: RippleButton(
                      onTap: () {
                        final FocusScopeNode currentFocus =
                            FocusScope.of(context);
                        if (!currentFocus.hasPrimaryFocus) {
                          currentFocus.unfocus();
                        }
                        return showDialog(
                          context: context,
                          barrierColor: Colors.transparent.withOpacity(0.1),
                          builder: (BuildContext context) {
                            return DialogChangePicture(
                              isDualScreen: controller.isDualScreen.value,
                              onPressedToOpenCamera: () =>
                                  controller.imgFromCameraProfile(context),
                              onPressedToOpenGallery: () =>
                                  controller.imgFromCameraGallery(context),
                            );
                          },
                        );
                      },
                      child: Center(
                        child: MontserratTextView(
                          value: 'Change Picture',
                          alignText: AlignTextType.center,
                          color: AppColors.flatGold,
                          size: SizeConfig.safeHorizontal(4),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: SizedBox(
            width: SizeConfig.screenWidth,
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: Expanded(child: _firstNameFormWrapper(controller)),
                ),
                const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 4)),
                ResponsiveRowColumnItem(
                  child: Expanded(child: _lastNameFormWrapper(controller)),
                ),
              ],
            ),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _phoneNumberFormWrapper(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _emailAddressFormWrapper(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(child: _genderFormWrapper(context, controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
        ResponsiveRowColumnItem(child: _birthOfDateFormWrapper(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
        ResponsiveRowColumnItem(child: _selectProvince(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 4)),
        ResponsiveRowColumnItem(child: _selectCity(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(child: _addressFormWrapper(controller)),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(
          child: Center(
            child: Obx(
              () => CustomFlatButton(
                width: SizeConfig.screenWidth,
                loading: controller.isLoading.value,
                onTap: () {
                  final FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  controller.onPressVerify();
                },
                text: 'edit_profile_button_label'.tr,
              ),
            ),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 7)),
      ],
    );
  }

  Widget _imageMemberProfile(
    BuildContext context,
    EditProfileController controller,
  ) {
    return RippleButton(
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        controller.openImageProfile(context);
      },
      child: Container(
        width: SizeConfig.horizontal(33),
        height: SizeConfig.horizontal(33),
        decoration: BoxDecoration(
          color: AppColors.whiteBackground,
          shape: BoxShape.circle,
        ),
        //! image profile in here
        child: Obx(() {
          if (controller.isImageFromFile.isFalse) {
            if (controller.profilePic.isEmpty) {
              return const SizedBox.shrink();
            } else {
              return ClipRRect(
                borderRadius: BorderRadius.circular(
                  SizeConfig.horizontal(33),
                ),
                child: CachedNetworkImage(
                  imageUrl: controller.profilePic.value,
                  fit: BoxFit.cover,
                  placeholder: (BuildContext context, String url) =>
                      const CircularProgressIndicator(),
                  errorWidget:
                      (BuildContext context, String url, dynamic error) =>
                          const SizedBox.shrink(),
                ),
              );
            }
          } else {
            // ignore: unnecessary_null_comparison
            if (controller.cameraImageProfile == null) {
              return const SizedBox.shrink();
            } else {
              return ClipRRect(
                borderRadius: BorderRadius.circular(
                  SizeConfig.horizontal(33),
                ),
                child: Image.file(
                  controller.cameraImageProfile!,
                  fit: BoxFit.cover,
                ),
              );
            }
          }
        }),
      ),
    );
  }

  Widget _firstNameFormWrapper(EditProfileController controller) {
    return Obx(
      () => FirstName(
        firstNameController: controller.firstName,
        errorText: controller.firstNameErrorMessage.value,
      ),
    );
  }

  Widget _lastNameFormWrapper(EditProfileController controller) {
    return Obx(
      () => LastName(
        lastNameController: controller.lastName,
        errorText: controller.lastNameErrorMessage.value,
      ),
    );
  }

  Widget _phoneNumberFormWrapper(EditProfileController controller) {
    return Obx(
      () => PhoneNumber(
        // phoneNumberController: _controller.phoneNumber,
        // hintText: "sign_in_hint_phone_number".tr,
        phoneNumber: controller.phoneNumber.value,
        defaultCountry: controller.defaultCountry,
        countryCode: controller.defaultCountryCallingCode,
        // errorText: _controller.phoneNumberErrorMessage.value,
        // isReadOnly: true,
      ),
    );
  }

  Widget _emailAddressFormWrapper(EditProfileController controller) {
    // log("error message ${controller.emailAddressErrorMessage.value}");
    return Obx(
      () => EmailAddress(
        emailAddressController: controller.emailAddress,
        errorText: controller.emailAddressErrorMessage.value,
        isReadOnly: false,
      ),
    );
  }

  Widget _genderFormWrapper(
    BuildContext context,
    EditProfileController controller,
  ) {
    return Obx(
      () => Gender(
        groupValue: controller.genderSelection.value,
        onChanged: (Object? value) {
          controller.onChangeGender(value, context);
        },
      ),
    );
  }

  Widget _birthOfDateFormWrapper(EditProfileController controller) {
    return Obx(
      () => BirthDate(
        onDateTimeChange: (DateTime value) {
          controller.onChangeDatePicker(value);
        },
        selectedDate: controller.selectedDate.value,
        isReadOnly: true,
      ),
    );
  }

  Widget _selectProvince(EditProfileController controller) {
    return Obx(
      () => ProvinceDropDown(
        // ignore: invalid_use_of_protected_member
        provinceList: controller.provinceList.value,
        onChanged: controller.onChangeProvince,
        valueDisplay: controller.provinceId.value,
      ),
    );
  }

  Widget _selectCity(EditProfileController controller) {
    return Obx(
      () => ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: CityDropDown(
              // ignore: invalid_use_of_protected_member
              cityList: controller.cityList.value,
              onChanged: controller.onChangeCity,
              valueDisplay: controller.cityId.value,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextError(
              value: controller.cityErrorMessage.value,
            ),
          ),
        ],
      ),
    );
  }

  Widget _addressFormWrapper(EditProfileController controller) {
    return Obx(
      () => Address(
        addressController: controller.userAddress,
        hintText: '',
        errorText: controller.addressErrorMessage.value,
      ),
    );
  }
}
