import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../../controllers/profile/personal_info/personal_info_controller.dart';
import '../../../../utils/app_colors.dart';
import '../../../../utils/assets_list.dart';
import '../../../../utils/size_config.dart';
import '../../../widgets/buttons/custom_flat_buttons.dart';
import '../../../widgets/buttons/montserrat_button.dart';
import '../../../widgets/frame/frame_scaffold.dart';
import '../../../widgets/frame_view/frame_action_wrapper.dart';
import '../../../widgets/layouts/space_sizer.dart';
import '../../../widgets/shimmers/personal_info_shimmer.dart';
import '../../../widgets/text/montserrat_text_view.dart';
import '../../widgets/member_card_profile.dart';

class PersonalInfoPage extends StatefulWidget {
  const PersonalInfoPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return PersonalInfoPageState();
  }
}

class PersonalInfoPageState extends State<PersonalInfoPage> {
  PersonalInfoController? controller;

  @override
  void initState() {
    super.initState();
    controller = Get.put(PersonalInfoController(pageContext: context));
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 45.0,
        elevation: 0,
        avoidBottomInset: true,
        isUseLeading: false,
        isImplyLeading: false,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        action: FrameActionWrapper(
          title: 'Personal Information',
          fontWeight: FontWeight.bold,
          onPressed: () {
            Get.back();
          },
        ),
        view: _content(),
      ),
    );
  }

  Widget _content() {
    return RefreshIndicator(
      triggerMode: controller!.isDualScreen.isTrue
          ? controller!.foldHinge.value <= 30
              ? RefreshIndicatorTriggerMode.onEdge
              : RefreshIndicatorTriggerMode.anywhere
          : RefreshIndicatorTriggerMode.anywhere,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(8)),
        child: _wrapper(context),
      ),
      onRefresh: () async {
        controller?.getMemberAccountInfo();
      },
    );
  }

  Widget _wrapper(BuildContext context) {
    return Obx(() {
      if (controller?.isLoading.isTrue ?? true) {
        return const PersonalInfoShimmer();
      } else {
        if (controller?.isDualScreen.isTrue ?? false) {
          return _foldWrapper(context);
        } else {
          return _contentWrapper(context);
        }
      }
    });
  }

  Widget _foldWrapper(BuildContext context) {
    if (controller!.foldHinge.value <= 30.0) {
      return _contentWrapper(context);
    } else {
      return _foldContent();
    }
  }

  Widget _foldContent() {
    return SingleChildScrollView(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MemberCardProfile(controller: controller!),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: CustomFlatButton(
              onTap: () {
                controller?.onPressNavigateToEditProfile(context);
              },
              text: 'Edit Personal Info',
              width: SizeConfig.screenWidth,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(child: _memberStatusButton()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
          ResponsiveRowColumnItem(
            child: MontserratButton(
              onTap: () {
                controller?.onPressNavigateDeleteMyAccount();
              },
              value: 'Delete My Account',
              weight: FontWeight.bold,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 6.5)),
        ],
      ),
    );
  }

  Widget _contentWrapper(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverFillRemaining(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Expanded(
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    columnMainAxisSize: MainAxisSize.min,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(
                        child: MemberCardProfile(controller: controller!),
                      ),
                      const ResponsiveRowColumnItem(
                        child: SpaceSizer(vertical: 1.5),
                      ),
                      ResponsiveRowColumnItem(
                        child: CustomFlatButton(
                          onTap: () {
                            controller?.onPressNavigateToEditProfile(context);
                          },
                          text: 'Edit Personal Info',
                          width: SizeConfig.screenWidth,
                        ),
                      ),
                      const ResponsiveRowColumnItem(
                        child: SpaceSizer(vertical: 1.5),
                      ),
                      ResponsiveRowColumnItem(child: _memberStatusButton()),
                    ],
                  ),
                ),
              ),
              ResponsiveRowColumnItem(
                child: MontserratButton(
                  onTap: () {
                    controller?.onPressNavigateDeleteMyAccount();
                  },
                  value: 'Delete My Account',
                  weight: FontWeight.bold,
                ),
              ),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 6.5)),
            ],
          ),
        ),
      ],
    );
  }

  Widget _memberStatusButton() {
    return Obx(() {
      if (controller?.memberVerified.value == 0 &&
          controller?.memberVerifiedRequest.value == 0) {
        return CustomFlatButton(
          onTap: () {
            controller?.imgFromCameraIdCard();
          },
          text: 'account_unverified_label'.tr,
          width: SizeConfig.screenWidth,
        );
      } else if (controller?.memberVerified.value == 0 &&
          controller?.memberVerifiedRequest.value == 1) {
        return Container(
          height: SizeConfig.vertical(5),
          width: SizeConfig.screenWidth,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            color: AppColors.greyDisabled,
          ),
          child: MontserratTextView(
            value: 'account_verifying_label'.tr,
            fontWeight: FontWeight.w700,
            size: SizeConfig.safeBlockHorizontal * 4.5,
          ),
        );
      } else {
        return Container(
          height: SizeConfig.vertical(5),
          width: SizeConfig.screenWidth,
          alignment: Alignment.center,
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            rowMainAxisAlignment: MainAxisAlignment.center,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Image.asset(
                  Assets.iconSuccess,
                  color: AppColors.whiteBackground,
                  width: SizeConfig.horizontal(5),
                  height: SizeConfig.horizontal(5),
                ),
              ),
              const ResponsiveRowColumnItem(
                child: SpaceSizer(
                  horizontal: 2,
                ),
              ),
              ResponsiveRowColumnItem(
                child: MontserratTextView(
                  value: 'account_verified_label'.tr,
                  fontWeight: FontWeight.w400,
                  size: SizeConfig.safeBlockHorizontal * 4.5,
                  color: AppColors.whiteBackground,
                ),
              ),
            ],
          ),
        );
      }
    });
  }
}
