import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/profile/profile_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/profile_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';
import '../widgets/dialog_qr_code.dart';
import '../widgets/member_button_change_device.dart';
import '../widgets/my_account_detail.dart';
import '../widgets/my_membership_detail.dart';

class ProfilePage extends StatefulWidget {
  // constructor
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProfileViewState();
  }
}

class ProfileViewState extends State<ProfilePage> {
  // ini profile controller
  ProfileController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return RefreshIndicator(
      // triggerMode: RefreshIndicatorTriggerMode.anywhere,
      child: Container(
        color: AppColors.basicDark,
        child: _obxWrapper(context),
      ),
      onRefresh: () async {
        controller.isPullDownRequest.value = true;
        await controller.onRefreshData();
      },
    );
  }

  Widget _obxWrapper(BuildContext context) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const ProfileShimmer();
      } else {
        return _contentWrapper(context);
      }
    });
  }

  Widget _contentWrapper(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(8)),
      child: Align(
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
            ResponsiveRowColumnItem(child: _qrCodeButtonWrapper(context)),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
            ResponsiveRowColumnItem(
              child: MyMembershipdDetail(controller: controller),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
            ResponsiveRowColumnItem(
              child: MyAccountDetail(controller: controller),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
            ResponsiveRowColumnItem(
              child: _memberButtonChangeDeviceWrapper(context),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
            ResponsiveRowColumnItem(child: _statusDevice()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
            ResponsiveRowColumnItem(child: _logoutButtonWrapper()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
            ResponsiveRowColumnItem(
              child: Obx(
                () => _versionNumber(controller.versionApp.value ?? ''),
              ),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 20)),
          ],
        ),
      ),
    );
  }

  Widget _qrCodeButtonWrapper(BuildContext context) {
    return CustomFlatButton(
      width: SizeConfig.screenWidth,
      textSize: 4.5,
      text: 'View QR Code',
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext dialogContext) {
            return Obx(
              () => DialogQRCode(
                loading: controller.isLoadingPersonalInfo.value,
                qrData: controller.memberPhoneNumber.value,
                isDualScreen: controller.isDualScreen.value,
                onRequest: () async {
                  await controller.getMemberAccountInfo();
                },
              ),
            );
          },
        );
      },
    );
  }

  Widget _memberButtonChangeDeviceWrapper(BuildContext context) {
    return Obx(
      () => MemberButtonChangeDevice(
        width: SizeConfig.screenWidth,
        textSize: 4.5,
        status: controller.getStatusMyDevice(),
        onTap: () {
          controller.popUpRemoveDevice(context);
        },
      ),
    );
  }

  Widget _statusDevice() {
    return Obx(() {
      if (controller.getStatusMyDevice()) {
        return MontserratTextView(
          value: 'Your request is in progress',
          size: SizeConfig.safeHorizontal(4.5),
          color: AppColors.whiteBackground,
        );
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _logoutButtonWrapper() {
    return CustomFlatButton(
      onTap: () {
        Future<void>.microtask(() {
          setState(() {});
        }).then((_) {
          controller.popUpLogout(context);
        });
      },
      width: SizeConfig.screenWidth,
      textSize: 4.5,
      text: 'Logout',
    );
  }

  Widget _versionNumber(String version) {
    return MontserratTextView(
      value: 'Version $version',
      color: AppColors.darkGold,
      size: SizeConfig.safeHorizontal(3.5),
    );
  }
}
