import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/profile/terms_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/desc_shimmer.dart';

class TermsView extends StatelessWidget {
  const TermsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: GetBuilder<TermsController>(
          init: TermsController(pageContext: context),
          builder: (TermsController controller) {
            return FrameScaffold(
              heightBar: 65.0,
              elevation: 0,
              avoidBottomInset: true,
              isUseLeading: false,
              isImplyLeading: false,
              color: Platform.isIOS ? AppColors.darkBackground : null,
              statusBarColor: AppColors.darkBackground,
              statusBarIconBrightness: Brightness.light,
              statusBarBrightness: Brightness.dark,
              colorScaffold: AppColors.darkBackground,
              action: FrameActionWrapper(
                title: controller.getTitle(),
                fontWeight: FontWeight.bold,
                onPressed: () {
                  Get.back();
                },
              ),
              view: Container(
                color: AppColors.darkBackground,
                padding: EdgeInsets.only(
                  left: SizeConfig.horizontal(2.5),
                  right: SizeConfig.horizontal(2.5),
                  // top: SizeConfig.vertical(1),
                ),
                child: Obx(() {
                  if (controller.isLoading.isTrue) {
                    // return Container();
                    return const DescShimmer();
                  } else {
                    return SingleChildScrollView(
                      child: ResponsiveRowColumn(
                        layout: ResponsiveRowColumnType.COLUMN,
                        children: <ResponsiveRowColumnItem>[
                          ResponsiveRowColumnItem(
                            child: Html(
                              data: controller.termsData,
                              style: <String, Style>{
                                'h1': Style(color: AppColors.whiteBackground),
                                'h2': Style(color: AppColors.whiteBackground),
                                'h3': Style(color: AppColors.whiteBackground),
                                'p': Style(color: AppColors.whiteBackground),
                              },
                            ),
                          ),
                          const ResponsiveRowColumnItem(
                            child: SpaceSizer(vertical: 5),
                          ),
                        ],
                      ),
                    );
                  }
                }),
              ),
            );
          }),
    );
  }
}
