import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/text/montserrat_text_view.dart';

class CustomGoldCardWidget extends StatelessWidget {
  const CustomGoldCardWidget({
    Key? key,
    this.width,
    this.height,
    this.child,
  }) : super(key: key);

  final double? width;
  final double? height;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: width == null ? null : SizeConfig.horizontal(width!),
      height: height == null ? null : SizeConfig.vertical(height!),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
        color: AppColors.flatGold,
        border: Border.all(width: 0, color: AppColors.flatGold),
      ),
      child: child,
    );
  }
}

// custom wrapper monserrat
Widget montserratWrapper({required String value, Color? color}) {
  return Flexible(
    child: MontserratTextView(
      value: value,
      fontWeight: FontWeight.w300,
      size: SizeConfig.safeHorizontal(3.5),
      color: color ?? AppColors.basicDark,
    ),
  );
}

// custom detail Item
Widget detailItem({
  required String iconName,
  required Widget child,
  Color? tintColor,
  bool? isAddSpaceLeft = true,
  bool? isAddSpaceRight = true,
}) {
  return ResponsiveRowColumn(
    layout: ResponsiveRowColumnType.ROW,
    children: <ResponsiveRowColumnItem>[
      ResponsiveRowColumnItem(
        child: SizedBox(
          // ignore: use_if_null_to_convert_nulls_to_bools
          width: SizeConfig.horizontal(isAddSpaceLeft == true ? 4 : 0),
        ),
      ),
      ResponsiveRowColumnItem(
        child: Image.asset(
          iconName,
          width: SizeConfig.horizontal(6),
          height: SizeConfig.horizontal(6),
          fit: BoxFit.cover,
          color: tintColor,
        ),
      ),
      ResponsiveRowColumnItem(
        child: SizedBox(
          // ignore: use_if_null_to_convert_nulls_to_bools
          width: SizeConfig.horizontal(isAddSpaceRight == true ? 4 : 0),
        ),
      ),
      ResponsiveRowColumnItem(child: child),
    ],
  );
}
