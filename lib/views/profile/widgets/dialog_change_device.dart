import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../models/data/reasons/reason_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/layouts/under_line.dart';
import '../../widgets/text/montserrat_text_view.dart';

class DialogChangeDevice extends StatelessWidget {
  const DialogChangeDevice({
    Key? key,
    required this.onPressedYesDialog,
    required this.onPressedNoDialog,
    this.loading = false,
    this.isDualScreen = false,
    required this.isManualInput,
    required this.reasonInputController,
    this.reasonSelection,
    required this.onChanged,
    required this.reasonList,
  }) : super(key: key);

  final VoidCallback onPressedYesDialog;
  final VoidCallback onPressedNoDialog;
  final bool loading;
  final bool isDualScreen;
  final bool isManualInput;
  final TextEditingController reasonInputController;
  final String? reasonSelection;
  final Function(int?) onChanged;
  final List<ReasonDataSelectModel> reasonList;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal(context);
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      resizeToAvoidBottomInset: true,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(80),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogNormal(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      content: _dialogWrapper(context),
    );
  }

  Widget _dialogWrapper(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(
            vertical: SizeConfig.vertical(1.5),
            horizontal: SizeConfig.horizontal(2.5),
          ),
          decoration: _dialogDecor(),
          child: _dialogContent(context),
        ),
      ],
    );
  }

  Widget _dialogContent(BuildContext context) {
    return Column(
      children: <Widget>[
        Image.asset(Assets.iconNotify, height: SizeConfig.horizontal(15)),
        const SpaceSizer(vertical: 1),
        _changeDeviceLabel(),
        const SpaceSizer(vertical: 1),
        _reasonButton(context),
        const SpaceSizer(vertical: 1),
        _reasonTextField(),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        _quetionLabel(),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        _reminderLabel(),
        const SpaceSizer(vertical: 1),
        _noButton(context),
        const SpaceSizer(vertical: 1),
        _yesButton(),
      ],
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
          // AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _changeDeviceLabel() {
    return MontserratTextView(
      value: 'Change Device',
      alignText: AlignTextType.center,
      color: AppColors.darkBackground,
      size: SizeConfig.safeHorizontal(4),
      fontWeight: FontWeight.w700,
    );
  }

  Widget _quetionLabel() {
    return MontserratTextView(
      value: 'Are you sure you want to reset your device?',
      alignText: AlignTextType.center,
      color: AppColors.darkBackground,
      size: SizeConfig.safeHorizontal(3),
    );
  }

  Widget _reminderLabel() {
    return MontserratTextView(
      value:
          ' Once approved, the account on this device will be logged out and you can login on any device.',
      alignText: AlignTextType.center,
      color: AppColors.darkBackground,
      size: SizeConfig.safeHorizontal(3),
    );
  }

  Widget _reasonButton(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Reason',
            color: AppColors.darkBackground,
            fontWeight: FontWeight.w700,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(SizeConfig.horizontal(3)),
                  ),
                  border: Border.all(color: AppColors.darkBackground),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.horizontal(1),
                  vertical: SizeConfig.vertical(1),
                ),
                child: ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.ROW,
                  rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(
                      child: Flexible(
                        child: MontserratTextView(
                          value: reasonSelection ?? 'Please select',
                          color: AppColors.darkBackground,
                          fontWeight: FontWeight.w300,
                          size: SizeConfig.horizontal(3),
                        ),
                      ),
                    ),
                    const ResponsiveRowColumnItem(
                      child: Icon(Icons.arrow_drop_down),
                    ),
                  ],
                ),
              ),
              DropdownButtonHideUnderline(
                child: DropdownButton<int>(
                  isDense: true,
                  isExpanded: true,
                  dropdownColor: AppColors.darkBackground,
                  iconSize: 0,
                  borderRadius: BorderRadius.all(
                    Radius.circular(SizeConfig.horizontal(3)),
                  ),
                  // menuMaxHeight: SizeConfig.vertical(20),
                  items: reasonList.map((ReasonDataSelectModel item) {
                    return DropdownMenuItem<int>(
                      value: item.id,
                      child: MontserratTextView(
                        value: item.title,
                        color: AppColors.flatGold,
                        fontWeight: FontWeight.w300,
                        size: SizeConfig.horizontal(3.5),
                      ),
                    );
                  }).toList(),
                  onChanged: onChanged,
                  onTap: () {
                    final FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _reasonTextField() {
    if (isManualInput) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Type your reason',
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w700,
              size: SizeConfig.safeHorizontal(3),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(SizeConfig.horizontal(3)),
                ),
                border: Border.all(color: AppColors.darkBackground),
              ),
              child: TextField(
                controller: reasonInputController,
                cursorColor: AppColors.darkBackground,
                keyboardType: TextInputType.name,
                textAlign: TextAlign.left,
                cursorHeight: 14,
                style: GoogleFonts.montserrat(
                  color: AppColors.darkBackground,
                  fontSize: SizeConfig.safeHorizontal(3),
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w300,
                ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.horizontal(1),
                    vertical: SizeConfig.vertical(1),
                  ),
                  isDense: true,
                  border: InputBorder.none,
                  hintText: 'Type here..',
                  hintStyle: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: SizeConfig.safeHorizontal(3),
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: Underline()),
          // ResponsiveRowColumnItem(child: MontserratTextError(value: '')),
        ],
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget _noButton(BuildContext context) {
    return CustomFlatButton(
      onTap: () {
        onPressedNoDialog();
      },
      text: 'No',
      textSize: 3.5,
      height: isDualScreen ? SizeConfig.horizontal(12) : null,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }

  Widget _yesButton() {
    return Container(
      width: SizeConfig.horizontal(80),
      height: isDualScreen ? SizeConfig.horizontal(12) : SizeConfig.vertical(5),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.flatGold),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
        color: _confirmButtonColor(),
      ),
      child: _confirmButtonContent(),
    );
  }

  Color _confirmButtonColor() {
    if (reasonSelection != null) {
      if (loading) {
        return AppColors.greyDisabled;
      } else {
        return Colors.transparent;
      }
    } else {
      return AppColors.greyDisabled;
    }
  }

  Widget _confirmButtonContent() {
    if (reasonSelection != null) {
      return RippleButton(
        onTap: () {
          if (loading) {
            return;
          } else {
            onPressedYesDialog();
          }
        },
        child: SizedBox(
          width: SizeConfig.horizontal(80),
          height:
              isDualScreen ? SizeConfig.horizontal(12) : SizeConfig.vertical(5),
          child: Center(
            child: loading
                ? SizedBox(
                    width: SizeConfig.horizontal(5),
                    height: SizeConfig.horizontal(5),
                    child: CircularProgressIndicator(
                        color: AppColors.whiteSkeleton),
                  )
                : MontserratTextView(
                    value: 'Yes',
                    alignText: AlignTextType.center,
                    color: AppColors.whiteBackground,
                    size: SizeConfig.safeHorizontal(3.5),
                    fontWeight: FontWeight.w700,
                  ),
          ),
        ),
      );
    } else {
      return SizedBox(
        width: SizeConfig.horizontal(80),
        height:
            isDualScreen ? SizeConfig.horizontal(12) : SizeConfig.vertical(5),
        child: Center(
          child: MontserratTextView(
            value: 'Yes',
            alignText: AlignTextType.center,
            color: AppColors.whiteBackground,
            size: SizeConfig.safeHorizontal(3.5),
            fontWeight: FontWeight.w700,
          ),
        ),
      );
    }
  }
}
