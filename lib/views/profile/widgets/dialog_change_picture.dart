import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class DialogChangePicture extends StatelessWidget {
  const DialogChangePicture({
    Key? key,
    required this.onPressedToOpenCamera,
    required this.onPressedToOpenGallery,
    this.loading = false,
    this.isDualScreen = false,
  }) : super(key: key);

  final VoidCallback onPressedToOpenCamera;
  final VoidCallback onPressedToOpenGallery;
  final bool loading;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal();
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper() {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _dialogWrapper(),
      ),
    );
  }

  Widget _dialogNormal() {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: _dialogWrapper(),
    );
  }

  Widget _dialogWrapper() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.vertical(3),
        horizontal: SizeConfig.horizontal(5),
      ),
      decoration: _dialogDecor(),
      child: _dialogContent(),
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
          // AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _dialogContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _optionLabel(),
        const SpaceSizer(vertical: 3),
        _openCameraButton(),
        const SpaceSizer(vertical: 1),
        _openGallery(),
      ],
    );
  }

  Widget _optionLabel() {
    return MontserratTextView(
      value: 'Choose option',
      alignText: AlignTextType.center,
      size: SizeConfig.safeHorizontal(6),
      fontWeight: FontWeight.w300,
    );
  }

  Widget _openCameraButton() {
    return CustomFlatButton(
      onTap: () => onPressedToOpenCamera(),
      text: 'Open Camera',
      loading: loading,
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }

  Widget _openGallery() {
    return CustomFlatButton(
      onTap: () => onPressedToOpenGallery(),
      text: 'Open Gallery',
      loading: loading,
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }
}
