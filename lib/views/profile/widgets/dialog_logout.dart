import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class DialogLogout extends StatelessWidget {
  const DialogLogout({
    Key? key,
    required this.onPressedInDialog,
    this.isDualScreen = false,
  }) : super(key: key);

  final VoidCallback onPressedInDialog;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal(context);
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogNormal(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: _dialogWrapper(context),
    );
  }

  Widget _dialogWrapper(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(
            vertical: SizeConfig.vertical(3),
            horizontal: SizeConfig.horizontal(5),
          ),
          decoration: _dialogDecor(),
          child: _dialogContent(context),
        ),
      ],
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
          // AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _dialogContent(BuildContext context) {
    return Column(
      children: <Widget>[
        Image.asset(Assets.iconSuccess, height: SizeConfig.vertical(10)),
        const SpaceSizer(vertical: 2),
        _quetionLabel(),
        const SpaceSizer(vertical: 3),
        _yesButtton(),
        const SpaceSizer(vertical: 1),
        _noButton(context),
      ],
    );
  }

  Widget _quetionLabel() {
    return MontserratTextView(
      value: 'Are you sure you want to log out?',
      alignText: AlignTextType.center,
      color: AppColors.darkBackground,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _yesButtton() {
    return CustomFlatButton(
      onTap: () {
        onPressedInDialog();
      },
      text: 'Yes, I want to log out',
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }

  Widget _noButton(BuildContext context) {
    return CustomFlatButton(
      onTap: () {
        Navigator.of(context).pop();
      },
      text: 'No',
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }
}
