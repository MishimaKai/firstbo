import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/layouts/space_sizer.dart';

class DialogMemberBenefit extends StatelessWidget {
  // constructor
  const DialogMemberBenefit({
    Key? key,
    required this.benefit,
    required this.tnc,
    this.isDualScreen = false,
  }) : super(key: key);
  // parameter
  final String benefit;
  final String tnc;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal(context);
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogNormal(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: const EdgeInsets.fromLTRB(10, 20.0, 10, 24.0),
      content: _dialogWrapper(context),
    );
  }

  Widget _dialogWrapper(BuildContext context) {
    return Container(
      height: SizeConfig.vertical(50),
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(1.5)),
      decoration: _dialogDecor(),
      child: CustomScrollView(slivers: <Widget>[_sliverFillWrapper(context)]),
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _sliverFillWrapper(BuildContext context) {
    return SliverFillRemaining(
      hasScrollBody: false,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _benefitContent()),
          ResponsiveRowColumnItem(child: _tncContent()),
          ResponsiveRowColumnItem(child: Expanded(child: Container())),
          ResponsiveRowColumnItem(child: _closeButton(context)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ],
      ),
    );
  }

  Widget _benefitContent() {
    return Html(
      data: benefit,
      style: <String, Style>{
        'h1': Style(color: AppColors.darkBackground),
        'h2': Style(color: AppColors.darkBackground),
        'h3': Style(color: AppColors.darkBackground),
        'p3': Style(color: AppColors.darkBackground),
      },
    );
  }

  Widget _tncContent() {
    return Html(
      data: tnc,
      style: <String, Style>{
        'h1': Style(color: AppColors.darkBackground),
        'h2': Style(color: AppColors.darkBackground),
        'h3': Style(color: AppColors.darkBackground),
        'p3': Style(color: AppColors.darkBackground),
      },
    );
  }

  Widget _closeButton(BuildContext context) {
    return CustomFlatButton(
      onTap: () {
        Navigator.of(context).pop();
      },
      text: 'Close',
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }
}
