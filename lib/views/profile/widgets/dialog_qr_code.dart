import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';
import '../../widgets/layouts/space_sizer.dart';

class DialogQRCode extends StatefulWidget {
// constructor
  const DialogQRCode(
      {Key? key,
      required this.loading,
      required this.qrData,
      this.isDualScreen = false,
      required this.onRequest})
      : super(key: key);
  // parameter
  final bool loading;
  final String qrData;
  final bool isDualScreen;
  final Function() onRequest;

  @override
  State<StatefulWidget> createState() => DialogQRCodeState();
}

class DialogQRCodeState extends State<DialogQRCode> {
  @override
  void initState() {
    super.initState();
    if (widget.qrData.isEmpty) {
      widget.onRequest();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (widget.isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal(context);
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogNormal(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _dialogWrapper(context),
      ),
    );
  }

  Widget _dialogWrapper(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: SizeConfig.vertical(3),
              horizontal: SizeConfig.horizontal(2),
            ),
            decoration: _dialogDecor(),
            child: _dialogContent(context),
          ),
        ),
      ],
    );
  }

  Widget _dialogContent(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: widget.loading
              ? SizedBox(
                  height: SizeConfig.vertical(20),
                  child: Center(
                    child: CircularProgressIndicator(
                      color: AppColors.darkBackground,
                    ),
                  ))
              : ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.COLUMN,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(child: _qrImageWrapper()),
                    const ResponsiveRowColumnItem(
                        child: SpaceSizer(vertical: 1.5)),
                    ResponsiveRowColumnItem(
                      child: Text(
                        widget.qrData,
                        style: GoogleFonts.montserrat(
                          fontWeight: FontWeight.w700,
                          fontSize: SizeConfig.safeHorizontal(3.5),
                          color: AppColors.darkBackground,
                        ),
                      ),
                    ),
                  ],
                ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
        ResponsiveRowColumnItem(child: _closeButton(context)),
      ],
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _qrImageWrapper() {
    return QrImage(
      data: widget.qrData,
      backgroundColor: AppColors.flatGold,
      foregroundColor: AppColors.darkBackground,
      size: SizeConfig.horizontal(50),
    );
  }

  Widget _closeButton(BuildContext context) {
    return CustomFlatButton(
      onTap: () {
        Navigator.of(context).pop();
      },
      text: 'Close',
      textSize: 3.5,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }
}
