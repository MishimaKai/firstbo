import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../helpers/camera_service.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/montserrat_button.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class IDCardPickerPage extends StatefulWidget {
  const IDCardPickerPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => IDCardPickerPageState();
}

class IDCardPickerPageState extends State<IDCardPickerPage>
    with WidgetsBindingObserver {
  // Timer? onCheckImage;

  bool isTakePicture = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    CameraService.onInitCamera();
    CameraService.cameraController?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });

    // onCheckImage = Timer.periodic(const Duration(milliseconds: 500), (timer) {
    //   if (CameraService.captureId != null) {
    //     // log("TEST IMAGE = ${CameraService.captureId}");
    //     Get.back(result: {"image": CameraService.captureId});
    //     timer.cancel();
    //   }
    // });
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    // onCheckImage?.cancel();
    CameraService.onDisposeCamera();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    CameraService.lifeCycleHandler(state);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      extendBodyBehindAppBar: true,
      action: FrameActionWrapper(
        title: 'Verify ID Card',
        fontWeight: FontWeight.bold,
        onPressed: () {
          Get.back();
        },
      ),
      view: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          systemNavigationBarColor: AppColors.darkBackground,
          systemNavigationBarIconBrightness: Brightness.light,
        ),
        child: Container(
          color: AppColors.darkBackground,
          margin: EdgeInsets.only(top: AppBar().preferredSize.height + 8),
          child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.center,
            children: <Widget>[
              CameraService.onLaunchCamera(),
              _overlayOnTake(),
              _mainWrapper(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _overlayOnTake() {
    if (isTakePicture) {
      return ColorFiltered(
        colorFilter: ColorFilter.mode(
          AppColors.darkBackground.withOpacity(0.95),
          BlendMode.srcOut,
        ),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                color: Colors.black,
                backgroundBlendMode: BlendMode.dstOut,
              ),
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                columnMainAxisAlignment: MainAxisAlignment.center,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: Align(
                      child: Container(
                        width: SizeConfig.horizontal(85),
                        height: SizeConfig.vertical(27),
                        color: Colors
                            .red, // Color does not matter but should not be transparent
                      ),
                    ),
                  ),
                  const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 27))
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget _mainWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _toggleFlashWrapper()),
        ResponsiveRowColumnItem(
          child: Container(
            width: SizeConfig.horizontal(85),
            height: SizeConfig.vertical(27),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(
                width: SizeConfig.horizontal(1.5),
                color: AppColors.whiteBackground,
              ),
            ),
          ),
        ),
        ResponsiveRowColumnItem(child: _bottomCameraControlWrapper()),
      ],
    );
  }

  Widget _toggleFlashWrapper() {
    if (isTakePicture) {
      return Container(
        height: SizeConfig.vertical(8),
        color: AppColors.darkBackground,
      );
    } else {
      return Container(
        alignment: Alignment.centerLeft,
        color: AppColors.darkBackground,
        height: SizeConfig.vertical(8),
        padding: EdgeInsets.only(left: SizeConfig.horizontal(5)),
        child: RippleButton(
          onTap: () {
            CameraService.toggleFlashCamera();
          },
          splashColor: AppColors.rippleColorDark,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2),
              vertical: SizeConfig.vertical(1),
            ),
            child: Image.asset(
              Assets.iconFlash,
              width: SizeConfig.horizontal(8),
              height: SizeConfig.horizontal(8),
            ),
          ),
        ),
      );
    }
  }

  Widget _bottomCameraControlWrapper() {
    if (isTakePicture) {
      return Container(
        margin: EdgeInsets.only(top: SizeConfig.vertical(12)),
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(10)),
        height: SizeConfig.vertical(23),
        color: AppColors.darkBackground,
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.ROW,
          rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: MontserratButton(
                onTap: () {
                  setState(() {
                    isTakePicture = false;
                  });
                  CameraService.onHoldPreview(isTakePicture);
                },
                value: 'Retry',
                isUnderLine: false,
                isUseRipple: true,
              ),
            ),
            ResponsiveRowColumnItem(
              child: MontserratButton(
                onTap: () {
                  // log("TEST IMAGE = ${CameraService.captureId}");
                  Get.back(result: <String, File?>{
                    'image': CameraService.captureId
                  });
                },
                value: 'Submit',
                customPadding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.horizontal(2),
                  vertical: SizeConfig.vertical(1),
                ),
                isUnderLine: false,
                isUseRipple: true,
              ),
            ),
          ],
        ),
      );
    } else {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(5),
              ),
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(5),
                vertical: SizeConfig.vertical(2),
              ),
              decoration: BoxDecoration(
                color: AppColors.flatGold,
                borderRadius: BorderRadius.all(
                  Radius.circular(SizeConfig.horizontal(3)),
                ),
              ),
              child: MontserratTextView(
                alignText: AlignTextType.left,
                size: SizeConfig.safeHorizontal(4),
                fontWeight: FontWeight.w400,
                value:
                    'Pastikan seluruh bagian e-KTP kamu berada dalam bingkai.',
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
          ResponsiveRowColumnItem(
            child: Container(
              height: SizeConfig.vertical(23),
              color: AppColors.darkBackground,
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(5),
                vertical: SizeConfig.vertical(2),
              ),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  _buttonControl(),
                  _buttonControlCenter(),
                ],
              ),
            ),
          ),
        ],
      );
    }
  }

  Widget _buttonControl() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnMainAxisAlignment: MainAxisAlignment.center,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: _cancelButtonWrapper(),
              ),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
            ],
          ),
        ),
      ],
    );
  }

  Widget _cancelButtonWrapper() {
    return MontserratButton(
      onTap: () {
        Get.back();
      },
      value: 'Cancel',
      isUnderLine: false,
      isUseRipple: true,
    );
  }

  Widget _buttonControlCenter() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _cameraLabel()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(child: _buttonCenter()),
      ],
    );
  }

  Widget _cameraLabel() {
    return MontserratTextView(
      value: 'PHOTO',
      color: AppColors.flatGold,
      fontWeight: FontWeight.w300,
      size: SizeConfig.safeHorizontal(3.5),
      alignText: AlignTextType.center,
    );
  }

  Widget _buttonCenter() {
    return RippleButton(
      onTap: () async {
        await CameraService.takePictureFromCamera();
        setState(() {
          isTakePicture = true;
        });
        CameraService.onHoldPreview(isTakePicture);
      },
      child: Container(
        width: SizeConfig.horizontal(18),
        height: SizeConfig.horizontal(18),
        decoration: BoxDecoration(
          color: AppColors.whiteBackground,
          shape: BoxShape.circle,
        ),
        child: Container(
          width: SizeConfig.horizontal(15),
          height: SizeConfig.horizontal(15),
          decoration: BoxDecoration(
            color: AppColors.darkBackground,
            shape: BoxShape.circle,
          ),
          child: Container(
            width: SizeConfig.horizontal(14),
            height: SizeConfig.horizontal(14),
            decoration: BoxDecoration(
              color: AppColors.whiteBackground,
              shape: BoxShape.circle,
            ),
          ),
        ),
      ),
    );
  }
}
