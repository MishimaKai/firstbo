import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/text/montserrat_text_view.dart';
import 'custom_gold_card_widget.dart';

class MemberBenefitProfile extends StatelessWidget {
  const MemberBenefitProfile({
    required this.benefitList,
    Key? key,
  }) : super(key: key);

  final List<String?> benefitList;

  bool _onCheckListEmpty() {
    bool isEmpty = false;
    // default list with empty string on array
    if (benefitList.length == 1) {
      if (benefitList[0]!.isEmpty) {
        isEmpty = true;
      } else {
        isEmpty = false;
      }
    } else {
      // if list more then 1 or empty
      if (benefitList.isNotEmpty) {
        isEmpty = false;
      } else {
        isEmpty = true;
      }
    }

    return isEmpty;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Obx(() {
      if (_onCheckListEmpty() == false) {
        return CustomGoldCardWidget(
          width: 85,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(5),
              vertical: SizeConfig.vertical(2),
            ),
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              columnCrossAxisAlignment: CrossAxisAlignment.start,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: 'Member Benefit',
                    fontWeight: FontWeight.bold,
                    size: SizeConfig.safeHorizontal(4),
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: SizedBox(height: SizeConfig.vertical(1)),
                ),
                ResponsiveRowColumnItem(
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.COLUMN,
                    children: List<ResponsiveRowColumnItem>.generate(
                      benefitList.length,
                      (int index) {
                        return ResponsiveRowColumnItem(
                          child: _benefitItemWrapper(benefitList[index]!),
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _benefitItemWrapper(String benefit) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _benefitItem(benefit)),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(1)),
        ),
      ],
    );
  }

  Widget _benefitItem(String benefit) {
    return Padding(
      padding: EdgeInsets.only(left: SizeConfig.horizontal(2)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Container(
              height: SizeConfig.horizontal(1),
              width: SizeConfig.horizontal(1),
              decoration: BoxDecoration(
                color: AppColors.basicDark,
                shape: BoxShape.circle,
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            child: SizedBox(
              width: SizeConfig.horizontal(2),
            ),
          ),
          ResponsiveRowColumnItem(
            child: montserratWrapper(value: benefit),
          ),
        ],
      ),
    );
  }
}
