import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';

class MemberButtonChangeDevice extends StatelessWidget {
  const MemberButtonChangeDevice({
    required this.status,
    required this.onTap,
    this.width,
    this.textSize,
    Key? key,
  }) : super(key: key);

  final bool status;
  final void Function() onTap;
  final double? width;
  final double? textSize;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      child: CustomFlatButton(
        loading: status,
        width: width ?? SizeConfig.screenWidth,
        onTap: onTap,
        text: 'Change Device',
        textSize: textSize,
      ),
    );
  }
}
