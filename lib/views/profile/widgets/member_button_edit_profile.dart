import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../../widgets/buttons/custom_flat_buttons.dart';

class MemberButtonLogout extends StatelessWidget {
  const MemberButtonLogout({
    required this.onTap,
    Key? key,
  }) : super(key: key);

  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      child: CustomFlatButton(
        width: SizeConfig.screenWidth,
        onTap: onTap,
        text: 'Logout',
      ),
    );
  }
}
