import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/profile/personal_info/personal_info_controller.dart';
import '../../../helpers/date_helper.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/text/montserrat_text_view.dart';
import 'custom_gold_card_widget.dart';

class MemberCardProfile extends StatelessWidget {
  const MemberCardProfile({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final PersonalInfoController controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Align(
      alignment: Alignment.topCenter,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _imageMemberProfile(context)),
          ResponsiveRowColumnItem(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(SizeConfig.horizontal(2)),
                  bottomRight: Radius.circular(SizeConfig.horizontal(2)),
                ),
                color: AppColors.flatGold,
                border: Border.all(width: 0, color: AppColors.flatGold),
              ),
              child: _memberCardDetail(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _imageMemberProfile(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          height: SizeConfig.horizontal(45),
          decoration: BoxDecoration(color: AppColors.darkBackground),
        ),
        Container(
          height: SizeConfig.horizontal(20),
          width: SizeConfig.screenWidth,
          decoration: BoxDecoration(
            color: AppColors.flatGold,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(SizeConfig.horizontal(2)),
              topRight: Radius.circular(SizeConfig.horizontal(2)),
            ),
            border: Border.all(width: 0, color: AppColors.flatGold),
          ),
        ),
        Container(
          width: SizeConfig.horizontal(42),
          height: SizeConfig.horizontal(42),
          decoration: BoxDecoration(
            color: AppColors.darkBackground,
            shape: BoxShape.circle,
          ),
        ),
        _positionedImage(context),
      ],
    );
  }

  Widget _positionedImage(BuildContext context) {
    return Positioned(
      bottom: 6,
      child: RippleButton(
        onTap: () {
          controller.openImageProfile(context);
        },
        child: Container(
          width: SizeConfig.horizontal(38),
          height: SizeConfig.horizontal(38),
          decoration: BoxDecoration(
            color: AppColors.whiteBackground,
            shape: BoxShape.circle,
          ),
          //! image profile in here
          child: _imageProfileWrapper(),
        ),
      ),
    );
  }

  Widget _imageProfileWrapper() {
    return Obx(() {
      if (controller.isImageFromFile.value == false) {
        if (controller.profilePicture.isNotEmpty) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(
              SizeConfig.horizontal(38),
            ),
            child: CachedNetworkImage(
              imageUrl: controller.profilePicture.value,
              fit: BoxFit.cover,
              placeholder: (BuildContext context, String url) =>
                  const CircularProgressIndicator(),
              errorWidget: (BuildContext context, String url, dynamic error) =>
                  const SizedBox.shrink(),
            ),
          );
        } else {
          return const SizedBox.shrink();
        }
      } else {
        return ClipRRect(
          borderRadius: BorderRadius.circular(SizeConfig.horizontal(38)),
          child: Image.file(
            controller.cameraImageProfile!,
            fit: BoxFit.cover,
          ),
        );
      }
    });
  }

  Widget _memberCardDetail() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconPhone,
            tintColor: AppColors.darkBackground,
            child: _phoneNumber(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconProfile,
            tintColor: AppColors.darkBackground,
            child: _nameInfo(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconGender,
            tintColor: AppColors.darkBackground,
            child: _genderInfo(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconMail,
            tintColor: AppColors.darkBackground,
            child: _emailInfo(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconBirth,
            tintColor: AppColors.darkBackground,
            child: _birthInfo(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
        ResponsiveRowColumnItem(
          child: detailItem(
            iconName: Assets.iconPin,
            tintColor: AppColors.darkBackground,
            child: _addressInfo(),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(2)),
        ),
      ],
    );
  }

  Widget _phoneNumber() {
    return Obx(
      () => montserratWrapper(value: controller.memberPhoneNumber.value),
    );
  }

  Widget _nameInfo() {
    return Obx(
      () => montserratWrapper(
          value: '${controller.firstName.value} ${controller.lastName.value}'),
    );
  }

  Widget _genderInfo() {
    return Obx(
      () => montserratWrapper(
        value: controller.memberGender.value,
      ),
    );
  }

  Widget _emailInfo() {
    return Obx(
      () => montserratWrapper(
        value: controller.memberEmailAddress.value,
      ),
    );
  }

  Widget _birthInfo() {
    return Obx(
      () => montserratWrapper(
        value: DateHelper.getBirthOfDateDisplayer(
          controller.memberDob.value,
        ),
      ),
    );
  }

  Widget _addressInfo() {
    return Obx(
      () => Flexible(
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnCrossAxisAlignment: CrossAxisAlignment.start,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value: controller.memberAddress.value,
                fontWeight: FontWeight.w300,
                size: SizeConfig.safeHorizontal(3.5),
                color: AppColors.basicDark,
              ),
            ),
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value:
                    '${controller.provinceName.value}, ${controller.cityName.value}',
                fontWeight: FontWeight.w300,
                size: SizeConfig.safeHorizontal(3.5),
                color: AppColors.basicDark,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
