import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/text/montserrat_text_view.dart';

class MemberStatusAccountMembership extends StatelessWidget {
  const MemberStatusAccountMembership({
    required this.membershipName,
    Key? key,
  }) : super(key: key);

  final String membershipName;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return MontserratTextView(
      value: membershipName,
      color: AppColors.flatGold,
      fontWeight: FontWeight.w300,
      size: SizeConfig.safeHorizontal(4),
    );
  }
}
