import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:open_store/open_store.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/profile/profile_controller.dart';
import '../../../helpers/device_info.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class MyAccountDetail extends StatelessWidget {
  // constructor
  const MyAccountDetail({Key? key, required this.controller}) : super(key: key);

  // parameter
  final ProfileController? controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return _boxMyAccount(context);
    // return ResponsiveRowColumn(
    //   layout: ResponsiveRowColumnType.COLUMN,
    //   columnCrossAxisAlignment: CrossAxisAlignment.start,
    //   children: <ResponsiveRowColumnItem>[
    //     ResponsiveRowColumnItem(
    //       child: MontserratTextView(
    //         value: 'My Account',
    //         size: SizeConfig.safeHorizontal(5.5),
    //         color: AppColors.flatGold,
    //         fontWeight: FontWeight.w700,
    //         alignText: AlignTextType.left,
    //       ),
    //     ),
    //     const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
    //     ResponsiveRowColumnItem(child: _boxMyAccount(context)),
    //   ],
    // );
  }

  Widget _boxMyAccount(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(2),
      ),
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(
        color: AppColors.flatGold,
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconProfile,
              name: 'Personal Information',
              onTap: () {
                controller?.navigateToPersonalInfo();
              },
            ),
          ),
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconTnC,
              name: 'Term & Condition',
              onTap: () {
                controller?.navigateToTerms(0);
              },
              tintColor: AppColors.basicDark,
            ),
          ),
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconPrivacy,
              name: 'Privacy Policy',
              onTap: () {
                controller?.navigateToTerms(1);
              },
              tintColor: AppColors.basicDark,
            ),
          ),
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconWhatsapp,
              name: 'Call Us (Whatsapp)',
              onTap: () {
                controller?.visitWhastapp(context);
              },
              tintColor: AppColors.basicDark,
            ),
          ),
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconInstagram,
              name: 'Visit Our Instagram',
              onTap: () {
                controller?.visitInstagram();
              },
              tintColor: AppColors.basicDark,
            ),
          ),
          ResponsiveRowColumnItem(
            child: _detailItemMyAccount(
              iconName: Assets.iconRate,
              name: 'Rate Our App',
              onTap: () async {
                OpenStore.instance.open(
                  appStoreId: '1609916429',
                  androidAppBundleId: await DeviceInfo().getPackageName(),
                );
              },
              tintColor: AppColors.basicDark,
            ),
          ),
        ],
      ),
    );
  }

  Widget _detailItemMyAccount({
    required String iconName,
    required String name,
    required VoidCallback onTap,
    Color? tintColor,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(1),
        vertical: SizeConfig.horizontal(1),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: RippleButton(
              onTap: onTap,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.horizontal(2),
                  vertical: SizeConfig.vertical(1),
                ),
                child: ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.ROW,
                  rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <ResponsiveRowColumnItem>[
                    ResponsiveRowColumnItem(
                      child: ResponsiveRowColumn(
                        layout: ResponsiveRowColumnType.ROW,
                        children: <ResponsiveRowColumnItem>[
                          ResponsiveRowColumnItem(
                            child: ResponsiveRowColumn(
                              layout: ResponsiveRowColumnType.ROW,
                              children: <ResponsiveRowColumnItem>[
                                ResponsiveRowColumnItem(
                                  child: Image.asset(
                                    iconName,
                                    width: SizeConfig.horizontal(6),
                                    height: SizeConfig.horizontal(6),
                                    color: tintColor,
                                  ),
                                ),
                                const ResponsiveRowColumnItem(
                                  child: SpaceSizer(horizontal: 2),
                                ),
                                ResponsiveRowColumnItem(
                                  child: MontserratTextView(
                                    value: name,
                                    size: SizeConfig.safeHorizontal(4),
                                    color: AppColors.basicDark,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    ResponsiveRowColumnItem(
                      child: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.rotationY(math.pi),
                        child: Image.asset(
                          Assets.iconBackDark,
                          width: SizeConfig.horizontal(6),
                          height: SizeConfig.horizontal(6),
                          color: tintColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Divider(
              color: AppColors.darkGold,
              height: SizeConfig.vertical(0.5),
            ),
          )
        ],
      ),
    );
  }
}
