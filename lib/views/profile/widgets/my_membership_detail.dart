import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/profile/profile_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/shimmer_placeholder.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';
import 'circle_thumb_shape.dart';
import 'dialog_member_benefit.dart';

class MyMembershipdDetail extends StatelessWidget {
  const MyMembershipdDetail({Key? key, required this.controller})
      : super(key: key);

  final ProfileController? controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'My Membership',
            size: SizeConfig.safeHorizontal(5.5),
            color: AppColors.flatGold,
            fontWeight: FontWeight.w700,
            alignText: AlignTextType.left,
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
        ResponsiveRowColumnItem(child: _boxMyMembershipDetail(context)),
        ResponsiveRowColumnItem(child: _boxMyMembershipRank(context)),
      ],
    );
  }

  // Box MyMembership Detail
  Widget _boxMyMembershipDetail(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(2),
      ),
      width: SizeConfig.screenWidth,
      decoration: BoxDecoration(
        color: AppColors.flatGold,
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      ),
      child: _membershipContent(context),
    );
  }

  Widget _membershipLoading() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(20),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(15),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: ShimmerPlaceholder(
                  borderRadius:
                      BorderRadius.circular(SizeConfig.horizontal(10)),
                  width: SizeConfig.horizontal(25),
                ),
              ),
              ResponsiveRowColumnItem(
                child: ShimmerPlaceholder(
                  borderRadius:
                      BorderRadius.circular(SizeConfig.horizontal(10)),
                  width: SizeConfig.horizontal(15),
                ),
              ),
            ],
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(80),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: ShimmerPlaceholder(
                  borderRadius:
                      BorderRadius.circular(SizeConfig.horizontal(10)),
                  width: SizeConfig.horizontal(25),
                ),
              ),
              ResponsiveRowColumnItem(
                child: ShimmerPlaceholder(
                  borderRadius:
                      BorderRadius.circular(SizeConfig.horizontal(10)),
                  width: SizeConfig.horizontal(15),
                ),
              ),
            ],
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(80),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(65),
          ),
        ),
      ],
    );
  }

  // content of my membership Widget
  Widget _membershipContent(BuildContext context) {
    return Obx(() {
      if (controller!.isLoadingProfile.isTrue) {
        return _membershipLoading();
      } else {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value:
                    '${controller?.firstName.value} ${controller?.lastName.value}',
                size: SizeConfig.safeHorizontal(4),
              ),
            ),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
            ResponsiveRowColumnItem(
              child: MontserratTextView(
                value: '${controller?.membershipName.value}',
                size: SizeConfig.safeHorizontal(4.5),
                fontWeight: FontWeight.w700,
              ),
            ),
            ResponsiveRowColumnItem(child: _spendingWrapper(context)),
          ],
        );
      }
    });
  }

  Widget _spendingWrapper(BuildContext context) {
    return Obx(() {
      if (controller != null) {
        if (controller!.isMembershipEmpty.value == true) {
          return const SizedBox.shrink();
        } else {
          return ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
              ResponsiveRowColumnItem(child: _labelSliderSpendingPower()),
              ResponsiveRowColumnItem(
                child: Obx(
                  () => _levelBar(
                    context: context,
                    percentageCurrent:
                        controller?.spendingPowerPercentageCurrent.value ?? 0,
                    percentageMax:
                        controller?.spendingPowerPercentageMax.value ?? 0,
                    percentageMin:
                        controller?.spendingPowerPercentageMin.value ?? 0,
                  ),
                ),
              ),
              ResponsiveRowColumnItem(child: _labelNoticeSpendingPower()),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
              ResponsiveRowColumnItem(child: _badgeNotifyRank()),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
              ResponsiveRowColumnItem(child: _labelSliderTotalSpending()),
              ResponsiveRowColumnItem(
                child: Obx(
                  () => _levelBar(
                    context: context,
                    percentageCurrent:
                        controller?.totalspendingPercentageCurrent.value ?? 0,
                    percentageMax:
                        controller?.totalspendingPercentageMax.value ?? 0,
                    percentageMin:
                        controller?.totalspendingPercentageMin.value ?? 0,
                  ),
                ),
              ),
              ResponsiveRowColumnItem(child: _labelNoticeTotalSpending()),
            ],
          );
        }
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _labelNoticeSpendingPower() {
    return Obx(() {
      if (controller != null) {
        if (controller!.nextMembership == null) {
          return const SizedBox.shrink();
        } else {
          if (controller!.reqNextMembershipSpendingPower.value == null) {
            return const SizedBox.shrink();
          } else if (controller!.reqNextMembershipSpendingPower.value == 0) {
            return const SizedBox.shrink();
          } else {
            return SizedBox(
              width: SizeConfig.screenWidth,
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                rowMainAxisAlignment: MainAxisAlignment.center,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: Flexible(
                      child: RichText(
                        text: TextSpan(
                          children: <InlineSpan>[
                            TextSpan(
                              text: 'Get',
                              style: GoogleFonts.montserrat(
                                color: AppColors.darkBackground,
                                fontSize: SizeConfig.horizontal(3.5),
                              ),
                            ),
                            const WidgetSpan(
                              child: SpaceSizer(horizontal: 0.5),
                            ),
                            WidgetSpan(
                              child: Image.asset(
                                Assets.iconStar,
                                width: SizeConfig.horizontal(5),
                                height: SizeConfig.horizontal(5),
                                color: AppColors.darkBackground,
                              ),
                            ),
                            const WidgetSpan(
                              child: SpaceSizer(horizontal: 0.5),
                            ),
                            TextSpan(
                              text:
                                  '${controller!.reqNextMembershipSpendingPower.value}',
                              style: GoogleFonts.montserrat(
                                color: AppColors.darkBackground,
                                fontSize: SizeConfig.horizontal(3.5),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TextSpan(
                              text: ' to reach ',
                              style: GoogleFonts.montserrat(
                                color: AppColors.darkBackground,
                                fontSize: SizeConfig.horizontal(3.5),
                              ),
                            ),
                            TextSpan(
                              text: controller!.nextMembership.value,
                              style: GoogleFonts.montserrat(
                                color: AppColors.darkBackground,
                                fontSize: SizeConfig.horizontal(3.5),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        }
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _labelNoticeTotalSpending() {
    return Obx(() {
      if (controller != null) {
        if (controller!.reqNextLevelMembershipTotalSpending.value != null) {
          return RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              children: <InlineSpan>[
                TextSpan(
                  text: 'Spend ',
                  style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: SizeConfig.horizontal(3.5),
                  ),
                ),
                TextSpan(
                  text:
                      '${controller!.displayReqNextLevelMembershipTotalSpending.value}',
                  style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: SizeConfig.horizontal(3.5),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text: ' more to reach ',
                  style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: SizeConfig.horizontal(3.5),
                  ),
                ),
                TextSpan(
                  text: controller!.nextMembership.value,
                  style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: SizeConfig.horizontal(3.5),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          );
        } else {
          return const SizedBox.shrink();
        }
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  // wrapper for both level bar
  Widget _labelSliderSpendingPower() {
    return Obx(
      () => ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        rowPadding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(1),
        ),
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _spendingPowerWrapper()),
          ResponsiveRowColumnItem(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: Image.asset(
                    Assets.iconStar,
                    width: SizeConfig.horizontal(5),
                    height: SizeConfig.horizontal(5),
                  ),
                ),
                const ResponsiveRowColumnItem(
                  child: SpaceSizer(horizontal: 0.5),
                ),
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: '${controller?.currentSpendingPower.value}',
                    size: SizeConfig.safeHorizontal(4.5),
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // spending power label
  Widget _spendingPowerWrapper() {
    return Obx(
      () => ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Spending Power',
              size: SizeConfig.safeHorizontal(4),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Container(
              margin: EdgeInsets.only(left: SizeConfig.horizontal(1.2)),
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(1.5),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
                color: AppColors.basicDark,
              ),
              child: MontserratTextView(
                value:
                    '${controller?.currentTransaction.value} / ${controller?.minTransaction.value}',
                size: SizeConfig.safeHorizontal(4),
                color: AppColors.flatGold,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _badgeNotifyRank() {
    return Obx(() {
      if (controller?.spendingPowerStayWarningShow.value == 1) {
        return Container(
          margin: EdgeInsets.symmetric(
            horizontal: SizeConfig.vertical(1),
            vertical: SizeConfig.vertical(1),
          ),
          width: SizeConfig.screenWidth,
          height: SizeConfig.vertical(10),
          decoration: BoxDecoration(
            color: AppColors.basicDark,
            borderRadius: BorderRadius.circular(
              SizeConfig.horizontal(3),
            ),
          ),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Container(
                  width: SizeConfig.horizontal(20),
                  height: SizeConfig.horizontal(10),
                  decoration: BoxDecoration(
                    color: AppColors.basicDark,
                    shape: BoxShape.circle,
                  ),
                  child: Image.asset(
                    Assets.iconNotify,
                    color: AppColors.flatGold,
                  ),
                ),
              ),
              ResponsiveRowColumnItem(
                child: SizedBox(
                  width: SizeConfig.horizontal(50),
                  child: ResponsiveRowColumn(
                    layout: ResponsiveRowColumnType.ROW,
                    children: <ResponsiveRowColumnItem>[
                      ResponsiveRowColumnItem(
                        child: Flexible(
                          child: RichText(
                            text: TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                  text: 'Get',
                                  style: GoogleFonts.montserrat(
                                    color: AppColors.flatGold,
                                    fontSize: SizeConfig.horizontal(3.5),
                                  ),
                                ),
                                const WidgetSpan(
                                  child: SpaceSizer(horizontal: 0.5),
                                ),
                                WidgetSpan(
                                  child: Image.asset(
                                    Assets.iconStar,
                                    width: SizeConfig.horizontal(5),
                                    height: SizeConfig.horizontal(5),
                                    color: AppColors.flatGold,
                                  ),
                                ),
                                const WidgetSpan(
                                  child: SpaceSizer(horizontal: 0.5),
                                ),
                                TextSpan(
                                  text:
                                      '${controller?.spendingPowerStayWarningSpendSp.value}',
                                  style: GoogleFonts.montserrat(
                                    color: AppColors.flatGold,
                                    fontSize: SizeConfig.horizontal(3.5),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      ' to stay in ${controller?.currentMembership.value} before',
                                  style: GoogleFonts.montserrat(
                                    color: AppColors.flatGold,
                                    fontSize: SizeConfig.horizontal(3.5),
                                  ),
                                ),
                                TextSpan(
                                  text: ' ${controller?.membershipReset.value}',
                                  style: GoogleFonts.montserrat(
                                    color: AppColors.flatGold,
                                    fontSize: SizeConfig.horizontal(3.5),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _labelSliderTotalSpending() {
    return Obx(
      () => ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        rowPadding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(1),
        ),
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: 'Total Spending',
              size: SizeConfig.safeHorizontal(4),
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: '${controller?.displayCurrentTotalSpending.value}',
              size: SizeConfig.safeHorizontal(4),
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      ),
    );
  }

  Widget _levelBar({
    required BuildContext context,
    required double percentageCurrent,
    required double percentageMax,
    required double percentageMin,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: SizeConfig.vertical(2)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(3),
      decoration: BoxDecoration(
        border: Border.all(
          color: percentageCurrent == percentageMax
              ? Colors.transparent
              : AppColors.basicDark,
        ),
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
      ),
      child: SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTickMarkColor: AppColors.basicDark,
          activeTrackColor: AppColors.basicDark,
          inactiveTickMarkColor: AppColors.flatGold,
          inactiveTrackColor: Colors.transparent,
          overlayShape: SliderComponentShape.noOverlay,
          thumbColor: AppColors.basicDark,
          trackHeight: SizeConfig.vertical(3),
          thumbShape: CircleThumbShape(
            thumbRadius: SizeConfig.horizontal(4),
          ),
          trackShape: SliderCustomTrackShape(),
        ),
        child: Slider(
          value: percentageCurrent == 100
              ? percentageCurrent - 5
              : percentageCurrent + 5,
          onChanged: (dynamic value) {},
          min: percentageMin,
          max: percentageMax,
        ),
      ),
    );
  }

  Widget _boxMyMembershipRank(BuildContext context) {
    return Obx(() {
      if (controller!.membershipRank.isNotEmpty) {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
            ResponsiveRowColumnItem(
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.horizontal(2),
                  vertical: SizeConfig.vertical(0.5),
                ),
                width: SizeConfig.screenWidth,
                decoration: BoxDecoration(
                  color: AppColors.flatGold,
                  borderRadius: BorderRadius.circular(
                    SizeConfig.horizontal(2),
                  ),
                ),
                child: ResponsiveRowColumn(
                  layout: ResponsiveRowColumnType.COLUMN,
                  children: <ResponsiveRowColumnItem>[
                    const ResponsiveRowColumnItem(
                      child: SpaceSizer(vertical: 1.5),
                    ),
                    ResponsiveRowColumnItem(
                      child: MontserratTextView(
                        value: 'Membership Rank',
                        size: SizeConfig.safeHorizontal(4),
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const ResponsiveRowColumnItem(
                      child: SpaceSizer(vertical: 2),
                    ),
                    ResponsiveRowColumnItem(child: _statusMembership(context)),
                  ],
                ),
              ),
            ),
          ],
        );
      } else {
        return const SizedBox.shrink();
      }
    });
  }

  Widget _statusMembership(BuildContext context) {
    return SizedBox(
      height: SizeConfig.vertical(17),
      child: Obx(() {
        if (controller!.isLoadingProfile.isFalse) {
          return ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: controller!.membershipRank.length,
            itemBuilder: (BuildContext context, int index) {
              return ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                children: <ResponsiveRowColumnItem>[
                  const ResponsiveRowColumnItem(
                    child: SpaceSizer(horizontal: 2),
                  ),
                  ResponsiveRowColumnItem(
                    child: _rankButton(
                      context: context,
                      rankName: '${controller!.membershipRank[index]?.name}',
                      rankImage: '${controller!.membershipRank[index]?.image}',
                      tnc: '${controller!.membershipRank[index]?.tnc}',
                      benefit: '${controller!.membershipRank[index]?.benefit}',
                      isActive:
                          controller!.membershipRank[index]?.isActive ?? 0,
                      isDualScreen: controller!.isDualScreen.value,
                    ),
                  ),
                  const ResponsiveRowColumnItem(
                    child: SpaceSizer(horizontal: 2),
                  ),
                ],
              );
            },
          );
        } else {
          return ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              const SpaceSizer(horizontal: 2),
              ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      width: SizeConfig.horizontal(17),
                      height: SizeConfig.horizontal(17),
                      shape: BoxShape.circle,
                    ),
                  ),
                  const ResponsiveRowColumnItem(
                    child: SpaceSizer(vertical: 2),
                  ),
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      borderRadius:
                          BorderRadius.circular(SizeConfig.horizontal(10)),
                      width: SizeConfig.horizontal(15),
                    ),
                  ),
                ],
              ),
              const SpaceSizer(horizontal: 2),
              ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      width: SizeConfig.horizontal(17),
                      height: SizeConfig.horizontal(17),
                      shape: BoxShape.circle,
                    ),
                  ),
                  const ResponsiveRowColumnItem(
                    child: SpaceSizer(vertical: 2),
                  ),
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      borderRadius:
                          BorderRadius.circular(SizeConfig.horizontal(10)),
                      width: SizeConfig.horizontal(15),
                    ),
                  ),
                ],
              ),
              const SpaceSizer(horizontal: 2),
              ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.COLUMN,
                children: <ResponsiveRowColumnItem>[
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      width: SizeConfig.horizontal(17),
                      height: SizeConfig.horizontal(17),
                      shape: BoxShape.circle,
                    ),
                  ),
                  const ResponsiveRowColumnItem(
                    child: SpaceSizer(vertical: 2),
                  ),
                  ResponsiveRowColumnItem(
                    child: ShimmerPlaceholder(
                      borderRadius:
                          BorderRadius.circular(SizeConfig.horizontal(10)),
                      width: SizeConfig.horizontal(15),
                    ),
                  ),
                ],
              ),
              const SpaceSizer(horizontal: 2),
            ],
          );
        }
      }),
    );
  }

  Widget _rankButton(
      {required BuildContext context,
      required String rankName,
      required String rankImage,
      required String benefit,
      required String tnc,
      required int isActive,
      required bool isDualScreen}) {
    if (isActive == 1) {
      return RippleButton(
        onTap: () {
          showDialog(
            context: context,
            builder: (BuildContext dialogContext) {
              return DialogMemberBenefit(
                benefit: benefit,
                tnc: tnc,
                isDualScreen: isDualScreen,
              );
            },
          );
        },
        radius: 2,
        child: SizedBox(
          height: SizeConfig.vertical(15),
          width: SizeConfig.horizontal(20),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Container(
                  width: SizeConfig.horizontal(17),
                  height: SizeConfig.horizontal(17),
                  decoration: BoxDecoration(
                    color: AppColors.basicDark,
                    shape: BoxShape.circle,
                  ),
                  child: CachedNetworkImage(
                    imageUrl: rankImage,
                    placeholder: (BuildContext context, String url) {
                      return const CircularProgressIndicator();
                    },
                    errorWidget:
                        (BuildContext context, String url, dynamic error) {
                      return const SizedBox.shrink();
                    },
                  ),
                ),
              ),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
              ResponsiveRowColumnItem(
                child: MontserratTextView(
                  value: rankName,
                  alignText: AlignTextType.center,
                  size: SizeConfig.safeHorizontal(3.5),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return RippleButton(
        onTap: () {
          showDialog(
            context: context,
            builder: (BuildContext dialogContext) {
              return DialogMemberBenefit(
                benefit: benefit,
                tnc: tnc,
                isDualScreen: isDualScreen,
              );
            },
          );
        },
        radius: 2,
        child: SizedBox(
          height: SizeConfig.vertical(15),
          width: SizeConfig.horizontal(20),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: Opacity(
                  opacity: 0.2,
                  child: Container(
                    width: SizeConfig.horizontal(17),
                    height: SizeConfig.horizontal(17),
                    decoration: BoxDecoration(
                      color: AppColors.basicDark,
                      shape: BoxShape.circle,
                    ),
                    child: CachedNetworkImage(
                      imageUrl: rankImage,
                      placeholder: (BuildContext context, String url) {
                        return const CircularProgressIndicator();
                      },
                      errorWidget:
                          (BuildContext context, String url, dynamic error) {
                        return const SizedBox.shrink();
                      },
                    ),
                  ),
                ),
              ),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
              ResponsiveRowColumnItem(
                child: Opacity(
                  opacity: 0.2,
                  child: MontserratTextView(
                    value: rankName,
                    alignText: AlignTextType.center,
                    size: SizeConfig.safeHorizontal(3.5),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
