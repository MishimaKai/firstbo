import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/qr_code/qr_code_controller.dart';
import '../../utils/app_colors.dart';
import '../../utils/assets_list.dart';
import '../../utils/size_config.dart';
import '../widgets/buttons/montserrat_button.dart';
import '../widgets/buttons/ripple_button.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/layouts/space_sizer.dart';

class QRCodePage extends StatefulWidget {
  const QRCodePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return QRCodePageState();
  }
}

class QRCodePageState extends State<QRCodePage> {
  QRCodeController? controller;

  @override
  void initState() {
    super.initState();
    controller = QRCodeController(pageContext: context);
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.doPauseCamera();
    } else if (Platform.isIOS) {
      controller!.doResumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return FrameScaffold(
      heightBar: 0,
      elevation: 0,
      avoidBottomInset: false,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      view: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          systemNavigationBarColor: AppColors.darkBackground,
          systemNavigationBarIconBrightness: Brightness.light,
        ),
        child: Container(
          color: AppColors.darkBackground,
          child: _mainWrapper(context, controller!),
        ),
      ),
    );
  }

  Widget _mainWrapper(BuildContext context, QRCodeController qrCodeController) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: _toggleFlashWrapper(qrCodeController),
        ),
        ResponsiveRowColumnItem(
          child: _qrCodeWrapper(context, qrCodeController),
        ),
        ResponsiveRowColumnItem(
          child: _bottomCameraControlWrapper(qrCodeController),
        ),
      ],
    );
  }

  Widget _toggleFlashWrapper(QRCodeController qrCodeController) {
    return Container(
      margin: EdgeInsets.only(left: SizeConfig.horizontal(5)),
      alignment: Alignment.centerLeft,
      color: AppColors.darkBackground,
      height: SizeConfig.vertical(8),
      child: RippleButton(
        onTap: () {
          qrCodeController.doToggleFlash();
        },
        splashColor: AppColors.rippleColorDark,
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2),
              vertical: SizeConfig.vertical(1)),
          child: Image.asset(
            Assets.iconFlash,
            width: SizeConfig.horizontal(8),
            height: SizeConfig.horizontal(8),
          ),
        ),
      ),
    );
  }

  Widget _qrCodeWrapper(
    BuildContext context,
    QRCodeController qrCodeController,
  ) {
    return Expanded(
      flex: 3,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          QRView(
            key: GlobalKey(debugLabel: 'QR'),
            onQRViewCreated: (QRViewController controller) {
              return qrCodeController.onQRViewCreated(controller, context);
            },
            overlay: QrScannerOverlayShape(
              borderColor: AppColors.flatGold,
              borderRadius: SizeConfig.horizontal(5),
              borderLength: 30,
              borderWidth: 10,
              cutOutSize: SizeConfig.horizontal(68),
            ),
          ),
          Obx(() {
            if (qrCodeController.isLoading.isTrue) {
              return Container(
                width: SizeConfig.horizontal(60),
                height: SizeConfig.horizontal(60),
                decoration: BoxDecoration(
                  color: AppColors.darkBackground.withOpacity(0.80),
                  borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
                ),
                child: Center(
                  child: CircularProgressIndicator(
                    color: AppColors.flatGold,
                  ),
                ),
              );
            } else {
              return const SizedBox.shrink();
            }
          }),
        ],
      ),
    );
  }

  Widget _bottomCameraControlWrapper(QRCodeController qrCodeController) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(5),
          vertical: SizeConfig.vertical(2),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            _buttonControl(qrCodeController),
            // _buttonControlCenter(),
          ],
        ),
      ),
    );
  }

  Widget _buttonControl(QRCodeController qrCodeController) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnMainAxisAlignment: MainAxisAlignment.center,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(
                child: _cancelButtonWrapper(qrCodeController),
              ),
              const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
            ],
          ),
        ),
      ],
    );
  }

  Widget _cancelButtonWrapper(QRCodeController qrCodeController) {
    return MontserratButton(
      onTap: () {
        Get.back();
      },
      value: 'Cancel',
      customPadding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(1),
      ),
      isUnderLine: false,
      isUseRipple: true,
    );
  }
}
