import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_row_column.dart';

import '../../controllers/balances/redeem/redeem_detail_controller.dart';
import '../../utils/app_colors.dart';
import '../../utils/enums.dart';
import '../../utils/page_names.dart';
import '../../utils/size_config.dart';
import '../widgets/buttons/custom_flat_buttons.dart';
import '../widgets/dialogs/custom_dialog.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/frame_view/frame_action_wrapper.dart';
import '../widgets/layouts/shimmer_placeholder.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/layouts/under_line.dart';
import '../widgets/shimmers/item_detail_shimmer.dart';
import '../widgets/text/montserrat_text_view.dart';

class RedeemItemDetailPage extends StatelessWidget {
  const RedeemItemDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: FrameScaffold(
        heightBar: 45.0,
        elevation: 0,
        avoidBottomInset: true,
        isUseLeading: false,
        isImplyLeading: false,
        color: Platform.isIOS ? AppColors.darkBackground : null,
        statusBarColor: AppColors.darkBackground,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.dark,
        colorScaffold: AppColors.darkBackground,
        action: _frameAction(),
        view: _getBuilderWrapper(context),
      ),
    );
  }

  Widget _frameAction() {
    return FrameActionWrapper(
      title: PageNames.redeemPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        Get.back();
      },
    );
  }

  Widget _getBuilderWrapper(BuildContext context) {
    return GetBuilder<RedeemItemDetailController>(
      init: RedeemItemDetailController(pageContext: context),
      builder: (RedeemItemDetailController controller) {
        return _obxWrapper(context, controller);
      },
    );
  }

  Widget _obxWrapper(
    BuildContext context,
    RedeemItemDetailController controller,
  ) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const ItemDetailShimmer();
      } else {
        return Container(
          color: AppColors.darkBackground,
          margin: EdgeInsets.only(
            bottom: SizeConfig.vertical(1),
            top: SizeConfig.vertical(1),
          ),
          child: _priceItemDetailWrapper(context, controller),
        );
      }
    });
  }

  Widget _priceItemDetailWrapper(
    BuildContext context,
    RedeemItemDetailController controller,
  ) {
    return SingleChildScrollView(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _bannerImageContent(controller),
          ),
          ResponsiveRowColumnItem(
            child: _descriptionRedeemItem(context, controller),
          ),
        ],
      ),
    );
  }

  Widget _bannerImageContent(RedeemItemDetailController controller) {
    return AspectRatio(
      aspectRatio: 1 / 1,
      child: CachedNetworkImage(
        imageUrl: controller.getRedeemImage(),
        fit: BoxFit.fill,
        placeholder: (BuildContext context, String url) {
          return ShimmerPlaceholder(width: SizeConfig.screenWidth);
        },
        errorWidget: (BuildContext context, String url, dynamic error) =>
            const SizedBox.shrink(),
      ),
    );
  }

  Widget _descriptionRedeemItem(
    BuildContext context,
    RedeemItemDetailController controller,
  ) {
    return Container(
      width: SizeConfig.screenWidth,
      color: AppColors.darkBackground,
      padding: EdgeInsets.only(
        left: SizeConfig.horizontal(1.5),
        right: SizeConfig.horizontal(1.5),
        top: SizeConfig.vertical(2),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          _titleRedeemItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _priceRedeemItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2),
              ),
              child: const Underline(),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _descriptionDetailRedeemItem(controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          _buttonRedeem(context, controller),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
        ],
      ),
    );
  }

  ResponsiveRowColumnItem _titleRedeemItem(
    RedeemItemDetailController controller,
  ) {
    return ResponsiveRowColumnItem(
      child: Padding(
        padding: EdgeInsets.only(
          left: SizeConfig.horizontal(1.5),
          right: SizeConfig.horizontal(1.5),
        ),
        child: MontserratTextView(
          value: controller.getRedeemName(),
          color: AppColors.whiteBackground,
          size: SizeConfig.safeHorizontal(5),
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  ResponsiveRowColumnItem _descriptionDetailRedeemItem(
    RedeemItemDetailController controller,
  ) {
    return ResponsiveRowColumnItem(
      child: Html(
        data: controller.getRedeemDetail(),
        style: <String, Style>{'p': Style(color: AppColors.whiteBackground)},
      ),
    );
  }

  ResponsiveRowColumnItem _priceRedeemItem(
    RedeemItemDetailController controller,
  ) {
    final MontserratTextStyle montserratStyle = MontserratTextStyle();
    return ResponsiveRowColumnItem(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.only(
                left: SizeConfig.horizontal(1.5),
                right: SizeConfig.horizontal(1.5),
              ),
              child: RichText(
                text: TextSpan(
                  text: controller.getRedeemPoints(),
                  style: montserratStyle.flatGoldDescriptionPriceItem6(),
                  children: <TextSpan>[
                    TextSpan(
                      text: ' points',
                      style: montserratStyle.flatGoldDescriptionPriceItem4(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ResponsiveRowColumnItem _buttonRedeem(
    BuildContext context,
    RedeemItemDetailController controller,
  ) {
    return ResponsiveRowColumnItem(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: CustomFlatButton(
              width: SizeConfig.horizontal(92),
              loading: controller.isLoadingRedeem.value,
              onTap: () => showDialog(
                barrierColor: Colors.transparent.withOpacity(0.1),
                context: context,
                builder: (BuildContext dialogContext) => Obx(
                  () => CustomDialog(
                    isDualScreen: controller.isDualScreen.value,
                    onPressedInDialog: () async {
                      final bool result = await controller.postRedeem();
                      if (result) {
                        // ignore: use_build_context_synchronously
                        Navigator.of(dialogContext).pop();
                        return showDialog(
                          context: context,
                          barrierColor: Colors.transparent.withOpacity(0.1),
                          builder: (BuildContext nestedDialogContext) =>
                              CustomDialog(
                            isDualScreen: controller.isDualScreen.value,
                            onPressedInDialog: () {
                              Navigator.of(nestedDialogContext).pop();
                              Get.back();
                            },
                            typeDialog: TypeDialog.redeemSuccess,
                            descRedeem:
                                'You successfully redeemed ${controller.getRedeemPoints()} points for ${controller.getRedeemName()}',
                          ),
                        );
                      } else {
                        // ignore: use_build_context_synchronously
                        Navigator.of(dialogContext).pop();
                      }
                    },
                    typeDialog: TypeDialog.redeemVerification,
                    descRedeem:
                        'Do you want to redeem ${controller.getRedeemName()} for ${controller.getRedeemPoints()} points ?',
                    loading: controller.isLoadingRedeem.value,
                  ),
                ),
              ),
              text: 'Redeem',
            ),
          ),
        ],
      ),
    );
  }
}
