import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import '../../controllers/balances/redeem/redeem_point_controller.dart';
import '../../models/data/redeem/redeem_item_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/enums.dart';
import '../../utils/size_config.dart';
import '../widgets/buttons/ripple_button.dart';
import '../widgets/layouts/custom_empty_state.dart';
import '../widgets/layouts/price_item.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/layouts/under_line.dart';
import '../widgets/shimmers/badge_shimmer.dart';
import '../widgets/shimmers/price_shimmer.dart';
import '../widgets/text/montserrat_text_view.dart';

class RedeemPage extends StatefulWidget {
  const RedeemPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return RedeemViewState();
  }
}

class RedeemViewState extends State<RedeemPage> {
  RedeemPointController? controller;

  late AutoScrollController _autoScrollController;

  @override
  void initState() {
    super.initState();
    controller = getx.Get.put(RedeemPointController(pageContext: context));
    _autoScrollController = AutoScrollController(
      viewportBoundaryGetter: () {
        return Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom);
      },
      axis: Axis.horizontal,
    );
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return RefreshIndicator(
      triggerMode: RefreshIndicatorTriggerMode.anywhere,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
        child: Stack(children: <Widget>[_wrapper(context, controller)]),
      ),
      onRefresh: () async {
        await controller?.onRefreshPage();
      },
    );
  }

  Widget _wrapper(BuildContext context, RedeemPointController? controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _categoryTab(context, controller)),
        ResponsiveRowColumnItem(
          child: Expanded(child: _content(context, controller)),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
      ],
    );
  }

  Widget _categoryTab(
    BuildContext context,
    RedeemPointController? controller,
  ) {
    return getx.Obx(() {
      if (controller?.isLoading.isTrue ?? true) {
        return _loadingTabs();
      } else {
        return _listTabsWrapper();
      }
    });
  }

  Widget _loadingTabs() {
    return SizedBox(
      height: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.vertical(8),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.vertical(10),
          ),
        ],
      ).value,
      // ignore: prefer_const_constructors
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        // ignore: prefer_const_literals_to_create_immutables
        children: <ResponsiveRowColumnItem>[
          const ResponsiveRowColumnItem(child: BadgeShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 4)),
          const ResponsiveRowColumnItem(child: BadgeShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 4)),
          const ResponsiveRowColumnItem(child: BadgeShimmer()),
        ],
      ),
    );
  }

  Widget _listTabsWrapper() {
    return SizedBox(
      height: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.vertical(8),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.vertical(10),
          ),
        ],
      ).value,
      child: _listTabs(),
    );
  }

  Widget _listTabs() {
    return ListView.builder(
      itemCount: controller?.onTapActiveList.length,
      controller: _autoScrollController,
      scrollDirection: Axis.horizontal,
      itemBuilder: (BuildContext context, int index) {
        return _categoryTabItem(
          controller: controller,
          indeTab: index,
          indexActiveItem: controller?.onTapActiveList[index].index ?? 0,
          name: controller?.onTapActiveList[index].name ?? '',
          onTap: () async {
            controller?.onTapChangeTab(
              controller!.onTapActiveList[index].name,
              index,
            );
            await _autoScrollController.scrollToIndex(
              index,
              preferPosition: AutoScrollPosition.begin,
            );
          },
        );
      },
    );
  }

  Widget _categoryTabItem({
    required int indeTab,
    required int indexActiveItem,
    required String name,
    required Function() onTap,
    required RedeemPointController? controller,
  }) {
    return _wrapScrollTag(
      controller: controller,
      index: indeTab,
      child: Container(
        padding: EdgeInsets.all(SizeConfig.horizontal(4)),
        width: SizeConfig.horizontal(30),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              columnMainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _tabName(name)),
                const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.9)),
                ResponsiveRowColumnItem(child: _tabUnderline(indexActiveItem))
              ],
            ),
            RippleButton(onTap: onTap),
          ],
        ),
      ),
    );
  }

  Widget _wrapScrollTag({
    required RedeemPointController? controller,
    required int index,
    required Widget child,
  }) {
    return AutoScrollTag(
      key: ValueKey<int>(index),
      controller: _autoScrollController,
      index: index + 1,
      child: child,
    );
  }

  Widget _tabName(String name) {
    return MontserratTextView(
      value: name,
      fontWeight: FontWeight.w300,
      alignText: AlignTextType.center,
      size: SizeConfig.safeHorizontal(3),
      color: AppColors.whiteBackground,
    );
  }

  Widget _tabUnderline(int index) {
    return getx.Obx(
      () {
        if (controller?.categoryWillRefreshId.value == index) {
          return const Underline();
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget _content(BuildContext context, RedeemPointController? controller) {
    return Container(
      width: SizeConfig.screenWidth,
      margin: EdgeInsets.only(
        left: SizeConfig.horizontal(1),
        right: SizeConfig.horizontal(1),
      ),
      // padding: EdgeInsets.only(bottom: SizeConfig.vertical(3)),
      child: PagedGridView<int, RedeemItemModel>(
        pagingController: controller!.redeemItemListController,
        padding: EdgeInsets.only(
          // top: SizeConfig.vertical(2),
          bottom: SizeConfig.vertical(20),
        ),
        showNewPageProgressIndicatorAsGridChild: false,
        builderDelegate: PagedChildBuilderDelegate<RedeemItemModel>(
          itemBuilder: (BuildContext context, RedeemItemModel item, int index) {
            return SizedBox(
              height: SizeConfig.horizontal(50),
              child: PriceItem(
                id: item.id,
                image: item.image.original ?? '',
                productName: item.name,
                price: item.amount,
                lengthItem: controller.itemList.length,
                index: index,
                typeShowPriceItem: TypeShowPriceItem.isDisplayForPackages,
                typePriceItem: TypePriceItem.isPricePoint,
                navigatePriceItem: NavigatePriceItem.toRedeem,
              ),
            );
          },
          firstPageProgressIndicatorBuilder: (BuildContext context) => SizedBox(
            height: SizeConfig.screenHeight,
            child: const PriceShimmer(length: 6),
          ),
          newPageProgressIndicatorBuilder: (BuildContext context) => SizedBox(
            height: ResponsiveValue<double>(
              context,
              defaultValue: SizeConfig.vertical(22),
              valueWhen: <Condition<double>>[
                Condition<double>.equals(
                  name: MOBILE,
                  value: SizeConfig.vertical(25),
                ),
              ],
            ).value,
            child: const PriceShimmer(length: 3),
          ),
          noItemsFoundIndicatorBuilder: (BuildContext context) =>
              const CustomEmptyState(
            caption: 'No redeem available',
          ),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 4 / 6.5,
          mainAxisSpacing: SizeConfig.vertical(1),
          crossAxisSpacing: SizeConfig.horizontal(2.5),
          crossAxisCount: 3,
        ),
      ),
    );
  }
}
