import 'dart:io';

import 'package:country_calling_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../controllers/account/register_controller.dart';
import '../../routes/route_names.dart';
import '../../utils/app_colors.dart';
import '../../utils/page_names.dart';
import '../../utils/size_config.dart';
import '../widgets/buttons/custom_flat_buttons.dart';
import '../widgets/form/address.dart';
import '../widgets/form/birth_date.dart';
import '../widgets/form/city_drop_down.dart';
import '../widgets/form/email_address.dart';
import '../widgets/form/first_name.dart';
import '../widgets/form/gender.dart';
import '../widgets/form/last_name.dart';
import '../widgets/form/phone_number.dart';
import '../widgets/form/province_drop_down.dart';
import '../widgets/frame/frame_scaffold.dart';
import '../widgets/frame_view/frame_action_wrapper.dart';
import '../widgets/layouts/space_sizer.dart';
import '../widgets/text/montserrat_text_error.dart';

/// [RegisterPage] represent user register
/// user can register by [phone number] or [email address] if those both
/// data didn't exist on database, after user register their data and pass the validation
/// will navigate to verification page, the verification will be sent to
/// phone number via whatsapp or email address
class RegisterPage extends StatelessWidget {
  // constructor
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // intiate size config device
    SizeConfig().init(context);

    /// [FrameScaffold] for handle UI theme, also register controller
    /// using [GetBuilder]
    return FrameScaffold(
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: _actionWrapper(context),
      view: GetBuilder<RegisterController>(
        init: RegisterController(pageContext: context),
        builder: (RegisterController registerController) =>
            _annotatedWrapper(context, registerController),
      ),
    );
  }

  // custom action wrapper for back button
  /// when on pressed, unfocus virtual keyboard using [FocusScopeNode]
  Widget _actionWrapper(BuildContext context) {
    return FrameActionWrapper(
      title: PageNames.registerPage,
      fontWeight: FontWeight.bold,
      onPressed: () {
        // Get.back();
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
        GetInstance().resetInstance();
        Get.offAllNamed(RouteNames.signInRoute);
      },
    );
  }

  /// handle system UI using [AnnotatedRegion] and sset background color
  Widget _annotatedWrapper(
    BuildContext context,
    RegisterController controller,
  ) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _mainViewWrapper(context: context, controller: controller),
    );
  }

  /// wrap [_mainViewWrapper()] for [_formRegisterWrapper()]
  /// that function widget will represent form
  Widget _mainViewWrapper({
    required BuildContext context,
    required RegisterController controller,
  }) {
    return Container(
      color: AppColors.darkBackground,
      padding: EdgeInsets.only(
        left: SizeConfig.horizontal(2.5),
        right: SizeConfig.horizontal(2.5),
        top: SizeConfig.vertical(1),
      ),
      child: _formRegisterWrapper(
        context: context,
        registerController: controller,
      ),
    );
  }

  // form wrappe
  Widget _formRegisterWrapper({
    required BuildContext context,
    required RegisterController registerController,
  }) {
    return SingleChildScrollView(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _firstAndLastNameWrapper(registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _phoneNumberFormWrapper(registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _emailAddressFormWrapper(registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _genderFormWrapper(
              context: context,
              registerController: registerController,
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3.5)),
          ResponsiveRowColumnItem(
            child: _birthOfDateFormWrapper(registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3.5)),
          ResponsiveRowColumnItem(child: _selectProvince(registerController)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3.5)),
          ResponsiveRowColumnItem(child: _selectCity(registerController)),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _addressFormWrapper(registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: _registerButtonWrapper(context, registerController),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 10)),
        ],
      ),
    );
  }

  // wrapper with contain first name and last name form
  Widget _firstAndLastNameWrapper(RegisterController registerController) {
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Expanded(
              child: _firstNameFormWrapper(registerController),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 4)),
          ResponsiveRowColumnItem(
            child: Expanded(
              child: _lastNameFormWrapper(registerController),
            ),
          ),
        ],
      ),
    );
  }

  // form first name
  Widget _firstNameFormWrapper(RegisterController registerController) {
    return Obx(
      () => FirstName(
        firstNameController: registerController.firstName,
        errorText: registerController.firstNameErrorMessage.value,
      ),
    );
  }

  // form last name
  Widget _lastNameFormWrapper(RegisterController registerController) {
    return Obx(
      () => LastName(
        lastNameController: registerController.lastName,
        errorText: registerController.lastNameErrorMessage.value,
      ),
    );
  }

  // form phone number but read only
  Widget _phoneNumberFormWrapper(RegisterController registerController) {
    return Obx(
      () => PhoneNumber(
        phoneNumber: registerController.phoneNumber.value,
        defaultCountry: Image.asset(
          registerController.flagName,
          package: countryCodePackageName,
        ),
        countryCode: registerController.defaultCountryCallingCode,
      ),
    );
  }

  // form email address
  Widget _emailAddressFormWrapper(RegisterController registerController) {
    return Obx(
      () => EmailAddress(
        emailAddressController: registerController.emailAddress,
        errorText: registerController.emailAddressErrorMessage.value,
        isReadOnly: registerController.isEmailAddressReadOnly.value,
      ),
    );
  }

  // form gender with radio button
  Widget _genderFormWrapper({
    required BuildContext context,
    required RegisterController registerController,
    ResponsiveRowColumnType? layoutType,
  }) {
    return Obx(
      () => Gender(
        groupValue: registerController.genderSelection.value,
        onChanged: (Object? value) {
          registerController.onChangeGender(value, context);
        },
        layoutType: layoutType,
      ),
    );
  }

  // form birth of date
  Widget _birthOfDateFormWrapper(RegisterController registerController) {
    return Obx(
      () => BirthDate(
        onDateTimeChange: (DateTime value) {
          registerController.onChangeDatePicker(value);
        },
        selectedDate: registerController.selectedDate.value,
      ),
    );
  }

  // form dropdown for province
  Widget _selectProvince(RegisterController registerController) {
    return Obx(
      () => ProvinceDropDown(
        // ignore: invalid_use_of_protected_member
        provinceList: registerController.provinceList.value,
        onChanged: registerController.onChangeProvince,
        valueDisplay: registerController.provinceId.value,
      ),
    );
  }

  // form dropdown for city
  Widget _selectCity(RegisterController registerController) {
    return Obx(
      () => ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: CityDropDown(
              // ignore: invalid_use_of_protected_member
              cityList: registerController.cityList.value,
              onChanged: registerController.onChangeCity,
              valueDisplay: registerController.cityId.value,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextError(
              value: registerController.cityErrorMessage.value,
            ),
          ),
        ],
      ),
    );
  }

  // form user address
  Widget _addressFormWrapper(RegisterController registerController) {
    return Obx(
      () => Address(
        addressController: registerController.userAddress,
        hintText: '',
        errorText: registerController.addressErrorMessage.value,
      ),
    );
  }

  // register button with on tap unfocus softkeybaord
  Widget _registerButtonWrapper(
    BuildContext context,
    RegisterController registerController,
  ) {
    return Center(
      child: Obx(
        () => CustomFlatButton(
          width: SizeConfig.screenWidth,
          loading: registerController.isLoading.value,
          onTap: () {
            final FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
            registerController.onPressRegister();
          },
          text: 'register_button_label'.tr,
        ),
      ),
    );
  }
}
