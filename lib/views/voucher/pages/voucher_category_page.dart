import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../controllers/balances/voucher/voucher_category_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../widgets/voucher_category_item.dart';

class VoucherCategoryPage extends StatelessWidget {
  // constructor
  const VoucherCategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: FrameActionWrapper(
        title: PageNames.voucherPage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          Get.back(result: <String, bool>{'update_balance': true});
        },
      ),
      view: GetBuilder<VoucherCategoryController>(
        init: VoucherCategoryController(pageContext: context),
        builder: (VoucherCategoryController controller) {
          return _annotatedWrapper(controller);
        },
      ),
    );
  }

  Widget _annotatedWrapper(VoucherCategoryController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _mainViewWrapper(controller),
    );
  }

  Widget _mainViewWrapper(VoucherCategoryController controller) {
    return RefreshIndicator(
      triggerMode: RefreshIndicatorTriggerMode.anywhere,
      child: Container(
        color: AppColors.darkBackground,
        padding: EdgeInsets.only(
          left: SizeConfig.horizontal(2.5),
          right: SizeConfig.horizontal(2.5),
          top: SizeConfig.vertical(1),
        ),
        child: _categoryList(controller),
      ),
      onRefresh: () async {
        controller.refreshByPulldown();
      },
    );
  }

  Widget _categoryList(VoucherCategoryController controller) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const SingleChildScrollView(
          child: ItemShimmer(length: 5, height: 12),
        );
      } else {
        if (controller.categoryList.isNotEmpty) {
          return ListView.builder(
            itemCount: controller.categoryList.length,
            itemBuilder: (BuildContext context, int index) {
              return VoucherCategoryItem(
                type: controller.categoryList[index].type,
                roomName: controller.categoryList[index].group,
                hourName: controller.categoryList[index].name,
                totalAmount: controller.categoryList[index].formattedAmount,
                onTap: () {
                  controller.onNavigateToList(index);
                },
              );
            },
          );
        } else {
          return const CustomEmptyState(
            caption: 'No voucher available',
          );
        }
      }
    });
  }
}
