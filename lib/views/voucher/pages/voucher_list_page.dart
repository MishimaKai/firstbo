import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/balances/voucher/voucher_list_controller.dart';
import '../../../models/data/voucher/list/voucher_item.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../../widgets/frame/frame_scaffold.dart';
import '../../widgets/frame_view/frame_action_wrapper.dart';
import '../../widgets/layouts/custom_empty_state.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';
import '../widgets/voucher_list_item.dart';
import '../widgets/voucher_outlet_button.dart';
import '../widgets/voucher_sheet.dart';

class VoucherListPage extends StatelessWidget {
  // constructor
  const VoucherListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      heightBar: 45.0,
      elevation: 0,
      avoidBottomInset: true,
      isUseLeading: false,
      isImplyLeading: false,
      color: Platform.isIOS ? AppColors.darkBackground : null,
      statusBarColor: AppColors.darkBackground,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
      colorScaffold: AppColors.darkBackground,
      action: FrameActionWrapper(
        title: PageNames.voucherPage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          Get.back();
        },
      ),
      view: GetBuilder<VoucherListController>(
        init: VoucherListController(pageContext: context),
        builder: (VoucherListController controller) {
          return _annotatedWrapper(controller);
        },
      ),
    );
  }

  Widget _annotatedWrapper(VoucherListController controller) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkBackground,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: _mainViewWrapper(controller),
    );
  }

  Widget _mainViewWrapper(VoucherListController controller) {
    return RefreshIndicator(
      triggerMode: RefreshIndicatorTriggerMode.anywhere,
      child: Container(
        color: AppColors.darkBackground,
        child: _content(controller),
      ),
      onRefresh: () async {
        await controller.refreshByPulldown();
      },
    );
  }

  Widget _content(VoucherListController controller) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _headerDisplay(controller)),
        ResponsiveRowColumnItem(child: _outletButtonDisplay(controller)),
        ResponsiveRowColumnItem(child: _spaceDisplay(controller)),
        ResponsiveRowColumnItem(child: _voucherListDisplay(controller)),
      ],
    );
  }

  Widget _headerDisplay(VoucherListController controller) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const ItemShimmer(length: 1, height: 10, radius: 0);
      } else {
        if (controller.onWhenItemNeededEmpty()) {
          return const SizedBox.shrink();
        } else {
          return _headerVoucherTypeWrapper(
            controller.type,
            controller.roomName.value,
            controller.hourName.value,
            controller.formattedAmount.value,
          );
        }
      }
    });
  }

  Widget _outletButtonDisplay(VoucherListController controller) {
    return Obx(
      () => VoucherOutletButton(
        onTap: () {
          Get.bottomSheet(
            VoucherSheet(controller: controller),
            persistent: false,
            isScrollControlled: true,
          );
        },
        isEmpty: controller.onWhenItemNeededEmpty(),
        isLoading: controller.isLoading.value,
        selectedOutlet: controller.selectedOutlet.value,
        selectedFormattedAmount: controller.selectedFormattedAmount.value,
      ),
    );
  }

  Widget _spaceDisplay(VoucherListController controller) {
    return Obx(() {
      if (controller.isLoading.isTrue) {
        return const SizedBox.shrink();
      } else {
        if (controller.onWhenItemNeededEmpty()) {
          return const SizedBox.shrink();
        } else {
          return const SpaceSizer(vertical: 1);
        }
      }
    });
  }

  Widget _voucherListDisplay(VoucherListController controller) {
    return Expanded(
      child: PagedListView<int, VoucherItemModel>(
        pagingController: controller.voucherListController,
        builderDelegate: PagedChildBuilderDelegate<VoucherItemModel>(
          itemBuilder: (
            BuildContext context,
            VoucherItemModel item,
            int index,
          ) {
            return VoucherListItem(
              index: index,
              type: item.type,
              total: item.total,
              used: item.used,
              remaining: item.remaining,
              roomAndHourName: item.typeName,
              validEnd: item.validEnd,
              isValid: item.isValid == 1,
              textValidStart: item.textValidStart,
              textValidEnd: item.textValidEnd,
            );
          },
          firstPageProgressIndicatorBuilder: (BuildContext context) {
            return Container(
              margin:
                  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2)),
              child: const ItemShimmer(length: 5, isAdjustTop: true),
            );
          },
          newPageProgressIndicatorBuilder: (BuildContext context) {
            return Container(
              margin:
                  EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2)),
              child: const ItemShimmer(length: 1, isAdjustTop: true),
            );
          },
          noItemsFoundIndicatorBuilder: (BuildContext context) =>
              const CustomEmptyState(
            caption: 'No voucher available',
          ),
        ),
      ),
    );
  }

  Widget _headerVoucherTypeWrapper(
    int type,
    String roomParam,
    String hourParam,
    String amountParam,
  ) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2.5),
        vertical: SizeConfig.vertical(1),
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[AppColors.flatGold, AppColors.darkFlatGold],
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: _voucherInfoWrapper(type, roomParam, hourParam),
          ),
          ResponsiveRowColumnItem(child: _voucherCategoryAmount(amountParam)),
        ],
      ),
    );
  }

  Widget _voucherInfoWrapper(int type, String roomParam, String hourParam) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _voucherIconType(type)),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 1.5)),
        ResponsiveRowColumnItem(
          child: _voucherCategoryNameWrapper(roomParam, hourParam),
        ),
      ],
    );
  }

  Widget _voucherIconType(int type) {
    return Image.asset(
      type == 1 || type == 2
          ? Assets.iconKaraokeNew
          : type == 3
              ? Assets.iconDining
              : Assets.iconParty,
      height: SizeConfig.horizontal(18),
      width: SizeConfig.horizontal(18),
    );
  }

  Widget _voucherCategoryNameWrapper(String roomParam, String hourParam) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _roomName(roomParam)),
        ResponsiveRowColumnItem(child: _hourName(hourParam)),
      ],
    );
  }

  Widget _roomName(String param) {
    return MontserratTextView(
      value: param,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _hourName(String param) {
    return MontserratTextView(
      value: param,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _voucherCategoryAmount(String amountParam) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(0.5),
      ),
      decoration: BoxDecoration(
        color: AppColors.darkBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.horizontal(2)),
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _voucherAmount(amountParam)),
          ResponsiveRowColumnItem(child: _totalAmountLabel()),
        ],
      ),
    );
  }

  Widget _voucherAmount(String amountParam) {
    return MontserratTextView(
      value: amountParam,
      color: AppColors.flatGold,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _totalAmountLabel() {
    return MontserratTextView(
      value: 'Total Amount',
      color: AppColors.flatGold,
      fontWeight: FontWeight.w400,
      size: SizeConfig.safeHorizontal(3),
    );
  }
}
