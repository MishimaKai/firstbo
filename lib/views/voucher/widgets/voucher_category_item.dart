import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class VoucherCategoryItem extends StatelessWidget {
  // constructor
  const VoucherCategoryItem({
    required this.type,
    required this.roomName,
    required this.hourName,
    required this.totalAmount,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  // param
  final int type;
  final String roomName;
  final String hourName;
  final String totalAmount;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(1.5)),
      decoration: BoxDecoration(
        color: AppColors.flatGold,
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.horizontal(2)),
        ),
      ),
      child: RippleButton(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.horizontal(2.5),
            vertical: SizeConfig.vertical(1.5),
          ),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(child: _voucherInfoWrapper()),
              ResponsiveRowColumnItem(child: _voucherCategoryAmount()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _voucherInfoWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _voucherIconType()),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 1.5)),
        ResponsiveRowColumnItem(child: _voucherCategoryNameWrapper()),
      ],
    );
  }

  Widget _voucherIconType() {
    return Image.asset(
      type == 1 || type == 2
          ? Assets.iconKaraokeNew
          : type == 3
              ? Assets.iconDining
              : Assets.iconParty,
      height: SizeConfig.horizontal(18),
      width: SizeConfig.horizontal(18),
    );
  }

  Widget _voucherCategoryNameWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _roomName()),
        ResponsiveRowColumnItem(child: _hourName()),
      ],
    );
  }

  Widget _roomName() {
    return MontserratTextView(
      value: roomName,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _hourName() {
    return MontserratTextView(
      value: hourName,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _voucherCategoryAmount() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(0.5),
      ),
      decoration: BoxDecoration(
        color: AppColors.darkBackground,
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.horizontal(2)),
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _voucherAmount()),
          ResponsiveRowColumnItem(child: _totalAmountLabel()),
        ],
      ),
    );
  }

  Widget _voucherAmount() {
    return MontserratTextView(
      value: totalAmount,
      color: AppColors.flatGold,
      fontWeight: FontWeight.bold,
      size: SizeConfig.safeHorizontal(4),
    );
  }

  Widget _totalAmountLabel() {
    return MontserratTextView(
      value: 'Total Amount',
      color: AppColors.flatGold,
      fontWeight: FontWeight.w400,
      size: SizeConfig.safeHorizontal(3),
    );
  }
}
