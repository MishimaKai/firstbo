import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../helpers/string_helper.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/layouts/space_sizer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class VoucherListItem extends StatelessWidget {
  // constructor
  const VoucherListItem({
    required this.index,
    required this.type,
    required this.total,
    required this.used,
    required this.remaining,
    required this.roomAndHourName,
    required this.validEnd,
    required this.isValid,
    required this.textValidStart,
    required this.textValidEnd,
    Key? key,
  }) : super(key: key);

  final int index;
  final int type;
  final int total;
  final int used;
  final int remaining;
  final String roomAndHourName;
  final String validEnd;
  final bool isValid;
  final String textValidStart;
  final String textValidEnd;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.fromLTRB(
        SizeConfig.horizontal(2),
        index == 0 ? SizeConfig.vertical(0.5) : 0,
        // SizeConfig.vertical(1),
        SizeConfig.horizontal(2),
        SizeConfig.vertical(1),
      ),
      decoration: BoxDecoration(
        color:
            isValid ? AppColors.flatGold : AppColors.flatGold.withOpacity(0.5),
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.horizontal(2)),
        ),
      ),
      child: _content(),
    );
  }

  Widget _content() {
    return RippleButton(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(2.5),
          vertical: SizeConfig.vertical(1.5),
        ),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: _priceWrapper()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
            ResponsiveRowColumnItem(child: _expiredWrapper()),
            const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
            ResponsiveRowColumnItem(child: _roomAndHourNameWrapper()),
          ],
        ),
      ),
    );
  }

  Widget _priceWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            type == 1 || type == 2
                ? Assets.iconKaraokeNew
                : type == 3
                    ? Assets.iconDining
                    : Assets.iconParty,
            width: SizeConfig.horizontal(18),
            height: SizeConfig.horizontal(18),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2.5)),
        ResponsiveRowColumnItem(child: _priceInfo()),
      ],
    );
  }

  Widget _priceInfo() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _priceLabel()),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
        ResponsiveRowColumnItem(child: _priceDisplayWrapper()),
      ],
    );
  }

  Widget _priceLabel() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Total',
            fontWeight: FontWeight.bold,
            size: SizeConfig.safeHorizontal(4),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Used',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Remaining',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
      ],
    );
  }

  Widget _priceDisplayWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _totalPriceDisplay()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        ResponsiveRowColumnItem(child: _usedPriceDisplay()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        ResponsiveRowColumnItem(child: _remainingPriceDisplay()),
      ],
    );
  }

  Widget _totalPriceDisplay() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: ':',
            fontWeight: FontWeight.bold,
            size: SizeConfig.safeHorizontal(4),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Rp.${StringHelper.convertToRupiah(total)}',
            fontWeight: FontWeight.bold,
            size: SizeConfig.safeHorizontal(4),
          ),
        ),
      ],
    );
  }

  Widget _usedPriceDisplay() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: ':',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Rp.${StringHelper.convertToRupiah(used)}',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.2),
          ),
        ),
      ],
    );
  }

  Widget _remainingPriceDisplay() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: ':',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.2),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2)),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Rp.${StringHelper.convertToRupiah(remaining)}',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.2),
          ),
        ),
      ],
    );
  }

  Widget _expiredWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            Assets.iconClock,
            width: SizeConfig.horizontal(8),
            height: SizeConfig.horizontal(8),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2.5)),
        ResponsiveRowColumnItem(child: _expiredInfo())
      ],
    );
  }

  Widget _expiredInfo() {
    return Flexible(
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: textValidStart,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.2),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: textValidEnd,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.2),
            ),
          ),
        ],
      ),
    );
  }

  Widget _roomAndHourNameWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Image.asset(
            Assets.iconLogoDark,
            width: SizeConfig.horizontal(8),
            height: SizeConfig.horizontal(8),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 2.5)),
        ResponsiveRowColumnItem(
          child: Flexible(
            child: MontserratTextView(
              value: roomAndHourName,
              fontWeight: FontWeight.w600,
              size: SizeConfig.safeHorizontal(3.2),
            ),
          ),
        ),
      ],
    );
  }
}
