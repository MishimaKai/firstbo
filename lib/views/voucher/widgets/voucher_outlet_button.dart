import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/shimmers/item_shimmer.dart';
import '../../widgets/text/montserrat_text_view.dart';

class VoucherOutletButton extends StatelessWidget {
  // constructor
  const VoucherOutletButton({
    Key? key,
    required this.onTap,
    required this.isEmpty,
    required this.isLoading,
    required this.selectedOutlet,
    required this.selectedFormattedAmount,
  }) : super(key: key);

  // param
  final Function() onTap;
  final bool isEmpty;
  final bool isLoading;
  final String selectedOutlet;
  final String selectedFormattedAmount;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: _marginSet(),
      decoration: _decor(),
      child: _mainViewWrapper(),
    );
  }

  EdgeInsets? _marginSet() {
    if (isLoading) {
      return EdgeInsets.fromLTRB(
        SizeConfig.horizontal(2),
        SizeConfig.vertical(1),
        SizeConfig.horizontal(2),
        0,
      );
    } else {
      if (isEmpty) {
        return null;
      } else {
        return EdgeInsets.fromLTRB(
          SizeConfig.horizontal(2),
          SizeConfig.vertical(1),
          SizeConfig.horizontal(2),
          0,
        );
      }
    }
  }

  BoxDecoration? _decor() {
    if (isLoading) {
      return null;
    } else {
      if (isEmpty) {
        return null;
      } else {
        return BoxDecoration(
          color: AppColors.darkBackground,
          border: Border.all(color: AppColors.flatGold),
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.horizontal(2)),
          ),
        );
      }
    }
  }

  Widget _mainViewWrapper() {
    if (isLoading) {
      return const ItemShimmer(length: 1, height: 5);
    } else {
      if (isEmpty) {
        return const SizedBox.shrink();
      } else {
        return RippleButton(
          onTap: onTap,
          radius: 2.5,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal(2),
              vertical: SizeConfig.vertical(1),
            ),
            child: _content(),
          ),
        );
      }
    }
  }

  Widget _content() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _outletName()),
        ResponsiveRowColumnItem(child: _outlateValue()),
      ],
    );
  }

  Widget _outletName() {
    return MontserratTextView(
      value: selectedOutlet,
      color: AppColors.flatGold,
      fontWeight: FontWeight.w400,
      size: SizeConfig.horizontal(3.5),
    );
  }

  Widget _outlateValue() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: selectedFormattedAmount,
            color: AppColors.flatGold,
            fontWeight: FontWeight.bold,
            size: SizeConfig.horizontal(3.5),
          ),
        ),
        ResponsiveRowColumnItem(
          child: Icon(Icons.arrow_drop_down, color: AppColors.flatGold),
        ),
      ],
    );
  }
}
