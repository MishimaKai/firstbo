import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/balances/voucher/voucher_list_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../../widgets/buttons/ripple_button.dart';
import '../../widgets/frame/frame_sheet.dart';
import '../../widgets/layouts/under_line.dart';
import '../../widgets/text/montserrat_text_view.dart';

class VoucherSheet extends StatelessWidget {
  // constructor
  const VoucherSheet({Key? key, required this.controller}) : super(key: key);

  final VoucherListController controller;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameSheet(
      height: SizeConfig.vertical(35),
      backgroundColor: AppColors.flatGold,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
          SizeConfig.horizontal(2.5),
          0,
          SizeConfig.horizontal(2.5),
          SizeConfig.vertical(1),
        ),
        child: _listWrapper(controller),
      ),
    );
  }

  Widget _listWrapper(VoucherListController controller) {
    return ListView.builder(
      itemCount: controller.outletList.length,
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      itemBuilder: (BuildContext context, int index) {
        return Obx(
          () => _outletItem(
            id: controller.outletList[index].id,
            name: controller.outletList[index].name,
            formattedAmount: controller.outletList[index].formattedAmount,
            selectedValue: controller.selectedOutletId.value,
            onTap: () {
              controller.onTapOutletItem(
                  controller.outletList[index].id, context);
            },
          ),
        );
      },
    );
  }

  Widget _outletItem({
    required int id,
    required String name,
    required String formattedAmount,
    required int selectedValue,
    required Function() onTap,
  }) {
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(1.5)),
      child: RippleButton(
        onTap: onTap,
        radius: 0,
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnMainAxisSize: MainAxisSize.min,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(
              child: _outletItemValue(
                name: name,
                formattedAmount: formattedAmount,
              ),
            ),
            ResponsiveRowColumnItem(
              child: Underline(lineColor: AppColors.darkBackground),
            ),
          ],
        ),
      ),
    );
  }

  Widget _outletItemValue({
    required String name,
    required String formattedAmount,
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(1.2)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.min,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: name,
                    fontWeight: FontWeight.w400,
                    size: SizeConfig.safeHorizontal(3.5),
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: MontserratTextView(
                    value: formattedAmount,
                    fontWeight: FontWeight.bold,
                    size: SizeConfig.safeHorizontal(3.5),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
