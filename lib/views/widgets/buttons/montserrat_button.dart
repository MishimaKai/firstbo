import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';
import 'ripple_button.dart';

class MontserratButton extends StatelessWidget {
  const MontserratButton(
      {Key? key,
      required this.onTap,
      required this.value,
      this.color,
      this.isUnderLine,
      this.isUseRipple,
      this.customPadding,
      this.weight,
      this.fontSize})
      : super(key: key);

  final Function() onTap;
  final String value;
  final Color? color;
  final bool? isUnderLine;
  final bool? isUseRipple;
  final EdgeInsetsGeometry? customPadding;
  final FontWeight? weight;
  final double? fontSize;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isUseRipple == null || isUseRipple == false) {
      return GestureDetector(
        onTap: onTap,
        child: Padding(
          padding: customPadding ??
              EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(5),
                vertical: SizeConfig.vertical(1),
              ),
          child: _montserratWrapper(),
        ),
      );
    } else {
      return RippleButton(
        onTap: onTap,
        child: Padding(
          padding: customPadding ??
              EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(5),
                vertical: SizeConfig.vertical(1),
              ),
          child: _montserratWrapper(),
        ),
      );
    }
  }

  Widget _montserratWrapper() {
    return MontserratTextView(
      value: value,
      size: SizeConfig.safeHorizontal(fontSize ?? 4),
      fontWeight: weight ?? FontWeight.w300,
      color: color ?? Colors.white,
      isUnderline: isUnderLine ?? true,
    );
  }
}
