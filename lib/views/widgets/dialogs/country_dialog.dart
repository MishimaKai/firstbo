import 'package:country_calling_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';

import '../../../models/data/country_code/country_code_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../buttons/ripple_button.dart';
import '../layouts/space_sizer.dart';
import '../text/montserrat_text_view.dart';

class CountryDialog extends StatefulWidget {
  const CountryDialog({
    required this.onTap,
    required this.countryList,
    this.isDualScreen = false,
    Key? key,
  }) : super(key: key);

  final Function(
    String flagName,
    String countryName,
    int countryCode,
  ) onTap;

  final List<CountryIdentity> countryList;

  final bool isDualScreen;

  @override
  State<StatefulWidget> createState() {
    return CountryDialogState();
  }
}

class CountryDialogState extends State<CountryDialog> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (widget.isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal();
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignDialogWrapper() {
    return Align(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(5),
          vertical: SizeConfig.vertical(2.5),
        ),
        decoration: BoxDecoration(
          color: AppColors.darkBackground,
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.horizontal(3)),
          ),
        ),
        width: SizeConfig.horizontal(75),
        height: SizeConfig.vertical(40),
        child: _dialogWrapper(),
      ),
    );
  }

  Widget _dialogNormal() {
    return AlertDialog(
      title: MontserratTextView(
        value: 'Select Region',
        color: AppColors.whiteBackground,
        fontWeight: FontWeight.w300,
        alignText: AlignTextType.center,
        size: SizeConfig.safeHorizontal(4),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
      ),
      backgroundColor: AppColors.darkBackground,
      content: _dialogWrapper(),
    );
  }

  Widget _dialogWrapper() {
    return SizedBox(
      width: double.maxFinite,
      height: SizeConfig.vertical(40),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: widget.countryList.length,
        itemBuilder: (BuildContext context, int index) {
          return _itemCountry(index);
        },
      ),
    );
  }

  Widget _itemCountry(int index) {
    return Container(
      margin: EdgeInsets.only(
        bottom: index == widget.countryList.length - 1
            ? 0
            : SizeConfig.vertical(1.5),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(2),
        vertical: SizeConfig.vertical(1),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
        border: Border.all(color: AppColors.flatGold),
      ),
      child: _countryButton(index),
    );
  }

  Widget _countryButton(int index) {
    return RippleButton(
      onTap: () {
        return widget.onTap(
          widget.countryList[index].flag,
          widget.countryList[index].name,
          int.parse(widget.countryList[index].code),
        );
      },
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _countryFlag(index)),
          const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 1)),
          ResponsiveRowColumnItem(child: _countryName(index)),
        ],
      ),
    );
  }

  Widget _countryFlag(int index) {
    return Image.asset(
      widget.countryList[index].flag,
      package: countryCodePackageName,
      width: SizeConfig.horizontal(7.5),
      height: SizeConfig.horizontal(7.5),
    );
  }

  Widget _countryName(int index) {
    return Flexible(
      child: MontserratTextView(
        value: widget.countryList[index].name,
        color: AppColors.whiteBackground,
        fontWeight: FontWeight.w300,
        size: SizeConfig.safeHorizontal(4),
        maxLines: 2,
      ),
    );
  }
}
