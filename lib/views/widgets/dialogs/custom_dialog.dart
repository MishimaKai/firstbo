import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../buttons/custom_flat_buttons.dart';
import '../buttons/montserrat_button.dart';
import '../buttons/ripple_button.dart';
import '../layouts/shimmer_placeholder.dart';
import '../layouts/space_sizer.dart';
import '../text/montserrat_text_view.dart';

class CustomDialog extends StatelessWidget {
  const CustomDialog({
    Key? key,
    required this.onPressedInDialog,
    required this.typeDialog,
    this.image,
    this.descRedeem,
    this.loading = false,
    this.isDualScreen = false,
  }) : super(key: key);

  final TypeDialog typeDialog;
  final VoidCallback onPressedInDialog;
  final String? image;
  final String? descRedeem;
  final bool loading;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _fordableDialog(context);
    } else {
      return _normalDialog(context);
    }
  }

  Widget _fordableDialog(BuildContext context) {
    if (typeDialog == TypeDialog.promoEvent) {
      /// IF TYPE DIALOG PROMO EVENT
      return _promoEventFoldable(context);
    } else {
      /// OTHER THAN NOT AN PROMO EVENT DIALOG TYPE
      return _otherThanPromoEventFoldable(context);
    }
  }

  Widget _normalDialog(BuildContext context) {
    if (typeDialog == TypeDialog.promoEvent) {
      /// IF TYPE DIALOG PROMO EVENT
      return _promoEventDialog(context);
    } else {
      /// OTHER THAN NOT AN PROMO EVENT DIALOG TYPE
      return _otherThanPromoEvent(context);
    }
  }

  Widget _promoEventFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignPromoDialogWrapper(),
        ],
      ),
    );
  }

  Widget _otherThanPromoEventFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignOtherThanPromoEventWrapper(context),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _alignPromoDialogWrapper() {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: _promoEventWrappper(),
      ),
    );
  }

  Widget _alignOtherThanPromoEventWrapper(BuildContext context) {
    return Align(
      child: SizedBox(
        width: SizeConfig.horizontal(65),
        child: __otherThanPromoEventWrapper(context),
      ),
    );
  }

  AlertDialog _promoEventDialog(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.only(
        left: SizeConfig.horizontal(10),
        right: SizeConfig.horizontal(10),
      ),
      contentPadding: EdgeInsets.zero,
      backgroundColor: Colors.transparent,
      content: _promoEventWrappper(),
    );
  }

  Widget _promoEventWrappper() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Stack(
          alignment: Alignment.center,
          children: <Widget>[_imageWrapper(), _buttonWrapper()],
        ),
      ],
    );
  }

  Widget _imageWrapper() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      child: AspectRatio(
        aspectRatio: 1 / 1,
        child: CachedNetworkImage(
          imageUrl: image!,
          fit: BoxFit.cover,
          placeholder: (BuildContext context, String url) {
            return ShimmerPlaceholder(
              height: SizeConfig.vertical(45),
              width: SizeConfig.horizontal(90),
              borderRadius: BorderRadius.circular(
                SizeConfig.horizontal(2),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buttonWrapper() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      child: AspectRatio(
        aspectRatio: 1 / 1,
        child: RippleButton(onTap: () {
          onPressedInDialog();
        }),
      ),
    );
  }

  AlertDialog _otherThanPromoEvent(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      content: __otherThanPromoEventWrapper(context),
    );
  }

  Widget __otherThanPromoEventWrapper(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.vertical(3),
        horizontal: SizeConfig.horizontal(5),
      ),
      decoration: _dialogDecor(),
      child: _dialogContent(context),
    );
  }

  BoxDecoration _dialogDecor() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: <Color>[
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.flatGold,
          AppColors.darkFlatGold,
          // AppColors.darkFlatGold,
        ],
      ),
    );
  }

  Widget _dialogContent(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        /// IMAGE DIALOG WHEN TYPE DIALOG VERIFIED ID SUCCESS
        Image.asset(_imageTypeWrapper(), height: SizeConfig.vertical(10)),
        const SpaceSizer(vertical: 2),
        _textTittle(),
        const SpaceSizer(vertical: 1),
        _textDesc(),
        const SpaceSizer(vertical: 1),
        _spaceWrapper(),
        _buttonConfirmWrapper(),
        _spaceWrapper(),
        _cancelButtonWrapper(context),
      ],
    );
  }

  /// TITLE DIALOG WHEN TYPE DIALOG VERIFIED ID SUCCESS
  Widget _textTittle() {
    return MontserratTextView(
      value: _textTitleWrapper(),
      color: AppColors.darkBackground,
      size: SizeConfig.safeHorizontal(5),
      fontWeight: FontWeight.w700,
      alignText: AlignTextType.center,
    );
  }

  /// DESCRIPTION DIALOG WHEN TYPE DIALOG VERIFIED ID SUCCESS
  Widget _textDesc() {
    return MontserratTextView(
      value: _textDescWrapper(),
      color: AppColors.darkBackground,
      alignText: AlignTextType.center,
      size: SizeConfig.safeHorizontal(3.5),
      fontWeight: FontWeight.w300,
    );
  }

  String _imageTypeWrapper() {
    if (typeDialog == TypeDialog.verifiedIdSuccess) {
      return Assets.iconSuccess;
    } else if (typeDialog == TypeDialog.redeemVerification) {
      return Assets.iconGift;
    } else {
      return Assets.iconGiftOpen;
    }
  }

  String _textTitleWrapper() {
    if (typeDialog == TypeDialog.verifiedIdSuccess) {
      return 'title_id'.tr;
    } else if (typeDialog == TypeDialog.redeemVerification ||
        typeDialog == TypeDialog.redeemSuccess) {
      return 'title_redeem'.tr;
    } else {
      return '';
    }
  }

  String _textDescWrapper() {
    if (typeDialog == TypeDialog.verifiedIdSuccess) {
      return 'description_id'.tr;
    } else if (typeDialog == TypeDialog.redeemVerification ||
        typeDialog == TypeDialog.redeemSuccess) {
      return descRedeem ?? '';
    } else {
      return '';
    }
  }

  Widget _spaceWrapper() {
    if (typeDialog == TypeDialog.verifiedIdSuccess) {
      return const SizedBox.shrink();
    } else {
      return SizedBox(height: SizeConfig.vertical(1));
    }
  }

  Widget _buttonConfirmWrapper() {
    if (typeDialog == TypeDialog.redeemVerification) {
      return _buttonRedeemVerify();
    } else if (typeDialog == TypeDialog.redeemSuccess) {
      return _buttonRedeemSuccess();
    } else {
      return const SizedBox.shrink();
    }
  }

  /// BUTTON DIALOG WHEN TYPE DIALOG REDEEM VERIFICATION
  Widget _buttonRedeemVerify() {
    return CustomFlatButton(
      onTap: () => onPressedInDialog(),
      text: 'button_redeem_verification'.tr,
      loading: loading,
      textSize: 3,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }

  /// BUTTON DIALOG WHEN TYPE DIALOG REDEEM SUCCESS
  Widget _buttonRedeemSuccess() {
    return CustomFlatButton(
      onTap: () => onPressedInDialog(),
      text: 'button_redeem_success'.tr,
      textSize: 3,
      backgroundColor: AppColors.darkBackground,
      textColor: AppColors.flatGold,
      textColorLoading: AppColors.basicDark,
    );
  }

  Widget _cancelButtonWrapper(BuildContext context) {
    if (typeDialog == TypeDialog.redeemVerification) {
      return MontserratButton(
        onTap: () => Navigator.pop(context),
        value: 'Cancel',
        color: AppColors.basicDark,
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}
