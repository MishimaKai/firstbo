import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../buttons/ripple_button.dart';
import '../layouts/space_sizer.dart';
import '../text/montserrat_text_view.dart';

class ForceUpdateDialog extends StatelessWidget {
  const ForceUpdateDialog({
    Key? key,
    required this.onTap,
    this.isDualScreen = false,
  }) : super(key: key);

  final Function() onTap;
  final bool isDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (isDualScreen) {
      return _dialogFoldable(context);
    } else {
      return _dialogNormal();
    }
  }

  Widget _dialogFoldable(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          _gestureWrapper(context),
          _alignDialogWrapper(),
        ],
      ),
    );
  }

  Widget _gestureWrapper(BuildContext context) {
    return Container(color: Colors.transparent);
  }

  Widget _alignDialogWrapper() {
    return Align(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(5),
          vertical: SizeConfig.vertical(2.5),
        ),
        decoration: BoxDecoration(
          color: AppColors.darkBackground,
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.horizontal(3)),
          ),
        ),
        width: SizeConfig.horizontal(75),
        child: _contentWrapper(),
      ),
    );
  }

  Widget _dialogNormal() {
    return AlertDialog(
      title: MontserratTextView(
        value: 'force_update_dialog_label'.tr,
        color: AppColors.whiteBackground,
        fontWeight: FontWeight.w300,
        alignText: AlignTextType.center,
        size: SizeConfig.safeHorizontal(4),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
      ),
      backgroundColor: AppColors.darkBackground,
      contentPadding: const EdgeInsets.fromLTRB(0, 24.0, 0, 0),
      content: _contentWrapper(),
    );
  }

  Widget _contentWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisSize: MainAxisSize.min,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _dialogMessage()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 5)),
        ResponsiveRowColumnItem(child: _dialogButtonWrapper()),
      ],
    );
  }

  Widget _dialogMessage() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Flexible(
            child: MontserratTextView(
              value: 'force_update_dialog_message'.tr,
              fontWeight: FontWeight.w300,
              size: SizeConfig.safeHorizontal(3.5),
              color: AppColors.flatGold,
              alignText: AlignTextType.center,
            ),
          ),
        ),
      ],
    );
  }

  Widget _dialogButtonWrapper() {
    return Container(
      height: SizeConfig.vertical(8),
      decoration: BoxDecoration(
        border: Border(top: BorderSide(color: AppColors.flatGold)),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _buttonContent())
        ],
      ),
    );
  }

  Widget _buttonContent() {
    return Expanded(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          MontserratTextView(
            value: 'UPDATE',
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3.5),
            color: AppColors.flatGold,
            alignText: AlignTextType.center,
          ),
          RippleButton(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SizeConfig.horizontal(3)),
              bottomRight: Radius.circular(SizeConfig.horizontal(3)),
            ),
            onTap: onTap,
          ),
        ],
      ),
    );
  }
}
