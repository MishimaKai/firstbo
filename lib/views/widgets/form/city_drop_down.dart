import 'package:flutter/material.dart';

import '../../../models/data/province_city/province_city_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layouts/under_line.dart';
import '../text/montserrat_text_view.dart';

class CityDropDown extends StatelessWidget {
  const CityDropDown({
    required this.cityList,
    this.valueDisplay,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  final List<Datum> cityList;
  final int? valueDisplay;
  final Function(int?) onChanged;

  String onWillDisplay() {
    String containValue = 'Select city';
    if (valueDisplay == null) {
      return containValue;
    } else {
      for (final Datum element in cityList) {
        if (valueDisplay == element.id) {
          containValue = element.name;
        }
      }
    }

    return containValue;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DropdownButton<int>(
      hint: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
        child: MontserratTextView(
          value: onWillDisplay(),
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w300,
          size: SizeConfig.safeHorizontal(3),
        ),
      ),
      // value: "Select City",
      isDense: true,
      isExpanded: true,
      iconEnabledColor: AppColors.whiteBackground,
      style: TextStyle(
        color: AppColors.whiteBackground,
        fontWeight: FontWeight.w300,
      ),
      underline: const Underline(),
      items: cityList.map((Datum item) {
        return DropdownMenuItem<int>(
          value: item.id,
          child: MontserratTextView(
            value: item.name,
            color: AppColors.basicDark,
            fontWeight: FontWeight.w300,
          ),
        );
      }).toList(),
      onChanged: onChanged,
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
    );
  }
}
