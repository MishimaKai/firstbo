import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    Key? key,
    required this.controller,
    this.inputType,
    this.hintText,
    this.password,
    this.iconPrefix,
    this.changeIconSuffix,
    this.focusNode,
    this.errorText,
    this.autofillHint,
    this.labelText,
  }) : super(key: key);

  final TextEditingController controller;
  final TextInputType? inputType;
  final String? hintText;
  final bool? password;
  final IconData? iconPrefix;
  final bool? changeIconSuffix;
  final FocusNode? focusNode;
  final String? errorText;
  final Iterable<String>? autofillHint;
  final String? labelText;

  @override
  CustomTextFieldState createState() => CustomTextFieldState();
}

class CustomTextFieldState extends State<CustomTextField> {
  bool isShowPassword = true;

  dynamic onShowPassword() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.labelText ?? '',
            style: GoogleFonts.roboto(
              color: AppColors.basicDark,
              fontSize: SizeConfig.horizontal(4),
              fontWeight: FontWeight.w300,
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
        SizedBox(height: SizeConfig.vertical(1.2)),
        TextField(
          keyboardType: widget.inputType,
          controller: widget.controller,
          textAlign: TextAlign.left,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: widget.hintText,
            contentPadding: EdgeInsets.all(SizeConfig.vertical(1.5)),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            errorText: widget.errorText,
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.redAlert, width: 2),
            ),
            suffixIconConstraints: BoxConstraints(
              maxWidth: SizeConfig.horizontal(9),
            ),
            suffixIcon: widget.changeIconSuffix != null
                ? Container(
                    decoration: BoxDecoration(
                      // color: Colors.red,
                      borderRadius: BorderRadius.all(
                        Radius.circular(SizeConfig.horizontal(3)),
                      ),
                    ),
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Colors.transparent,
                        onTap: onShowPassword,
                        child: Ink(
                          width: SizeConfig.horizontal(10),
                          height: SizeConfig.horizontal(12),
                          child: Icon(
                            isShowPassword
                                ? Icons.visibility_off
                                : Icons.visibility,
                            color: isShowPassword
                                ? AppColors.basicDark
                                : AppColors.greyDisabled,
                          ),
                        ),
                      ),
                    ),
                  )
                : null,
          ),
          obscureText:
              // ignore: avoid_bool_literals_in_conditional_expressions
              widget.changeIconSuffix != null ? isShowPassword : false,
          focusNode: widget.focusNode,
          autofillHints: widget.autofillHint,
          style: GoogleFonts.roboto(
            color: AppColors.basicDark,
            fontStyle: FontStyle.normal,
          ),
        ),
      ],
    );
  }
}
