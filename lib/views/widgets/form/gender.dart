import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layouts/space_sizer.dart';
import '../layouts/under_line.dart';
import '../text/montserrat_text_view.dart';

class Gender extends StatelessWidget {
  const Gender({
    Key? key,
    required this.groupValue,
    required this.onChanged,
    this.isReadOnly = false,
    this.layoutType,
  }) : super(key: key);

  final String groupValue;
  final Function(Object?) onChanged;
  final bool isReadOnly;
  final ResponsiveRowColumnType? layoutType;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'register_gender_form_label'.tr,
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(child: _genderField()),
        ResponsiveRowColumnItem(
          child: layoutType != null
              ? layoutType == ResponsiveRowColumnType.COLUMN
                  ? const SizedBox.shrink()
                  : const Underline()
              : const Underline(),
        ),
      ],
    );
  }

  Widget _genderField() {
    if (layoutType != null) {
      if (layoutType == ResponsiveRowColumnType.ROW) {
        return Container(
          height: SizeConfig.vertical(5),
          padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            children: <ResponsiveRowColumnItem>[
              ResponsiveRowColumnItem(child: _maleRadio()),
              const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 6)),
              ResponsiveRowColumnItem(child: _femaleRadio()),
            ],
          ),
        );
      } else {
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnMainAxisSize: MainAxisSize.min,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: _maleRadio()),
            ResponsiveRowColumnItem(child: _femaleRadio()),
          ],
        );
      }
    } else {
      return Container(
        height: SizeConfig.vertical(5),
        padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.ROW,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: _maleRadio()),
            const ResponsiveRowColumnItem(child: SpaceSizer(horizontal: 6)),
            ResponsiveRowColumnItem(child: _femaleRadio()),
          ],
        ),
      );
    }
  }

  Widget _maleRadio() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Male',
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        ResponsiveRowColumnItem(
          child: Radio<String>(
            value: 'Male',
            groupValue: groupValue,
            onChanged: isReadOnly ? null : onChanged,
            activeColor: AppColors.flatGold,
            fillColor: MaterialStateProperty.all(AppColors.flatGold),
          ),
        ),
      ],
    );
  }

  Widget _femaleRadio() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'Female',
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
          // Text(
        ),
        ResponsiveRowColumnItem(
          child: Radio<String>(
            value: 'Female',
            groupValue: groupValue,
            onChanged: isReadOnly ? null : onChanged,
            activeColor: AppColors.flatGold,
            fillColor: MaterialStateProperty.all(AppColors.flatGold),
          ),
        ),
      ],
    );
  }
}
