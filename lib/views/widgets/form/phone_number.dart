import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../layouts/space_sizer.dart';
import '../layouts/under_line.dart';
import '../text/montserrat_text_view.dart';

class PhoneNumber extends StatelessWidget {
  const PhoneNumber({
    Key? key,
    required this.phoneNumber,
    required this.defaultCountry,
    required this.countryCode,
  }) : super(key: key);

  final String phoneNumber;
  final Widget defaultCountry;
  final String countryCode;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: 'register_phone_number_form_label'.tr,
            color: AppColors.whiteBackground,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1.5)),
        // ResponsiveRowColumnItem(child: _phoneNumberField(context)),
        ResponsiveRowColumnItem(child: _phoneNumberRead()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
        const ResponsiveRowColumnItem(child: Underline()),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        // ResponsiveRowColumnItem(child: MontserratTextError(value: errorText)),
      ],
    );
  }

  Widget _phoneNumberRead() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowCrossAxisAlignment: CrossAxisAlignment.end,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: Container(
            padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.2)),
            height: SizeConfig.vertical(2.5),
            child: defaultCountry,
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2)),
        ),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: countryCode,
            size: SizeConfig.safeHorizontal(4),
            color: AppColors.whiteBackground,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w300,
            alignText: AlignTextType.center,
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2)),
        ),
        ResponsiveRowColumnItem(
          child: MontserratTextView(
            value: phoneNumber,
            size: SizeConfig.safeHorizontal(4),
            color: AppColors.lightDark,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w300,
            alignText: AlignTextType.center,
          ),
        ),
      ],
    );
  }
}
