import 'package:flutter/material.dart';

import '../../../models/data/province_city/province_city_model.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layouts/under_line.dart';
import '../text/montserrat_text_view.dart';

class ProvinceDropDown extends StatelessWidget {
  const ProvinceDropDown({
    required this.provinceList,
    this.valueDisplay,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  final List<Datum> provinceList;
  final int? valueDisplay;
  final Function(int?) onChanged;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DropdownButton<int>(
      hint: Padding(
        padding: EdgeInsets.only(bottom: SizeConfig.vertical(0.5)),
        child: MontserratTextView(
          size: SizeConfig.safeHorizontal(2.8),
          value: valueDisplay == null
              ? 'Select Province'
              : provinceList[valueDisplay! - 1].name,
          color: AppColors.whiteBackground,
          fontWeight: FontWeight.w300,
        ),
      ),
      isDense: true,
      isExpanded: true,
      iconEnabledColor: AppColors.whiteBackground,
      underline: const Underline(),
      items: provinceList.map((Datum item) {
        return DropdownMenuItem<int>(
          value: item.id,
          child: MontserratTextView(
            value: item.name,
            color: AppColors.basicDark,
            fontWeight: FontWeight.w300,
          ),
        );
      }).toList(),
      onChanged: onChanged,
      onTap: () {
        final FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
    );
  }
}
