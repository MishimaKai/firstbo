import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../controllers/home/member_account_info_controller.dart';
import '../../../controllers/profile/profile_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/assets_list.dart';
import '../../../utils/size_config.dart';
import '../layouts/space_sizer.dart';
import '../text/montserrat_text_view.dart';
import 'frame_app_bar.dart';
import 'frame_widget_controller.dart';

class FrameScaffoldBottomNavigation extends StatelessWidget {
  // constructor
  FrameScaffoldBottomNavigation({
    Key? key,
    this.titleScreen,
    this.heightBar,
    this.color,
    this.isCenter,
    this.elevation,
    this.elevationAppBar,
    this.isUseLeading,
    this.onBack,
    this.customLeading,
    this.action,
    this.isImplyLeading,
    this.customTitle,
    this.colorScaffold,

    // system status bar
    this.statusBarColor,
    this.statusBarIconBrightness,
    this.statusBarBrightness,
  }) : super(key: key);

  final FrameController _controller = Get.find();
  final MemberAccountInfoController _infoController = Get.find();
  final ProfileController _profileController = Get.find();

  final String? titleScreen;
  final double? heightBar;
  final Color? color;
  final bool? isCenter;
  final double? elevation;
  final double? elevationAppBar;
  final bool? isUseLeading;
  final dynamic Function()? onBack;
  final Widget? customLeading;
  final Widget? action;
  final bool? isImplyLeading;
  final Widget? customTitle;
  final Color? colorScaffold;

  // system status bar
  final Color? statusBarColor;
  final Brightness? statusBarIconBrightness;
  final Brightness? statusBarBrightness;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DefaultTabController(
      length: _controller.widgetViewList.length,
      child: _getBuilderBottomNav(context),
    );
  }

  Obx _getBuilderBottomNav(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor: colorScaffold,
        extendBody: true,
        resizeToAvoidBottomInset: false,
        appBar: FrameAppBar(
          titleScreen: titleScreen,
          heightBar: heightBar,
          color: color,
          elevation: elevationAppBar,
          isCenter: isCenter,
          isUseLeading: isUseLeading,
          onBack: onBack,
          customLeading: customLeading,
          action: action,
          isImplyLeading: isImplyLeading,
          customTitle: customTitle,
          statusBarColor: statusBarColor,
          statusBarIconBrightness: statusBarIconBrightness,
          statusBarBrightness: statusBarBrightness,
        ),
        body: SafeArea(
          bottom: false,
          child: _controller.widgetViewList[_controller.defaultIndex.value],
        ),
        // body: _viewWrapper(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: _qrButton(context),
        bottomNavigationBar: _bottomBar(),
      );
    });
  }

  Widget _qrButton(BuildContext context) {
    return SizedBox(
      height: SizeConfig.horizontal(20),
      width: SizeConfig.horizontal(20),
      child: FloatingActionButton(
        backgroundColor: AppColors.flatGold,
        // elevation: 0,
        onPressed: () {
          // Get.toNamed(RouteNames.scanQRCode);
          _controller.onPressNavigateToQrCode(context);
        },
        shape: const SquircleBorder(
          side: BorderSide(width: 0, color: Colors.transparent),
        ),
        child: Image.asset(
          Assets.iconQr,
          width: SizeConfig.horizontal(13),
          height: SizeConfig.horizontal(13),
        ),
      ),
    );
  }

  Widget _bottomBar() {
    return BottomAppBar(
      shape: const AutomaticNotchedShape(RoundedRectangleBorder()),
      notchMargin: 0,
      color: Platform.isAndroid ? Colors.transparent : AppColors.flatGold,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      child: _bottomBarContent(),
    );
  }

  Widget _bottomBarContent() {
    return Container(
      color: Platform.isIOS ? AppColors.flatGold : null,
      decoration: Platform.isAndroid
          ? BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[
                  AppColors.flatGold,
                  AppColors.flatGold,
                  AppColors.flatGold,
                  AppColors.darkFlatGold,
                  // AppColors.darkFlatGold,
                ],
              ),
            )
          : null,
      child: BottomNavigationBar(
        onTap: (int index) {
          if (index == 0) {
            _infoController.getRequestMemberAccountInfo();
          } else if (index == 4) {
            _profileController.onRefreshData();
          }
          return _controller.onTapNavigation(index);
        },
        type: BottomNavigationBarType.fixed,
        elevation: 0,
        // backgroundColor: AppColors.flatGold,
        backgroundColor: Colors.transparent,
        selectedItemColor: AppColors.basicDark,
        unselectedItemColor: AppColors.basicDark,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        selectedLabelStyle: const TextStyle(fontSize: 0),
        unselectedLabelStyle: const TextStyle(fontSize: 0),
        iconSize: 0,
        items: <BottomNavigationBarItem>[
          _bottomNavigationBarItemDefault(
            asset: Assets.iconHome,
            label: 'Home',
            index: 0,
          ),
          _bottomNavigationBarItemDefault(
            asset: Assets.iconGallery,
            label: 'Gallery',
            index: 1,
          ),
          const BottomNavigationBarItem(icon: SizedBox.shrink(), label: ''),
          _bottomNavigationBarItemDefault(
            asset: Assets.iconPoint,
            label: 'Redeem',
            index: 3,
          ),
          _bottomNavigationBarItemDefault(
            asset: Assets.iconProfile,
            label: 'Profile',
            index: 4,
          ),
        ],
      ),
    );
  }

  BottomNavigationBarItem _bottomNavigationBarItemDefault({
    required String asset,
    required String label,
    required int index,
    double? widthIcon,
    double? heightIcon,
  }) {
    return BottomNavigationBarItem(
      icon: Padding(
        padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
        child: _menuItemWrapper(
          asset: asset,
          label: label,
          index: index,
          widthIcon: widthIcon,
          heightIcon: heightIcon,
        ),
      ),
      label: '',
    );
  }

  Widget _menuItemWrapper({
    required String asset,
    required String label,
    required int index,
    double? widthIcon,
    double? heightIcon,
  }) {
    if (_controller.onTapIdentifierList[index].isOnTapped) {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.end,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              asset,
              width: SizeConfig.horizontal(widthIcon ?? 6),
              height: SizeConfig.horizontal(heightIcon ?? 6),
              fit: BoxFit.cover,
              color: AppColors.darkBackground,
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: label,
              size: SizeConfig.safeHorizontal(
                widthIcon == null && heightIcon == null ? 3 : 2.8,
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 0.5)),
          ResponsiveRowColumnItem(
            child: Container(
              height: SizeConfig.vertical(0.5),
              width: SizeConfig.horizontal(10),
              color: AppColors.basicDark,
            ),
          ),
        ],
      );
    } else {
      return ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisAlignment: MainAxisAlignment.center,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Image.asset(
              asset,
              width: SizeConfig.horizontal(widthIcon ?? 6),
              height: SizeConfig.horizontal(heightIcon ?? 6),
              fit: BoxFit.cover,
              color: AppColors.darkBackground.withOpacity(0.5),
            ),
          ),
          ResponsiveRowColumnItem(
            child: MontserratTextView(
              value: label,
              size: SizeConfig.safeHorizontal(3),
              color: AppColors.darkBackground.withOpacity(0.5),
            ),
          ),
        ],
      );
    }
  }
}

class SquircleBorder extends ShapeBorder {
  const SquircleBorder({
    this.side = BorderSide.none,
    this.superRadius = 5.0,
  });

  final BorderSide side;
  final double superRadius;

  static Path _squirclePath(Rect rect, double superRadius) {
    final Offset c = rect.center;
    final double dx = c.dx * (1.0 / superRadius);
    final double dy = c.dy * (1.0 / superRadius);
    return Path()
      ..moveTo(c.dx, 0.0)
      ..relativeCubicTo(c.dx - dx, 0.0, c.dx, dy, c.dx, c.dy)
      ..relativeCubicTo(0.0, c.dy - dy, -dx, c.dy, -c.dx, c.dy)
      ..relativeCubicTo(-(c.dx - dx), 0.0, -c.dx, -dy, -c.dx, -c.dy)
      ..relativeCubicTo(0.0, -(c.dy - dy), dx, -c.dy, c.dx, -c.dy)
      ..close();
  }

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.all(side.width);

  @override
  Path getInnerPath(Rect rect, {TextDirection? textDirection}) {
    return _squirclePath(rect.deflate(side.width), superRadius);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection? textDirection}) {
    return _squirclePath(rect, superRadius);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection? textDirection}) {
    switch (side.style) {
      case BorderStyle.none:
        break;
      case BorderStyle.solid:
        final Path path = getOuterPath(rect.deflate(side.width / 2.0),
            textDirection: textDirection);
        canvas.drawPath(path, side.toPaint());
    }
  }

  @override
  ShapeBorder scale(double t) {
    return SquircleBorder(
      side: side.scale(t),
      superRadius: superRadius * t,
    );
  }
}
