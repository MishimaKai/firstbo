import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

class FrameSheet extends StatelessWidget {
  // constructor
  const FrameSheet({
    Key? key,
    required this.height,
    required this.child,
    this.backgroundColor,
  }) : super(key: key);

  final double height;
  final Widget child;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: height,
      padding: EdgeInsets.symmetric(vertical: SizeConfig.vertical(2)),
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.whiteBackground,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(SizeConfig.horizontal(2)),
          topRight: Radius.circular(SizeConfig.horizontal(2)),
        ),
      ),
      child: child,
    );
  }
}
