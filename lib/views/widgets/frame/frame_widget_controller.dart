import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helpers/shared_pref.dart';
import '../../../models/UI/frame/on_tap_identifier.dart';
import '../../../routes/route_names.dart';
import '../../../utils/global_value.dart';
import '../../gallery/pages/gallery_page.dart';
import '../../home/pages/home_page.dart';
import '../../profile/pages/profile_page.dart';
import '../../redeem/redeem_page.dart';
import '../layouts/montserrat_snackbar.dart';

class FrameController extends GetxController {
  final SharedPref _sharedPref = SharedPref();
  RxInt defaultIndex = RxInt(0);
  RxInt initateCategory = RxInt(GlobalValue.redeemCategoryId);
  RxList<OnTapIdentifier> onTapIdentifierList =
      RxList<OnTapIdentifier>(<OnTapIdentifier>[
    OnTapIdentifier(name: 'Home', index: 0, isOnTapped: true),
    OnTapIdentifier(name: 'Gallery', index: 1, isOnTapped: false),
    OnTapIdentifier(name: '', index: 2, isOnTapped: false),
    OnTapIdentifier(name: 'Redeem', index: 3, isOnTapped: false),
    OnTapIdentifier(name: 'Profile', index: 4, isOnTapped: false),
  ]);
  List<Widget> widgetViewList = <Widget>[
    const HomePage(),
    const GalleryPage(),
    const SizedBox.shrink(),
    const RedeemPage(),
    const ProfilePage(),
  ];
  RxBool isOutsideFrame = RxBool(false);

  @override
  void onInit() {
    initateCategorySharedPref();
    super.onInit();
  }

  dynamic initateCategorySharedPref() async {
    final int? readId = await _sharedPref.readRedeemCategoryId();
    if (readId != null) {
      initateCategory.value = readId;
      GlobalValue.redeemCategoryId = initateCategory.value;
    }
  }

  // function for change state value
  void onTapNavigation(int index) {
    defaultIndex.value = index;
    if (GlobalValue.redeemCategoryId != initateCategory.value) {
      initateCategorySharedPref();
    } else {
      GlobalValue.redeemCategoryId = initateCategory.value;
    }
    for (final OnTapIdentifier element in onTapIdentifierList) {
      if (element.index == index) {
        onTapIdentifierList[index].isOnTapped = true;
      } else {
        onTapIdentifierList[element.index].isOnTapped = false;
      }
    }
  }

  dynamic onPressNavigateToQrCode(BuildContext context) async {
    final dynamic result = await Get.toNamed(RouteNames.scanQRCode);
    if (result != null) {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context).showSnackBar(
        montserratSnackbar(
            title: (result as Map<String, dynamic>)['title'].toString(),
            message: result['message'].toString(),
            status: result['success'] as bool),
      );
    }
  }
}
