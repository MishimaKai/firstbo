import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../controllers/home/member_account_info_controller.dart';
import '../../../controllers/profile/profile_controller.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/page_names.dart';
import '../../../utils/size_config.dart';
import '../frame/frame_scaffold_bottom_nav.dart';
import '../frame/frame_widget_controller.dart';
import 'frame_action_wrapper.dart';

class FrameView extends StatefulWidget {
  const FrameView({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => FrameViewState();
}

class FrameViewState extends State<FrameView> {
  final FrameController _controller = Get.put(
    FrameController(),
    permanent: true,
  );

  @override
  void initState() {
    super.initState();
    Get.put(MemberAccountInfoController(pageContext: context), permanent: true);
    Get.put(ProfileController(pageContext: context), permanent: true);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.darkFlatGold,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
      child: Obx(
        () => FrameScaffoldBottomNavigation(
          isImplyLeading: false,
          elevationAppBar: 0,
          heightBar: _whenUseHeightBar(),
          colorScaffold: AppColors.darkBackground,
          isCenter: false,
          action: _whenUseCustomLeading(),
          statusBarColor: AppColors.darkBackground,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
        ),
      ),
    );
  }

  double _whenUseHeightBar() {
    if (_controller.defaultIndex.value == 0) {
      return 0;
    } else {
      return 45.0;
    }
  }

  Widget? _whenUseCustomLeading() {
    if (_controller.defaultIndex.value == 1) {
      return FrameActionWrapper(
        title: PageNames.galleryPage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          _controller.onTapNavigation(0);
        },
      );
    } else if (_controller.defaultIndex.value == 3) {
      return FrameActionWrapper(
        title: PageNames.redeemPage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          _controller.onTapNavigation(0);
        },
      );
    } else if (_controller.defaultIndex.value == 4) {
      return FrameActionWrapper(
        title: PageNames.profilePage,
        fontWeight: FontWeight.bold,
        onPressed: () {
          _controller.onTapNavigation(0);
        },
      );
    } else {
      return null;
    }
  }
}
