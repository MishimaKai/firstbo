import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';

class CustomEmptyState extends StatelessWidget {
  const CustomEmptyState({
    Key? key,
    this.height,
    required this.caption,
  }) : super(key: key);

  final double? height;
  final String caption;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      height: height != null ? SizeConfig.vertical(height!) : null,
      width: SizeConfig.screenWidth,
      child: Center(
        child: MontserratTextView(
          value: caption,
          color: AppColors.whiteBackground,
          alignText: AlignTextType.center,
          fontWeight: FontWeight.w300,
          size: SizeConfig.safeHorizontal(3.5),
        ),
      ),
    );
  }
}
