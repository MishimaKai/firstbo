import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../models/data/outlet_info/outlet_info_model.dart';
import '../../../routes/route_names.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../buttons/ripple_button.dart';
import 'shimmer_placeholder.dart';

class EventItem extends StatelessWidget {
  const EventItem({
    required this.id,
    required this.image,
    required this.outletInfo,
    required this.startDate,
    required this.lengthItem,
    required this.index,
    required this.typeEvent,
    Key? key,
  }) : super(key: key);

  final int id;
  final String image;
  final List<OutletInfoModel> outletInfo;
  final String startDate;
  final int lengthItem;
  final int index;
  final TypeEventItem typeEvent;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ClipRRect(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          SizedBox(
            width: SizeConfig.horizontal(80),
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.cover,
              placeholder: (BuildContext context, String url) {
                return ShimmerPlaceholder(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.vertical(35),
                  borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
                );
              },
              errorWidget: (BuildContext context, String url, dynamic error) =>
                  const SizedBox.shrink(),
            ),
          ),
          RippleButton(
            onTap: () {
              Get.toNamed(RouteNames.eventsDetailPage, arguments: id);
            },
            radius: 2,
          )
        ],
      ),
    );
  }
}
