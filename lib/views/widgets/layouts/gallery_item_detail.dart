import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import 'shimmer_placeholder.dart';

class GalleryItemThumbnail extends StatelessWidget {
  const GalleryItemThumbnail({
    Key? key,
    required this.displayOrder,
    required this.image,
    required this.onTap,
  }) : super(key: key);

  final int displayOrder;
  final String image;
  final GestureTapCallback onTap;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.vertical(1),
        horizontal: SizeConfig.horizontal(2),
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Hero(
          tag: displayOrder,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
            // child: Image.network(
            //   image,
            //   width: SizeConfig.horizontal(25),
            //   height: SizeConfig.vertical(20),
            //   fit: BoxFit.cover,
            // ),
            child: CachedNetworkImage(
              imageUrl: image,
              fit: BoxFit.cover,
              width: SizeConfig.horizontal(25),
              height: SizeConfig.vertical(20),
              placeholder: (BuildContext context, String url) {
                return ShimmerPlaceholder(
                  width: SizeConfig.horizontal(25),
                  height: SizeConfig.vertical(20),
                  borderRadius: BorderRadius.circular(
                    SizeConfig.horizontal(2.5),
                  ),
                );
              },
              errorWidget: (BuildContext context, String url, dynamic error) =>
                  const SizedBox.shrink(),
            ),
          ),
        ),
      ),
    );
  }
}
