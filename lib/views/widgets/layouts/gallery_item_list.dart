import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import 'shimmer_placeholder.dart';

class GalleryItemList extends StatelessWidget {
  const GalleryItemList({
    required this.image,
    required this.lengthItem,
    required this.index,
    Key? key,
  }) : super(key: key);

  final String image;
  final int lengthItem;
  final int index;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.only(
        left: SizeConfig.horizontal(5),
        right: index == lengthItem - 1 ? SizeConfig.horizontal(5) : 0,
      ),
      height: SizeConfig.vertical(16),
      width: SizeConfig.horizontal(75),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        // child: Image.network(image, fit: BoxFit.cover),
        child: CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.cover,
          placeholder: (BuildContext context, String url) {
            return ShimmerPlaceholder(
              width: SizeConfig.horizontal(75),
              height: SizeConfig.vertical(16),
              borderRadius: BorderRadius.circular(
                SizeConfig.horizontal(2.5),
              ),
            );
          },
          errorWidget: (BuildContext context, String url, dynamic error) =>
              const SizedBox.shrink(),
        ),
      ),
    );
  }
}
