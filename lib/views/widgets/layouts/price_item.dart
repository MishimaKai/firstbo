import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../helpers/string_helper.dart';
import '../../../routes/route_names.dart';
import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../buttons/ripple_button.dart';
import '../text/montserrat_text_view.dart';
import 'shimmer_placeholder.dart';

class PriceItem extends StatelessWidget {
  const PriceItem({
    required this.id,
    required this.image,
    required this.productName,
    required this.price,
    required this.lengthItem,
    required this.index,
    required this.typeShowPriceItem,
    required this.typePriceItem,
    required this.navigatePriceItem,
    Key? key,
  }) : super(key: key);

  final int id;
  final String image;
  final String productName;
  final int price;
  final int lengthItem;
  final int index;
  final TypeShowPriceItem typeShowPriceItem;
  final TypePriceItem typePriceItem;
  final NavigatePriceItem navigatePriceItem;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: typeShowPriceItem == TypeShowPriceItem.isDisplayForHome
          ? EdgeInsets.only(
              left: SizeConfig.horizontal(1.5),
              right: index == lengthItem - 1 ? SizeConfig.horizontal(2.5) : 0,
            )
          : null,
      child: _contentWrapper(context),
    );
  }

  Widget _contentWrapper(BuildContext context) {
    return SizedBox(
      width: SizeConfig.horizontal(30),
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.all(Radius.circular(SizeConfig.horizontal(2))),
              color: AppColors.itemDark,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
            ),
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.COLUMN,
              // columnMainAxisSize: MainAxisSize.min,
              columnCrossAxisAlignment: CrossAxisAlignment.start,
              children: <ResponsiveRowColumnItem>[
                ResponsiveRowColumnItem(child: _imageWrapper(context)),
                ResponsiveRowColumnItem(child: _priceinfoItem(context)),
              ],
            ),
          ),
          RippleButton(
            onTap: () {
              if (navigatePriceItem == NavigatePriceItem.none) {
                return null;
              } else {
                navigatePriceItem == NavigatePriceItem.toFoodPackages
                    ? getx.Get.toNamed(RouteNames.foodItemDetailPage,
                        arguments: id)
                    : navigatePriceItem == NavigatePriceItem.toBottlePackages
                        ? getx.Get.toNamed(RouteNames.bottleItemDetailPage,
                            arguments: id)
                        : getx.Get.toNamed(RouteNames.redeemItemDetailPage,
                            arguments: id);
              }
            },
            radius: 2,
          ),
        ],
      ),
    );
  }

  Widget _imageWrapper(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(SizeConfig.horizontal(2)),
        topRight: Radius.circular(SizeConfig.horizontal(2)),
      ),
      child: AspectRatio(
        aspectRatio: 1 / 1,
        child: CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.cover,
          width: SizeConfig.horizontal(30),
          placeholder: (BuildContext context, String url) {
            return ShimmerPlaceholder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(SizeConfig.horizontal(2)),
                topRight: Radius.circular(SizeConfig.horizontal(2)),
              ),
              width: SizeConfig.horizontal(30),
              height: SizeConfig.vertical(25),
            );
          },
          errorWidget: (BuildContext context, String url, dynamic error) =>
              const SizedBox.shrink(),
        ),
      ),
    );
  }

  Widget _priceinfoItem(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.horizontal(2),
          vertical: SizeConfig.vertical(1),
        ),
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnCrossAxisAlignment: CrossAxisAlignment.start,
          columnMainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: _nameItem()),
            ResponsiveRowColumnItem(child: _priceItem(context)),
          ],
        ),
      ),
    );
  }

  Widget _nameItem() {
    return MontserratTextView(
//      value: productName.length > 35
//          ? '${productName.substring(0, 35)}...'
//          : productName,
      value: productName,
      color: AppColors.whiteBackground,
      size: SizeConfig.safeHorizontal(3),
      fontWeight: FontWeight.w300,
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
    );
  }

  Widget _priceItem(BuildContext context) {
    if (typePriceItem == TypePriceItem.isPriceCurrency) {
      if (price != 0) {
        return Flexible(
          child: RichText(
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              style: _priceTextStyle(context),
              children: _typePriceSpan(),
            ),
          ),
        );
      } else {
        return SizedBox(
          height: ResponsiveValue<double>(
            context,
            defaultValue: SizeConfig.horizontal(5),
            valueWhen: <Condition<double>>[
              Condition<double>.equals(
                name: MOBILE,
                value: SizeConfig.horizontal(3),
              ),
            ],
          ).value,
        );
      }
    } else {
      return Flexible(
        child: RichText(
          overflow: TextOverflow.ellipsis,
          text: TextSpan(
            style: _priceTextStyle(context),
            children: _typePointSpan(),
          ),
        ),
      );
    }
  }

  TextStyle _priceTextStyle(BuildContext context) {
    return GoogleFonts.montserrat(
      color: AppColors.flatGold,
      fontSize: ResponsiveValue<double>(
        context,
        defaultValue: SizeConfig.safeHorizontal(3.5),
        valueWhen: <Condition<double>>[
          Condition<double>.equals(
            name: MOBILE,
            value: SizeConfig.safeHorizontal(3.5),
          ),
        ],
      ).value,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
    );
  }

  List<InlineSpan> _typePriceSpan() {
    return <TextSpan>[
      TextSpan(
        text: 'Rp ',
        style: TextStyle(fontSize: SizeConfig.safeHorizontal(3.5)),
      ),
      TextSpan(text: StringHelper.convertToRupiah(price)),
    ];
  }

  List<InlineSpan> _typePointSpan() {
    return <TextSpan>[
      TextSpan(text: price.toString()),
      TextSpan(
        text: ' points',
        style: TextStyle(fontSize: SizeConfig.safeHorizontal(3.5)),
      ),
    ];
  }
}
