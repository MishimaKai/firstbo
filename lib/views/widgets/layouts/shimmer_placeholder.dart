import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';

class ShimmerPlaceholder extends StatelessWidget {
  const ShimmerPlaceholder({
    required this.width,
    this.height,
    this.borderRadius,
    this.shape,
    this.baseColor,
    this.lightColor,
    Key? key,
  }) : super(key: key);

  final double width;
  final double? height;
  final BorderRadiusGeometry? borderRadius;
  final BoxShape? shape;
  final Color? baseColor;
  final Color? lightColor;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Shimmer.fromColors(
      baseColor: baseColor ?? AppColors.shimmerBase,
      highlightColor: lightColor ?? AppColors.shimmerHightlight,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: borderRadius,
          shape: shape ?? BoxShape.rectangle,
        ),
        height: height ?? SizeConfig.vertical(2),
        width: width,
      ),
    );
  }
}
