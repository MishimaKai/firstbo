import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';

class SpaceSizer extends StatelessWidget {
  const SpaceSizer({
    this.vertical,
    this.horizontal,
    Key? key,
  }) : super(key: key);

  final double? vertical;
  final double? horizontal;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
      height: vertical != null ? SizeConfig.vertical(vertical!) : null,
      width: horizontal != null ? SizeConfig.horizontal(horizontal!) : null,
    );
  }
}
