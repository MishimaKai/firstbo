import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../text/montserrat_text_view.dart';

class Tabs extends StatelessWidget {
  const Tabs({
    required this.tabNames,
    required this.views,
    this.fontSize,
    this.controller,
    Key? key,
  }) : super(key: key);

  final List<String> tabNames;
  final List<Widget> views;
  final Double? fontSize;
  final TabController? controller;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabNames.length,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _tabBar()),
          ResponsiveRowColumnItem(child: _tabBarView()),
        ],
      ),
    );
  }

  Widget _tabBar() {
    return TabBar(
      controller: controller,
      isScrollable: tabNames.length > 2,
      indicatorColor: AppColors.flatGold,
      tabs: _tabName(),
    );
  }

  Widget _tabBarView() {
    return Expanded(
      child: TabBarView(
        controller: controller,
        children: views,
      ),
    );
  }

  List<Widget> _tabName() {
    // contain list
    final List<Widget> containList = <Widget>[];
    // loop the list param and then add to container list
    for (final String item in tabNames) {
      containList.add(
        Padding(
          padding: EdgeInsets.all(SizeConfig.horizontal(4)),
          child: MontserratTextView(
            value: item,
            fontWeight: FontWeight.w300,
            size: SizeConfig.safeHorizontal(3),
            color: AppColors.whiteBackground,
          ),
        ),
      );
    }

    return containList;
  }
}
