import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';

class BadgeShimmer extends StatelessWidget {
  const BadgeShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.all(SizeConfig.horizontal(1)),
      child: ShimmerPlaceholder(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        width: SizeConfig.horizontal(20),
        height: SizeConfig.vertical(4),
      ),
    );
  }
}
