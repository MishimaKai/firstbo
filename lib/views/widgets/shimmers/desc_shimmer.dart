import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';
import '../layouts/space_sizer.dart';

class DescShimmer extends StatelessWidget {
  const DescShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      color: AppColors.darkBackground,
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Container(
              width: SizeConfig.screenWidth,
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
                vertical: SizeConfig.vertical(2),
              ),
              child: _titleShimmer(),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: Container(
              width: SizeConfig.screenWidth,
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2.5),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: List<Widget>.generate(
                  10,
                  (int index) => _contentShimmer(index),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _titleShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
            width: SizeConfig.screenWidth / 2,
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
            width: SizeConfig.horizontal(35),
          ),
        ),
      ],
    );
  }

  Widget _contentShimmer(int index) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(3)),
            width: index.isOdd
                ? SizeConfig.screenWidth / 2
                : SizeConfig.screenWidth / 1.5,
          ),
        ),
        const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
      ],
    );
  }
}
