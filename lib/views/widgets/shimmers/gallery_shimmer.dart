import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';
import '../layouts/space_sizer.dart';

class GalleryShimmer extends StatelessWidget {
  const GalleryShimmer({
    required this.length,
    Key? key,
  }) : super(key: key);

  final int length;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: List<Widget>.generate(
        length,
        (int index) => _itemGalleryShimmer(),
      ),
    );
  }

  Widget _itemGalleryShimmer() {
    return Container(
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(2)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
              child: ShimmerPlaceholder(
                height: SizeConfig.vertical(25),
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 2)),
          ResponsiveRowColumnItem(
            child: ShimmerPlaceholder(
              width: SizeConfig.screenWidth / 2,
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: ShimmerPlaceholder(
              width: SizeConfig.screenWidth / 3,
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(5)),
            ),
          ),
        ],
      ),
    );
  }
}
