import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';
import '../layouts/space_sizer.dart';

class HomeShimmer extends StatelessWidget {
  const HomeShimmer({this.isForDualScreen, Key? key}) : super(key: key);

  final bool? isForDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _personShimmerWrapper()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(
            child: ShimmerPlaceholder(
              width: SizeConfig.screenWidth,
              height: SizeConfig.vertical(8),
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
            ),
          ),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(child: _labelEventShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(child: _listEventShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(child: _labelPromoShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 1)),
          ResponsiveRowColumnItem(child: _listPromoShimmer(context)),
        ],
      ),
    );
  }

  Widget _personShimmerWrapper() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _personShimmer()),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(8),
            height: SizeConfig.horizontal(8),
            shape: BoxShape.circle,
          ),
        ),
      ],
    );
  }

  Widget _personShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(12),
            height: SizeConfig.horizontal(12),
            shape: BoxShape.circle,
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(width: SizeConfig.horizontal(2.5)),
        ),
        ResponsiveRowColumnItem(child: _personShimmerContent()),
      ],
    );
  }

  Widget _personShimmerContent() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(15),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(1)),
        ),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(12),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
      ],
    );
  }

  Widget _labelEventShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(20),
            height: SizeConfig.vertical(2),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(10),
            height: SizeConfig.vertical(2),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
      ],
    );
  }

  Widget _listEventShimmer() {
    return SizedBox(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(35),
      child: CarouselSlider.builder(
        itemCount: 5,
        itemBuilder: (BuildContext context, int index, int realIndex) {
          return ShimmerPlaceholder(
            width: SizeConfig.screenWidth,
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          );
        },
        options: CarouselOptions(
          autoPlay: true,
          enlargeCenterPage: true,
          aspectRatio: 1 / 2,
        ),
      ),
    );
  }

  Widget _labelPromoShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.ROW,
      rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(20),
            height: SizeConfig.vertical(2),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            width: SizeConfig.horizontal(10),
            height: SizeConfig.vertical(2),
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
          ),
        ),
      ],
    );
  }

  Widget _listPromoShimmer(BuildContext context) {
    return SizedBox(
      height: isForDualScreen ?? false
          ? SizeConfig.vertical(25)
          : ResponsiveValue<double>(
              context,
              defaultValue: SizeConfig.vertical(22),
              valueWhen: <Condition<double>>[
                Condition<double>.equals(
                  name: MOBILE,
                  value: SizeConfig.vertical(20),
                ),
              ],
            ).value,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            padding: EdgeInsets.only(
              left: index == 0 ? 0 : SizeConfig.horizontal(2.5),
              right: index == 5 - 1 ? SizeConfig.horizontal(5) : 0,
            ),
            child: ShimmerPlaceholder(
              borderRadius: BorderRadius.circular(SizeConfig.horizontal(2.5)),
              width: SizeConfig.horizontal(30),
              height: isForDualScreen ?? false
                  ? SizeConfig.vertical(25)
                  : ResponsiveValue<double>(
                      context,
                      defaultValue: SizeConfig.vertical(22),
                      valueWhen: <Condition<double>>[
                        Condition<double>.equals(
                          name: MOBILE,
                          value: SizeConfig.vertical(20),
                        ),
                      ],
                    ).value,
            ),
          );
        },
      ),
    );
  }
}
