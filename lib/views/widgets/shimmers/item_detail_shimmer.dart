import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/enums.dart';
import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';
import 'desc_shimmer.dart';

class ItemDetailShimmer extends StatelessWidget {
  const ItemDetailShimmer({
    this.type = TypeDetailShimmer.generic,
    this.height,
    this.borderRadius,
    this.isForDualScreen,
    Key? key,
  }) : super(key: key);

  final TypeDetailShimmer type;
  final double? height;
  final BorderRadiusGeometry? borderRadius;
  final bool? isForDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    if (type == TypeDetailShimmer.generic) {
      return _viewDetailWrapper();
    } else {
      return _viewDetailSpecifyWrapper();
    }
  }

  Widget _viewDetailWrapper() {
    return Container(
      color: AppColors.darkBackground,
      margin: EdgeInsets.only(bottom: SizeConfig.vertical(1)),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _bannerEventShimmer()),
          const ResponsiveRowColumnItem(child: DescShimmer()),
        ],
      ),
    );
  }

  Widget _viewDetailSpecifyWrapper() {
    return Container(
      color: AppColors.darkBackground,
      padding: EdgeInsets.only(
        left: SizeConfig.horizontal(1),
        right: SizeConfig.horizontal(1),
        top: SizeConfig.vertical(2),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(2),
              ),
              child: _bannerEventShimmer(),
            ),
          ),
          const ResponsiveRowColumnItem(child: DescShimmer()),
        ],
      ),
    );
  }

  Widget _bannerEventShimmer() {
    return ShimmerPlaceholder(
      borderRadius: borderRadius,
      width: SizeConfig.screenWidth,
      height: height ?? SizeConfig.vertical(40),
    );
  }
}
