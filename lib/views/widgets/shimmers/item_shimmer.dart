import 'package:flutter/material.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';

class ItemShimmer extends StatelessWidget {
  const ItemShimmer({
    required this.length,
    this.height,
    this.isAdjustTop,
    this.radius,
    Key? key,
  }) : super(key: key);

  final int length;
  final double? height;
  final bool? isAdjustTop;
  final double? radius;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: List<Widget>.generate(
        length,
        (int index) => _itemVoucherShimmer(index),
      ),
    );
  }

  Widget _itemVoucherShimmer(int index) {
    return Container(
      margin: EdgeInsets.only(
        // ignore: use_if_null_to_convert_nulls_to_bools
        top: isAdjustTop == true && index == 0 ? SizeConfig.vertical(1) : 0,
        bottom: SizeConfig.vertical(1),
      ),
      child: ShimmerPlaceholder(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(radius ?? 2)),
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(height ?? 25),
      ),
    );
  }
}
