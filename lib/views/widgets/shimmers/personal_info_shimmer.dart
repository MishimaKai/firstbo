import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';

class PersonalInfoShimmer extends StatelessWidget {
  const PersonalInfoShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(child: _cardWrapper()),
      ],
    );
  }

  Widget _cardWrapper() {
    return SizedBox(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          _memberProfileCardShimmer(),
          _memberProfilePictureShimmer(),
        ],
      ),
    );
  }

  Widget _memberProfileCardShimmer() {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      children: <ResponsiveRowColumnItem>[
        ResponsiveRowColumnItem(
          child: SizedBox(height: SizeConfig.vertical(10)),
        ),
        ResponsiveRowColumnItem(
          child: ShimmerPlaceholder(
            borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
            width: SizeConfig.screenWidth,
            height: SizeConfig.vertical(45),
          ),
        ),
      ],
    );
  }

  Widget _memberProfilePictureShimmer() {
    return SizedBox(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            width: SizeConfig.horizontal(45),
            height: SizeConfig.horizontal(45),
            decoration: BoxDecoration(
              color: AppColors.darkBackground,
              shape: BoxShape.circle,
            ),
          ),
          ShimmerPlaceholder(
            width: SizeConfig.horizontal(38),
            height: SizeConfig.horizontal(38),
            shape: BoxShape.circle,
          ),
        ],
      ),
    );
  }
}
