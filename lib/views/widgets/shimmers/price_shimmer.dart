import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';

class PriceShimmer extends StatelessWidget {
  const PriceShimmer({
    required this.length,
    Key? key,
  }) : super(key: key);

  final int length;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GridView.count(
      crossAxisCount: 3,
      childAspectRatio: 2 / 3,
      mainAxisSpacing: SizeConfig.vertical(1),
      crossAxisSpacing: SizeConfig.horizontal(2.5),
      children: List<Widget>.generate(
        length,
        (int index) => _itemShimmer(context: context),
      ),
    );
  }

  Widget _itemShimmer({required BuildContext context}) {
    return Container(
      padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
      child: ShimmerPlaceholder(
        borderRadius: BorderRadius.circular(SizeConfig.horizontal(2)),
        width: SizeConfig.horizontal(35),
        height: ResponsiveValue<double>(
          context,
          defaultValue: SizeConfig.vertical(22),
          valueWhen: <Condition<double>>[
            Condition<double>.equals(
              name: MOBILE,
              value: SizeConfig.vertical(25),
            ),
          ],
        ).value,
      ),
    );
  }
}
