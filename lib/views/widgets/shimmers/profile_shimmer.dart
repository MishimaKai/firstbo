import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_row_column.dart';

import '../../../utils/size_config.dart';
import '../layouts/shimmer_placeholder.dart';
import '../layouts/space_sizer.dart';

class ProfileShimmer extends StatelessWidget {
  const ProfileShimmer({this.isForDualScreen, Key? key}) : super(key: key);

  final bool? isForDualScreen;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.horizontal(8),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: <ResponsiveRowColumnItem>[
          ResponsiveRowColumnItem(child: _qrButtonShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
          ResponsiveRowColumnItem(child: _titleShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
          ResponsiveRowColumnItem(child: _myMembershipShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
          ResponsiveRowColumnItem(child: _titleShimmer()),
          const ResponsiveRowColumnItem(child: SpaceSizer(vertical: 3)),
          ResponsiveRowColumnItem(child: _myAccountShimmer()),
        ],
      ),
    );
  }

  Widget _qrButtonShimmer() {
    return ShimmerPlaceholder(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
      width: SizeConfig.horizontal(80),
      height: SizeConfig.vertical(5),
    );
  }

  Widget _titleShimmer() {
    return ShimmerPlaceholder(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
      width: SizeConfig.horizontal(40),
      height: SizeConfig.vertical(3.5),
    );
  }

  Widget _myMembershipShimmer() {
    return ShimmerPlaceholder(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
      width: SizeConfig.horizontal(80),
      height: SizeConfig.vertical(25),
    );
  }

  Widget _myAccountShimmer() {
    return ShimmerPlaceholder(
      borderRadius: BorderRadius.circular(SizeConfig.horizontal(10)),
      width: SizeConfig.horizontal(80),
      height: SizeConfig.vertical(40),
    );
  }
}
