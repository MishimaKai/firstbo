#! /bin/bash
############################################################
# Help                                                     #
############################################################
Help() {
  # Display Help
  echo
  echo "This program will help us to release application in single command"
  echo
  echo "Usage: ./release.sh [-h|-d|-r]"
  echo
  echo "options:"
  echo "-h,        Print this Help."
  echo "-d,        Release demo version"
  echo "-r,        Release live version."
  echo
}


############################################################
# Clean                                                    #
############################################################
CleanUp() {
  # Clean the flutter project
  fvm flutter clean
  # clean android dependencies
  cd android/
  ./gradlew clean
  # clearn ios dependencies
  cd ..
  cd ios/
  pod cache clean --all
  cd ..
}

############################################################
# Install                                                  #
############################################################
Install() {
  # pub get flutter project
  fvm flutter pub get
  # make sure depedency installed or config gradle are correct
  cd android/
  ./gradlew --refresh-dependencies
  cd ..
  # install dependency for platform ios
  cd ios/
  pod deintegrate
  pod setup
  pod install
  cd ..
}

# variables
# file service for android
android_live_service=android/app/google-services-live.json  # mark file and disable live version for android
android_demo_service=android/app/google-services-demo.json  # mark file and disable demo version for android
android_core_service=android/app/google-services.json       # rename file live or demo for activate google service for android
# file service for ios
ios_live_service=ios/Runner/GoogleService-Info-live.plist   # mark file and disable live version for ios
ios_demo_service=ios/Runner/GoogleService-Info-demo.plist   # mark file and disable demo version for ios
ios_core_service=ios/Runner/GoogleService-Info.plist        # rename file live or demo for activate google service for ios

error_message="Rename File Service Failed: Check you service file first"
demo_success="Configuration For Demo Version Success!"
live_success="Configuration For Live Version Success!"

# Get the options
while getopts "drh" option; do
  case $option in
    d)# execute ./release.sh -d
      # check if file demo is exist
      if [ -f "$android_demo_service" ]; then
        # activate demo version
        # android
        mv $android_core_service $android_live_service          # disable live service for android
        mv $android_demo_service $android_core_service          # enable demo service for android

        if [ -f "$ios_demo_service" ]; then
          # ios
          mv $ios_core_service $ios_live_service                # disable live service for ios
          mv $ios_demo_service $ios_core_service                # enable demo service for android
          CleanUp                                               # call clean up method
          Install                                               # call install method
          echo $demo_success
          echo
        else
          echo $error_message
        fi
      else
        echo $error_message
      fi
      exit
      ;;
    r)# execute ./release.sh -r
      if [ -f "$android_live_service" ]; then
        # activate live version
        # android
        mv $android_core_service $android_demo_service          # disable demo service
        mv $android_live_service $android_core_service          # enable live service

        if [ -f "$ios_live_service" ]; then
          # ios
          mv $ios_core_service $ios_demo_service                # disable demo service for ios
          mv $ios_live_service $ios_core_service                # enable live service for ios
          CleanUp                                               # call clean up method
          Install                                               # call install method
          echo $live_success
          echo
        else
          echo $error_message
        fi
      else
        echo $error_message
      fi
      exit
      ;;
    h)  # execute ./release.sh -h
      Help                                    # show help to terminal
      exit
      ;;
    \?) # Invalid option
      echo "Error: Invalid option"
      exit
      ;;
  esac
done
